# SQLToNRACert

A formally verified translation from canonical SQL to the nested
relational algebra of [Q*Cert](https://querycert.github.io), in Coq.


## Installation
This work is known to compile with OCaml-4.09.1, Coq-8.11.*, and JsAst-2.0.0.

### Install Opam

```
sudo curl -sL https://raw.githubusercontent.com/ocaml/opam/master/shell/install.sh > install.sh
echo | sudo sh install.sh
eval $(opam env)
opam switch create 4.09.1 || true
opam switch set 4.09.1
opam update || true
```

### Prepare Coq dependencies
```
opam repo add coq-released https://coq.inria.fr/opam/released
```

### Compilation and installation

Have OCaml and Coq executables in your path:

```
opam install -y .
```


## License
This code is released under the terms of the LGPLv3 license; see LICENSE for details.


## Companion paper
[Translating Canonical SQL to Imperative Code in
Coq](https://arxiv.org/abs/2203.08941) (Section 4), Véronique Benzaken, Évelyne
Contejean, Mohammed Houssem Hachmaoui, Chantal Keller, Louis Mandel,
Avraham Shinnar and Jérôme Siméon, [Proceedings of the ACM on
Programming Languages, OOPSLA
2022](https://2022.splashcon.org/track/splash-2022-oopsla).
