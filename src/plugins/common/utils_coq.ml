(************************************************************************************)
(**                                                                                 *)
(**                          The SQLToNRACert Library                               *)
(**                                                                                 *)
(**                       LMF, CNRS & Université Paris-Saclay                       *)
(**                                                                                 *)
(**                        Copyright 2016-2022 : FormalData                         *)
(**                                                                                 *)
(**         Authors: Chantal Keller                                                 *)
(**                                                                                 *)
(************************************************************************************)


(* Collecting Coq terms *)

let gen_constant_in_modules s = UnivGen.constr_of_monomorphic_global @@ Coqlib.lib_ref s
let gen_constant prefix constant = lazy (gen_constant_in_modules (prefix^"."^constant))


(* Positive *)
let positive_prefix = "num.pos"
let cpositive = gen_constant positive_prefix "type"
let cxI = gen_constant positive_prefix "xI"
let cxO = gen_constant positive_prefix "xO"
let cxH = gen_constant positive_prefix "xH"

(* N *)
let n_prefix = "num.N"
let cN = gen_constant n_prefix "type"
let cN0 = gen_constant n_prefix "N0"
let cNpos = gen_constant n_prefix "Npos"

(* Z *)
let z_prefix = "num.Z"
let cZ = gen_constant z_prefix "type"
let cZ0 = gen_constant z_prefix "Z0"
let cZpos = gen_constant z_prefix "Zpos"
let cZneg = gen_constant z_prefix "Zneg"

(* Booleans *)
let bool_prefix = "core.bool"
let cbool = gen_constant bool_prefix "type"
let ctrue = gen_constant bool_prefix "true"
let cfalse = gen_constant bool_prefix "false"

(* Lists *)
let list_prefix = "core.list"
let clist = gen_constant list_prefix "type"
let cnil = gen_constant list_prefix "nil"
let ccons = gen_constant list_prefix "cons"

(* Option *)
let option_prefix = "core.option"
let coption = gen_constant option_prefix "type"
let cSome = gen_constant option_prefix "Some"
let cNone = gen_constant option_prefix "None"

(* Pairs *)
let pair_prefix = "core.prod"
let cprod = gen_constant pair_prefix "type"
let cpair = gen_constant pair_prefix "intro"

(* Strings *)
let string_prefix = "plugins.syntax"
let cAscii = gen_constant string_prefix "Ascii"
let cString = gen_constant string_prefix "String"
let cstring = gen_constant string_prefix "string"
let cEmptyString = gen_constant string_prefix "EmptyString"


(* Generic functions to construct Coq terms *)

let mkName s =
  let id = Names.Id.of_string s in
  Names.Name id
let mklApp f args = Constr.mkApp (Lazy.force f, args)

let mkList ty elem l =
  List.fold_right (fun c acc -> mklApp ccons [|ty; elem c; acc|]) l (mklApp cnil [|ty|])

let mkBool = function
  | true -> Lazy.force ctrue
  | false -> Lazy.force cfalse

let mkPositive i =
  if i <= 0 then assert false else
  let rec mkPos i =
    if i = 1 then Lazy.force cxH
    else if i mod 2 = 0 then mklApp cxO [|mkPos (i/2)|]
    else mklApp cxI [|mkPos (i/2)|] in
  mkPos i

let mkN i =
  if i = 0 then Lazy.force cN0
  else if i > 0 then mklApp cNpos [|mkPositive i|]
  else failwith "Sqlparser.mkN: not a positive integer"

let mkZ i =
  if i = 0 then Lazy.force cZ0
  else if i > 0 then mklApp cZpos [|mkPositive i|]
  else mklApp cZneg [|mkPositive (-i)|]


let mkUConst : Constr.t -> Evd.side_effects Declare.proof_entry = fun c ->
  let env = Global.env () in
  let evd = Evd.from_env env in
  let evd, ty = Typing.type_of env evd (EConstr.of_constr c) in
  Declare.definition_entry
    ~opaque:false
    ~inline:false
    ~types:(EConstr.Unsafe.to_constr ty) (* Cannot contain evars since it comes from a Constr.t *)
    ~univs:(Evd.univ_entry ~poly:false evd)
    c


(* Functions to construct Coq strings *)

let mkAscii c =
  let a = int_of_char c in
  let t = Array.make 8 (Lazy.force cfalse) in
  let power = ref 1 in
  for i = 0 to 7 do
    if (a / !power) mod 2 = 1 then t.(i) <- Lazy.force ctrue;
    power := 2 * !power
  done;
  mklApp cAscii t

let mkString s =
  let rec mkString i acc =
    if i = -1 then acc else
      mkString (i-1) (mklApp cString [|mkAscii s.[i]; acc|]) in
  mkString ((String.length s) - 1) (Lazy.force cEmptyString)


(* Common Datacert terms *)
let attribute_prefix = "datacert.attribute"
let cattribute = gen_constant attribute_prefix "type"
let cAttr_string = gen_constant attribute_prefix "Attr_string"
let cAttr_Z = gen_constant attribute_prefix "Attr_Z"
let cAttr_bool = gen_constant attribute_prefix "Attr_bool"
let cAttr_float = gen_constant attribute_prefix "Attr_float"

let value_prefix = "datacert.value"
let cvalue = gen_constant value_prefix "type"
let cValue_string = gen_constant value_prefix "Value_string"
let cValue_Z = gen_constant value_prefix "Value_Z"
let cValue_bool = gen_constant value_prefix "Value_bool"

let relname_prefix = "datacert.relname"
let crelname = gen_constant relname_prefix "type"
let cRel = gen_constant relname_prefix "Rel"

let and_or_prefix = "datacert.and_or"
let cAnd_F = gen_constant and_or_prefix "And_F"
let cOr_F = gen_constant and_or_prefix "Or_F"

let predicate_prefix = "datacert.predicate"
let cpredicate = gen_constant predicate_prefix "type"
let cPredicate = gen_constant predicate_prefix "Predicate"

let syntax_prefix = "datacert.syntax"
let c_funterm = gen_constant syntax_prefix "_funterm"
let c_F_Constant = gen_constant syntax_prefix "_F_Constant"
let c__Sql_Dot = gen_constant syntax_prefix "__Sql_Dot"
let c_F_Expr = gen_constant syntax_prefix "_F_Expr"
let c_aggterm = gen_constant syntax_prefix "_aggterm"
let c_A_Expr = gen_constant syntax_prefix "_A_Expr"
let c_A_agg = gen_constant syntax_prefix "_A_agg"
let c_A_fun = gen_constant syntax_prefix "_A_fun"
let csymbol = gen_constant syntax_prefix "symbol"
let cSymbol = gen_constant syntax_prefix "Symbol"
let cAggregate = gen_constant syntax_prefix "Aggregate"
let c_select = gen_constant syntax_prefix "_select"
let c_Select_As = gen_constant syntax_prefix "_Select_As"
