(************************************************************************************)
(**                                                                                 *)
(**                          The SQLToNRACert Library                               *)
(**                                                                                 *)
(**                       LMF, CNRS & Université Paris-Saclay                       *)
(**                                                                                 *)
(**                        Copyright 2016-2022 : FormalData                         *)
(**                                                                                 *)
(**         Authors: Chantal Keller                                                 *)
(**                                                                                 *)
(************************************************************************************)


open Utils_coq
open Basics


(* Names of relations *)
let mk_relname = Utils_coq.mkString
let mk_tablename t = mklApp cRel [|mk_relname t|]

(* Names of attributes *)
let mk_aname = mkString


(* Typed attributes *)
let mk_typed_attribute_name rn (an, t) =
  let c =
    if rn then
      string_of_attribute_name "_" an
    else
      string_of_attribute_name_wrn an
  in
  let attr =
    match t with
      | TString -> cAttr_string
      | TInt -> cAttr_Z
      | TBool -> cAttr_bool
      | TDouble -> cAttr_float
  in
  mklApp attr [|mk_aname c|]

(* Values, e.g. [3], [2017-03-28] *)
let mk_value = function
  (* We consider NULL values as untyped, and put them into bool values *)
  | VNull -> mklApp cValue_bool [|mklApp cNone [|Lazy.force cbool|]|]
  | VString s -> mklApp cValue_string [|mklApp cSome [|Lazy.force cstring; mkString s|]|]
  | VInt i -> mklApp cValue_Z [|mklApp cSome [|Lazy.force cZ; mkZ i|]|]
  | VBool b -> mklApp cValue_bool [|mklApp cSome [|Lazy.force cbool; mkBool b|]|]
  | VFloat f -> failwith "Floating-point values cannot be realized in Coq"


(* Function symbols, e.g. [+], [-], ... *)
let mk_symb s a t =
  let s_string = string_of_symb s a t in
  mklApp cSymbol [|mkString s_string|]

(* Predicate symbols, e.g. [<=], [>], ... *)
let mk_predicate p t =
  let p_string = string_of_predicate p t in
  mklApp cPredicate [|mkString p_string|]

(* Aggregate symbols, e.g. [count], [avg], ... *)
let mk_aggregate a t =
  let a_string = string_of_aggregate a t in
  mklApp cAggregate [|mkString a_string|]


(* Logical connectives *)
let mk_and_or = function
  | And_F -> Lazy.force cAnd_F
  | Or_F -> Lazy.force cOr_F
