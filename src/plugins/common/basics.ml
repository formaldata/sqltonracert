(************************************************************************************)
(**                                                                                 *)
(**                          The SQLToNRACert Library                               *)
(**                                                                                 *)
(**                       LMF, CNRS & Université Paris-Saclay                       *)
(**                                                                                 *)
(**                        Copyright 2016-2022 : FormalData                         *)
(**                                                                                 *)
(**         Authors: Chantal Keller                                                 *)
(**                                                                                 *)
(************************************************************************************)


(* Basic types *)

(* Names of relations *)
type relname = string
let string_of_relname (r:relname) : string = r

(* Types *)
type coltype = TString | TInt | TBool | TDouble
let string_of_coltype = function
  | TInt -> "int"
  | TString -> "text"
  | TBool -> "boolean"
  | TDouble -> "double precision"


(* Names of attributes *)
type aname = string
let string_of_aname (a:aname) : string = a


type attribute_name = relname option * aname
let string_of_attribute_name sep : attribute_name -> string = function
  | (Some t, a) -> (string_of_relname t)^sep^(string_of_aname a)
  | (None, a) -> string_of_aname a
let string_of_attribute_name_wrn ((_,a):attribute_name) : string =
  string_of_aname a


(* Typed attributes *)
type typed_aname = aname * coltype
let string_of_typed_aname ((a,t):typed_aname) =
  (string_of_aname a) ^ " " ^ (string_of_coltype t)
let string_of_typed_aname_wt ((a,_):typed_aname) = string_of_aname a
type typed_attribute_name = attribute_name * coltype
let string_of_typed_attribute_name_wt sep ((a,_):typed_attribute_name) =
  string_of_attribute_name sep a
let string_of_typed_attribute_name_wt_wrn ((a,_):typed_attribute_name) =
  string_of_attribute_name_wrn a

(* Values, e.g. [3], [2017-03-28] *)
type value =
  | VNull
  | VString of string
  | VInt of int
  | VBool of bool
  | VFloat of float
let string_of_value = function
  | VNull -> "NULL"
  | VString s -> "'"^s^"'"
  | VInt i -> string_of_int i
  | VBool b -> string_of_bool b
  | VFloat f -> string_of_float f


(* Function symbols, e.g. [+], [-], ... *)
type symb = string
let string_of_symb (s:symb) (a:int) (t:coltype) : string =
  match s with
    | "+" -> "plus"
    | "*" -> "mult"
    | "-" -> if a = 2 then "minus" else "opp"
    | _ -> s

(* Predicate symbols, e.g. [<=], [>], ... *)
type predicate = string
let string_of_predicate (p:predicate) (t:coltype) : string =
  match p with
    | "<" -> "<"
    | "<=" -> "<="
    | ">" -> ">"
    | ">=" -> ">="
    | "=" -> "="
    | "<>" -> "<>"
    | _ -> p
let comm_pred (p:predicate) : predicate =
  match p with
    | "<" -> ">"
    | "<." -> ">."
    | "<=" -> ">="
    | "<=." -> ">=."
    | ">" -> "<"
    | ">." -> "<."
    | ">=" -> "<="
    | ">=." -> "<=."
    | "=" -> "="
    | "<>" -> "<>"
    | _ -> p

(* Aggregate symbols, e.g. [count], [avg], ... *)
type aggregate = string
let string_of_aggregate (s:aggregate) (t:coltype) : string =
  match s with
    | "count" -> "count"
    | "sum" -> "sum"
    | "max" -> "max"
    | "avg" -> "avg"
    | _ -> s


(* Set operations between queries *)
type set_op =
  | Union
  | Intersect
  | Except
let string_of_set_op = function
  | Union -> "UNION"
  | Intersect -> "INTERSECT"
  | Except -> "EXCEPT"

(* Logical connectives *)
type and_or =
  | And_F
  | Or_F
let string_of_and_or = function
  | And_F -> "AND"
  | Or_F -> "OR"

(* Quantifiers *)
type quantifier =
  | Forall_F
  | Exists_F
let string_of_quantifier = function
  | Forall_F -> "ALL"
  | Exists_F -> "ANY"
