(************************************************************************************)
(**                                                                                 *)
(**                          The SQLToNRACert Library                               *)
(**                                                                                 *)
(**                       LMF, CNRS & Université Paris-Saclay                       *)
(**                                                                                 *)
(**                        Copyright 2016-2022 : FormalData                         *)
(**                                                                                 *)
(**         Authors: Chantal Keller                                                 *)
(**                                                                                 *)
(************************************************************************************)


(* Miscellanous useful types and functions *)

type ('a, 'b) sum =
  | Left : 'a -> ('a, 'b) sum
  | Right : 'b -> ('a, 'b) sum

let rec partition = function
  | [] -> ([], [])
  | (Left a)::l -> let (l1, l2) = partition l in (a::l1, l2)
  | (Right b)::l -> let (l1, l2) = partition l in (l1, b::l2)


let fold_and_add_comma f l = String.concat ", " (List.map f l)
