(************************************************************************************)
(**                                                                                 *)
(**                          The SQLToNRACert Library                               *)
(**                                                                                 *)
(**                       LMF, CNRS & Université Paris-Saclay                       *)
(**                                                                                 *)
(**                        Copyright 2016-2022 : FormalData                         *)
(**                                                                                 *)
(**         Authors: Chantal Keller                                                 *)
(**                                                                                 *)
(************************************************************************************)


(* Parsing a SQL query *)

let parse_query squery =
  let squery = String.lowercase_ascii squery in
  let linebuf = Lexing.from_string squery in
  try
    Sql_parser.line Sql_lexer.token linebuf
  with
    | Sql_lexer.Error msg -> failwith msg
    | Sql_parser.Error -> failwith ("At offset "^(string_of_int (Lexing.lexeme_start linebuf))^": syntax error.\n")
