(************************************************************************************)
(**                                                                                 *)
(**                          The SQLToNRACert Library                               *)
(**                                                                                 *)
(**                       LMF, CNRS & Université Paris-Saclay                       *)
(**                                                                                 *)
(**                        Copyright 2016-2022 : FormalData                         *)
(**                                                                                 *)
(**         Authors: Chantal Keller                                                 *)
(**                                                                                 *)
(************************************************************************************)


(* Collecting Coq terms *)

open Utils_coq
open Basics
open Basics_coq

(* Datacert terms *)
let syntax_prefix = "datacert.syntax"
let c_Group_By = gen_constant syntax_prefix "_Group_By"
let c_Union = gen_constant syntax_prefix "_Union"
let c_Inter = gen_constant syntax_prefix "_Inter"
let c_Diff = gen_constant syntax_prefix "_Diff"
let cdb_state = gen_constant syntax_prefix "db_state"
let ccreate_table = gen_constant syntax_prefix "create_table"
let c_Select_Star = gen_constant syntax_prefix "_Select_Star"
let c_Select_List = gen_constant syntax_prefix "__Select_List"
let c_Group_Fine = gen_constant syntax_prefix "_Group_Fine"
let c_Att_As = gen_constant syntax_prefix "_Att_As"
let c_Att_Ren_Star = gen_constant syntax_prefix "_Att_Ren_Star"
let c_Sql_Table = gen_constant syntax_prefix "_Sql_Table"
let c_Sql_Select = gen_constant syntax_prefix "_Sql_Select"
let c_Sql_Set = gen_constant syntax_prefix "_Sql_Set"
let c_From_Item = gen_constant syntax_prefix "_From_Item"
let c_sql_from_item = gen_constant syntax_prefix "_sql_from_item"
let c_Sql_Conj = gen_constant syntax_prefix "_Sql_Conj"
let c_Sql_Not = gen_constant syntax_prefix "_Sql_Not"
let c_Sql_True = gen_constant syntax_prefix "_Sql_True"
let c_Sql_Pred = gen_constant syntax_prefix "_Sql_Pred"
let c_Sql_Quant = gen_constant syntax_prefix "_Sql_Quant"
let c_Sql_In = gen_constant syntax_prefix "_Sql_In"
let c_Sql_Exists = gen_constant syntax_prefix "_Sql_Exists"
let cSortedTuplesT = gen_constant syntax_prefix "SortedTuplesT"
let cTNull = gen_constant syntax_prefix "TNull"
let cmk_insert = gen_constant syntax_prefix "mk_insert"
let caggregate = gen_constant syntax_prefix"aggregate"

let att_renaming_item_prefix = "datacert.att_renaming_item"
let cAtt_Ren_List = gen_constant att_renaming_item_prefix "Att_Ren_List"

let att_renaming_prefix = "datacert.att_renaming"
let catt_renaming = gen_constant att_renaming_prefix "type"

let quantifier_prefix = "datacert.quantifier"
let cForall_F = gen_constant quantifier_prefix "Forall_F"
let cExists_F = gen_constant quantifier_prefix "Exists_F"


let mk_typed_attribute_name = mk_typed_attribute_name true


(* CREATE TABLE queries *)

let rec mk_funterm = function
  | Coq_sql_algebra.F_Constant v -> mklApp c_F_Constant [|mk_value v|]
  | Coq_sql_algebra.F_Dot an -> mklApp c__Sql_Dot [|mk_typed_attribute_name an|]
  | Coq_sql_algebra.F_Expr (s,l) ->
     (match l with
        | [] -> failwith "Sqlparser_coq.mk_funterm: function symbol applied to no term"
        | f::_ ->
           let t = Coq_sql_algebra.type_of_funterm f in
           mklApp c_F_Expr [|mk_symb s (List.length l) t; mkList (Lazy.force c_funterm) mk_funterm l|]
     )


let rec mk_aggterm = function
  | Coq_sql_algebra.A_Expr f -> mklApp c_A_Expr [|mk_funterm f|]
  | Coq_sql_algebra.A_agg (agg,f) ->
     let t = Coq_sql_algebra.type_of_funterm f in
     mklApp c_A_agg [|mk_aggregate agg t; mk_funterm f|]
  | Coq_sql_algebra.A_fun (s,l) ->
     (match l with
        | [] -> failwith "Sqlparser_coq.mk_aggterm: function symbol applied to no term"
        | a::_ ->
           let t = Coq_sql_algebra.type_of_aggterm a in
           mklApp c_A_fun [|mk_symb s (List.length l) t; mkList (Lazy.force c_aggterm) mk_aggterm l|]
     )


let mk_select = function
  | Coq_sql_algebra.Select_As (agg, (an, ty)) -> mklApp c_Select_As [|mk_aggterm agg; mk_typed_attribute_name ((None, an), ty)|]

let mk_select_list l = mkList (Lazy.force c_select) mk_select l


let mk_select_item = function
  | Coq_sql_algebra.Select_Star -> Lazy.force c_Select_Star
  | Coq_sql_algebra.Select_List l -> mklApp c_Select_List [|mk_select_list l|]


let mk_create_table_query (t, cols) =
  let tablename = mk_tablename t in
  let attributes = mkList (Lazy.force cattribute) mk_typed_attribute_name cols in
  let body = mklApp ccreate_table [|Constr.mkRel 1; tablename; attributes|] in
  Constr.mkLambda (Context.make_annot (mkName "db") Sorts.Relevant, Lazy.force cdb_state, body)


(* INSERT queries *)

let mk_insert_query (t, cols, valss) =
  let atts = mkList (Lazy.force cattribute) mk_typed_attribute_name cols in
  let valss = mkList (mklApp clist [|Lazy.force cvalue|])
               (mkList (Lazy.force cvalue) mk_value)
               valss
  in
  let body = mklApp cmk_insert [|Constr.mkRel 1 (*db*); mk_tablename t; atts; valss|] in
  Constr.mkLambda (Context.make_annot (mkName "db") Sorts.Relevant, Lazy.force cdb_state, body)


(* SELECT queries *)

let mk_group_by = function
  | ToCoq.Group_By l -> mklApp c_Group_By [|mkList (Lazy.force c_funterm) mk_funterm l|]
  | ToCoq.Group_Fine -> Lazy.force c_Group_Fine

let mk_att_renaming = function
  | ToCoq.Att_As (a1,a2) -> mklApp c_Att_As [|mk_typed_attribute_name a1; mk_typed_attribute_name a2|]

let mk_att_renaming_item = function
  | ToCoq.Att_Ren_Star -> Lazy.force c_Att_Ren_Star
  | ToCoq.Att_Ren_List l -> mklApp cAtt_Ren_List [|Lazy.force cTNull; mkList (mklApp catt_renaming [|Lazy.force cTNull|]) mk_att_renaming l|]

let mk_quantifier = function
  | Forall_F -> Lazy.force cForall_F
  | Exists_F -> Lazy.force cExists_F

let mk_set_op = function
  | Union -> Lazy.force c_Union
  | Intersect -> Lazy.force c_Inter
  | Except -> Lazy.force c_Diff

let rec mk_sql_query = function
  | ToCoq.Sql_Table name -> mklApp c_Sql_Table [|mk_tablename name|]
  | ToCoq.Sql_Set (op, q1, q2) ->
     mklApp c_Sql_Set [|mk_set_op op; mk_sql_query q1; mk_sql_query q2|]
  | ToCoq.Sql_Select (select, from, where, group_by, having) ->
     mklApp c_Sql_Select [|mk_select_item select; mkList (Lazy.force c_sql_from_item) mk_from_item from; mk_sql_formula where; mk_group_by group_by; mk_sql_formula having|]

and mk_from_item = function
  | ToCoq.From_Item (q, an) -> mklApp c_From_Item [|mk_sql_query q; mk_att_renaming_item an|]

and mk_sql_formula = function
  | Coq_sql_algebra.Sql_Conj (ao, f1, f2) -> mklApp c_Sql_Conj [|mk_and_or ao; mk_sql_formula f1; mk_sql_formula f2|]
  | Coq_sql_algebra.Sql_Not f -> mklApp c_Sql_Not [|mk_sql_formula f|]
  | Coq_sql_algebra.Sql_True -> Lazy.force c_Sql_True
  | Coq_sql_algebra.Sql_Pred (p, l) ->
     (match l with
        | [] -> failwith "Sqlparser_coq.mk_sql_formula: predicate symbol applied to no term"
        | a::_ ->
           let t = Coq_sql_algebra.type_of_aggterm a in
           mklApp c_Sql_Pred [|mk_predicate p t; mkList (Lazy.force c_aggterm) mk_aggterm l|]
     )
  | Coq_sql_algebra.Sql_Quant (q, p, l, qu) ->
     (match l with
        | [] -> failwith "Sqlparser_coq.mk_sql_formula: predicate symbol applied to no term"
        | a::_ ->
           let t = Coq_sql_algebra.type_of_aggterm a in
           mklApp c_Sql_Quant [|mk_quantifier q; mk_predicate p t; mkList (Lazy.force c_aggterm) mk_aggterm l; mk_sql_query qu|]
     )
  | Coq_sql_algebra.Sql_In (Coq_sql_algebra.Select_List l, qu) -> mklApp c_Sql_In [|mk_select_list l; mk_sql_query qu|]
  | Coq_sql_algebra.Sql_In (Coq_sql_algebra.Select_Star, _) -> failwith "Sqlparser.mk_sql_atom: in IN the select must be a list"
  | Coq_sql_algebra.Sql_Exists qu -> mklApp c_Sql_Exists [|mk_sql_query qu|]


(* Parsing a SQL query *)

let parse_query_to_coq squery res =
  try
    let oquery = Sqlparser.parse_query squery in
    let cquery =
      match oquery with
        | Sql_ast.SQL_Query q -> mk_sql_query (ToCoq.select (ToCoq.init_context ()) q)
        | Sql_ast.SQL_Create c -> mk_create_table_query (ToCoq.create_table c)
        | Sql_ast.SQL_Insert i -> mk_insert_query (ToCoq.insert i) in
    let ce1 = mkUConst cquery in
    let _ = Declare.declare_constant ~name:res ~kind:(Decls.IsDefinition Decls.Definition) (Declare.DefinitionEntry ce1) in
    ()
  with
    | Failure msg -> CErrors.user_err (Pp.str msg)
