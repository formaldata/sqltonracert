(************************************************************************************)
(**                                                                                 *)
(**                          The SQLToNRACert Library                               *)
(**                                                                                 *)
(**                       LMF, CNRS & Université Paris-Saclay                       *)
(**                                                                                 *)
(**                        Copyright 2016-2022 : FormalData                         *)
(**                                                                                 *)
(**         Authors: Chantal Keller                                                 *)
(**                                                                                 *)
(************************************************************************************)


Require Import Plugins.


(* Unit tests *)

Definition db_unit0 := init_db.

Parse_sql "create table table1 (a int, b int, c int);" create_table1_unit.
Parse_sql "create table table0 (a int, b int, c int);" create_table0_unit.
Parse_sql "create table table2 (a int, b bool, c text, d int);" create_table2_unit.

Parse_sql "insert into table1 (a,b,c) values (1,2,3), (1,2,4), (2,2,3), (0,0,0);" insert_table1_unit.
Parse_sql "insert into table0 values (1,2,3), (5,5,6), (6,5,6), (0,0,0);" insert_table0_unit.
Parse_sql "insert into table2 values (1, true, 'foo', 0),  (2, false, 'bar', 42);" insert_table2_unit.

Definition db_unit := insert_table0_unit (insert_table1_unit db_unit0).
Definition db_unit2 := insert_table2_unit db_unit0.

Definition eval_qu qu := eval_sql_query_in_state db_unit qu.
Definition eval_qu2 qu := eval_sql_query_in_state db_unit2 qu.

Parse_sql "select * from table1;" qu01.
Print qu01.
Eval vm_compute in (eval_qu qu01).
(* [(1,2,3); (1,2,4); (2,2,3); (0,0,0)] *)

Parse_sql "select a as g from table1;" qu02a.
Print qu02a.
Eval vm_compute in (eval_qu qu02a).
(* [(1); (1); (2); (0)] *)

Parse_sql "select t.d as g from table1 as t(d,e,f);" qu02b.
Print qu02b.
Eval vm_compute in (eval_qu qu02b).
(* [(1); (1); (2); (0)] *)

Parse_sql "select * from table1 where a = b;" qu03.
Print qu03.
Eval vm_compute in (eval_qu qu03).
(* [(2,2,3); (0,0,0)] *)

Parse_sql "select a, b from table1;" qu04.
Print qu04.
Eval vm_compute in (eval_qu qu04).
(* [(1,2); (1,2); (2,2); (0,0)] *)

Parse_sql "select a, b from table1 where a = b;" qu05.
Print qu05.
Eval vm_compute in (eval_qu qu05).
(* [(2,2); (0,0)] *)

Parse_sql "select a, a + b from table1;" qu06.
Print qu06.
Eval vm_compute in (eval_qu qu06).
(* [(1,3); (1,3); (2,4); (0,0)] *)

Parse_sql "select * from table1 as t1 where t1.a < 4;" qu07.
Print qu07.
Eval vm_compute in (eval_qu qu07).
(* [(1,2,3); (1,2,4); (2,2,3); (0,0,0)] *)

Parse_sql "select * from table1 as t1 where t1.a > 4;" qu08.
Print qu08.
Eval vm_compute in (eval_qu qu08).
(* [] *)

Parse_sql "select * from table1 as t1 where t1.a = 4;" qu09.
Print qu09.
Eval vm_compute in (eval_qu qu09).
(* [] *)

Parse_sql "select * from table1 as t1 where t1.a <> 4;" qu10.
Print qu10.
Eval vm_compute in (eval_qu qu10).
(* [(1,2,3); (1,2,4); (2,2,3); (0,0,0)] *)

Parse_sql "select * from table1 as t1 where t1.a < 4 or t1.a >= 7;" qu11.
Print qu11.
Eval vm_compute in (eval_qu qu11).
(* [(1,2,3); (1,2,4); (2,2,3); (0,0,0)] *)

Parse_sql "select * from table1 where 2 * a < 4;" qu12.
Print qu12.
Eval vm_compute in (eval_qu qu12).
(* [(1,2,3); (1,2,4); (0,0,0)] *)

Parse_sql "select t1.d as g from table1 as t1(d,e,f), table0 where t1.d = a;" qu13.
Print qu13.
Eval vm_compute in (eval_qu qu13).
(* [(1); (1); (0)] *)

Parse_sql "select t1.d as g from table1, table0 as t1(d,e,f) where t1.d = a;" qu14.
Print qu14.
Eval vm_compute in (eval_qu qu14).
(* [(1); (1); (0)] *)

(* ALL *)
Parse_sql "select * from table1 where (a+b) >= all (select (a0 + c1) as a0_plus_c1 from table0 as t0(a0,b0,c0), table1 as t1(a1,b1,c1));" qu15.
Print qu15.
(* Eval vm_compute in (eval_qu qu15). *)
(* [] *)

(* ALL *)
Parse_sql "select a, count(b) as count_b from table1 group by a having avg(c) >= all (select a from table1);" qu16.
Print qu16.
Eval vm_compute in (eval_qu qu16).
(* [(1,2); (2,1)] *)

(* ANY *)
Parse_sql "select a, count(b) as count_b from table1 group by a having avg(c) >= any (select a from table1);" qu17.
Print qu17.
Eval vm_compute in (eval_qu qu17).
(* [(1,2); (2,1); (0,1)] *)

Parse_sql "select a + b * avg(c) from table1 group by a, b;" qu18.
Print qu18.
Eval vm_compute in (eval_qu qu18).
(* [(7), (8), (0)] *)

(* ALL *)
Parse_sql "select a+b as a_b, count(b) as count_b from table1 group by a+b having avg(c) >= all (select a from table1);" qu19a.
Print qu19a.
Eval vm_compute in (eval_qu qu19a).
(* [(3,2); (4,1)] *)

(* GBY using the WHERE context *)
Parse_sql "select a+b as a_b, count(b) as count_b from table1 group by a_b having avg(c) >= all (select a from table1);" qu19b.
Print qu19b.
Eval vm_compute in (eval_qu qu19b).
(* [(3,2); (4,1)] *)

Parse_sql "select 2*(a+c) as twotimesaplusc, b as b2 from table1 group by (a+c), b having b > 1;" qu20.
Print qu20.
Eval vm_compute in (eval_qu qu20).
(* [(8,2); (10,2)] *)

(* IN *)
Parse_sql "select a, c, sum(b) as sumb from table0 group by a, c having a in (select b from table1);" qu21.
Print qu21.
Eval vm_compute in (eval_qu qu21).
(* [(0,0,0)] *)

Parse_sql "select * from table1, (select a as a1 from table1) as t1;" qu22.
Print qu22.
(* Eval vm_compute in (eval_qu qu22). *)

Parse_sql "select * from table1, (select a as a1 from table1) as t1(d);" qu23.
Print qu23.
(* Eval vm_compute in (eval_qu qu23). *)

Parse_sql "select * from table1, (select * from table1) as t1;" qu24.
Print qu24.
(* Eval vm_compute in (eval_qu qu24). *)

Parse_sql "select * from table1, (select * from table1) as t1(d,e,f);" qu25.
Print qu25.
(* Eval vm_compute in (eval_qu qu25). *)

Parse_sql "select * from table1, (select * from table0 as t0(d,e,f), table1) as t2 where table1.a = t2.b;" qu26.
Print qu26.
(* Eval vm_compute in (eval_qu qu26). *)

(* This one is not valid SQL, but it is valid SQLCoq *)
(* Parse_sql "select * from table1, (select a as a1 from table1);" qu27. *)
(* Print qu27. *)
(* (* Eval vm_compute in (eval_qu qu27). *) *)

(* IN *)
Parse_sql "select * from table1 as t2 where (t2.a, t2.b) in (select t1.a, t1.b from table1 as t1);" qu28.
Print qu28.
Eval vm_compute in (eval_qu qu28).
(* [(1,2,3); (1,2,4); (2,2,3); (0,0,0)] *)

Parse_sql "select * from table1 as t1 where (t1.a, t1.b) in (select t1.a, t1.b from table1 as t1);" qu28b.
Print qu28b.
Eval vm_compute in (eval_qu qu28b).
(* [(1,2,3); (1,2,4); (2,2,3); (0,0,0)] *)

(* ALL *)
Parse_sql "select * from table1 as t1 where a+3 >= all (select b from table0);" qu29.
Print qu29.
Eval vm_compute in (eval_qu qu29).
(* [(2,2,3)] *)

(* EXISTS *)
Parse_sql "select * from table1 as t1 where exists (select * from table0 where a = t1.b);" qu30.
Print qu30.
Eval vm_compute in (eval_qu qu30).
(* [(0,0,0)] *)

Parse_sql "select a1 from table1 as t1(a1,b1,b2) group by a1 having exists (select a4 from table0 as t4(a4,b4,c4) group by a4 having sum(1+0*a1) = 10);" qu31.
Print qu31.
Eval vm_compute in (eval_qu qu31).
(* No idea *)

Parse_sql "select sum(a) from table1;" qu32.
Print qu32.
Eval vm_compute in (eval_qu qu32).
(* [(4)] *)

(* NOT EXISTS *)
Parse_sql "select * from table1 as t1 where not exists (select * from table0 where a = t1.b);" qu33.
Print qu33.
Eval vm_compute in (eval_qu qu33).
(* [(1,2,3); (1,2,4); (2,2,3)] *)

(* NOT EXISTS *)
Parse_sql "select a1 from table1 as t1(a1,b1,b2) group by a1 having not exists (select a4 from table0 as t4(a4,b4,c4) group by a4 having sum(1+0*a1) = 10);" qu34.
Print qu34.
Eval vm_compute in (eval_qu qu34).
(* No idea *)

(* NOT ALL *)
Parse_sql "select * from table1 as t1 where not a+3 >= all (select b from table0);" qu35.
Print qu35.
Eval vm_compute in (eval_qu qu35).
(* [(1,2,3), (1,2,4), (0,0,0)] *)

(* NOT ANY *)
Parse_sql "select a, count(b) as count_b from table1 group by a having not avg(c) >= any (select a from table1);" qu36.
Print qu36.
Eval vm_compute in (eval_qu qu36).
(* [] *)

(* NOT IN *)
Parse_sql "select * from table1 as t2 where (t2.a, t2.b) not in (select t1.a, t1.b from table1 as t1);" qu37.
Print qu37.
Eval vm_compute in (eval_qu qu37).
(* [] *)

(* TABLE *)
Parse_sql "table table1;" qu38.
Print qu38.
Eval vm_compute in (eval_qu qu38).
(* [(1,2,3); (1,2,4); (2,2,3); (0,0,0)] *)

(* TABLE *)
Parse_sql "select a from (table table1) as t2;" qu39.
Print qu39.
Eval vm_compute in (eval_qu qu39).
(* [(1); (1); (2); (0)] *)

(* TABLE *)
Parse_sql "select t2.a from (table table1) as t2;" qu40.
Print qu40.
Eval vm_compute in (eval_qu qu40).
(* [(1); (1); (2); (0)] *)

(* TABLE + IN *)
Parse_sql "select b from table1 where (a,b,c) in (table table0);" qu41.
Print qu41.
Eval vm_compute in (eval_qu qu41).
(* [(2); (0)] *)

(* UNION *)
(* Returns a multiset + TODO returns nil *)
Parse_sql "(select * from table1) union (table table0);" qu42.
Print qu42.
Eval vm_compute in (eval_qu qu42).
(* [(1,2,3); (1,2,4); (2,2,3); (0,0,0); (5,5,6); (6,5,6)] *)

(* Again, a multiset *)
Parse_sql "(select a as a1, b as b1, c as c1 from table1) union (select a as a1, b as b1, c as c1 from table0);" qu42b.
Print qu42b.
Eval vm_compute in (eval_qu qu42b).
(* [(1,2,3); (1,2,4); (2,2,3); (0,0,0); (5,5,6); (6,5,6)] *)

Parse_sql "(select a as a1, b as b1, c as c1 from table1) union (select a as a0, b as b0, c as c0 from table0);" qu42c.
Print qu42c.
Eval vm_compute in (eval_qu qu42c).
(* [(1,2,3); (1,2,4); (2,2,3); (0,0,0); (5,5,6); (6,5,6)] *)

(* INTERSECT *)
(* Again nil *)
Parse_sql "(select * from table1) intersect (table table0);" qu43.
Print qu43.
Eval vm_compute in (eval_qu qu43).
(* [(1,2,3); (0,0,0)] *)

Parse_sql "(select a as a1, b as b1, c as c1 from table1) intersect (select a as a1, b as b1, c as c1 from table0);" qu43b.
Print qu43b.
Eval vm_compute in (eval_qu qu43b).
(* [(1,2,3); (0,0,0)] *)

Parse_sql "(select a as a1, b as b1, c as c1 from table1) intersect (select a as a0, b as b0, c as c0 from table0);" qu43c.
Print qu43c.
Eval vm_compute in (eval_qu qu43c).
(* [(1,2,3); (0,0,0)] *)

(* EXCEPT *)
(* Again nil *)
Parse_sql "(select * from table1) except (table table0);" qu44.
Print qu44.
Eval vm_compute in (eval_qu qu44).
(* [(1,2,4); (2,2,3)] *)

Parse_sql "(select a as a, b as b, c as c from table1) except (select a as a, b as b, c as c from table0);" qu44b.
Print qu44b.
Eval vm_compute in (eval_qu qu44b).
(* [(1,2,4); (2,2,3)] *)

Parse_sql "(select a as a1, b as b1, c as c1 from table1) except (select a as a0, b as b0, c as c0 from table0);" qu44c.
Print qu44c.
Eval vm_compute in (eval_qu qu44c).
(* [(1,2,4); (2,2,3)] *)

(* NULL *)
Parse_sql "insert into table1 (a,b) values (1,2), (1,2), (2,2), (0,0);" insert_table1_null1.
Definition db_unit_null1 := insert_table1_null1 db_unit0.
Parse_sql "insert into table1 values (1,2,null), (1,null,4), (null,2,3), (null,0,null);" insert_table1_null2.
Definition db_unit_null2 := insert_table1_null2 db_unit0.

(* Boolean values *)
Parse_sql "select * from table1 where true;" qu45a.
Print qu45a.
Eval vm_compute in (eval_qu qu45a).
(* [(1,2,3); (1,2,4); (2,2,3); (0,0,0)] *)

(* TODO *)
(* Parse_sql "select * from table1 where false;" qu45b. *)
(* Print qu45b. *)
(* Eval vm_compute in (eval_qu qu45b). *)
(* (* [] *) *)

(* Unary minus *)
Parse_sql "select - a from table1;" qu46.
Print qu46.
Eval vm_compute in (eval_qu qu46).

(* Data types *)
Parse_sql "select * from table2;" qu47.
Print qu47.
Eval vm_compute in (eval_qu2 qu47).
(* [(1, true, 'foo', 0),  (2, false, 'bar', 42)] *)

Parse_sql "select sum(d) from table2;" qu48.
Print qu48.
Eval vm_compute in (eval_qu2 qu48).
(* [(42)] *)





(* Current known bugs:
   - (select * ...) set_op ...
 *)

(* Currently not parsed:
   - DISTINCT (must be encoded into SQLCoq)
   - nested SELECT
   - where false
*)




(* Parse_postgres_plan "foo.sql" "foo.xml" foo. *)


(* Movie tests *)

(* Parse_sql "CREATE TABLE people (pid INTEGER, firstname VARCHAR(30), lastname VARCHAR(30));" create_table_movie_people. *)
(* Parse_sql "CREATE TABLE movie (mid INTEGER, title VARCHAR(90), year INTEGER, runtime INTEGER, rank INTEGER);" create_table_movie_movie. *)
(* Parse_sql "CREATE TABLE role (mid INTEGER, pid INTEGER, name VARCHAR(70));" create_table_movie_role. *)
(* Parse_sql "CREATE TABLE director (mid INTEGER, pid INTEGER);" create_table_movie_director. *)
(* Definition db_movie := create_table_movie_director (create_table_movie_role (create_table_movie_movie (create_table_movie_people init_db))). *)

(* Notation OMEGA p f lc q := (EA.Equery_Omega p f lc q). *)
(* Notation NATURAL_JOIN q1 q2 := (EA.Equery_NaturalJoin FiniteCollection.FlagBag q1 q2). *)

(* Notation EXTALG4 := (ExtAlg SortedTuples.T (MyDBS db_movie) OP SqlEngine.OSymbol SqlEngine.OAgg Values.type_string). *)

(* Notation CODE a c := (EA.Code EXTALG4 a c). *)

(* Notation FINE :=  (EA.Fine EXTALG4). *)
(* Notation TRUE := (TTrue (EA.equery EXTALG4) (Data.predicate EXTALG4) *)
(*             (Expression.symbol (Data.T EXTALG4) (Data.symb EXTALG4))). *)

(* Notation F_DOT x := (Expression.A_Expr (Data.aggregate EXTALG4) *)
(*                                        (Expression.F_Dot (Data.T EXTALG4) (Data.symb EXTALG4) *)
(*                                                          x)). *)
(* Notation CST n := (Term.Term *)
(*                      (Expression.Constant (Data.T EXTALG4) *)
(*                         (Data.symb EXTALG4) (Values.Value_Z n)) nil). *)

(* Notation MOVIE := (EA.Equery_Basename EXTALG4 (Rel "movie")). *)
(* Notation ROLE := (EA.Equery_Basename EXTALG4 (Rel "role")). *)
(* Notation PEOPLE := (EA.Equery_Basename EXTALG4 (Rel "people")). *)
(* Notation DIRECTOR := (EA.Equery_Basename EXTALG4 (Rel "director")). *)

(* Notation VAR x y := (Term.Var *)
(*                      (Expression.symbol (Data.T EXTALG4) (Data.symb EXTALG4)) *)
(*                      (Term.Vrbl x y) :: nil)%list. *)

(* Notation "x 'DOT' a" := *)
(*   (Term.Term *)
(*      (Expression.Dot (Data.T EXTALG4) *)
(*                      (Data.symb EXTALG4) a) x) *)
(*     (at level 70, no associativity). *)

(* Notation "x '>' y" := (Atom (Predicate ">") (x :: y :: nil)%list). *)
(* Notation "x '<' y" := (Atom (Predicate "<") (x :: y :: nil)%list). *)

(* Notation M_MID := (Attr_N 0 "movie.mid"). *)
(* Notation M_TITLE := (Attr_N 0 "movie.title"). *)
(* Notation M_RANK := (Attr_N 0 "movie.rank"). *)
(* Notation M_RUNTIME := (Attr_N 0 "movie.runtime"). *)
(* Notation M_YEAR := (Attr_N 0 "movie.year"). *)

(* (* req_01 *) *)
(* Parse_sql "select mid, title from movie where mid > 2500;" mov01. *)
(* Eval vm_compute in (translate_query (mov01 db_movie)). *)

(* Parse_sql "select pid from people where lastname like 'A%';" mov01b. *)
(* Eval vm_compute in (translate_query (mov01b db_movie)). *)

(* Parse_sql "select people.pid from people, director where lastname like 'A%' and people.pid = director.pid;" mov01c. *)
(* Eval vm_compute in (translate_query (mov01c db_movie)). *)

(* (* req_02 *) *)
(* Parse_sql "select mid, title from movie where mid < 2000;" mov02. *)
(* Eval vm_compute in (translate_query (mov02 db_movie)). *)

(* (* req_03 *) *)
(* Parse_sql "select lastname, name from people, role, movie m where m.mid < 2000 and year < 2000;" mov03. *)
(* Eval vm_compute in (translate_query (mov03 db_movie)). *)

(* (* req_04 *) *)
(* Parse_sql "select m.mid, title from movie m, role r where r.mid = m.mid and year > 1980;" mov04. *)
(* Eval vm_compute in (translate_query (mov04 db_movie)). *)

(* (* req_05 *) *)
(* Parse_sql "select m.mid, title from movie m, role r where r.mid = m.mid and year > 2014;" mov05. *)
(* Eval vm_compute in (translate_query (mov05 db_movie)). *)

(* (* req_06 *) *)
(* Parse_sql "select p.pid, firstname from people p, role r where r.pid = p.pid;" mov06. *)
(* Eval vm_compute in (translate_query (mov06 db_movie)). *)

(* (* req_07 *) *)
(* Parse_sql "select p.pid, firstname from people p, role r where r.pid = p.pid and r.name like 'Z%';" mov07. *)
(* Eval vm_compute in (translate_query (mov07 db_movie)). *)

(* (* req_08 *) *)
(* Parse_sql "select p.pid, firstname from people p, role r where r.pid = p.pid and r.name like '%z%';" mov08. *)
(* Eval vm_compute in (translate_query (mov08 db_movie)). *)

(* (* req_09 *) *)
(* Parse_sql "select lastname from people, movie m where m.mid > 2997 and year < 1951;" mov09. *)
(* Eval vm_compute in (translate_query (mov09 db_movie)). *)

(* (* req_09 *) *)
(* Parse_sql "select * from role, movie m where m.mid = 2998 and m.mid = role.mid and year < 1951;" mov09b. *)
(* Eval vm_compute in (translate_query (mov09b db_movie)). *)

(* (* req_10 *) *)
(* Parse_sql "select * from role, movie m where m.mid <= 2998 and m.mid >=2998 and m.mid = role.mid and year < 1951;" mov10. *)
(* Eval vm_compute in (translate_query (mov10 db_movie)). *)

(* (* req_11 *) *)
(* Parse_sql "select * from role, movie m, director d where m.mid = 2998 and m.mid = role.mid and year < 1951 and role.mid = d.mid;" mov11. *)
(* Eval vm_compute in (translate_query (mov11 db_movie)). *)

(* (* req_12 *) *)
(* Parse_sql "select * from role, movie m, director d where m.mid = 2998 and m.mid = role.mid and year < 1951 and role.mid = (d.mid - 5);" mov12. *)
(* Eval vm_compute in (translate_query (mov12 db_movie)). *)

(* (* req_13 *) *)
(* Parse_sql "select * from role, movie m, director d where m.mid = 2998 and m.mid = role.mid and year < 1951 and (role.mid +5) = d.mid;" mov13. *)
(* Eval vm_compute in (translate_query (mov13 db_movie)). *)

(* (* req_14 *) *)
(* Parse_sql "select * from role, movie m, director d where m.mid = 2998 and m.mid = role.mid and year < 1951 and (2998 +5) = d.mid;" mov14. *)
(* Eval vm_compute in (translate_query (mov14 db_movie)). *)

(* (* req_15 *) *)
(* Parse_sql "select * from role, movie m, director d where m.mid = role.mid and year < 1951 and role.mid = d.mid;" mov15. *)
(* Eval vm_compute in (translate_query (mov15 db_movie)). *)


(* (* Raghu tests *) *)

(* Parse_sql "CREATE TABLE sailors(sid integer, sname varchar(20), rating integer, age integer);" create_table_raghu_sailors. *)
(* Parse_sql "CREATE TABLE boats(bid integer, bname varchar(20), color varchar(20));" create_table_raghu_boats. *)
(* Parse_sql "CREATE TABLE reserves(sid integer, bid integer, dday varchar(20));" create_table_raghu_reserves. *)
(* Definition db_raghu := create_table_raghu_reserves (create_table_raghu_boats (create_table_raghu_sailors init_db)). *)

(* (* req_01 *) *)
(* Parse_sql "select * from sailors t0, reserves t1(sidR, bid, dday) where t1.sidR = t0.sid;" rag01. *)
(* Eval vm_compute in (translate_query (rag01 db_raghu)). *)

(* (* req_02 *) *)
(* (* Parse_sql "select distinct s.sid from sailors s, sailors s2 where s2.sname = 'horatio' and s.rating > s2.rating;" rag02. *) *)
(* (* Eval vm_compute in (translate_query (rag02 db_raghu)). *) *)

(* (* req_03 *) *)
(* Parse_sql "select r.sid from boats b, reserves r(sid, bidR, dday) where r.bidR = b.bid and b.color = 'red';" rag03. *)
(* Eval vm_compute in (translate_query (rag03 db_raghu)). *)

(* (* req_04 *) *)
(* Parse_sql "select s.rating r1, count( * ) from sailors s where s.rating > 5 and s.age = 20 group by r1 having count (s.sname) > 2;" rag04. *)
(* Eval vm_compute in (translate_query (rag04 db_raghu)). *)

(* (* req_05 *) *)
(* (* Parse_sql "select s.sid from sailors s where exists (select * from sailors s2 where s2.sname = 'horatio' and s.rating > s2.rating);" rag05. *) *)
(* (* Eval vm_compute in (translate_query (rag05 db_raghu)). *) *)

(* (* req_06 *) *)
(* Parse_sql "select s.sid from sailors s where s.rating > any (select s2.rating from sailors s2 where s2.sname = 'horatio');" rag06. *)
(* Eval vm_compute in (translate_query (rag06 db_raghu)). *)

(* (* req_07 *) *)
(* (* Parse_sql "select s.sid from sailors s, reserves r where (s.sid,s.sname,s.rating, s.age) in (select * from sailors s2 where s2.sname = 'horatio' and s.rating > s2.rating and exists (select * from reserves r where r.bid = 103 and s.sid = r.sid));" rag07. *) *)
(* (* Eval vm_compute in (translate_query (rag07 db_raghu)). *) *)

(* (* req_08 *) *)
(* (* Parse_sql "select s.sid from sailors s, reserves r where (s.sid,s.sname,s.rating, s.age) in (select * from sailors s2 where s2.sname = 'horatio' and s.rating > s2.rating and exists (select * from reserves r1 where r1.bid = 103 and s.sid = r.sid));" rag08. *) *)
(* (* Eval vm_compute in (translate_query (rag08 db_raghu)). *) *)

(* (* req_09 *) *)
(* Parse_sql "select s.sid from sailors s, reserves r where (s.sid,s.sname,s.rating, s.age) in (select * from sailors s2 where s2.sname = 'horatio' and s.rating > s2.rating);" rag09. *)
(* Eval vm_compute in (translate_query (rag09 db_raghu)). *)

(* (* req_10 *) *)
(* (* Parse_sql "select s.sid from sailors s, reserves r where exists (select * from sailors s2 where s2.sname = 'horatio' and s.rating > s2.rating and exists (select * from reserves r where r.bid = 103 and s.sid = 2999));" rag10. *) *)
(* (* Eval vm_compute in (translate_query (rag10 db_raghu)). *) *)

(* (* req_11 *) *)
(* (* Parse_sql "select s.sid from sailors s, reserves r where exists (select * from sailors s2 where s2.sname = 'horatio' and s.rating > s2.rating and exists (select * from reserves r where r.bid = 103 and s.sid = r.sid));" rag11. *) *)
(* (* Eval vm_compute in (translate_query (rag11 db_raghu)). *) *)

(* (* req_12 *) *)
(* Parse_sql "select s.sid from sailors s, sailors s2 where s2.sname = 'horatio' and s.rating > s2.rating;" rag12. *)
(* Eval vm_compute in (translate_query (rag12 db_raghu)). *)

(* (* req_13 *) *)
(* (* Parse_sql "select s.sname from sailors s where exists (select * from reserves r where r.bid = 103 and s.sid = r.sid and dday = '9/8/98');" rag13. *) *)
(* (* Eval vm_compute in (translate_query (rag13 db_raghu)). *) *)

(* (* req_14 *) *)
(* (* Parse_sql "select s.sname from sailors s where exists (select * from reserves r where r.bid = 103 and s.sid = r.sid);" rag14. *) *)
(* (* Eval vm_compute in (translate_query (rag14 db_raghu)). *) *)

(* (* req_15 *) *)
(* (* Parse_sql "select s.sname from sailors s where s.sid >  (select max(r.sid) from reserves r where r.bid = 103  and dday = '9/8/98');" rag15. *) *)
(* (* Eval vm_compute in (translate_query (rag15 db_raghu)). *) *)

(* (* req_16 *) *)
(* Parse_sql "select s.sname from sailors s where s.sid > all (select r.sid from reserves r where r.bid = 103  and dday = '9/8/98');" rag16. *)
(* Eval vm_compute in (translate_query (rag16 db_raghu)). *)

(* (* req_17 *) *)
(* Parse_sql "select sid  from sailors where rating > any (select rating from sailors where sname = 'horatio');" rag17. *)
(* Eval vm_compute in (translate_query (rag17 db_raghu)). *)
