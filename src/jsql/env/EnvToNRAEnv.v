(************************************************************************************)
(**                                                                                 *)
(**                          The SQLToNRACert Library                               *)
(**                                                                                 *)
(**                       LMF, CNRS & Université Paris-Saclay                       *)
(**                                                                                 *)
(**                        Copyright 2016-2022 : FormalData                         *)
(**                                                                                 *)
(**         Authors: Véronique Benzaken                                             *)
(**                  Évelyne Contejean                                              *)
(**                  Mohammed Hachmaoui                                             *)
(**                  Chantal Keller                                                 *)
(**                                                                                 *)
(************************************************************************************)

Set Implicit Arguments.

Require Export Bool.

From SQLFS Require Export ListSort.
From SQLFS Require Import SqlAlgebra.

Require Import MoreSugar TupleToData.

(* Undelimit Scope nraenv_scope. *)
(* Undelimit Scope nraenv_core_scope. *)

Module EnvToNRAEnv.

  Export TupleToData .
  
  Class EnvToNRAEnv  `{frt : foreign_runtime} `{TD : @TupleToData frt}  : Type .

    Notation group_by  := (group_by TRcd ).
    Notation nslice := ((Fset.set A) * group_by)%type.
    Notation nenv := (list nslice).
    Notation env := (env TRcd).
    
    Section Sec.
      
      Context {frt : foreign_runtime}.
      Context {TD:TupleToData.TupleToData}.
      Context {EN : EnvToNRAEnv}.


      Section Translation.
        
        Fixpoint env_to_data  (e : env): data :=
          drec(
              match e with
              | nil => nil
              | (_,lt)::tl =>
                ("slc"%string,dcoll (listT_to_listD  lt))::("tl"%string,env_to_data tl)::nil
              end).
        
        
        Fixpoint env_dot_to_nraenv (ne:nenv) a : nraenv :=
          match ne with
          | nil   =>
            (NRAEnvConst
               (value_to_data
                  (default_value _ (type_of_attribute _ a))))
          | (s1,g1)::tl =>
            let hd :=
                NRAEnvHdDef 
                  (drec (tuple_as_dpairs (default_tuple TRcd s1)))
                  (NRAEnvUnop (OpDot "slc") NRAEnvEnv) in
            if a inS? s1
            then
              (NRAEnvUnop (OpDot (attribute_to_string a))  hd)
            else
              NRAEnvAppEnv
                (env_dot_to_nraenv tl a) (NRAEnvUnop (OpDot "tl") NRAEnvEnv)  
          end.
        
      End Translation.

      Section Lemmas.
        
        Section TranslatableEnv.
          

          
          Definition is_a_translatable_e_strong (e:env ) :=
            forallb
              (fun slc =>
                 match slc with
                 | (s,Group_By _ g,_) =>               
                   forallb (fun ag => match ag with | A_Expr  (F_Dot  a) => a inS? s| _ => false end)  g
                 | _ => true
                 end)
              e.

          
          Definition is_a_translatable_e e :=
            forallb (fun ag => match ag with | A_Expr  (F_Dot  _ ) => true | _ => false end)  (flat_map (groups_of_env_slice TRcd) e).

          Lemma is_a_translatable_e_eq_strong :
            forall e,  is_a_translatable_e_strong e = true -> is_a_translatable_e e = true.
          Proof.
            unfold is_a_translatable_e, is_a_translatable_e_strong.
            intros e.
            rewrite 2 forallb_forall.
            intros He.
            intros x Hx.
            rewrite in_flat_map in Hx.
            destruct Hx as [ [[]] [Hx'1 Hx'2]].
            apply He in Hx'1.
            unfold groups_of_env_slice in Hx'2.
            destruct g.
            - rewrite forallb_forall in Hx'1.
              assert (G1 := Hx'1 _ Hx'2).
              destruct x;try discriminate.
              destruct f;try discriminate;apply eq_refl.
            - rewrite in_map_iff in Hx'2.
              destruct Hx'2 as [x'' [ ]];subst;apply eq_refl.
          Qed.
          
          Lemma is_a_translatable_e_groups_are_attributes :
            forall  e,
              is_a_translatable_e e = true ->
              forall ag,
                In ag (flat_map (groups_of_env_slice TRcd) e) -> exists a, ag = A_Expr  (F_Dot  a).
          Proof.
            unfold is_a_translatable_e,groups_of_env_slice.
            intros e He.
            rewrite forallb_forall in He.
            intros ag Hag.
            apply He in Hag.
            destruct ag;try discriminate.
            destruct f;try discriminate.
            exists a;apply eq_refl.
          Qed.
          
          Lemma is_a_translatable_e_well_formed_ft_F_Expr_subterms :
            forall  e,
              is_a_translatable_e e = true ->          
              forall s l,
                well_formed_ft TRcd e (F_Expr TRcd s l) = true  ->
                forall ft, In ft l -> well_formed_ft TRcd e ft = true .
          Proof.
            intros e He s l Wfl ft Hft.
            unfold well_formed_ft in Wfl.
            simpl in Wfl.
            rewrite orb_true_iff in Wfl.
            destruct Wfl as [Wfl|Wfl].
            -  rewrite Oset.mem_bool_true_iff in Wfl.
               rewrite in_extract_funterms in Wfl.
               apply (is_a_translatable_e_groups_are_attributes _ He) in Wfl.
               destruct Wfl;discriminate.
            - rewrite forallb_forall in Wfl.
              apply Wfl;apply Hft.
          Qed.

          Lemma is_a_translatable_e_cons_tuple :
            forall env s1 g1 lt1,
              is_a_translatable_e ((s1, g1, lt1) :: env) = true ->
              forall t : tuple, In t lt1 -> is_a_translatable_e ((s1, Group_Fine _ , t :: nil) :: env) = true.
          Proof.
            intros env s1 g1 lt1.
            unfold is_a_translatable_e.
            rewrite  forallb_forall.
            intros H t Ht.
            rewrite forallb_forall.
            intros  ag Hag.
            simpl in Hag.
            rewrite in_app_iff in Hag.
            rewrite in_map_iff in Hag.
            destruct Hag;
              [destruct H0 as [x [<- Hx]];apply eq_refl| ].
            simpl in H.
            assert (H := fun x X => H x (proj2 (in_app_iff _ _ _) X)).
            refine (H _ (or_intror H0)).
          Qed.  
          
          Lemma is_a_translatable_e_cons_tuple_alt :
            forall env s1 g1 lt1,
              is_a_translatable_e ((s1, g1, lt1) :: env) = true ->
              forall t : tuple, In t lt1 -> is_a_translatable_e ((s1, g1, t :: nil) :: env) = true.
          Proof.
            intros env s1 g1 lt1.
            unfold is_a_translatable_e.
            rewrite  forallb_forall.
            intros H t Ht.
            rewrite forallb_forall.
            intros  ag Hag.
            apply H in Hag.
            destruct ag;try discriminate.
            destruct f;try discriminate.
            apply eq_refl.
          Qed.
          
          Lemma is_a_translatable_e_cons :
            forall e slc,
              is_a_translatable_e (slc :: e) = true -> is_a_translatable_e e = true.
          Proof.
            unfold is_a_translatable_e.
            intros e slc.
            do 2 rewrite forallb_forall.
            intros He x Hx.
            simpl in He.
            apply He.
            apply  in_or_app.
            right;apply Hx.
          Qed.

          Lemma is_a_translatable_e_Group_Fine :
            forall e s lt,  is_a_translatable_e e = true -> is_a_translatable_e ((s, Group_Fine TRcd, lt) :: e) = true.
          Proof.
            intros e s lt Wt.
            unfold is_a_translatable_e;simpl.
            rewrite forallb_app.
            unfold is_a_translatable_e in Wt.
            rewrite Wt.
            rewrite forallb_map.
            split;[|apply eq_refl].
            clear.
            induction ({{{s}}});trivial.
          Qed.

          Lemma is_a_translatable_e_eq :
            forall e1 e2,  weak_equiv_env TRcd e1 e2 -> is_a_translatable_e e1 = is_a_translatable_e e2.
          Proof.
            intros e1 e2 Heq.
            unfold is_a_translatable_e.        
            apply (Oeset.forallb_eq (oeset_of_oset (OAggT TRcd) )).
            - intros a.
              revert Heq.
              induction 1;[apply eq_refl| ].
              simpl.
              do 2 rewrite Oeset.mem_bool_app.
              rewrite IHHeq.
              apply f_equal2;[|apply eq_refl].
              destruct x as [[]],y as [[]].
              simpl.
              destruct H;subst.
              rewrite (Fset.elements_spec1 _ _ _ H).
              apply eq_refl.
            - intros.
              apply Oset.compare_eq_iff in H0;subst;apply eq_refl.
          Qed.      

          Lemma find_eval_env_is_a_translatable_e_if :
            forall ft env l,
              is_a_translatable_e  env = true ->
              find_eval_env TRcd env ft  = Some l
              -> is_a_translatable_e l = true .
          Proof.
            intros ft.
            fix lem 1.
            intros  [|slc1 nenv];intros l WS Hf.
            - cbn;inversion Hf as [I1].
              destruct is_built_upon_ag;[|discriminate].
              inversion I1;apply WS.
            - simpl in Hf.
              destruct find_eval_env eqn:D1.
              + inversion Hf;subst;clear Hf.
                apply is_a_translatable_e_cons in WS.
                destruct slc1,p.
                inversion H0;subst.
                refine (lem _ _ WS D1).
              + destruct slc1,p.
                destruct is_a_suitable_env eqn:D2;[|discriminate].
                inversion Hf;apply WS.
          Qed.
          
        End TranslatableEnv.

        Section EnvToNRAEnv.

          (* Notation aggterm := (@aggterm TRcd). *)
          (* Notation funterm := (@funterm TRcd). *)
          
          Definition equiv_nslice (nc1 nc2 : nslice) :=
            fst nc1 =S= fst nc2 /\ snd nc1 = snd nc2.

          Lemma  weak_equiv_env_slice_equiv_nslice :
            forall sc1 sc2, weak_equiv_env_slice TRcd sc1 sc2 <-> equiv_nslice (fst sc1) (fst sc2).
          Proof.
            intros [[]][[]].
            refine (conj  (fun x => x) (fun x => x)).
          Qed.
          
          Definition equiv_nenv  (ne1 ne2 : nenv) :=
            Forall2 equiv_nslice ne1 ne2.

          Lemma equiv_nenv_sym :
            forall e1 e2, equiv_nenv e1 e2 <-> equiv_nenv e2 e1.
          Proof.
            intro e1; induction e1 as [ | [sa1 g1] e1]; intro e2; simpl; split.
            - intro H; inversion H; constructor 1.
            - intro H; inversion H; constructor 1.
            - destruct e2 as [ | [sa2 g2] e2]; intro H; inversion H; subst.
              constructor 2; [ | rewrite <- IHe1; assumption].
              simpl in H3; simpl; destruct H3 as [H3 H3']; split; [ | apply sym_eq; assumption].
              rewrite (Fset.equal_eq_2 _ _ _ _ H3); apply Fset.equal_refl.
            - destruct e2 as [ | [sa2 g2] e2]; intro H; inversion H; subst.
              constructor 2; [ | rewrite IHe1; assumption].
              simpl in H3; destruct H3 as [H3 K3]; simpl in K3;subst; simpl; repeat split.
              unfold equiv_nenv in H; inversion H;simpl in H3;subst.
              rewrite (Fset.equal_eq_2 _ _ _ _ H3); apply Fset.equal_refl.
          Qed.
          
          Lemma  weak_equiv_env_equiv_nenv :
            forall e1 e2, weak_equiv_env TRcd e1 e2 <-> equiv_nenv (map fst e1) (map fst e2).
          Proof.
            intros e1 e2.
            unfold weak_equiv_env,equiv_nenv.
            unfold weak_equiv_env_slice,equiv_nslice.
            split;intro H.
            - refine  (Forall2_map_f _ _ _  e1 e2 _).
              revert H.
              refine (Forall2_incl _ _ _ _ _ ).
              intros [[]] [[]] Hx Hy.
              exact (fun x => x).
            - rewrite <- Forall2_map in H.
              revert H.
              refine (Forall2_incl _ _ _ _ _ ).
              intros [[]] [[]] Hx Hy.
              exact (fun x => x).
          Qed.

          Lemma equiv_env_equiv_nenv :
            forall e1 e2,
              equiv_env TRcd e1 e2 -> equiv_nenv (map fst e1) (map fst e2).
          Proof.
            intros e1 e2 H.
            refine (proj1 (weak_equiv_env_equiv_nenv _ _)  (equiv_env_weak_equiv_env _ _ _ H)).
          Qed.      

          Definition groups_of_nslice (nslc: nslice) :=
            match nslc  with
            | (_,Group_By _ g') => g'
            | (sa,Group_Fine _) =>
              map (fun a : attribute => A_Expr  (F_Dot  a)) ({{{sa}}})
            end.
          
          Definition is_a_suitable_nenv (sa:Fset.set A) (ne:nenv) f := 
            is_built_upon_ag
              TRcd
              (map (fun a : attribute => A_Expr  (F_Dot  a)) ({{{sa}}}) ++
                   flat_map groups_of_nslice ne) f.
          
          Lemma  is_a_suitable_env_nenv_eq :
            forall s env ft,
              is_a_suitable_nenv  s (map fst env) ft =
              is_a_suitable_env TRcd s env ft.
          Proof.
            intro s.
            unfold is_a_suitable_env,is_a_suitable_nenv.
            intros env ft.
            apply f_equal2;[|apply eq_refl].
            apply f_equal2;[apply eq_refl| ].
            rewrite flat_map_concat_map.
            rewrite map_map.
            rewrite <- flat_map_concat_map.
            apply ListFacts.flat_map_eq.
            intros [[s1 g1] lt1] Ha.
            apply eq_refl.
          Qed.
          
          Definition is_a_suitable_nenv_eq :
            forall e sa1 env1 sa2 env2,
              sa1 =S= sa2 ->
              equiv_nenv  env1 env2 ->
              is_a_suitable_nenv  sa1 env1 e = is_a_suitable_nenv  sa2 env2 e.
          Proof.
            assert (H : forall env1 env2 slc1, 
                       equiv_nenv env1 env2 -> In slc1 env1 -> 
                       exists slc2, In slc2 env2 /\ equiv_nslice  slc1 slc2).
            {
              intro env1; induction env1 as [ | s1 e1]; intros [ | s2 e2] slc1 He Hs.
              - contradiction Hs.
              - inversion He.
              - inversion He.
              - simpl in He; inversion He; subst.
                simpl in Hs; destruct Hs as [Hs | Hs].
                + subst slc1; exists s2; split; [left | ]; trivial.
                + destruct (IHe1 _ _ H4 Hs) as [slc2 [K1 K2]].
                  exists slc2; split; [right | ]; trivial.
            }
            intros e sa1 env1 sa2 env2 Hsa Henv.
            unfold is_a_suitable_nenv; rewrite BasicFacts.eq_bool_iff; split; 
              apply is_built_upon_ag_incl; intros f Hf;
                (destruct (in_app_or _ _ _ Hf) as [Kf | Kf]; apply in_or_app; [left | right]).
            - rewrite <- (Fset.elements_spec1 _ _ _ Hsa); assumption.
            - rewrite in_flat_map in Kf; destruct Kf as [[_sa1 g1] [H1 H2]].
              destruct (H _ _ _ Henv H1) as [[_sa2 g2] [H3 H4]].
              rewrite in_flat_map; exists (_sa2, g2); split; [assumption | ].
              simpl in H4.
              simpl; simpl in H2.
              assert (H4' := proj2 H4);simpl in H4'; subst g2.
              assert (H4' := proj1 H4);simpl in H4'; rewrite <- (Fset.elements_spec1 _ _ _ H4'); apply H2.
            - rewrite (Fset.elements_spec1 _ _ _ Hsa); assumption.
            - rewrite in_flat_map in Kf; destruct Kf as [[_sa1 g1] [H1 H2]].
              rewrite equiv_nenv_sym in Henv.
              destruct (H _ _ _ Henv H1) as [[_sa2 g2] [H3 H4]].
              rewrite in_flat_map; exists (_sa2, g2); split; [assumption | ].
              simpl in H4.
              simpl; simpl in H2.
              assert (H4' := proj2 H4); simpl in H4';subst g2.
              assert (H4' := proj1 H4); simpl in H4';rewrite <- (Fset.elements_spec1 _ _ _ H4'); apply H2.
          Qed.

          
          Fixpoint find_eval_nenv ne ag :=
            match ne with
            | nil => if is_built_upon_ag TRcd nil ag then Some nil else None
            | (sa1, _) :: env' =>
              match find_eval_nenv  env' ag with
              | Some l0 => Some l0
              | None => if is_a_suitable_nenv  sa1 env' ag then Some ne else None
              end
            end.      

          
          Lemma find_eval_nenv_is_built_upon_ag_nil_true :
            forall ft , is_built_upon_ag TRcd nil ft = true ->
                   forall nenv,
                     find_eval_nenv nenv ft = Some nil.
          Proof.
            intros ft Hft.
            fix lem 1;intros  [|slc1 nenv].
            - cbn;rewrite Hft;apply eq_refl.
            - cbn;rewrite lem;destruct slc1;apply eq_refl.
          Qed.
          
          Lemma find_eval_env_well_formed_e_if :
            forall ft env l,
              well_formed_e TRcd  env = true ->
              find_eval_env  TRcd env ft  = Some l
              -> well_formed_e  TRcd l = true .
          Proof.
            intros ft.
            fix lem 1.
            intros  [|slc1 nenv];intros l WS Hf.
            - cbn;inversion Hf as [I1].
              destruct is_built_upon_ag;[|discriminate].
              inversion I1;apply WS.
            - simpl in Hf.
              destruct find_eval_env eqn:D1.
              + inversion Hf;subst;clear Hf.
                apply well_formed_e_cons in WS.
                destruct slc1,p.
                inversion H0;subst.
                refine (lem _ _ WS D1).
              + destruct slc1,p.
                destruct is_a_suitable_env eqn:D2;[|discriminate].
                inversion Hf;apply WS.
          Qed.

          Lemma  find_eval_env_nenv_eq_none :
            forall env ft,
              find_eval_nenv (map fst env) ft = None <->
              find_eval_env  TRcd  env ft = None.
          Proof.
            induction env as [|[[s1 g1] lt1] env];intro ft;split;intro H;cbn in *.
            - destruct is_built_upon_ag;[discriminate|apply eq_refl].
            - destruct is_built_upon_ag;[discriminate|apply eq_refl].
            - destruct find_eval_nenv eqn:D1;
                [discriminate| ].
              rewrite <- is_a_suitable_env_nenv_eq .
              destruct is_a_suitable_nenv;[discriminate| ].
              rewrite IHenv in D1.
              rewrite D1.
              apply eq_refl.
            - destruct find_eval_env eqn:D1;[discriminate| ].
              rewrite <- is_a_suitable_env_nenv_eq in H.
              destruct is_a_suitable_nenv;[discriminate| ].
              rewrite <- IHenv in D1.
              rewrite D1.
              apply eq_refl.
          Qed.
          
          Lemma  find_eval_env_nenv_eq :
            forall env ft,
              find_eval_nenv (map fst env) ft =
              lift (fun l => map fst l) (find_eval_env TRcd env ft) .
          Proof.
            induction env as [|[[s1 g1] lt1] env];intro ft;cbn in *.
            - destruct is_built_upon_ag;apply eq_refl.
            - destruct find_eval_nenv eqn:D1.
              + rewrite IHenv in D1.
                unfold lift in D1.
                destruct find_eval_env eqn:d2;[| discriminate].
                rewrite <- D1.
                apply eq_refl.
              + rewrite <- is_a_suitable_env_nenv_eq .
                rewrite IHenv in D1.
                unfold lift in D1.
                destruct find_eval_env eqn:d2;[discriminate | ].
                destruct is_a_suitable_nenv;apply eq_refl.
          Qed.

          Lemma find_eval_nenv_eq:
            forall ag ne1 ne2,
              equiv_nenv ne1 ne2 ->
              match find_eval_nenv  ne1 ag with
              | Some e1 => match find_eval_nenv  ne2 ag with
                          | Some e2 => equiv_nenv  e1 e2
                          | None => False
                          end
              | None => match find_eval_nenv  ne2 ag with
                       | Some _ => False
                       | None => True
                       end
              end.
          Proof.
            intros e env1; induction env1 as [ | [sa1 g1] env1]; intros [ | [sa2 g2] env2] He.
            - simpl; case (is_built_upon_ag TRcd nil e); trivial.
            - inversion He.
            - inversion He.
            - inversion He; subst.
              assert (IH := IHenv1 _ H4).
              simpl.
              destruct (find_eval_nenv  env1 e) as [_l1 | ];
                destruct (find_eval_nenv  env2 e) as [_l2 | ]; try contradiction IH.
              + assumption.
              +  assert (H1 := proj1 H2).
                 simpl in H1.
                 rewrite <- (is_a_suitable_nenv_eq  e _ _ H1 H4).
                 case (is_a_suitable_nenv  sa1 env1 e).
                 * constructor 2; trivial.
                 * trivial.
          Qed.
          
        End EnvToNRAEnv.


        Section EnvDotToNRAEnv.
          
          Lemma env_dot_to_nraenv_equiv_nenv_eq :
            forall  a,
            forall e1 e2,
              equiv_nenv e1 e2 ->
              env_dot_to_nraenv e1 a =
              env_dot_to_nraenv e2  a.
          Proof.
            intros a.
            induction e1 as [|[s1 g1] e1];intros [|[s2 g2] e2]  Heq.
            - apply eq_refl.
            -  inversion Heq.      
            -  inversion Heq.
            - simpl.
              inversion Heq;subst.
              rewrite (IHe1 _ H4).
              rewrite (Fset.mem_eq_2 _ _ _ (proj1 H2)).
              unfold default_tuple,tuple_as_dpairs,tuple_as_pairs.
              rewrite (Fset.elements_spec1 _ _ _ (labels_mk_tuple _ _ _)).       
              rewrite (Fset.elements_spec1 _ _ _ (labels_mk_tuple _ _ _)).
              unfold equiv_nslice in H2;simpl in H2.
              rewrite (Fset.elements_spec1 _ _ _ (proj1 H2)).
              simpl.
              destruct (a inS? s2) eqn:D1;[|apply eq_refl].
              apply f_equal.
              apply f_equal2;[|apply eq_refl].
              apply f_equal.
              rewrite 2 map_map;simpl.
              apply map_ext.
              intros a1.
              apply f_equal.
              rewrite dot_mk_tuple.
              rewrite dot_mk_tuple.
              rewrite (Fset.mem_eq_2 _ _ _ (proj1 H2)).
              apply eq_refl.
          Qed.
          
          Lemma env_dot_to_nraenv_ignores_did :
            forall br r aenv (a:attribute) ,
            forall reg (id1 id2: data),
              nraenv_eval  r  br (env_dot_to_nraenv aenv a)  reg id1  =
              nraenv_eval  r br (env_dot_to_nraenv aenv a) reg id2.
          Proof.
            intros br i.
            induction aenv as [ |[s1 g1] aenv];intros a reg id1 id2.
            -  apply eq_refl.
            - cbn.
              destruct (a inS? s1) eqn:D1;swap 1 2.
              +  cbn.
                 unfold olift.
                 destruct reg;try apply eq_refl.
                 destruct (edot l "tl") eqn:D2;
                   [apply IHaenv|apply eq_refl].
              + cbn.
                unfold olift.        
                apply eq_refl.
          Qed.
          
          Lemma env_dot_to_nraenv_in_eq :
            forall  (env1 env2:env  ) slc a ,
              a inS (fst (fst slc)) -> 
              env_dot_to_nraenv (map fst (slc::env1))  a =
              env_dot_to_nraenv (map fst (slc::env2))  a.
          Proof.
            destruct env1  as [ | [[s1 g1] lt1] env1];
              intros [ | [[s2 g2]lt2 ] env2] [[s g] lt] a  Hs;
              cbn;simpl in Hs;rewrite Hs;apply eq_refl.
          Qed.    
          
          Lemma env_dot_to_nraenv_is_sound_quick : forall br i env s g lt,
              well_formed_e TRcd ((s,g,lt)::env) = true ->
              forall did  a,
                well_formed_ft TRcd ((s,g,lt)::env) ( (F_Dot  a))  = true ->
                nraenv_eval
                  br i 
                  (env_dot_to_nraenv (map fst ((s,g,lt)::env)) a)
                  (env_to_data ((s,g,lt)::env)  ) did = 
                nraenv_eval
                  br i
                  (env_dot_to_nraenv (map fst ((s,g,lt)::env)) a)
                  ((drec (("slc"%string,dcoll (listT_to_listD (quicksort OTuple lt)))::("tl"%string,env_to_data env)::nil))) did.
          Proof.
            intros br i env s g lt We did a Wa.
            unfold nraenv_eval;simpl.
            destruct (a inS? s) eqn:D1;[|apply eq_refl].    
            simpl;unfold olift,listT_to_listD;simpl.
            unfold default_tuple.
            unfold edot.
            rewrite <- map_rec_sort;
              [|intros x y;split;intro H;apply H].
            rewrite map_normalize_normalized_eq;swap 1 2.
            - rewrite Forall_forall.
              intros x Hx.
              unfold tuple_as_dpairs in Hx.
              apply rec_sort_in in Hx.
              rewrite in_map_iff in Hx.
              destruct Hx as [a' [Ga Fa]].
              simpl in *.
              destruct x;inversion Ga;simpl;subst.
              rewrite (value_to_data_normalize br).
              apply normalize_normalizes.            
            - destruct (quicksort OTuple lt) eqn:D2.
              + rewrite quicksort_nil in D2;subst;apply eq_refl.
              + destruct lt;[discriminate| ].
                simpl map;cbv iota beta.
                simpl.
                do 2 rewrite assoc_lookupr___find_eq.
                unfold lift.
                apply BasicFacts.match_option_eq.
                assert (G0  := We).
                apply well_formed_e_cons_spec in G0.
                destruct G0 as [H1 [H2 [ H3 H4]]].
                assert (B1 : a inS labels t0).
                {
                  rewrite (Fset.mem_eq_2 _ _ _  (H3 _ (or_introl eq_refl))).
                  apply D1.
                }
                assert (B2 : a inS labels t).
                {
                  refine (eq_trans (Fset.mem_eq_2 _ _ _  (H3 _ _) _) D1).
                  rewrite In_quicksort,D2;left;apply eq_refl.
                }
                rewrite (tuple_tuple_as_pairs_dot_in _ _ _ B2).          
                rewrite (tuple_tuple_as_pairs_dot_in _ _ _ B1).
                apply f_equal.
                destruct g;swap 1 2.
                * destruct lt;[|destruct H1].
                  inversion D2;subst.
                  apply eq_refl.
                * unfold well_formed_ag,well_formed_ft in Wa.
                  simpl in Wa.
                  rewrite extract_funterms_app in Wa.
                  rewrite Oset.mem_bool_app,orb_true_iff in Wa.
                  assert (H1 := H1 (A_Expr (F_Dot a))  ).           
                  destruct Wa.
                  -- rewrite Oset.mem_bool_true_iff,in_extract_funterms in H.
                     assert (H1 := H1 H ).
                     destruct H1 as [H1 H5].
                     assert (H5 := H5 t).
                     rewrite In_quicksort,D2 in H5.
                     assert (A1 := H5 (or_introl eq_refl) _  H).
                     simpl in A1.
                     rewrite B1,B2 in A1.
                     rewrite A1;apply eq_refl.               
                  -- clear H1.
                     destruct (H2 a);[rewrite D1 in H0;discriminate| ].
                     clear H2.
                     assert (A2 := well_formed_ft_variables_ft_env TRcd _ (F_Dot a) H4 H).
                     unfold variables_ft in A2.
                     rewrite Fset.subset_spec in A2.
                     assert (A2 := A2 a).
                     rewrite Fset.singleton_spec,Oset.eq_bool_refl in A2.
                     assert (A2 := A2 eq_refl).
                     rewrite Fset.mem_Union in A2.
                     destruct A2 as [s'  [A2 A3]].
                     rewrite in_map_iff in A2.
                     destruct A2 as [slc [A2 A4]].
                     assert (H0 := H0 slc A4).
                     destruct slc as [[ ]];subst.
                     simpl in H0.
                     rewrite A3 in H0.
                     discriminate.
          Qed.

          Lemma env_dot_to_nraenv_is_sound : forall br rb e a,
              well_formed_e TRcd e = true ->        
              well_formed_ft TRcd e (F_Dot a)   = true ->
              forall did  ,
                nraenv_eval
                  br rb
                  (env_dot_to_nraenv (map fst e)  a)
                  (env_to_data e)  did =
                Some  (value_to_data  (interp_dot _ e a)).
          Proof.
            intros br i.
            simpl in *.
            induction e as [|[[s1 g1] lt1] env];intros a We Wft did .
            - simpl;rewrite (value_to_data_normalize  br) at 2.
              apply eq_refl.
            - rewrite (env_dot_to_nraenv_is_sound_quick _ _ _ _ _ _ We _ _ Wft).
              unfold nraenv_eval in *.
              simpl.
              (* assert (A1 := env_to_data_is_rec ((s1, g1, lt1) :: env)). *)
              (* destruct A1 as [l1 [l2 A1]]. *)
              assert (W0 := We).
              apply well_formed_e_cons_spec in W0.
              destruct W0 as [W1 [W4 [W2 W3]]].
              assert (IH := IHenv a W3);clear IHenv.        
              destruct (a inS? s1) eqn:D1;swap 1 2.
              + assert (B1 : Oset.mem_bool (OFun TRcd) (F_Dot a) (extract_funterms TRcd (flat_map (groups_of_env_slice TRcd) env)) = true ).
                {
                  unfold well_formed_ft in Wft.
                  simpl in Wft.              
                  rewrite extract_funterms_app in Wft.
                  rewrite Oset.mem_bool_app in Wft.
                  rewrite orb_true_iff in Wft.
                  destruct Wft;[|apply H].            
                  destruct g1;swap 1 2.
                  - destruct lt1;[destruct W1| ].
                    destruct lt1;[|destruct W1].
                    rewrite extract_funterms_A_Expr in H.
                    rewrite Oset.mem_bool_true_iff in H.
                    rewrite in_map_iff in H.
                    destruct H as [a' [Ha' H]].
                    inversion Ha';subst.
                    apply Fset.in_elements_mem in H.
                    rewrite H in D1;discriminate.
                  - destruct lt1;[destruct W1| ].              
                    rewrite Oset.mem_bool_true_iff in H.
                    rewrite in_extract_funterms in H.
                    assert (W1 := W1 (A_Expr (F_Dot a)) H).
                    destruct W1.              
                    simpl in H0.
                    unfold well_formed_ft in H0.
                    simpl in H0.
                    rewrite Oset.mem_bool_true_iff in H0.
                    rewrite in_extract_funterms in H0.
                    apply In_app in H0.
                    rewrite Oset.mem_bool_true_iff.
                    rewrite in_extract_funterms.
                    destruct H0;[|apply H0].
                    rewrite in_map_iff in H0.
                    destruct H0 as [x [H0 H8]].
                    unfold default_tuple in H8.
                    rewrite (Fset.elements_spec1 _ _ _ (labels_mk_tuple _ _ _ )) in H8.
                    inversion H0;subst.
                    apply Fset.in_elements_mem in H8.
                    rewrite H8 in D1;discriminate.
                }
                assert (IH := IH  B1 did).
                simpl.
                rewrite IH.
                do 2 apply f_equal.
                destruct (quicksort OTuple lt1) eqn:D2;[apply eq_refl| ].
                assert (Aux2 : In t lt1).
                { rewrite In_quicksort;rewrite D2.
                  left;apply eq_refl. }
                rewrite (Fset.mem_eq_2 _ _ _ (W2 _ Aux2)).
                rewrite D1.
                apply eq_refl.
              + simpl;unfold olift,listT_to_listD;simpl.
                simpl.
                unfold default_tuple.
                unfold edot.
                rewrite <- map_rec_sort;
                  [|intros x y;split;intro H;apply H].
                rewrite map_normalize_normalized_eq;swap 1 2.
                * rewrite Forall_forall.
                  intros x Hx.
                  unfold tuple_as_dpairs in Hx.
                  apply rec_sort_in in Hx.
                  rewrite in_map_iff in Hx.
                  destruct Hx as [a' [Ga Fa]].
                  simpl in *.
                  destruct x;inversion Ga;simpl;subst.
                  rewrite (value_to_data_normalize br).
                  apply normalize_normalizes.            
                *  destruct (quicksort OTuple lt1) eqn:D2.
                   -- simpl map;cbv iota zeta beta;unfold dnone.              
                      rewrite assoc_lookupr_drec_sort.
                      unfold tuple_as_dpairs,tuple_as_pairs;simpl.
                      rewrite map_map;simpl fst.
                      rewrite (Fset.elements_spec1 _ _ _ (labels_mk_tuple _ _ _)).
                      assert (A3 := in_dom_lookupr
                                     (map
                                        (fun x : attribute =>
                                           (attribute_to_string x,
                                            value_to_data
                                              (snd (x, dot (mk_tuple s1 (fun a0 : attribute => default_value TRcd (type_of_attribute TRcd a0))) x))))
                                        ({{{s1}}})) (attribute_to_string a) ODT_eqdec).
                      unfold domain in A3.
                      rewrite map_map in A3.
                      assert (G1 : In (attribute_to_string a) (map (fun x : attribute => attribute_to_string x) ({{{s1}}}))).
                      {
                        rewrite in_map_iff.
                        exists a;split;[apply eq_refl| ].
                        apply Fset.mem_in_elements.
                        apply D1.
                      }
                      apply A3 in G1.
                      destruct G1 as [v G1].
                      refine (eq_trans G1  _).
                      apply f_equal.
                      apply assoc_lookupr_in in G1.
                      rewrite in_map_iff in G1.
                      destruct G1 as [a1 [G1 G2]].
                      apply eq_sym in G1;inversion G1;subst.
                      apply f_equal.
                      apply Fset.in_elements_mem in G2.
                      assert (A4 := (attribute_to_string_inj )  _ _ H0).
                      subst.
                      rewrite dot_mk_tuple,G2.
                      apply eq_sym.
                      apply (well_formed_e_interp_dot _ s1 g1 lt1);[|apply G2].
                      apply We;apply G2.
                   -- simpl map;cbv iota beta.
                      (* rewrite assoc_lookupr_drec_sort. *)
                      unfold tuple_as_dpairs,tuple_as_pairs;simpl.
                      rewrite map_map;simpl fst.
                      assert (Aux2 : In t lt1).
                      { rewrite In_quicksort;rewrite D2.
                        left;apply eq_refl. }
                      rewrite (Fset.mem_eq_2 _ _ _ (W2 _ Aux2)).
                      simpl fst.
                      rewrite D1.
                      rewrite <- (Fset.mem_eq_2 _ _ _ (W2 _ Aux2)) in D1.
                      assert (A5 := tuple_tuple_as_pairs_dot_in TRcd _ a D1).
                      assert (A6 := find_assoc_lookupr___some_sorted   _ _ A5).
                      refine (eq_trans _ A6).
                      unfold tuple_as_dpairs,tuple_as_pairs;simpl.
                      rewrite map_map.
                      apply eq_refl.
          Qed.
          
          Lemma eval_env_dot_to_nraenv_equiv_env_eq :
            forall br i did a,
            forall e1 e2,
              equiv_env TRcd e1 e2 ->
              well_formed_e TRcd e1 = true ->
              well_formed_ft TRcd e1 (F_Dot a) = true ->        
              nraenv_eval br i (env_dot_to_nraenv (map fst e1) a) (env_to_data e1)   did =
              nraenv_eval br i (env_dot_to_nraenv (map fst e2)  a )(env_to_data e2)  did.
          Proof.
            intros br i did a e1 e2 Heq We Wf.
            rewrite (env_dot_to_nraenv_is_sound _ _ _ _ We Wf).
            rewrite (well_formed_e_eq Heq) in We.
            rewrite (well_formed_ft_eq _ _ _ _ (equiv_env_weak_equiv_env _ _ _ Heq)) in Wf.
            rewrite (env_dot_to_nraenv_is_sound _ _ _ _ We Wf).
            do 2 apply f_equal.
            apply interp_dot_eq;apply Heq.
          Qed.

          
        End  EnvDotToNRAEnv.

        Section Types.
                
          Definition well_typed_slice (slc: env_slice _) :=
            well_typed_list_tuple (snd slc).
          
          Lemma well_typed_slice_eq :
            forall x y : nslice * list tuple, equiv_env_slice TRcd x y -> well_typed_slice x = well_typed_slice y.
          Proof.
            
            intros [[] ]  [[] ].
            intros;simpl.
            unfold well_typed_slice.
            simpl.
            simpl in H.
            destruct H as [[] [H0 H]].
            apply (well_typed_list_tuple_eq H).
          Qed.
          
          Definition well_typed_e e :=
            forallb well_typed_slice e.
          
          Lemma well_typed_e_cons :
            forall slc e,
              well_typed_e (slc::e) = true <->
              well_typed_e e = true /\
              well_typed_slice slc = true.
          Proof.
            intros slc e.
            simpl;rewrite andb_true_iff.
            split;intros [H1 H2];split;assumption.
          Qed.            
          
          Lemma well_typed_e_eq:
            forall e1 e2,
              equiv_env _ e1 e2 -> well_typed_e  e1 = well_typed_e  e2.
            Proof.
              induction 1;
                [apply eq_refl| ].
              simpl.
              rewrite IHForall2.
              apply f_equal2;[|apply eq_refl].
              revert x y H.
              clear.
              apply well_typed_slice_eq.
            Qed.
            
          Lemma type_of_interp_dot_eq :
            forall e,
              well_typed_e e = true ->
              forall a,
              type_of_value TRcd (interp_dot TRcd e a) =
              type_of_attribute TRcd a.
          Proof.
            induction e as [|[[s1 g1] lt1] e IH];intros.
            - simpl.
              apply type_of_default_value_eq.
            - simpl.
              assert (IH := (IH (proj1 (proj1 (well_typed_e_cons _ _) H)))).
              destruct (quicksort OTuple lt1) eqn:D1;
                [apply IH| ].
              destruct (a inS? labels t ) eqn:D2;
                [ |apply IH].
              assert (H2 := proj2 (proj1 (well_typed_e_cons _ _) H)).
              unfold well_typed_slice in H2.
              simpl in H2.
              unfold well_typed_list_tuple in H2.
              rewrite forallb_forall in H2.
              assert (H2 := H2 t).
              rewrite (In_quicksort OTuple) in H2.
              rewrite D1 in H2.
              assert (H2 := H2 (or_introl eq_refl)).
              rewrite forallb_forall in H2.
              apply well_typed_attribute_spec.
              apply Fset.mem_in_elements in D2.
              assert (H2 := H2 a D2).
              apply H2.
          Qed.
          
          Lemma find_eval_env_well_typed_e_if :
            forall ft env l,
              well_typed_e env = true ->
              find_eval_env  TRcd env ft  = Some l
              -> well_typed_e   l = true .
          Proof.
            intros ft.
            fix lem 1.
            intros  [|slc1 nenv];intros l WS Hf.
            - cbn;inversion Hf as [I1].
              destruct is_built_upon_ag;[|discriminate].
              inversion I1;apply WS.
            - simpl in Hf.
              destruct find_eval_env eqn:D1.
              + inversion Hf;subst;clear Hf.
                apply well_typed_e_cons in WS.
                destruct slc1,p.
                inversion H0;subst.
                refine (lem _ _ (proj1 WS) D1).
              + destruct slc1,p.
                destruct is_a_suitable_env eqn:D2;[|discriminate].
                inversion Hf;apply WS.
          Qed.

          
          Lemma well_typed_e_cons_tuple :
            forall env s1 g1 lt1,
              well_typed_e  ((s1, g1, lt1) :: env) = true ->
              forall t,
                In t lt1 ->
                well_typed_e  ((s1, Group_Fine _, t :: nil) :: env) = true.
          Proof.
            intros env s1 g1 lt1 Hx.
            apply well_typed_e_cons in Hx.
            intros t Ht.
            simpl.
            apply andb_true_iff.
            split;[|apply Hx].
            unfold well_typed_slice in *.
            apply forallb_forall.
            unfold well_typed_list_tuple in Hx.
            rewrite  forallb_forall in Hx.
            intros.
            apply Hx.
            simpl in  H.
            destruct H;
              [subst|destruct H].
            apply Ht.
          Qed.

        End Types.
        
      End Lemmas.
      
    End Sec.
    
End EnvToNRAEnv.
