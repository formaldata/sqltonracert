(************************************************************************************)
(**                                                                                 *)
(**                          The SQLToNRACert Library                               *)
(**                                                                                 *)
(**                       LMF, CNRS & Université Paris-Saclay                       *)
(**                                                                                 *)
(**                        Copyright 2016-2022 : FormalData                         *)
(**                                                                                 *)
(**         Authors: Véronique Benzaken                                             *)
(**                  Évelyne Contejean                                              *)
(**                  Mohammed Hachmaoui                                             *)
(**                  Chantal Keller                                                 *)
(**                                                                                 *)
(************************************************************************************)

Require Export TnullTD.

Require Import InstanceToNRAEnv.

From SQLFS Require Import GenericInstance.

Section Sec.

  Definition relname_to_string : GenericInstance.relname -> String.string.
    intros [r]; exact r.
  Defined.

  Lemma relname_to_string_inj :
    forall a1 a2, relname_to_string a1 = relname_to_string a2 -> a1 = a2.
  Proof.
    intros [ ] [ ] H.
    simpl in H.
    subst;apply eq_refl.
  Qed.

  
  Global Instance tnull_IN basesort :
    InstanceToNRAEnv.InstanceToNRAEnv ORN :=
    @InstanceToNRAEnv.InstanceToNRAEnv.mk_C
      tnull_frt tnull_TD
      relname ORN relname_to_string relname_to_string_inj basesort.

End Sec.
