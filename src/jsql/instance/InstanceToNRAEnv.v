(************************************************************************************)
(**                                                                                 *)
(**                          The SQLToNRACert Library                               *)
(**                                                                                 *)
(**                       LMF, CNRS & Université Paris-Saclay                       *)
(**                                                                                 *)
(**                        Copyright 2016-2022 : FormalData                         *)
(**                                                                                 *)
(**         Authors: Véronique Benzaken                                             *)
(**                  Évelyne Contejean                                              *)
(**                  Mohammed Hachmaoui                                             *)
(**                  Chantal Keller                                                 *)
(**                                                                                 *)
(************************************************************************************)

Set Implicit Arguments.

Require Import TupleToData.

Module InstanceToNRAEnv.
  
  Import TupleToData.

  Class InstanceToNRAEnv
        `{frt:foreign_runtime}
        `{TD : @TupleToData frt}
        {relname : Type}
        (ORN : OrderedSet.Oset.Rcd relname) : Type :=
    mk_C { 
        relname_to_string : relname -> String.string;        
        relname_to_string_inj :
          forall x y : relname, relname_to_string x = relname_to_string y -> x = y;
        basesort : relname -> Fset.set A; }.
  
  Section Sec.

    Hypothesis relname : Type.
    Hypothesis ORN : Oset.Rcd relname.
    
    Context{frt:foreign_runtime}.   
    Context {TD :TupleToData}.
    Context {IN : InstanceToNRAEnv ORN}.
    
    Definition instance_to_bindings i :=
      (map (fun x =>  match x with
                     | (r, b) => (relname_to_string r, dcoll (bagT_to_listD  b))
                     end) i).

     Definition well_sorted_instance i :=
      Oset.all_diff_bool
        ORN  (map fst i)
        &&
        forallb
        (fun p =>
           forallb
             (fun t =>
                Fset.equal _ (labels t) (basesort (fst p)))
             (Febag.elements BTupleT (snd p)))
        i.
    
    (* Definition well_sorted_instance_prop i := *)
    (*   NoDup (map fst i) /\ *)
    (*   forall r (b:bagT), In (r,b) i -> forall t , t inBE b -> labels  t =S= basesort  r. *)

    (* Lemma well_sorted_instance_bool_prop_ok : *)
    (*   forall i, *)
    (*     well_sorted_instance i = true <-> well_sorted_instance_prop i. *)
    (* Proof. *)
    (*   intros i;unfold *)

     Lemma NoDup_domain_instance_to_bindings :
        forall i,
          well_sorted_instance i  = true ->
          NoDup (domain (instance_to_bindings i)).
     Proof.
       unfold well_sorted_instance.
       intros i.
       rewrite Bool.andb_true_iff.
       rewrite <- Oset.all_diff_bool_ok.
       rewrite all_diff_NoDup_same.
       intros [H1 _].
       replace (domain (instance_to_bindings i)) with
           (map relname_to_string (map fst i)).
       - apply map_inj_NoDup;[|apply H1].
         apply relname_to_string_inj.
       - unfold domain,instance_to_bindings.
         rewrite 2 map_map.
         apply map_ext.
         intros [ ];apply eq_refl.
     Qed.
            
        Lemma edot_find_some :          
          forall i,
            well_sorted_instance i = true ->
            forall r,
            In r (map fst i) ->
            exists l,
              Permutation (bagT_to_listD match Oset.find ORN r i with
                                       | Some b => b
                                       | None => emptysetBE
                                       end) l /\
              edot (rec_sort (instance_to_bindings i)) (relname_to_string r) = Some (dcoll l).
        Proof.
          intros i Hi r Hr.
          destruct Oset.find eqn:D1;
            [|destruct (Oset.find_none_alt _ _ _ D1 Hr)].
          exists (bagT_to_listD b).
          split;[apply Permutation_refl| ].
          unfold edot.
          apply Oset.find_some in D1.
          rewrite assoc_lookupr_drec_sort.
          refine (in_assoc_lookupr_nodup _ _ _ _ _ _).
          - apply (NoDup_domain_instance_to_bindings _ Hi).
          -   unfold instance_to_bindings.
              rewrite in_map_iff.
               exists (r,b);split;auto.
        Qed.

        Lemma edot_find_none i r :
          ~ In r (map fst i) ->
          edot (rec_sort (instance_to_bindings i)) (relname_to_string r) = None.
        Proof.
          intro Hr.
          unfold edot.
          rewrite assoc_lookupr_drec_sort.
          apply assoc_lookupr_nin_none.
          unfold instance_to_bindings.
          rewrite domain_eq, map_map. intro H; apply Hr.
          rewrite in_map_iff in *.
          destruct H as [[r0 b] Hx]. simpl in Hx.
          exists (r0,b);split;intuition.
          simpl. now apply relname_to_string_inj.
        Qed.


        Definition well_typed_instance (i:list (relname * (Febag.bag (Fecol.CBag (CTuple TRcd))))) :=
          forallb well_typed_list_tuple (map (fun x => Febag.elements _ (snd x)) i).

        Definition is_translatable i :=
          well_sorted_instance i && well_typed_instance i.        
        
  End Sec.
  
End InstanceToNRAEnv.
