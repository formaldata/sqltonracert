(************************************************************************************)
(**                                                                                 *)
(**                          The SQLToNRACert Library                               *)
(**                                                                                 *)
(**                       LMF, CNRS & Université Paris-Saclay                       *)
(**                                                                                 *)
(**                        Copyright 2016-2022 : FormalData                         *)
(**                                                                                 *)
(**         Authors: Véronique Benzaken                                             *)
(**                  Évelyne Contejean                                              *)
(**                  Mohammed Hachmaoui                                             *)
(**                  Chantal Keller                                                 *)
(**                                                                                 *)
(************************************************************************************)

Set Implicit Arguments.

Require Import NArith List String Bool.

Require Import Permutation.
From SQLFS Require Import ListPermut SqlAlgebra Formula.

Require Import FormulaToNRAEnv Qcert.Utils.Utils.

Require Import Qcert.NRAEnv.NRAEnvRuntime Aux.
From SQLFS Require Import FiniteSet OrderedSet FiniteCollection FiniteBag FlatData.

Module QueryToNRAEnv.
  
  Export FormulaToNRAEnv.

  Class QueryToNRAEnv
        `{frt:foreign_runtime}         
        `{TD : @TupleToData frt}        
        `{SN : @SymbolToNRAEnv frt TD}       
        {EN : @EnvToNRAEnv frt TD}
        `{FTN : @FTermToNRAEnv frt TD SN EN}        
        `{AN : @AggregateToNRAEnv frt TD }
        `{ATN : @ATermToNRAEnv frt TD SN AN EN  FTN}
        {relname : Type}
        {ORN : Oset.Rcd relname}
        `{IN : @InstanceToNRAEnv frt TD _ ORN}
        `{BD : @BoolBToData  frt (B TRcd)}
        `{PN :  @PredicateToNRAEnv frt TD BD}    
        `{FN :  @FormulaToNRAEnv
                  frt TD SN EN FTN AN ATN
                  relname ORN IN BD PN}: Type.

    Section Sec.
      
      Import FormulaToNRAEnv.

      Hypothesis relname : Type.
      Hypothesis ORN : Oset.Rcd relname.
      
      Context {frt : foreign_runtime}.
      Context {TD:TupleToData}.
      Context {SN:SymbolToNRAEnv}.
      Context {EN : EnvToNRAEnv}.
      Context {AN:AggregateToNRAEnv}.
      Context {FTN:FTermToNRAEnv}.  
      Context {ATN:ATermToNRAEnv}.
      Context {IN : InstanceToNRAEnv ORN}.
      Context {BD : BoolBToData (B TRcd)}.
      Context {PN : PredicateToNRAEnv}.
      Context {FN : FormulaToNRAEnv}.
      Context {QN : QueryToNRAEnv}.

      Notation query := (query TRcd relname ).
      Notation q_formula :=
        (sql_formula TRcd query ).
      Notation query_size q :=
        (Tree.tree_size
           ((@tree_of_query TRcd  relname) q)).
      Notation sql_formula_size f :=
        (Tree.tree_size
           (tree_of_sql_formula
              (@tree_of_query _ relname )f)).
      Notation size_funterm ft :=
        (@size_funterm TRcd ft).
      Notation sort := (SqlAlgebra.sort).  

      Notation eval_query i :=
        (eval_query  basesort
                     (fun r =>
                        match Oset.find ORN r i with
                        | Some b => b
                        |None => Febag.empty _
                        end) unknownB
                     contains_nulls).
      
      Notation eval_sql_formula :=
        (Formula.eval_sql_formula 
           unknownB
           contains_nulls).
      
      Arguments Q_Empty_Tuple {T} {relname}.
      Arguments Q_Table {T} {relname}.
      Arguments Sql_True {T} {dom}.

      Section Translation.
        
        Definition push_in_nenv_then_eval nt n :=
          (NRAEnvAppEnv
             n
             (NRAEnvBinop 
                OpRecConcat
                (NRAEnvUnop  (OpRec "tl") NRAEnvEnv)
                (NRAEnvUnop  (OpRec "slc") nt))).
        
        Definition set_op_to_binary_op o :
          (@binary_op
             (@foreign_runtime_data frt)
             (@foreign_runtime_operators frt)) :=
          match o with
          | Union => OpBagUnion
          | UnionMax => OpBagMax
          | Inter => OpBagMin
          | Diff => OpBagDiff
          end.
        
        Fixpoint query_to_nraenv nenv (q : query) : nraenv :=
          match q with
          | Q_Empty_Tuple =>
            (NRAEnvConst (dcoll (drec nil::nil)))
          | Q_Table r =>
            (NRAEnvGetConstant (relname_to_string r))
          | Q_Set o q1 q2 =>
            if sort basesort q1 =S?= sort basesort q2 then
              NRAEnvBinop
                (set_op_to_binary_op o)
                (query_to_nraenv nenv q1)
                (query_to_nraenv nenv q2)
            else
              NRAEnvConst (dcoll nil)
          | Q_NaturalJoin q1 q2 =>  
            if Fset.is_empty
                 A (sort basesort q1 interS sort basesort q2) then
              NRAEnvProduct
                (query_to_nraenv nenv q1)
                (query_to_nraenv nenv q2)
            else
              NRAEnvConst (dcoll nil)           
          | Q_Pi s q =>
            (NRAEnvMap
               (push_in_nenv_then_eval
                  (NRAEnvUnop OpBag NRAEnvID)
                  (select_list_to_nraenv
                     ((sort basesort q,Group_Fine _)::nenv) s)))              
              (query_to_nraenv nenv q)              
          | Q_Sigma Sql_True q => query_to_nraenv nenv q
          | Q_Sigma f q =>
            (NRAEnvSelect
               (push_in_nenv_then_eval
                  (NRAEnvUnop OpBag NRAEnvID)
                  (NRAEnvIsTrueB
                     (formula_to_nraenv_gen
                        query_to_nraenv
                        ((sort basesort q,Group_Fine _)::nenv) f)))
               (query_to_nraenv nenv q))
              
          | Q_Gamma s lf Sql_True q =>
            match extract_grouping_criteria lf with
              | Some gc =>
                if Fset.mk_set A gc subS? sort basesort q then
                  let g_label := Fresh.find_fresh_string (map attribute_to_string gc) in
                  let groups :=
                      NRAEnvMap
                        (NRAEnvUnop (OpDot g_label) NRAEnvID)
                        (NRAEnvGroupBy g_label (map attribute_to_string gc) (query_to_nraenv nenv q)) in
                  NRAEnvMap
                    (push_in_nenv_then_eval
                       NRAEnvID
                       (select_list_to_nraenv
                          ((sort basesort q,Group_By _ lf)::nenv) s))
                    groups
                else                  
                  (NRAEnvConst (dcoll nil))
              | _ => 
                (NRAEnvConst (dcoll nil))
              end
          | Q_Gamma s lf f q =>
            (* if let e :=  (map (fun ns => ( ns, (default_tuple TRcd (fst ns))::nil))) nenv in *)
            (*    well_formed_s *)
            (*      TRcd (env_g TRcd e (Group_By TRcd lf) ( (default_tuple TRcd (sort basesort q))::nil)) *)
            (*      (match s with _Select_List _ l => l end) then *)
              (* be careful, in NRAEnv, grouping is made only wrt attributes *)
              match extract_grouping_criteria lf with
              | Some gc =>
                if Fset.mk_set A gc subS? sort basesort q then
                  let g_label := Fresh.find_fresh_string (map attribute_to_string gc) in
                  let groups :=
                      NRAEnvMap
                        (NRAEnvUnop (OpDot g_label) NRAEnvID)
                        (NRAEnvGroupBy g_label (map attribute_to_string gc) (query_to_nraenv nenv q)) in
                  let filter_groups :=                      
                      NRAEnvSelect
                        (push_in_nenv_then_eval
                           NRAEnvID
                           (NRAEnvIsTrueB 
                              (formula_to_nraenv_gen
                                 query_to_nraenv
                                 ((sort basesort q,Group_By _ lf)::nenv) f)))
                        groups
                  in                  
                  NRAEnvMap
                    (push_in_nenv_then_eval
                       NRAEnvID
                       (select_list_to_nraenv
                          ((sort basesort q,Group_By _ lf)::nenv) s))
                    filter_groups
                else                  
                  (NRAEnvConst (dcoll nil))
              | _ => 
                (NRAEnvConst (dcoll nil))
              end
            (* else  *)
            (*   (NRAEnvConst (dcoll nil)) *)
          end.

        Definition formula_to_nraenv := formula_to_nraenv_gen query_to_nraenv.

      End Translation.

      Section Lemmas.
        
        Lemma nraenv_eval_query_id_eq :
          forall br i q aenv,
          forall (reg id1 id2:data),
            nraenv_eval
              br (rec_sort (instance_to_bindings i))
              (query_to_nraenv aenv q) reg id1 =
            nraenv_eval
              br (rec_sort (instance_to_bindings i))
              (query_to_nraenv aenv q) reg id2.
        Proof.
          intros br i q.
          unfold nraenv_eval.
          set (n := (Tree.tree_size (tree_of_query q))).
          assert (Hn := le_n n);unfold n at 1 in Hn.
          clearbody n; revert q Hn.
          induction n as [ | n];intros q Hn env;
            [destruct q;inversion Hn| intros reg id1 id2].
          destruct q as [ | | | | | | ].
          - apply eq_refl.
          - apply eq_refl.
          - simpl.
            apply AuxDatacert.query_size_Set in Hn.
            assert (IHq1 := IHn _ (proj1 Hn) env reg id1 id2).
            assert (IHq2 := IHn _ (proj2 Hn) env reg id2 id1).
            destruct Fset.equal.
            destruct s;
              simpl;unfold nraenv_eval in IHq1,IHq2;unfold Lift.olift2;
                simpl foreign_runtime_data in *;
                simpl in IHq1,IHq2;rewrite IHq2,IHq1;apply eq_refl.
            apply eq_refl.
          - simpl.
            destruct Fset.is_empty;[|apply eq_refl].
            apply query_size_Join in Hn.
            assert (IHq1 := IHn _ (proj1 Hn) env reg id1 id2).
            assert (IHq2 := IHn _ (proj2 Hn)env reg id2 id1).
            simpl;unfold nraenv_eval in IHq1,IHq2.
            unfold olift.
            simpl foreign_runtime_data in *.
            simpl in IHq1,IHq2;rewrite IHq1.
            rewrite IHq2;apply eq_refl.
          - simpl;unfold lift2.
            apply query_size_Pi in Hn.
            assert (IHq1 := IHn _ Hn env reg id1 id2).
            simpl;unfold nraenv_eval in IHq1.
            simpl foreign_runtime_data in *.
            simpl in IHq1;rewrite IHq1;apply eq_refl.
          - simpl;unfold lift2.
            apply query_size_Sigma in Hn.
            assert (IHq1 := IHn _ (proj1 Hn) env reg id1 id2).
            simpl;unfold nraenv_eval in IHq1.
            simpl foreign_runtime_data in *.
            destruct s;simpl in IHq1;simpl;rewrite IHq1;apply eq_refl.
          - simpl.
            (* destruct well_formed_s;[|apply eq_refl]. *)
            destruct s;
              try
                (destruct (extract_grouping_criteria l) as [nl| ] eqn:D3;
              [|apply eq_refl];
              destruct Fset.subset eqn:Ds;[|apply eq_refl];
                apply query_size_Gamma in Hn;
                assert (IHq1 := IHn _ (proj1 Hn) env reg id1 id2);
                simpl;unfold nraenv_eval in IHq1;
                  simpl foreign_runtime_data in *;
                  simpl in IHq1;rewrite IHq1;apply eq_refl).
        Qed.

        Lemma nraenv_eval_formula_id_eq :
          forall br i f aenv,
          forall (reg id1 id2:data),
            nraenv_eval
              br (rec_sort (instance_to_bindings i))
              (formula_to_nraenv aenv f) reg id1 =
            nraenv_eval
              br (rec_sort (instance_to_bindings i))
              (formula_to_nraenv aenv f) reg id2.
        Proof.
          eapply nraenv_eval_formula_id_eq;auto.
          do 5 intro;apply nraenv_eval_query_id_eq.
        Qed.

        Lemma eval_sql_formula_eq :  
          forall i f env1 env2,
            equiv_env TRcd env1 env2 -> 
            eval_sql_formula (eval_query i) env1 f = eval_sql_formula (eval_query i) env2 f.
        Proof.
          intros i f env1 env2 He.
          refine
            (proj2 (SqlAlgebra.eval_query_eq_etc _ _ _ _ _ _ _)
                   _ (le_n _) _ _ He ).
          apply contains_nulls_eq.
        Qed.

        Lemma ws_instance_tuples :
          forall i env (q:query ),
            well_sorted_instance i = true ->
            forall t , t inBE (eval_query i env  q) ->
                  labels t =S= sort basesort q.
        Proof.
          intros i env q Hi.
          unfold well_sorted_instance in Hi.
          rewrite andb_true_iff in Hi.
          destruct Hi as [Hi0 Hi].
          rewrite forallb_forall in Hi.
          induction q;intros t Ht.
          - cbn in *.
            assert
              (A1 :=
                 Febag.elements_singleton
                   (Fecol.CBag (Tuple.CTuple TRcd))
                   (Tuple.empty_tuple _)).
            destruct A1 as [t' [A11 A12]].
            rewrite Febag.mem_unfold in Ht.
            rewrite A12 in Ht;simpl in Ht.
            rewrite Bool.orb_false_r in Ht.
            rewrite Oeset.eq_bool_true_compare_eq in Ht.
            apply Oeset.compare_eq_sym in A11.
            assert (A2 := Oeset.compare_eq_trans _ _ _ _ Ht A11).
            apply Tuple.tuple_eq_labels in A2.
            rewrite <- (Fset.equal_eq_2
                         _ _ _ _ (Tuple.labels_empty_tuple _ )).
            apply A2.
          - simpl in Ht.
            destruct (Oset.find ORN r i) eqn:D1
            ;[|rewrite Febag.mem_unfold,
               Febag.elements_empty in Ht;
               inversion Ht].  
            apply Oset.find_some in D1.
            rewrite Febag.mem_unfold in Ht.
            rewrite Oeset.mem_bool_true_iff in Ht.
            destruct Ht as [t' [Heq Ht']].            
            assert (Hi := Hi (r,b) D1).
            rewrite forallb_forall in Hi.
            assert (Hi := Hi _ Ht').
            rewrite (Fset.equal_eq_1
                       _ _ _ _
                       (tuple_eq_labels _ _ _ Heq)).
            apply Hi.
          - simpl in Ht.
            destruct
              (sort basesort q1
               =S?= sort basesort q2) eqn:D1;
              [simpl|rewrite Febag.mem_unfold,
                     Febag.elements_empty in Ht;
                     inversion Ht].
            destruct s.
            + simpl in Ht.
              rewrite Febag.mem_union in Ht.
              rewrite Bool.Bool.orb_true_iff in Ht.
              destruct Ht as [Ht|Ht];
                [apply (IHq1 t Ht)|
                 rewrite (Fset.equal_eq_2 _ _ _ _ D1);
                 apply (IHq2 t Ht)].
            + simpl in Ht.
              rewrite Febag.mem_union_max in Ht.
              rewrite Bool.Bool.orb_true_iff in Ht.
              destruct Ht as [Ht|Ht];
                [apply (IHq1 t Ht)|
                 rewrite (Fset.equal_eq_2 _ _ _ _ D1)
                 ;apply (IHq2 t Ht)].
            + simpl in Ht.
              rewrite Febag.mem_inter in Ht.
              rewrite Bool.Bool.andb_true_iff in Ht.
              destruct Ht as [Ht _].
              apply (IHq1 t Ht).
            + simpl in Ht.
              apply Febag.diff_spec_weak in Ht.
              apply (IHq1 _ Ht).
          - simpl in Ht.
            rewrite Febag.mem_mk_bag in Ht.
            rewrite Oeset.mem_bool_true_iff in Ht.
            destruct Ht as [t' [Ht'1 Ht'2]].
            unfold natural_join_list,Join.theta_join_list in Ht'2.
            rewrite in_flat_map in Ht'2.
            destruct Ht'2 as [t'' [Ht''1 Ht''2]].
            apply Tuple.tuple_eq_labels in Ht'1.
            rewrite (Fset.equal_eq_1 _ _ _ _ Ht'1).
            simpl.
            rewrite Join.d_join_list_unfold in Ht''2.
            rewrite in_map_iff in Ht''2.
            destruct Ht''2 as [x [Hx1 Hx2]].
            assert (A1: t' =t= join_tuple  TRcd t'' x )
              by (subst;apply Oeset.compare_eq_refl).
            unfold join_tuple in A1.
            rewrite (Fset.equal_eq_1 _ _ _ _ (tuple_eq_labels _ _ _ A1)).
            apply Febag.in_elements_mem in Ht''1.
            assert (H1 := IHq1 _ Ht''1 );clear Ht''1 IHq1 Ht'1 Hx1.
            rewrite filter_In in Hx2.
            destruct Hx2 as [Hx2 _].
            apply Febag.in_elements_mem in Hx2.
            assert (H2 := IHq2 _ Hx2 );clear A1 Hx2 IHq2.
            rewrite Fset.equal_spec in *.
            intro e .
            rewrite (Fset.mem_eq_2 _ _ _ (labels_mk_tuple _ _ _ )).   
            rewrite 2 Fset.mem_union,<- H1,<- H2.
            apply eq_refl.
          - simpl in Ht.
            rewrite Febag.mem_map in Ht.
            destruct Ht as [t' [Ht'1 Ht'2]].
            apply Febag.in_elements_mem in Ht'2.
            apply Tuple.tuple_eq_labels in Ht'1.
            rewrite (Fset.equal_eq_1 _ _ _ _ Ht'1).
            rewrite (Fset.equal_eq_1 _ _ _ _ (Tuple.labels_mk_tuple _ _ _ )).
            destruct _s;simpl.
            apply Fset.equal_refl.
          - simpl in Ht.
            rewrite Febag.mem_filter in Ht.
            rewrite Bool.Bool.andb_true_iff in Ht.
            destruct Ht as [Ht _].
            apply (IHq _ Ht).
            intros x1 x2 Hx Hx'.
            refine (t_env_equiv _ _ _ _ _ _ _ q _ _ _ _ Hx');
              [apply contains_nulls_eq| ].
            rewrite Febag.mem_nb_occ in Hx.
            destruct ((Febag.nb_occ _ x1 _ ?= 0)%N) eqn:D1;
              try discriminate.
            rewrite N.compare_gt_iff in D1.
            rewrite N.ge_le_iff.
            rewrite <- N.le_succ_l in D1.
            apply D1.
          - rewrite eval_query_unfold in Ht.
            rewrite Febag.mem_mk_bag in Ht.
            rewrite Oeset.mem_bool_true_iff in Ht.
            destruct Ht as [u [Hu1 Hu2]].
            rewrite in_map_iff in Hu2.
            destruct Hu2 as [v [Hv1 Hv2]].
            rewrite filter_In in Hv2.
            destruct Hv2 as [Hv2 Hv3].
            rewrite <- Hv1 in Hu1.
            apply Tuple.tuple_eq_labels in Hu1.
            rewrite (Fset.equal_eq_1 _ _ _ _ Hu1).
            simpl.
            rewrite (Fset.equal_eq_1 _ _ _ _ (Tuple.labels_mk_tuple _ _ _ )).
            destruct _s.
            apply Fset.equal_refl.
        Qed.

        Fixpoint is_complete_instance_q (i:list (relname * (Febag.bag (Fecol.CBag (CTuple TRcd))))) q :=
          match q with
          | Q_Empty_Tuple => true
          | Q_Table r =>
            Oset.mem_bool ORN r (map fst i)
          | Q_Set _ q1 q2 => is_complete_instance_q i q1 && is_complete_instance_q i q2
          | Q_NaturalJoin q1 q2 => is_complete_instance_q i q1 && is_complete_instance_q i q2
          | Q_Pi (_Select_List _ s) q1 => is_complete_instance_q i q1
          | Q_Sigma f q0 => is_complete_instance_q i q0 && is_complete_instance_f is_complete_instance_q i f
          | Q_Gamma (_Select_List _ s) g f q0 =>
            is_complete_instance_q i q0
            && is_complete_instance_f is_complete_instance_q i f
          end.

        Fixpoint is_translatable_q q :=
          match q with
          | Q_Empty_Tuple => true
          | Q_Table r => true
          | Q_Set _ q1 q2 => is_translatable_q q1 && is_translatable_q q2
          | Q_NaturalJoin q1 q2 =>
            Fset.is_empty A (sort basesort q1 interS sort basesort q2) &&
                          is_translatable_q q1 && is_translatable_q q2
          | Q_Pi (_Select_List _ s) q1 =>
            is_translatable_q
              q1
              &&
              forallb
              (fun x =>
                 match x with
                 | Select_As _ ag a =>
                   Oset.eq_bool
                     OType (type_of_aggterm ag)
                     (type_of_attribute TRcd a)
                 end)
              s
              &&
              Oset.all_diff_bool OAtt (map fst (map (pair_of_select TRcd) s))
          | Q_Sigma f q0 => is_translatable_q q0 && is_translatable_f is_translatable_q f
          | Q_Gamma (_Select_List _ s) g f q0 =>
            match extract_grouping_criteria g with
            | Some gc =>
              (Fset.mk_set A gc subS? sort basesort q0)
              &&
              is_translatable_q
                q0
                && is_translatable_f is_translatable_q f &&
                forallb
                (fun x =>
                   match x with
                   | Select_As _ ag a =>
                     Oset.eq_bool OType (type_of_aggterm ag) (type_of_attribute TRcd a)
                   end)
                s
                &&
                Oset.all_diff_bool OAtt (map fst (map (pair_of_select TRcd) s))
            | None => false
            end
          end.
        
        Lemma well_typed_instance_queries :
          forall i e q,
            well_typed_e e = true ->
            well_typed_instance i = true ->
            is_complete_instance_q i q = true ->
            is_translatable_q q = true ->
            well_typed_list_tuple (Febag.elements _ ((eval_query i) e q)) = true.
        Proof.
          do 3 intro.
          induction q;intros (* H0  *)H1 H2 H33 H3.
          - unfold well_typed_e,well_typed_instance in *.
            unfold well_typed_slice in *.
            unfold well_typed_list_tuple in *.
            unfold well_typed_attribute in *.
            repeat rewrite forallb_forall in *.
            intros.
            simpl in H.
            assert (G1 := Febag.elements_singleton BTupleT (empty_tuple TRcd)).
            destruct G1 as [t' [G1 G2]].
            rewrite G2 in H.
            destruct H as [H|H];[|destruct H].
            subst.
            rewrite forallb_forall.
            intros.
            rewrite <- (Fset.elements_spec1 _ _ _ (tuple_eq_labels _ _ _ G1)) in H.
            rewrite (Fset.elements_spec1 _ _ _ (labels_empty_tuple _ )) in H.
            rewrite Fset.elements_empty in H;inversion H.
          - unfold well_typed_e,well_typed_instance in *.
            unfold well_typed_slice in *.
            unfold well_typed_list_tuple in *.
            unfold well_typed_attribute in *.
            repeat rewrite forallb_forall in *.
            intros.
            simpl in H.
            destruct Oset.find eqn:D1.
            + assert
                (A1:
                   In
                     (Febag.elements BTupleT b)
                     (map (fun x0 : relname * bagT =>
                             Febag.elements BTupleT (snd x0)) i)).
              { rewrite in_map_iff.
                exists (r,b).
                split;[apply eq_refl| ].
                apply (Oset.find_some _ _ _ D1).
              }
              apply H2 in A1.
              rewrite forallb_forall in A1.
              apply A1 in H.
              apply H.
            + rewrite Febag.elements_empty in H;inversion H.
          - rewrite eval_query_unfold.
            destruct Fset.equal eqn:D1.
            + simpl in (* H0, *)H3, H33.
              rewrite andb_true_iff in (* H0, *)H3, H33.
              assert (IH1 := IHq1 (* (proj1 H0) *) H1 H2 (proj1 H33) (proj1 H3)).
              assert (IH2 := IHq2 (* (proj2 H0) *) H1 H2 (proj2 H33) (proj2 H3)).
              clear IHq1 IHq2 H1 H2.
              unfold well_typed_e,well_typed_instance in *.
              unfold well_typed_slice in *.
              unfold well_typed_list_tuple in *.
              unfold well_typed_attribute in *.
              repeat rewrite forallb_forall in *.
              intros x Hx.
              apply Febag.in_elements_mem in Hx.
              destruct s;simpl in Hx.
              * rewrite Febag.mem_union,orb_true_iff in Hx.
                destruct Hx as [Hx|Hx];rewrite Febag.mem_unfold in Hx.
                -- rewrite Oeset.mem_bool_true_iff in Hx.
                   destruct Hx as [x' [Hx1 Hx2]].       
                   assert (IH1 := IH1   _  Hx2).
                   rewrite forallb_forall in IH1.
                   rewrite forallb_forall.
                   intros.
                   rewrite (tuple_eq_dot_alt _ _ _ Hx1 _ (Fset.in_elements_mem _ _ _ H)).
                   rewrite (Fset.elements_spec1 _ _ _ (tuple_eq_labels _ _ _ Hx1)) in H.       
                   apply (IH1 _ H).
                -- rewrite Oeset.mem_bool_true_iff in Hx.
                   destruct Hx as [x' [Hx1 Hx2]].       
                   assert (IH2 := IH2 _ Hx2).
                   rewrite forallb_forall in IH2.
                   rewrite forallb_forall.
                   intros.
                   rewrite (tuple_eq_dot_alt _ _ _ Hx1 _ (Fset.in_elements_mem _ _ _ H)).
                   rewrite (Fset.elements_spec1 _ _ _ (tuple_eq_labels _ _ _ Hx1)) in H.       
                   apply (IH2 _ H).
              * rewrite Febag.mem_union_max,orb_true_iff in Hx.
                destruct Hx as [Hx|Hx];rewrite Febag.mem_unfold in Hx.
                -- rewrite Oeset.mem_bool_true_iff in Hx.
                   destruct Hx as [x' [Hx1 Hx2]].       
                   assert (IH1 := IH1 _ Hx2).
                   rewrite forallb_forall in IH1.
                   rewrite forallb_forall.
                   intros.
                   rewrite (tuple_eq_dot_alt _ _ _ Hx1 _ (Fset.in_elements_mem _ _ _ H)).
                   rewrite (Fset.elements_spec1 _ _ _ (tuple_eq_labels _ _ _ Hx1)) in H.       
                   apply (IH1 _ H).
                -- rewrite Oeset.mem_bool_true_iff in Hx.
                   destruct Hx as [x' [Hx1 Hx2]].       
                   assert (IH2 := IH2 _ Hx2).
                   rewrite forallb_forall in IH2.
                   rewrite forallb_forall.
                   intros.
                   rewrite (tuple_eq_dot_alt _ _ _ Hx1 _ (Fset.in_elements_mem _ _ _ H)).
                   rewrite (Fset.elements_spec1 _ _ _ (tuple_eq_labels _ _ _ Hx1)) in H.       
                   apply (IH2 _ H).
              *  rewrite Febag.mem_inter,andb_true_iff in Hx.
                 destruct Hx as [Hx Hxx];rewrite Febag.mem_unfold in Hx.
                 rewrite Oeset.mem_bool_true_iff in Hx.
                 destruct Hx as [x' [Hx1 Hx2]].       
                 assert (IH1 := IH1 _ Hx2).
                 rewrite forallb_forall in IH1.
                 rewrite forallb_forall.
                 intros.
                 rewrite (tuple_eq_dot_alt _ _ _ Hx1 _ (Fset.in_elements_mem _ _ _ H)).
                 rewrite (Fset.elements_spec1 _ _ _ (tuple_eq_labels _ _ _ Hx1)) in H.       
                 apply (IH1 _ H).
              * apply Febag.diff_spec_weak in Hx.
                rewrite Febag.mem_unfold in Hx.
                rewrite Oeset.mem_bool_true_iff in Hx.
                destruct Hx as [x' [Hx1 Hx2]].       
                assert (IH1 := IH1 _ Hx2).
                rewrite forallb_forall in IH1.
                rewrite forallb_forall.
                intros.
                rewrite (tuple_eq_dot_alt _ _ _ Hx1 _ (Fset.in_elements_mem _ _ _ H)).
                rewrite (Fset.elements_spec1 _ _ _ (tuple_eq_labels _ _ _ Hx1)) in H.       
                apply (IH1 _ H).        
            + rewrite Febag.elements_empty;apply eq_refl.
          - rewrite eval_query_unfold.
            simpl in (* H0, *)H3, H33.
            rewrite andb_true_iff in (* H0, *)H3, H33.
            rewrite andb_true_iff in (* H0, *)H3.
            assert (IH1 := IHq1 (* (proj1 H0) *) H1 H2 (proj1 H33) (proj2 (proj1 H3))).
            assert (IH2 := IHq2 (* (proj2 H0) *) H1 H2 (proj2 H33) (proj2 H3)).
            clear IHq1 IHq2 H1 H2.
            unfold well_typed_e,well_typed_instance in *.
            unfold well_typed_slice in *.
            unfold well_typed_list_tuple in *.
            unfold well_typed_attribute in *.
            repeat rewrite forallb_forall in *.
            intros x Hx.
            apply Febag.in_elements_mem in Hx.
            rewrite Febag.mem_mk_bag in Hx.
            rewrite Oeset.mem_bool_true_iff in Hx.
            destruct Hx as [x1 [Heq1 Hx1]].
            rewrite in_natural_join_list in Hx1.
            destruct Hx1 as [x2 [x3 [Hx2 [Hx3 [Hj ->]]]]].
            rewrite forallb_forall.
            intros a Ha.
            rewrite (tuple_eq_dot_alt _ _ _ Heq1 _ (Fset.in_elements_mem _ _ _ Ha)).
            rewrite (Fset.elements_spec1 _ _ _ (tuple_eq_labels _ _ _ Heq1)) in Ha.
            rewrite join_tuple_unfold in *.
            rewrite (Fset.elements_spec1 _ _ _ (labels_mk_tuple _ _ _ )) in Ha.
            rewrite dot_mk_tuple.   
            apply Fset.in_elements_mem in Ha.
            rewrite Ha.
            rewrite Fset.mem_union,orb_true_iff in Ha.
            destruct Ha as [Ha|Ha].
            + rewrite Ha.
              assert (IH1 := IH1 _ Hx2).
              rewrite forallb_forall in IH1.
              apply IH1.
              apply Fset.mem_in_elements.
              apply Ha.
            + destruct (Fset.mem _ _ (_ x2)) eqn:D1.
              * assert (IH1 := IH1 _ Hx2).
                rewrite forallb_forall in IH1.
                apply IH1.
                apply Fset.mem_in_elements.
                apply D1.
              * assert (IH2 := IH2 _ Hx3).
                rewrite forallb_forall in IH2.
                apply IH2.
                apply Fset.mem_in_elements.
                apply Ha.
          - rewrite eval_query_unfold.
            destruct _s.
            simpl in (* H0, *)H3, H33.
            repeat rewrite andb_true_iff in H0.
            repeat rewrite andb_true_iff in H3.            
            repeat rewrite andb_true_iff in H33.
            assert (IH1 := IHq (* (proj1 (proj1 (proj1 H0))) *) H1 H2 H33 (proj1 (proj1 H3))).
            clear IHq H2.
            unfold well_typed_e,well_typed_instance.
            unfold well_typed_slice.
            unfold well_typed_list_tuple.
            unfold well_typed_attribute.
            repeat rewrite forallb_forall.
            unfold well_typed_e,well_typed_instance in IH1.
            unfold well_typed_slice in IH1.
            unfold well_typed_list_tuple in IH1.
            unfold well_typed_attribute in IH1.
            repeat rewrite forallb_forall in IH1.
            intros x Hx.
            apply Febag.in_elements_mem in Hx.
            rewrite Febag.mem_map in Hx.
            destruct Hx as [x' [Hxx' Hx']].
            assert (IH1 := IH1 _ Hx').
            rewrite forallb_forall in IH1.
            rewrite forallb_forall.
            intros a Ha.
            rewrite (tuple_eq_dot_alt _ _ _ Hxx' _ (Fset.in_elements_mem _ _ _ Ha)).    
            rewrite (Fset.elements_spec1 _ _ _ (tuple_eq_labels _ _ _ Hxx')) in Ha.
            rewrite 
              (Fset.elements_spec1 _ _ _ (labels_projection _ _ _)) in Ha.
            apply Fset.in_elements_mem in Ha.
            rewrite Fset.mem_mk_set in Ha.
            rewrite Oset.mem_bool_true_iff in Ha.
            rewrite in_map_iff in Ha.
            destruct Ha as [[agt a1] [-> Ha]].
            rewrite (dot_projection _ (env_t TRcd e x') _ _ agt Ha).
            + rewrite type_of_aggterm_eq.
              * destruct H3 as [[H3 H2] H4].
                rewrite forallb_forall in H2.
                apply (H2 _ Ha).
              * simpl;rewrite H1.
                rewrite andb_true_r.
                cbn.
                rewrite andb_true_r.
                rewrite forallb_forall.
                apply IH1.
            + rewrite <- Oset.all_diff_bool_ok in H3.
              apply H3.
          - rewrite eval_query_unfold.
            simpl in (* H0, *)H3, H33.
            rewrite andb_true_iff in H3, H33.
            assert (IH1 := IHq (* (proj1 H0) *) H1 H2 (proj1 H33) (proj1 H3)).
            clear IHq H1 H2.
            unfold well_typed_e,well_typed_instance in *.
            unfold well_typed_slice in *.
            unfold well_typed_list_tuple in *.
            unfold well_typed_attribute in *.
            repeat rewrite forallb_forall in *.
            intros x Hx.
            apply Febag.in_elements_mem in Hx.
            rewrite Febag.mem_filter in Hx.
            + rewrite andb_true_iff in Hx.
              destruct Hx as [Hx Hx'].
              rewrite Febag.mem_unfold in Hx.
              rewrite Oeset.mem_bool_true_iff in Hx.
              destruct Hx as [x' [Hx1 Hx2]].       
              assert (IH1 := IH1 _ Hx2).
              rewrite forallb_forall in IH1.
              rewrite forallb_forall.
              intros.
              rewrite (tuple_eq_dot_alt _ _ _ Hx1 _ (Fset.in_elements_mem _ _ _ H)).
              rewrite (Fset.elements_spec1 _ _ _ (tuple_eq_labels _ _ _ Hx1)) in H.       
              apply (IH1 _ H).
            + intros.
              apply f_equal.
              apply eval_sql_formula_eq.
              apply (equiv_env_t_eq _ _ _ H0).
          - rewrite eval_query_unfold.
            simpl in (* H0, *)H3, H33.
            destruct _s.
            destruct extract_grouping_criteria eqn:D1;[|discriminate].
            repeat rewrite andb_true_iff in H0.
            repeat rewrite andb_true_iff in H3.            
            repeat rewrite andb_true_iff in H33.
            assert (IH1 := IHq (* (proj1 (proj1 (proj1 H0))) *) H1 H2 (proj1 H33) (proj2 (proj1 (proj1 (proj1 H3))))).
            clear IHq H2.
            unfold well_typed_e,well_typed_instance.
            unfold well_typed_slice.
            unfold well_typed_list_tuple.
            unfold well_typed_attribute.
            repeat rewrite forallb_forall.
            unfold well_typed_e,well_typed_instance in IH1.
            unfold well_typed_slice in IH1.
            unfold well_typed_list_tuple in IH1.
            unfold well_typed_attribute in IH1.
            repeat rewrite forallb_forall in IH1.
            intros x Hx.
            apply Febag.in_elements_mem in Hx.
            rewrite Febag.mem_mk_bag in Hx.
            rewrite Oeset.mem_bool_true_iff in Hx.
            destruct Hx as [x1 [Heq1 Hx1]].
            apply in_map_iff in Hx1.
            destruct Hx1 as [lt1 [Heq2 Hlt1]].
            apply filter_In in Hlt1.
            destruct Hlt1 as [Hlt1 Hb1].
            unfold make_groups in Hlt1.
            apply in_map_iff in Hlt1.
            destruct Hlt1 as [[lv2 lt2] [Heq Hlvt]].
            simpl in Heq;subst.
            rewrite forallb_forall.
            intros a Ha.
            rewrite (tuple_eq_dot_alt _ _ _ Heq1 _ (Fset.in_elements_mem _ _ _ Ha)).    
            rewrite (Fset.elements_spec1 _ _ _ (tuple_eq_labels _ _ _ Heq1)) in Ha.            
            rewrite 
              (Fset.elements_spec1 _ _ _ (labels_projection _ _ _)) in Ha.
            apply Fset.in_elements_mem in Ha.
            rewrite Fset.mem_mk_set in Ha.
            rewrite Oset.mem_bool_true_iff in Ha.
            rewrite in_map_iff in Ha.
            destruct Ha as [[agt a1] [-> Ha]].
            rewrite (dot_projection _ (env_g TRcd e (Group_By TRcd l) lt1) _ _ agt Ha).
            + rewrite type_of_aggterm_eq.
              * destruct H3 as [[H3 H2] H4].
                rewrite forallb_forall in H2.
                apply (H2 _ Ha).
              * simpl;rewrite H1.
                rewrite andb_true_r.
                cbn.
                rewrite forallb_forall.
                intros tx Htx.
                assert (X1 := Partition.in_partition _ _ _ _ _ _ Hlvt Htx).
                apply (IH1 _ X1).
            + rewrite <- Oset.all_diff_bool_ok in H3.
              apply H3.
        Qed.

        Lemma well_typed_e_env_t :
          forall t1 i q env,
            In (drec (tuple_as_dpairs t1)) (bagT_to_listD ((eval_query i) env q)) ->
            well_typed_e env = true ->
            well_typed_instance i = true ->
            is_complete_instance_q i q = true ->
            is_translatable_q q = true ->
            well_typed_e ((sort basesort q, Group_Fine TRcd, t1 :: nil) :: env) =
            true.
        Proof.
          do 4 intro.
          intros Ht Typ Ti Tc Tq.
          unfold well_typed_e in *;simpl.
          rewrite Typ.
          rewrite andb_true_r.
          unfold well_typed_slice.
          assert (G1 := well_typed_instance_queries _ _ _ Typ Ti Tc Tq).
          unfold well_typed_list_tuple in *.
          rewrite forallb_forall in G1.
          simpl snd.
          simpl.
          rewrite andb_true_r.
          rewrite <- inBE_tuple_as_pairs_bagT in Ht.
          rewrite Febag.mem_unfold in Ht.
          rewrite Oeset.mem_bool_true_iff in Ht.
          destruct Ht as [xt [Hx Ht]].
          unfold well_typed_attribute.
          rewrite forallb_forall.
          intros.
          rewrite (tuple_eq_dot_alt _ _ _ Hx _ (Fset.in_elements_mem _ _ _ H)).
          rewrite (Fset.elements_spec1 _ _ _ (tuple_eq_labels _ _ _ Hx)) in H.
          assert (G1 := G1 xt).
          rewrite forallb_forall in G1;apply (G1 Ht);apply H.
        Qed.

        Lemma well_typed_e_env_g :
          forall lt g i q env,
            (forall t,
                In (drec (tuple_as_dpairs t)) (listT_to_listD lt) ->
                In (drec (tuple_as_dpairs t)) (bagT_to_listD ((eval_query i) env q))) ->
            well_typed_e env = true ->
            well_typed_instance i = true ->
            is_complete_instance_q i q = true ->
            is_translatable_q q = true ->
            well_typed_e ((sort basesort q, Group_By TRcd g, lt) :: env) =
            true.
        Proof.
          do 5 intro.
          intros Hlt Typ Ti Tc Tq.
          unfold well_typed_e in *;simpl.
          rewrite Typ.
          rewrite andb_true_r.
          unfold well_typed_slice.
          assert (G1 := well_typed_instance_queries _ _ _ Typ Ti Tc Tq).
          unfold well_typed_list_tuple in *.
          rewrite forallb_forall in G1.
          simpl snd.
          rewrite forallb_forall.
          intros t Ht.
          apply In_tuple_as_pairs_eq_listT in Ht.
          apply Hlt in Ht.
          rewrite  <- inBE_tuple_as_pairs_bagT in Ht.
          rewrite Febag.mem_unfold in Ht.
          rewrite Oeset.mem_bool_true_iff in Ht.
          destruct Ht as [xt [Hx Ht]].
          unfold well_typed_attribute.
          rewrite forallb_forall.
          intros.
          rewrite (tuple_eq_dot_alt _ _ _ Hx _ (Fset.in_elements_mem _ _ _ H)).
          rewrite (Fset.elements_spec1 _ _ _ (tuple_eq_labels _ _ _ Hx)) in H.
          assert (G1 := G1 xt).
          rewrite forallb_forall in G1;apply (G1 Ht);apply H.
        Qed.
        
        Lemma query_to_nraenv_Pi_is_sound :
          forall br i env q1 dq sl l,
            well_sorted_instance i = true ->
            well_typed_instance i =true ->
            well_typed_e env = true ->
            well_formed_e TRcd env = true ->
            is_a_translatable_e env = true ->
            is_complete_instance_q i q1 = true ->
            is_translatable_q q1 = true ->
            (* well_typed_s sl = true -> *)
            Fset.is_empty A (sort basesort q1
                                  interS
                                  Fset.Union A (map fst (map fst env))) = true ->
            well_formed_s
              TRcd (env_t
                      TRcd env
                      (default_tuple TRcd (sort basesort q1))) sl = true ->
            lift_map
              (fun din : data =>
                 nraenv_core_eval
                   br (rec_sort (instance_to_bindings i))
                   (nraenv_to_nraenv_core
                      (select_list_to_nraenv
                         ((sort basesort q1, Group_Fine TRcd ) :: map fst env)
                         (_Select_List _ sl)))
                   (drec
                      (("slc"%string, dcoll (din :: nil))
                         :: ("tl"%string, env_to_data env) :: nil)) din) l = 
            Some dq ->
            Permutation (bagT_to_listD ((eval_query i) env q1)) l ->
            Permutation
              (bagT_to_listD
                 (Febag.map BTupleT BTupleT
                            (fun t : tuple =>
                               mk_tuple
                                 (Fset.mk_set
                                    A (map
                                         fst
                                         (map
                                            (fun x : select TRcd =>
                                               match x with
                                               | Select_As _ e a => (a, e)
                                               end) sl)))
                                 (fun a : attribute =>
                                    match
                                      Oset.find
                                        OAtt a
                                        (map
                                           (fun x : select TRcd =>
                                              match x with
                                              | Select_As _ e a0 => (a0, e)
                                              end) sl)
                                    with
                                    | Some e =>
                                      interp_aggterm
                                        TRcd (env_t TRcd env t) e
                                    | None =>
                                      dot
                                        (default_tuple
                                           TRcd (emptysetS)) a
                                    end)) ((eval_query i) env q1))) dq.
        Proof.
          intros br i env q dq sl l WI Ti Typ WS Wt Tc Tq Aux Wsl Hl Hq.
          refine (permutation_bagT_lift_map _ _ _ _ Hl Hq).
          intros t1 Ht.
          apply eq_sym.  
          assert
            (Aux2 :
               well_formed_e
                 _ ((sort basesort q, Group_Fine _ , t1 :: nil)
                      :: env) = true)
            by
              refine
                (well_formed_e_env_t
                   _ _ _ Aux
                   (ws_instance_tuples _
                      env q WI t1 (Febag.in_elements_mem _ _ _ Ht))
                   WS).
          apply Febag.in_elements_mem in Ht.
          rewrite inBE_tuple_as_pairs_bagT in Ht.
          assert (Hd1 := Permutation_in _ Hq Ht).
          assert (Hd := (lift_map_some_in_1  _ _ Hl _ Hd1)).
          destruct Hd as [b [_ Hd]].
          assert
            (A1 :=
               select_list_eval_is_drec
                 _ _ _ ((sort basesort q, Group_Fine _ , t1 :: nil)
                          :: env) _ Hd).
          destruct A1;subst.
          assert
            (A2 :
               well_formed_s
                 _ ((sort basesort q, Group_Fine _, t1 :: nil)
                      :: env) sl = true)
            by apply (well_formed_s_Group_Fine _ _ _ _ Wsl).
          assert
            (A3 :
               is_a_translatable_e
                 ((sort basesort q, Group_Fine _, t1 :: nil)
                    :: env) = true)
            by apply (is_a_translatable_e_Group_Fine _ _ _ Wt).
          assert
            (A1 :=
               select_list_to_nraenv_is_sound
                 br (rec_sort (instance_to_bindings i)) _
                 (drec (tuple_as_dpairs t1))
                 (well_typed_e_env_t _ _ _ _ Ht Typ Ti Tc Tq)
                 Aux2 A3 _ A2).
          unfold nraenv_eval in A1.
          simpl in A1.
          refine (eq_trans A1 _).
          do 2 apply f_equal.
          apply tuple_as_dpairs_eq.
          apply Tuple.mk_tuple_eq;[apply Fset.mk_set_idem| ].
          intros a H1 H2.
          destruct Oset.find;[|apply eq_refl].
          apply eq_sym.
          apply interp_aggterm_eq.
          clear A2.
          assert (A2 := ws_instance_tuples  _ env q WI t1).
          constructor;[repeat split| ].
          apply A2.
          rewrite inBE_tuple_as_pairs_bagT.
          apply Ht.
          apply Oeset.permut_refl.
          apply Forall2_refl.
          unfold RelationClasses.Reflexive.
          unfold equiv_env_slice.
          intros [ [ ]];repeat split.
          apply Fset.equal_refl.
          apply Oeset.permut_refl.
        Qed.
        
        Lemma group_by_translation_is_sound_permut_permut_aggterm :
          forall g br i q la env did dq dg,
            (forall t : tuple,
                Oeset.mem_bool
                  OTuple t
                  (Febag.elements
                     BTupleT ((eval_query i) env q)) = true ->
                Fset.mk_set A la subS labels t) ->
            extract_grouping_criteria g = Some la ->
            nraenv_eval
              br (rec_sort (instance_to_bindings i))
              (query_to_nraenv (map fst env) q)
              (env_to_data env) did = Some (dcoll dq) ->
            Permutation (bagT_to_listD (eval_query i env q )) dq ->
            nraenv_eval
              br (rec_sort (instance_to_bindings i))
              (NRAEnvMap
                 (NRAEnvUnop
                    (OpDot
                       (find_fresh_string
                          (map attribute_to_string la))) NRAEnvID)
                 (NRAEnvGroupBy
                    (find_fresh_string
                       (map attribute_to_string la))
                    (map attribute_to_string la)
                    (query_to_nraenv (map fst env) q)))
              (env_to_data env) did = Some (dcoll dg) ->
            ListPermut._permut
              (Permutation (A := data))
              (map
                 (fun x => (listT_to_listD x))
                 (map
                    snd
                    (Partition.partition
                       (mk_olists (OVal _))                         
                       (fun t : tuple =>
                          map
                            (fun f0 : aggterm =>
                               interp_aggterm _ (env_t _ env t) f0) g)
                       (Febag.elements _ (eval_query i env q )))))
              (map (fun d => match d with dcoll ld => ld | _ => nil end) dg).
        Proof.
          unfold nraenv_eval.
          intros g br i q la env did dq dg.
          simpl.
          intros Aux Hg Hq  Hdq Hdg.
          unfold olift in Hdg.
          rewrite Hq in Hdg.
          simpl in Hdg.
          destruct lift_map as [ldq| ] eqn:D1 in Hdg;[|discriminate].
          simpl in Hdg.
          destruct lift_map as [ldq2| ] eqn:D2 in Hdg;[|discriminate].
          simpl in Hdg.
          destruct lift_map as [ldq3| ] eqn:D3 in Hdg;[|discriminate].
          inversion Hdg;subst;clear Hdg.
          assert (A1 := extract_grouping_criteria_rproject_aggterm env _ _ Aux Hdq Hg D1).
          assert (A2 := Permutation_bdistinct _ A1).
          assert (C1 := Permutation_bdistinct_partition_projection_aggterm env g (Febag.elements BTupleT ((eval_query i) env q)) Hg ).
          assert (A5 := Permutation_sym (Permutation_trans (Permutation_sym A2) (Permutation_sym C1))).
          assert (A3 := Permutation_lift_map_weak _ _ _ ( data_option_elim_some _ _) D2 A5).
          assert (A4 := Permutation_lift_map_weak _ _ _ ( data_option_elim_some _ _) D3 A3).
          rewrite <- _permut_eq_Permutation in *.
          refine
            (ListPermut._permut_trans
               (fun a b c _ _ _ => @Permutation_trans _ a b c) _
               (ListPermut._permut_incl
                  _ _
                  (ListPermut._permut_map
                     (@Permutation _) (fun d : data =>
                                         match d with
                                         | dcoll ld => ld
                                         | _ => nil
                                         end) _ _ A4)));
            swap 1 3;[intros;subst;apply Permutation_refl|trivial|idtac].
          revert Hdq.
          unfold bagT_to_listD in *.
          assert (A7 := Partition.partition_permut 
                         (mk_olists (OVal _))
                         (fun t : tuple =>
                            map
                              (fun f0 : aggterm =>
                                 interp_aggterm _ (env_t _ env t) f0) g)
                         (Febag.elements BTupleT ((eval_query i) env q))).
          assert (A6 := NoDup_map_snd_partition 
                         (mk_olists (OVal _))
                         (fun t : tuple =>
                            map
                              (fun f0 : aggterm =>
                                 interp_aggterm _ (env_t _ env t) f0) g)
                         (Febag.elements BTupleT ((eval_query i) env q))).
          revert Hg D1 Aux A7 A6.
          intros.
          assert ( A9 : ListPermut._permut
                          eq
                          (listT_to_listD
                             (flat_map
                                (fun x  => snd x)
                                (Partition.partition
                                   (mk_olists (OVal _))
                                   (fun t : tuple =>
                                      map
                                        (fun f0 : aggterm =>
                                           interp_aggterm
                                             _ (env_t _ env t) f0) g)
                                   (Febag.elements
                                      BTupleT ((eval_query i) env q))))) dq).
          {
            refine (ListPermut._permut_trans _ _ Hdq);[intros;subst;trivial| ].
            rewrite _permut_eq_Permutation.
            apply Permutation_sym.
            apply Permutation_listT_eq.
            simple refine (ListPermut._permut_incl _ _ _);[exact eq|intros;subst | ].
            intros;subst;apply Oeset.compare_eq_refl.
            apply Partition.partition_permut.
          }
          repeat rewrite map_map.
          refine (@ListPermut._permut_map
                    _ _ _ _ eq _ _ _ _ _
                    _ (ListPermut._permut_refl _ _ _));
            [|intros;apply eq_refl].
          intros a lt' Ha _ Hab.
          subst.
          destruct lt' as [lv' lt'].
          unfold data_option_elim.
          unfold lift.
          assert (A13 := A9).
          rewrite _permut_eq_Permutation in A13.
          rewrite Permutation_listT_spec in A13.
          destruct A13 as [lt2 [A13 <-]].
          unfold edot at 1.
          simpl assoc_lookupr.
          destruct lt';
            [destruct
               (Partition.in_partition_diff_nil
                  _ _ _ _ Ha eq_refl)| ].
          destruct lift_filter eqn:D11;
            [| destruct (lift_filter_group_by_neq_None_aggterm
                           _ _ _ _ D11)].
          unfold edot.
          simpl snd.
          cbv iota beta.
          rewrite assoc_lookupr_drec_sort.
          rewrite (@assoc_lookupr_app
                     _ _ _ _
                     (find_fresh_string (map attribute_to_string la))
                     ODT_eqdec).
          simpl.
          destruct string_eqdec as [C2|N1];
            [|destruct (N1 eq_refl)].
          apply lift_filter_Some_filter in D11.
          subst l.
          unfold olift2.
          unfold edot.   
          assert (F1 :
                    exists (llt3:list (list tuple)),
                      Permutation
                        (map
                           snd
                           (Partition.partition
                              (mk_olists (OVal _))
                              (fun t : tuple =>
                                 map
                                   (fun f0 : aggterm =>
                                      interp_aggterm _ (env_t _ env t) f0) g)
                              (Febag.elements BTupleT ((eval_query i) env q))))
                        ((t::lt') :: llt3) /\ ~ In (t::lt') llt3).
          {
            revert Ha A6;clear.
            induction Partition.partition;intros;[inversion Ha| ].
            inversion A6;subst.
            destruct Ha.
            - exists (map snd l);subst a;split;[apply Permutation_refl|apply H1].
            - destruct (IHl H H2) as [llt3 H3].
              exists (snd a::llt3);split.
              + refine (Permutation_trans (perm_skip _ (proj1 H3)) (perm_swap _ _ _)).
              + intros [N1|N1];[rewrite N1 in *;destruct (H1 (in_map _ _ _ H))|destruct (proj2 H3 N1) ].
          }
          destruct F1 as [llt3 [F11 F12]].
          assert (F2:
                    (t::lt' ++
                      (flat_map
                         (fun x : list tuple => x) llt3)) =PE=
                    flat_map (fun x => x)
                             (map
                                snd (Partition.partition
                                       (mk_olists (OVal _))
                                       (fun t : tuple =>
                                          map (fun f0 : aggterm =>
                                                 interp_aggterm
                                                   _ (env_t _ env t) f0) g)
                                       (Febag.elements
                                          BTupleT ((eval_query i) env q))))).
          { revert Ha F11;clear.
            revert llt3 t lt'.
            induction Partition.partition;intros;[destruct Ha|simpl].
            destruct Ha;[subst| ].
            - apply Permutation.Permutation_cons_inv in F11.
              rewrite app_comm_cons.
              refine (_permut_app (_permut_refl _ _ _) _);
                [intros;apply Oeset.compare_eq_refl| ].
              refine (@_permut_incl _ _ eq _ _ _ _ _);
                [intros;subst;apply Oeset.compare_eq_refl| ].
              rewrite <- _permut_eq_Permutation in F11.
              apply _permut_flat_map_strong .
              apply F11.
            - rewrite <- _permut_eq_Permutation in F11.
              assert (A1 := _permut_flat_map_strong F11).
              simpl in A1.
              refine (@_permut_incl _ _ eq _ _ _ _ _);
                [intros;subst;apply Oeset.compare_eq_refl| ].
              apply A1. }
          assert (F3 : t :: lt' ++ flat_map (fun x => x) llt3 =PE= lt2).
          { refine (_permut_trans _ _ A13);
              [do 6 intro;apply Oeset.compare_eq_trans| ].
            repeat rewrite flat_map_concat_map in *.
            rewrite map_map in F2.
            apply F2. }
          clear F2.
          apply Permutation_listT_eq in F3.
          refine (Permutation_trans _ (Permutation_filter _ _ _ F3)).
          rewrite app_comm_cons.
          rewrite listT_to_listD_app_distr.
          rewrite filter_app.
          assert (G1 :  Fset.mk_set A la subS labels t).
          { apply Aux.
            rewrite (Oeset.mem_permut_mem_strong _ _ A7).
            rewrite (Oeset.mem_bool_flat_map
                       (Partition.VOLA OTuple (mk_olists (OVal _)))).
            rewrite existsb_exists.
            exists (lv', t :: lt');split;
              [apply Ha|
               simpl;rewrite Oeset.eq_bool_refl;apply eq_refl].
          }
          replace (filter _ (listT_to_listD (flat_map _ _)))
            with (@nil data);[|apply eq_sym].
          * rewrite app_nil_r.
            simpl listT_to_listD.
            { replace (drec (tuple_as_dpairs t) :: listT_to_listD lt')
                with (filter
                        (fun _ => true)
                        (drec (tuple_as_dpairs t) :: listT_to_listD lt')) at 1;swap 1 2.
              - simpl;apply f_equal.
                apply true_filter;intros;apply eq_refl.
              - refine (Permutation_filter_alt _ _ _ (Permutation_refl _)).
                intros x Hx.
                destruct Hx.
                + subst x.
                  unfold unbdata.
                  destruct data_eq_dec as [_ | N1];[apply eq_refl| ].
                  rewrite <- (rpoject_mk_tuple_aggterm env) in N1;[|apply G1].
                  exfalso;apply N1.
                  apply f_equal.
                  refine (eq_trans eq_refl (list_map_aggterm_tuple_as_dpairs_eq_rec_sort _ _ _ Hg )).
                + destruct (in_listT_is_drec _ _ H) as [x' ->].
                  unfold unbdata.
                  destruct data_eq_dec as [_ | N1];[apply eq_refl| ].
                  exfalso;apply N1.
                  apply f_equal.
                  refine (eq_trans _ (list_map_aggterm_tuple_as_dpairs_eq_rec_sort _ _ _ Hg )).
                  assert (G3 :  Fset.mk_set A la subS labels x').
                  { apply Aux.
                    rewrite (Oeset.mem_permut_mem_strong _ _ A7).
                    rewrite (Oeset.mem_bool_flat_map (Partition.VOLA OTuple (mk_olists (OVal _)))).
                    rewrite existsb_exists.
                    exists (lv', t :: lt');split;[apply Ha|apply mem_tuple_as_pairs_listT in H].
                    simpl;rewrite H,Bool.orb_true_r;
                      apply eq_refl. }       
                  rewrite <- (rpoject_mk_tuple_aggterm env);swap 1 2;[apply G3| ].
                  rewrite Fset.subset_spec in G3,G1.
                  apply tuple_as_dpairs_eq.
                  apply mk_tuple_eq_2.
                  intros a1 Ha1.
                  assert (G5 := Partition.in_map_snd_partition (mk_olists (OVal _))
                                                              (fun t : tuple => map (fun f0 : aggterm => interp_aggterm _ (env_t _ env t) f0) g)
                                                              (Febag.elements BTupleT ((eval_query i) env q)) (t::lt') t (in_map _ _ _ Ha) (or_introl eq_refl)).
                  (* assert (G5 := A11 (fun (t : tuple) => interp_funterm (env_t env t) (F_Dot _ _ a1)) (t :: lt') G4 Ha). *)
                  apply mem_tuple_as_pairs_listT in H.
                  rewrite Oeset.mem_bool_true_iff in H.
                  destruct H as [x'' [X1 X2]].
                  assert (G4 := Partition.partition_homogeneous_values (mk_olists (OVal _))
                                                                      (fun t : tuple => map (fun f0 : aggterm => interp_aggterm _ (env_t _ env t) f0) g)
                                                                      (Febag.elements BTupleT ((eval_query i) env q)) _ _ Ha x'' (or_intror X2)).      
                  assert (G6 := Partition.partition_homogeneous_values (mk_olists (OVal _))
                                                                      (fun t : tuple => map (fun f0 : aggterm => interp_aggterm _ (env_t _ env t) f0) g)
                                                                      (Febag.elements BTupleT ((eval_query i) env q)) _ _ Ha t (or_introl eq_refl)).
                  rewrite <- G6 in G4.
                  rewrite <- ListFacts.map_eq in G4.
                  rewrite <- G4.
                  apply interp_aggterm_eq.
                  apply (equiv_env_t_eq _ _ _ X1).
                  apply extract_grouping_criteria_some_split in Hg.
                  apply Hg.
                  rewrite Fset.mem_mk_set,Oset.mem_bool_true_iff in Ha1.
                  apply Ha1.
            }
          * apply ListFacts.filter_false.
            intros x Hx.
            destruct (in_listT_is_drec _ _ Hx) as [y ->].
            unfold unbdata.
            destruct data_eq_dec as [Y1| _];[|apply eq_refl].
            rewrite _permut_eq_Permutation in A9.
            assert (C4: In (drec (tuple_as_dpairs y)) (listT_to_listD (t :: lt' ++ (flat_map (fun x => x) llt3) ))).
            { rewrite app_comm_cons,listT_to_listD_app_distr.
              rewrite in_app_iff.
              right;apply Hx. }
            apply (Permutation_in _ F3) in C4.
            apply (Permutation_in _ (Permutation_sym A9)) in C4.
            apply mem_tuple_as_pairs_listT in C4.
            rewrite <- (rpoject_mk_tuple_aggterm env) in Y1;swap 1 2;
              [apply Aux;rewrite (Oeset.mem_permut_mem_strong _ _ A7);apply C4| ].   
            inversion Y1 as [Y2].
            apply eq_sym in Y2.
            apply (eq_trans (list_map_aggterm_tuple_as_dpairs_eq_rec_sort _ _ _ Hg )) in Y2.
            apply tuple_as_dpairs_eq in Y2.
            rewrite <- mem_tuple_as_pairs_listT in Hx.
            rewrite (Oeset.mem_bool_flat_map  ((Partition.OLA OTuple))) in Hx.
            rewrite existsb_exists in Hx.
            destruct Hx as [lt'' [C41 C42]].
            rewrite Oeset.mem_bool_true_iff in C42.
            destruct C42 as [y'' [C42 C43]].
            assert (G7 : In lt''
                            (map snd
                                 (Partition.partition (mk_olists (OVal _)) (fun t : tuple => map (fun f0 : aggterm => interp_aggterm _ (env_t _ env t) f0) g)
                                                      (Febag.elements BTupleT ((eval_query i) env q))))).
            { refine (Permutation_in _ (Permutation_sym F11) (or_intror C41)). }
            rewrite in_map_iff in G7.
            destruct G7 as [[lv1 lt1] [G7 G8]].
            simpl in G7;subst.
            assert (G9 :=
                      Partition.partition_homogeneous_values
                        (mk_olists (OVal _))
                        (fun t : tuple => map (fun f0 : aggterm => interp_aggterm _ (env_t _ env t) f0) g)
                        (Febag.elements BTupleT ((eval_query i) env q)) _ _ G8 _ C43).
            assert (G10 :=
                      Partition.partition_homogeneous_values
                        (mk_olists (OVal _))
                        (fun t : tuple => map (fun f0 : aggterm => interp_aggterm _ (env_t _ env t) f0) g)
                        (Febag.elements BTupleT ((eval_query i) env q)) _ _ Ha _ (or_introl eq_refl)).
            simpl in G9,G10.
            assert (B9 : map (fun f0 : aggterm => interp_aggterm _ (env_t _ env y'') f0) g = map (fun f0 : aggterm => interp_aggterm _ (env_t _ env t) f0) g).
            { apply map_ext_in.
              intros b Hb.
              destruct (extract_grouping_criteria_some_split_alt_2 _ Hg).
              apply H in Hb.
              destruct Hb as [x [Hb1 Hb2]].
              subst b.
              apply tuple_eq_dot with (a := x) in Y2.
              rewrite <- (Oset.mem_bool_true_iff OAtt),<- (Fset.mem_mk_set A) in Hb1.
              rewrite 2 (Fset.mem_eq_2 _ _ _ (labels_mk_tuple _ _ _ )) in Y2.
              rewrite Hb1 in Y2.
              rewrite dot_mk_tuple in Y2.
              rewrite Hb1 in Y2.
              rewrite Y2.
              rewrite dot_mk_tuple,Hb1.
              apply eq_sym.
              refine (interp_aggterm_eq _ (A_Expr (F_Dot x)) (env_t _ env y) _ _).
              apply env_t_eq_2;apply C42. }
            assert (C9 := B9).
            rewrite G9,G10 in C9.
            subst lv1.
            rewrite C9 in *.
            rewrite G10 in *.
            exfalso;apply F12.
            revert G8 Ha C41.
            clear.
            intros.
            assert
              (G1 :=
                 Partition.partition_all_diff_values
                   (mk_olists (OVal _))
                   (fun t : tuple =>
                      map
                        (fun f0 : aggterm =>
                           interp_aggterm _ (env_t _ env t) f0) g)
                   (Febag.elements BTupleT ((eval_query i) env q))).
            rewrite all_diff_NoDup_same in G1.
            set (llt :=
                   Partition.partition
                     (mk_olists (OVal _))
                     (fun t : tuple =>
                        map
                          (fun f0 : aggterm =>
                             interp_aggterm _ (env_t _ env t) f0) g)
                     (Febag.elements BTupleT ((eval_query i) env q))) in *.
            induction llt;[inversion G8| ];destruct a.
            simpl in G1;inversion G1;subst.
            destruct G8,Ha.
          - inversion H;inversion H0;subst;apply C41.
          - inversion H;subst.
            rewrite not_in_map_iff in H1.
            assert (H1 := H1 (lv',t::lt')).
            destruct H1 as [H1|H1];[destruct (H1 eq_refl)|destruct (H1 H0)].
          - inversion H0;subst.
            rewrite in_map_iff in H1.
            apply Classical_Pred_Type.not_ex_all_not with (n := (lv',lt'')) in H1.
            apply Classical_Prop.not_and_or in H1.
            destruct H1 as [H1|H1];[destruct (H1 eq_refl)|destruct (H1 H)].
          - apply IHllt;assumption.
        Qed.
               
        Lemma well_formed_q_fresh_sort_q_env :
          forall q e,
            well_formed_q basesort e q = true ->
            Fset.is_empty
              A (sort basesort q
                      interS Fset.Union
                      A (map fst (map fst e))) = true.
        Proof.
          intros.
          erewrite <- well_formed_q_disj_sort;
            [|apply H].
          unfold fresh_att_in_env.
          do 3 apply f_equal.
          rewrite map_map.
          apply map_ext.
          intros [ []];apply eq_refl.
        Qed.

        Fixpoint more_well_formed_q (e:env ) q :=
          match q with
          | Q_Empty_Tuple => true
          | Q_Table r => true
          | Q_Set _ q1 q2 => more_well_formed_q e q1 && more_well_formed_q e q2
          | Q_NaturalJoin q1 q2 =>
            more_well_formed_q e q1 && more_well_formed_q e q2
          | Q_Pi (_Select_List _ s) q1 =>
            more_well_formed_q
              e q1
          | Q_Sigma f q0 => more_well_formed_q e q0 && more_well_formed_f more_well_formed_q (env_t _ e (default_tuple _ (sort basesort q0))) f
          | Q_Gamma (_Select_List _ s) g f q0 =>            
              more_well_formed_q
                e q0 &&  more_well_formed_f more_well_formed_q (env_g _ e (Group_By _ g) (default_tuple _ (sort basesort q0) :: nil)) f &&
            well_formed_s
              _
              (env_g
                 _ e
                 (Group_By _ g)
                 ( (default_tuple _ (sort basesort q0))::nil) ) s
          end.

        Notation more_well_formed_f := (more_well_formed_f more_well_formed_q).
        
        Lemma more_well_formed_q_well_formed_f_eq_conj :
    forall  n,
      (forall q,
       query_size   q <= n -> forall e1 e2, weak_equiv_env TRcd e1 e2 -> more_well_formed_q  e1 q = more_well_formed_q  e2 q) /\
      (forall f, sql_formula_size   f <= n -> forall e1 e2, weak_equiv_env TRcd e1 e2 -> more_well_formed_f e1 f = more_well_formed_f e2 f).
  Proof.
    induction n;[split;intros [ ] H2;inversion H2|split].
    - intros [ ] Hn e1 e2 Heq.
      + apply eq_refl.
      + apply eq_refl.
      + simpl.
        rewrite (proj1 IHn _ (proj1 (query_size_Set  Hn)) _ _ Heq).
        rewrite (proj1 IHn _ (proj2 (query_size_Set  Hn)) _ _ Heq).
        apply eq_refl.
      + simpl.
        rewrite (proj1 IHn _ (proj1 (query_size_Join   Hn))  _ _ Heq).
        rewrite (proj1 IHn _  (proj2 (query_size_Join Hn)) _ _ Heq).
        apply eq_refl.
      + simpl.
        rewrite (proj1 IHn _ ((query_size_Pi   Hn)) _ _ Heq).
        destruct _s.
        repeat rewrite <-  Bool.andb_assoc.
        apply eq_refl.
      + simpl;rewrite (proj1 IHn _ (proj1 (query_size_Sigma Hn)) _ _ Heq).
        apply f_equal.
        refine (proj2 IHn _ (proj2 (query_size_Sigma Hn)) _ _ _).
        constructor;[|apply Heq].
        constructor;[apply Fset.equal_refl|split].
      +  simpl;rewrite (proj1 IHn _ (proj1 (query_size_Gamma Hn))  _ _ Heq).
         destruct _s.
         do 2 rewrite <-  Bool.andb_assoc.
         apply f_equal.
         apply f_equal2.
         * refine (proj2 IHn _ (proj2 (query_size_Gamma Hn))  _ _ _).
           constructor;[|apply Heq].
           constructor;[apply Fset.equal_refl|split].
         * apply well_formed_s_eq.
           repeat constructor;[ |apply Heq].
           apply Fset.equal_refl.
    - intros [ ] Hn e1 e2 Heq.
      + simpl;apply f_equal2.
        refine (proj2 IHn _ (proj1 (sql_formula_size_Conj  Hn)) _ _ Heq).        
        refine (proj2 IHn _ (proj2 (sql_formula_size_Conj  Hn)) _ _ Heq).
      + refine (proj2 IHn _ ( (sql_formula_size_Not Hn))  _ _ Heq).        
      + apply eq_refl.
      + apply eq_refl.
      + simpl.        
        refine (proj1 IHn _ ( (sql_formula_size_Quant  Hn))  _ _ Heq).
      + simpl.
        refine (proj1 IHn  _ ( (sql_formula_size_In Hn)) _ _ Heq).
      + simpl.
        apply (proj1 IHn _ ( (sql_formula_size_Exists Hn)) _ _ Heq).
  Qed.
  
  Lemma more_well_formed_q_eq :
    forall  q e1 e2,
      weak_equiv_env _ e1 e2 -> more_well_formed_q e1 q = more_well_formed_q e2 q.
  Proof.
    intros q e1 e2 Heq.
    refine (proj1 (more_well_formed_q_well_formed_f_eq_conj  (query_size   q)) _ (le_n _) _ _ Heq).
  Qed.

  Lemma more_well_formed_f_eq :
    forall  f e1 e2,
      weak_equiv_env _ e1 e2 ->  more_well_formed_f  e1 f = more_well_formed_f  e2 f.
  Proof.
    intros f e1 e2 Heq.
    refine (proj2 (more_well_formed_q_well_formed_f_eq_conj  (sql_formula_size   f)) _ (le_n _) _ _ Heq).
  Qed.          
          
        Lemma query_to_nraenv_is_sound :
          forall br i, well_sorted_instance i = true ->
                  well_typed_instance i =  true ->
                  forall q,
                    is_complete_instance_q i q = true ->
                    is_translatable_q q = true ->
                    forall env,
                      well_typed_e env = true ->
                      well_formed_e TRcd env = true ->
                      is_a_translatable_e env = true ->
                      well_formed_q basesort env q = true ->
                      more_well_formed_q  env q = true ->                
                      exists dq, Permutation (bagT_to_listD (eval_query i env q )) dq /\
                            forall did,
                              nraenv_eval
                                br (rec_sort (instance_to_bindings i))
                                (query_to_nraenv (map fst env) q)
                                (env_to_data env) did = Some (dcoll dq).
        Proof.
          unfold nraenv_eval.
          intros br i WI Ti q.
          set (n := (Tree.tree_size (tree_of_query q))).
          assert (Hn := le_n n);unfold n at 1 in Hn.
          clearbody n; revert q Hn.   
          induction n as [ | n];intros q Hn (* WSort *) Tc Tq env Typ WS Te Wq W2q;
            [destruct q;inversion Hn| ].
          destruct q as [ |r |sop q1 q2 |q1 q2 | sl q1| f q|sl g f q].
          -             (* Q_Empty_Tuple - Qed *)
            exists (drec nil ::nil).
            split;[|intros;apply eq_refl].
            apply permutation_bagT_singleton_empty_tuple.
          -             (* Q_Table - Qed *)
            simpl. simpl in Tc.
            rewrite Oset.mem_bool_true_iff in Tc.
            destruct (edot_find_some _ WI _ Tc) as [lx [Ax Bx]].
            exists lx;split;auto.
          -             (* Q_Set - Qed *)
            simpl;unfold olift2.
            simpl in Wq,Tc,Tq,W2q.
            rewrite andb_true_iff in Wq,Tc,Tq,W2q.
            (* simpl in WSort. *)
            (* apply andb_true_iff in WSort. *)
            destruct (IHn q1 (proj1 (query_size_Set Hn))
                          (* (proj1 WSort) *) (proj1 Tc) (proj1 Tq) _ Typ WS Te (proj1 Wq) (proj1 W2q))
              as [l1 [B1 C1]].
            destruct (IHn q2 (proj2 (query_size_Set Hn))
                          (* (proj2 WSort) *) (proj2 Tc) (proj2 Tq)  _  Typ WS Te (proj2 Wq) (proj2 W2q))
              as [l2 [B2 C2]].
            destruct Fset.equal;swap 1 2.
            + unfold bagT_to_listD,listT_to_listD.
              rewrite Febag.elements_empty.
              simpl.
              exists nil;split;[constructor|intros;apply eq_refl].    
            + assert (G1 := Permutation_bagT_to_listD_set_op br sop _ _ B1 B2 ).
              destruct G1 as [dq [G1 G2]].
              exists dq;split;[apply G1| ].
              intros;simpl.
              unfold olift2;rewrite C1,C2.
              apply G2.     
          -             (* Q_Join - Qed *)
            assert (Hq1 := proj1 (query_size_Join Hn)).
            assert (Hq2 := proj2 (query_size_Join Hn)).
            simpl;cbv zeta in *.
            simpl well_formed_q in Wq.
            (* simpl in Typ. *)
            (* rewrite 2 andb_true_iff in Typ. *)
            simpl;unfold lift2,olift.
            unfold nraenv_eval;simpl.
            simpl in Wq,Tc,Tq,W2q;apply andb_true_iff in Wq.
            rewrite 2 andb_true_iff in Tq.
            rewrite andb_true_iff in Tc.
            rewrite  andb_true_iff in W2q.
            rewrite (proj1 (proj1 Tq)).
            assert (IHq1 := IHn _ Hq1 (* (proj2 (proj1 WSort)) *) (proj1 Tc) (proj2 (proj1 Tq)) env Typ WS Te
                               (proj1 Wq) (proj1 W2q)).
            assert (IHq2 := IHn _ Hq2 (* (proj2 (WSort)) *) (proj2 Tc) (proj2 ( Tq)) env Typ WS Te (proj2 Wq) (proj2 W2q)).
            destruct IHq1 as [dq1 [Gq1 IHq1 ]].
            destruct IHq2 as [dq2 [Gq2 IHq2 ]].
            simpl.
            unfold olift,lift.
            assert (exists dq, omap_product (fun _ : data => Some (dcoll dq2)) dq1 = Some dq).
            {
              assert (D1 := Permutation_listT_is_tuple _ Gq1).
              assert (D2 := Permutation_listT_is_tuple _ Gq2).
              revert D1 D2;clear.
              unfold omap_product.
              induction dq1;intros;[exists nil;apply eq_refl| ].
              destruct (IHdq1 (fun d H => D1 d (or_intror H)) D2) as [l3 H4].
              simpl;rewrite H4;simpl.
              destruct (D1 a (or_introl eq_refl));subst.
              assert (X1 : exists l5,
                         oncoll_map_concat
                           (fun _ : data => Some (dcoll dq2))
                           (drec (tuple_as_dpairs x)) = Some l5).
              { revert D1 D2.
                clear.
                unfold oncoll_map_concat,omap_concat.
                unfold orecconcat.
                induction dq2;[exists nil;apply eq_refl| ];intros.
                destruct (IHdq2 D1 (fun d H => D2 d (or_intror H))) as [l3 H4].
                simpl;unfold lift.
                destruct (D2 _ (or_introl eq_refl));subst.
                rewrite H4.
                exists (drec (rec_concat_sort
                           (tuple_as_dpairs x)
                           (tuple_as_dpairs x0)) :: l3);apply eq_refl.
              }
              destruct X1 as [ld X1].
              rewrite X1.
              exists (ld ++ l3).
              apply eq_refl.
            }
            destruct H.
            exists x;split.
            * refine (permutation_bagT_omap_product eq_refl H _ _ Gq1 Gq2 _).
              intros t1 t2 H1 H2.
              assert (B1 := ws_instance_tuples  _ env q1 WI t1 H1).
              assert (B2 := ws_instance_tuples  _ env q2 WI t2 H2).
              rewrite Fset.is_empty_spec.
              (* assert (D0 := proj1 (proj1 WSort)). *)
              rewrite Fset.is_empty_spec in Tq.
              rewrite <- (Fset.equal_eq_2 _ _ _ _ (proj1 (proj1 Tq))).
              rewrite Fset.equal_spec.
              intro a.
              rewrite 2 Fset.mem_inter.
              rewrite (Fset.mem_eq_2 _ _ _ B1).
              rewrite (Fset.mem_eq_2 _ _ _ B2).
              apply eq_refl.
            * intro did.
              rewrite IHq1.
              simpl.
              rewrite IHq2,H.
              apply eq_refl.
          -             (* Q_Pi - Qed *)
            assert (Hq1 := (query_size_Pi Hn)).
            simpl;cbv zeta in *.
            simpl;unfold lift2.
            simpl in Wq,Tc,Tq,W2q.
            destruct sl.
            rewrite 3 andb_true_iff in Wq.
            rewrite 2 andb_true_iff in Tq.
            assert (D0 := well_formed_q_fresh_sort_q_env _ _ (proj1 (proj1 (proj1 Wq)))).
            unfold nraenv_eval.
            simpl.
            unfold olift.
            (* simpl in Typ. *)
            (* rewrite andb_true_iff in Typ. *)
            (* simpl in Wq. *)
            (* destruct sl. *)
            (* rewrite 3 andb_true_iff in Wq. *)
            assert (Hq := IHn _ Hq1  (* WSort *) Tc (proj1 (proj1 Tq)) env Typ WS Te (proj1 (proj1 (proj1 Wq))) W2q).
            destruct Hq as [dq [Pq Hq]].
            simpl.
            unfold lift.
            assert (Aux1 : exists dq1,   lift_map
                                      (fun din : data =>
                                         nraenv_core_eval br (rec_sort (instance_to_bindings i)) (nraenv_to_nraenv_core ((select_list_to_nraenv ((sort basesort q1, Group_Fine _) :: map fst env) (_Select_List _ l)) ))
                                                          (drec (("slc"%string, dcoll (din :: nil)) :: ("tl"%string, env_to_data env) :: nil)) din) dq = Some dq1).
            {
              apply lift_map_Some_spec.
              intros d Hd.
              destruct (permutation_bagT_tuple _ Pq _ Hd) as [t ->].
              assert (G1 := select_list_to_nraenv_is_sound br (rec_sort (instance_to_bindings i)) (env_t _ env t) (drec (tuple_as_dpairs t))).
              unfold nraenv_eval in G1.
              simpl in G1.
              rewrite Fset.equal_refl in G1.
              simpl in G1.
              rewrite <- map_map in D0.
              unfold fst in D0.
              rewrite Fset.is_empty_spec in G1,D0.
              rewrite <- (Fset.equal_eq_2 _ _ _ _ D0) in G1.
              rewrite 2 map_map in G1.
              replace (map (fun slc : nslice * list tuple => let (y, _) := slc in let (sa, _) := y in sa) env) with
                  (map (fun x : nslice * list tuple => let (x0, _) := let (x0, _) := x in x0 in x0) env) in G1 by (apply map_ext;intros [[]];apply eq_refl).
              rewrite WS,andb_true_r in G1.
              assert (B1 := ws_instance_tuples  _ env q1 WI ).
              assert (X1 : well_typed_slice (labels t, Group_Fine TRcd, t :: nil) && well_typed_e env = true).
              {
                apply (well_typed_e_env_t _ _ _ _ (Permutation_in _ (Permutation_sym Pq) Hd) Typ Ti Tc (proj1 (proj1 Tq))).
              }
              assert ( G2 := fun H => G1 X1 (Fset.inter_eq_1 _ _ _ _ (B1 _ H)) ).
              rewrite inBE_tuple_as_pairs_bagT in G2.
              assert (G2 := G2 (Permutation_in _ (Permutation_sym Pq) Hd) (is_a_translatable_e_Group_Fine _ _ _ Te) l ).
              exists  (drec
                    (tuple_as_dpairs
                       (mk_tuple (Fset.mk_set A ({{{Fset.mk_set A (map fst (map (fun x : select _ => match x with
                                                                                                   | Select_As _ e a => (a, e)
                                                                                                   end) l))}}}))
                                 (fun a : attribute =>
                                    match Oset.find OAtt a (map (fun x : select _ => match x with
                                                                                    | Select_As _ e a0 => (a0, e)
                                                                                    end) l) with
                                    | Some e => interp_aggterm _ (env_t _ env t) e
                                    | None => dot (default_tuple _ (emptysetS)) a
                                    end)))).
              rewrite <- G2.
              + erewrite select_list_to_nraenv_equiv_env_eq_;[apply eq_refl| ].
                refine (@equiv_env_equiv_nenv _ _ ((labels t, Group_Fine _,t::nil)::env) ((sort basesort q1, Group_Fine _,t::nil)::env) _ ).
                apply equiv_env_cons_eq_1.
                apply B1.
                rewrite inBE_tuple_as_pairs_bagT.
                apply ( (Permutation_in _ (Permutation_sym Pq) Hd)).
              (* + apply (proj2 Typ). *)
              + rewrite <- (proj2 (proj1 (proj1 Wq))).
                apply well_formed_s_eq.
                unfold default_tuple,env_t.
                constructor;[|apply  weak_equiv_env_refl].
                repeat split.
                rewrite (Fset.equal_eq_2 _ _ _ _ (labels_mk_tuple _ _ _ )).
                apply B1.
                rewrite inBE_tuple_as_pairs_bagT.
                apply ( (Permutation_in _ (Permutation_sym Pq) Hd)).
            }
            destruct Aux1 as [lx Aux1] .
            unfold select_list_to_nraenv in Aux1.
            exists lx;split.
            * replace
                (map (fun x : select _ => match x with
                                         | Select_As _ _ a => a
                                         end) l)
                with
                  (map fst (map (fun x : select _ => match x with
                                                    | Select_As _ e a => (a,e)
                                                    end) l)) by (rewrite map_map;apply map_ext;intros [ ];apply eq_refl).
              refine (query_to_nraenv_Pi_is_sound _ _ _ _ _ WI Ti Typ WS Te Tc (proj1 (proj1 Tq)) D0 (proj2 (proj1 (proj1 Wq))) Aux1 Pq).
            * intro did.
              rewrite Hq.
              simpl in Aux1;simpl;rewrite Aux1.
              apply eq_refl.   
          -             (* Q_Sigma - Qed *)
            assert (Hq1 := proj1 (query_size_Sigma Hn)).
            assert (Hf1 := proj2 (query_size_Sigma Hn)).   
            simpl;cbv zeta in *.
            simpl in Wq,Tc,Tq,W2q.
            rewrite andb_true_iff in Wq,Tc,Tq,W2q.
            assert (D0 := well_formed_q_fresh_sort_q_env _ _ ( (proj1 Wq))).
            unfold nraenv_eval.
            simpl.
            unfold olift.    
            assert (Hq := IHn _ Hq1 (proj1 Tc) (proj1 Tq) env Typ WS Te (proj1 Wq) (proj1 W2q)).
            destruct Hq as [dq [Pq Hq]].
            (* rewrite Hq. *)
            simpl.
            assert (Aux :
                      forall q,
                        query_size q <= sql_formula_size f ->
                        is_complete_instance_q i q = true ->
                        is_translatable_q q = true ->
                        (* well_sorted_q q = true -> *)
                        forall (env : list (nslice * list tuple)),
                          well_typed_e env = true ->
                          well_formed_e _ env = true ->
                          is_a_translatable_e env = true ->
                          well_formed_q basesort env q = true ->                          
                          more_well_formed_q  env q = true ->                      
                          exists lq,
                            Permutation
                              (bagT_to_listD ((eval_query i) env q)) lq /\
                            forall did,
                              nraenv_eval
                                br
                                (rec_sort (instance_to_bindings i))
                                ( query_to_nraenv (map fst env) q )
                                (env_to_data env) did = Some (dcoll lq)).
            { intros q1 Hn1 Tc1 Tq1 env1 Typ1 WI1 G1 G2 G3.
              refine (IHn _ _ _  _ _ _ WI1  G1 G2 G3).
              refine (Le.le_trans _ _ _ _ Hf1).
              apply Hn1.
              apply Tc1.
              apply Tq1.
              apply Typ1.     
            }
            unfold lift.
            cbn.
            assert  (IHf1 := @formula_to_nraenv_is_sound
                              relname ORN frt TD SN EN AN
                              FTN ATN IN BD PN FN).      
            assert (IHf1 :=
                      IHf1 _
                          ws_instance_tuples _    _ _ well_typed_instance_queries 
                         _ i WI Ti f (proj2 Tc) (proj2 Tq) Aux).
            assert (A1 :  forall x : data,
                       In x dq ->
                       exists z : bool,
                         match
                           (br
                              ⊢ₑ nraenv_to_nraenv_core
                              (NRAEnvIsTrueB
                                 (formula_to_nraenv_gen
                                    query_to_nraenv
                                    ((sort basesort q, Group_Fine TRcd)
                                       :: map fst env) f)) @ₑ
                              x ⊣ rec_sort (instance_to_bindings i);
                            (drec (("slc"%string, dcoll (x :: nil))
                                     ::("tl"%string, env_to_data env)
                                     :: nil)))%nraenv_core
                         with
                         | Some (dbool b) => Some b
                         | _ => None
                         end= Some z).
            {
              intros x Hx.
              destruct (Permutation_listT_is_tuple _ Pq _ Hx) as [t ->].
              subst.
              assert (Aux2 : well_formed_e _ ((sort basesort q, Group_Fine _ , t :: nil) :: env) = true).
              {
                rewrite well_formed_e_cons_spec.
                repeat split;[| |apply WS].
                - intros a.
                  rewrite Fset.is_empty_spec in D0.
                  rewrite Fset.equal_spec in D0.
                  assert (D0 := D0 a).
                  rewrite Fset.empty_spec,Fset.mem_inter in D0.
                  rewrite andb_false_iff in D0.
                  destruct D0;[left;apply H|right].
                  intros slc Hs.
                  rewrite mem_Union_false in H.
                  apply H.
                  rewrite in_map_iff;exists (fst slc);repeat split.
                  rewrite in_map_iff;exists slc;repeat split.
                  apply Hs.
                - intros t2 [|[]];subst.
                  refine(ws_instance_tuples  _ env q WI t2 _).
                  rewrite inBE_tuple_as_pairs_bagT.
                  apply (Permutation_in _ (Permutation_sym Pq) Hx).
              }
              assert (X1 : well_typed_slice (labels t, Group_Fine TRcd, t :: nil) && well_typed_e env = true).
              {
                apply (well_typed_e_env_t _ _ _ _ (Permutation_in _ (Permutation_sym Pq) Hx) Typ Ti (proj1 Tc) ((proj1 Tq))).
              }              
              assert (IHf1 :=
                        IHf1
                          ((sort basesort q, Group_Fine _, t::nil) :: env)
                          X1 Aux2 (is_a_translatable_e_Group_Fine _ _ _ Te)).              
              unfold nraenv_eval in IHf1.
              simpl in IHf1.
              assert (D3 := NRAEnvIsTrueB_eq).
              unfold nraenv_eval in D3.
              rewrite (D3 _ 
                          (eval_sql_formula
                             (eval_query i)
                             ((sort basesort q, Group_Fine TRcd, t :: nil)
                                :: env) f)).
              + exists (Bool3.Bool.is_true
                     (B TRcd)
                     (eval_sql_formula
                        (eval_query i)
                        ((sort basesort q, Group_Fine TRcd, t :: nil)
                           ::env) f));apply eq_refl.
              + apply IHf1.
                rewrite <- (proj2 Wq).
                apply well_formed_f_eq.
                constructor;[|apply weak_equiv_env_refl].
                repeat split.
                unfold default_tuple.
                rewrite (Fset.equal_eq_2 _ _ _ _ (labels_mk_tuple _ _ _)).
                apply Fset.equal_refl.
                rewrite <- (proj2 W2q).
                apply more_well_formed_f_eq.
                constructor;[|apply weak_equiv_env_refl].
                repeat split.
                unfold default_tuple.
                rewrite (Fset.equal_eq_2 _ _ _ _ (labels_mk_tuple _ _ _)).
                apply Fset.equal_refl.
            }   
            exists
            (filter
               (fun x : data =>
                  match
                    match
                      (br
                         ⊢ₑ nraenv_to_nraenv_core
                         (NRAEnvIsTrueB
                            (formula_to_nraenv_gen
                               query_to_nraenv
                               ((sort basesort q, Group_Fine TRcd)
                                  :: map fst env) f)) @ₑ x
                         ⊣ rec_sort (instance_to_bindings i);
                       (drec (("slc"%string, dcoll (x :: nil))
                                :: ("tl"%string, env_to_data env)
                                :: nil)))%nraenv_core
                    with
                    | Some (dbool b) => Some b
                    | _ => None
                    end
                  with
                  | Some b => b
                  | None => false
                  end) dq);split.
            * refine (permutation_bagT_filter
                        _ _ _ _ Pq _
                        (lift_filter_some_filter_alt _ _ A1)).
              -- apply t_env_equiv.
                 apply contains_nulls_eq.
              -- intros t b Ht Hf.
                 destruct nraenv_core_eval eqn:D6 in Hf;
                   [|discriminate].  
                 destruct d;try discriminate.
                 inversion Hf;subst.
                 assert (Aux2 :
                           well_formed_e
                             _ ((sort basesort q, Group_Fine _ , t :: nil)
                                  :: env) = true).
                 {
                   rewrite well_formed_e_cons_spec.
                   repeat split;[| |apply WS].
                   - intros a.
                     rewrite Fset.is_empty_spec in D0.
                     rewrite Fset.equal_spec in D0.
                     assert (D0 := D0 a).
                     rewrite Fset.empty_spec,Fset.mem_inter in D0.
                     rewrite andb_false_iff in D0.
                     destruct D0;[left;apply H|right].
                     intros slc Hs.
                     rewrite mem_Union_false in H.
                     apply H.
                     rewrite in_map_iff;exists (fst slc);repeat split.
                     rewrite in_map_iff;exists slc;repeat split.
                     apply Hs.
                   - intros t2 [|[]];subst.
                     refine(ws_instance_tuples  _ env q WI t2 Ht). }
                 assert (G1 : well_formed_f
                                basesort ((sort basesort q, Group_Fine _, t :: nil)
                                            :: env) f = true).
                 {
                   rewrite <- (proj2 Wq).
                   apply well_formed_f_eq.
                   constructor;[|apply weak_equiv_env_refl].
                   repeat split.
                   unfold default_tuple.
                   rewrite (Fset.equal_eq_2
                              _ _ _ _ (labels_mk_tuple _ _ _)).
                   apply Fset.equal_refl.
                 }
                 assert (G2 : is_a_translatable_e
                                ((sort basesort q, Group_Fine _, t :: nil)
                                   :: env) = true)
                   by apply (is_a_translatable_e_Group_Fine _ _ _ Te).
                 clear Hf.
                 assert (A2 := ws_instance_tuples  _ env q WI t).
                 unfold env_t.
                 rewrite eval_sql_formula_eq
                   with (env2 := ((sort basesort q, Group_Fine _ , t :: nil)
                                   :: env));
                   [|apply equiv_env_cons_eq_1;apply A2;apply Ht].
                 assert (A3 :=
                           NRAEnvIsTrueB_eq
                             (formula_to_nraenv_gen
                                query_to_nraenv
                                ((sort basesort q, Group_Fine TRcd)
                                   :: map fst env) f)
                             (eval_sql_formula
                                (eval_query i)
                                ((sort basesort q, Group_Fine _ , t :: nil)
                                   :: env) f)).
                 assert (forall b1 b2, dbool b1 = dbool b2 -> b1 = b2)
                   by (intros x1 x2 Hx;inversion Hx;apply eq_refl).
                 apply H.
                 rewrite <- Some_inj.
                 rewrite <- D6.
                 apply eq_sym.
                 refine (A3 _ _ _ _ _ ).
                 intros did1.
                 refine (IHf1 _ _ Aux2 G2 G1 _ _).
                 rewrite inBE_tuple_as_pairs_bagT in Ht.
                 apply (well_typed_e_env_t _ _ _ _  Ht Typ Ti (proj1 Tc) (proj1 Tq)).
                                 rewrite <- (proj2 W2q).
                apply more_well_formed_f_eq.
                constructor;[|apply weak_equiv_env_refl].
                repeat split.
                unfold default_tuple.
                rewrite (Fset.equal_eq_2 _ _ _ _ (labels_mk_tuple _ _ _)).
                apply Fset.equal_refl.
            * intro did.
              destruct f;simpl;rewrite Hq;
                try (simpl;simpl in A1;rewrite (lift_filter_some_filter_alt _ _ A1);
                   apply eq_refl).
              do 2 apply  f_equal.
              apply eq_sym.
              apply ListFacts.filter_true.
              intros.
              assert (G1 := NRAEnvIsTrueB_eq).
              unfold nraenv_eval in G1.
              rewrite G1 with (b1:= Bool3.Bool.true (B TRcd)).
              -- apply Bool3.Bool.true_is_true_alt.
              --  intros did1.
                  cbn.
                  apply f_equal.
                  rewrite boolB_to_data_true.
                  apply normalize_data_dtrue.
          - (* Q_Gamma *)
            assert (Hq1 := proj1 (query_size_Gamma Hn)).
            assert (Hf1 := proj2 (query_size_Gamma Hn)).
            destruct sl.
            simpl in Tc,Tq.
            simpl;cbv zeta in *.
            assert (O1 :=
                      Partition.partition_permut
                        (mk_olists (OVal _))
                        (fun t : tuple =>
                           map
                             (fun f0 : aggterm =>
                                interp_aggterm _ (env_t _ env t) f0) g)
                        (Febag.elements BTupleT ((eval_query i) env q))).
            simpl.
            simpl in Wq,Tc,Tq.
            rewrite 3 andb_true_iff in Wq.
            destruct extract_grouping_criteria as [la| ] eqn:D1;[|discriminate].
            repeat rewrite andb_true_iff in Tq.
            repeat rewrite andb_true_iff in Tc.
            assert (D0 :=
                      well_formed_q_fresh_sort_q_env
                        _ _ (proj1 (proj1 (proj1 Wq)))).
            (* destruct well_formed_s eqn:F0. *)
            + {
                - simpl;unfold lift2.
                  assert (Ds := (proj1 (proj1 (proj1 (proj1 Tq))))).
                  rewrite Ds.
                  (* destruct Fset.subset eqn:Ds. *)
                  simpl;unfold lift,olift.
                  simpl in W2q.
                  rewrite 2 andb_true_iff in W2q.
                    assert
                      (Hq := IHn
                              _ Hq1 (proj1 Tc) (proj2 (proj1 (proj1 (proj1 Tq)))) env Typ WS Te
                              (proj1 (proj1 (proj1 Wq)))  (proj1 (proj1 W2q))).
                    destruct Hq as [lq [Pq Hq]].
                    unfold lift.
                    assert (Aux :
                              forall q,
                                query_size q <= sql_formula_size f ->
                                is_complete_instance_q i q = true ->
                                is_translatable_q q = true ->
                                forall (env : list (nslice * list tuple)) ,
                                  well_typed_e env = true ->
                                  well_formed_e _ env = true ->
                                  is_a_translatable_e env = true ->
                                  well_formed_q basesort env q = true ->
                                  more_well_formed_q  env q = true ->                                 
                                  exists lq,
                                    Permutation
                                      (bagT_to_listD ((eval_query i) env q)) lq /\
                                    forall (did : data),
                                      nraenv_eval
                                        br (rec_sort (instance_to_bindings i))
                                        (query_to_nraenv (map fst env) q)
                                        (env_to_data env) did = Some (dcoll lq)).
                    { intros q1 Hn1 XX1 X1  env1 Typ1 WI1 G3 G4 G5.
                      refine (IHn _ _ XX1 X1 _ Typ1 WI1  G3 G4 G5).
                      refine (Le.le_trans _ _ _ _ Hf1).
                      apply Hn1.    
                    }
                    assert  (IHf1 := @formula_to_nraenv_is_sound
                                      relname ORN frt TD SN EN AN
                                      FTN ATN IN BD PN FN).
                    assert (IHf1 :=
                              IHf1
                                _  ws_instance_tuples _ _
                                _ well_typed_instance_queries _ i WI Ti f (proj2 Tc) (proj2 (proj1 (proj1 Tq))) Aux ).
                    assert (A1 : exists map_ldq1, lift_map (fun din : data => match din with
                                                                        | drec r => Some (drec (rproject r (map attribute_to_string la)))
                                                                        | _ => None
                                                                        end) lq = Some map_ldq1).
                    {
                      apply lift_map_Some_spec.
                      intros d Hd.
                      destruct (in_listT_is_drec _ _ (Permutation_in _ (Permutation_sym Pq) Hd)).
                      subst.
                      exists (drec (rproject (tuple_as_dpairs x) (map attribute_to_string la))).
                      apply eq_refl.
                    }
                    destruct A1 as [map_ldq1 D4].
                    (* rewrite D4. *)
                    unfold rondcoll,lift_oncoll,lift.
                    simpl.
                    unfold olift2.
                    simpl.
                    assert (D5 : exists map_2,
                               lift_map
                                 (fun din : data =>
                                    match
                                      match
                                        match
                                          lift_filter
                                            (fun d1 : data =>
                                               match
                                                 match match d1 with
                                                       | drec r => Some (drec (rproject r (map attribute_to_string la)))
                                                       | _ => None
                                                       end with
                                                 | Some d2 => unbdata (fun x y : data => if data_eq_dec x y then true else false) d2 din
                                                 | None => None
                                                 end
                                               with
                                               | Some (dbool b) => Some b
                                               | _ => None
                                               end) lq
                                        with
                                        | Some a' => Some (dcoll a')
                                        | None => None
                                        end
                                      with
                                      | Some x' => Some (drec ((find_fresh_string (map attribute_to_string la), x') :: nil))
                                      | None => None
                                      end
                                    with
                                    | Some d2 => match din with
                                                | drec r1 => match d2 with
                                                            | drec r2 => Some (drec (rec_sort (r1 ++ r2)))
                                                            | _ => None
                                                            end
                                                | _ => None
                                                end
                                    | None => None
                                    end) (bdistinct map_ldq1) = Some map_2).
                    {
                      apply lift_map_Some_spec.
                      intros d Hd.
                      assert (exists lt',   lift_filter
                                         (fun d1 : data =>
                                            match
                                              match match d1 with
                                                    | drec r => Some (drec (rproject r (map attribute_to_string la)))
                                                    | _ => None
                                                    end with
                                              | Some d2 => unbdata (fun x y : data => if data_eq_dec x y then true else false) d2 d
                                              | None => None
                                              end
                                            with
                                            | Some (dbool b0) => Some b0
                                            | _ => None
                                            end) lq = Some lt').
                      {
                        rewrite lift_filter_some_filter_alt.
                        exists    (filter
                                (fun x : data =>
                                   match
                                     match
                                       match match x with
                                             | drec r => Some (drec (rproject r (map attribute_to_string la)))
                                             | _ => None
                                             end with
                                       | Some d2 => unbdata (fun x0 y : data => if data_eq_dec x0 y then true else false) d2 d
                                       | None => None
                                       end
                                     with
                                     | Some (dbool b0) => Some b0
                                     | _ => None
                                     end
                                   with
                                   | Some b => b
                                   | None => false
                                   end) lq);apply eq_refl.
                        intros d2 Hd2.
                        destruct (permutation_bagT_tuple _ Pq _ Hd2);subst.
                        apply In_bdistinct in Hd.
                        destruct (lift_map_some_in_2 _ _ D4 _ Hd) as [tt [X1 X2]].
                        destruct tt;try discriminate.
                        inversion X2.
                        destruct unbdata eqn:D6;[|discriminate].
                        destruct d0;try discriminate.
                        exists b;apply eq_refl.
                      }
                      destruct H.
                      rewrite H.
                      apply In_bdistinct in Hd.
                      destruct (lift_map_some_in_2 _ _ D4 _ Hd) as [tt [X1 X2]].
                      destruct tt;try discriminate.
                      inversion X2.
                      exists (drec (rec_sort (rproject l0 (map attribute_to_string la) ++ (find_fresh_string (map attribute_to_string la), dcoll x) :: nil))).
                      apply eq_refl.
                    }
                    destruct D5 as [map_2 Dpp].
                    destruct (lift_map _ (bdistinct map_ldq1) ) as [pp| ] eqn:D5;[|discriminate].
                    inversion Dpp;subst;clear Dpp.
                    assert (Aux6 : forall x : data, In x map_2 -> exists l0 l1, x = drec (rec_sort (l1 ++ (find_fresh_string (map attribute_to_string la), dcoll l0) :: nil))).
                    {
                      intros d Hd.
                      assert (G6 := lift_map_some_in_2 _ _ D5 _ Hd).
                      destruct G6 as [d6 [G6 F6]].
                      destruct lift_filter eqn:Y6 in F6 ;try discriminate.
                      destruct d6;try discriminate.
                      inversion F6.
                      exists l0,l1;apply eq_refl.
                    }
                    assert (D6 : exists map_3,
                               lift_map (fun din : data => match din with
                                                          | drec r => edot r (find_fresh_string (map attribute_to_string la))
                                                          | _ => None
                                                          end) map_2 = Some map_3).
                    {
                      apply lift_map_Some_spec.
                      intros a Ha.
                      destruct (Aux6 _ Ha) as [r [lr ->]].
                      exists (dcoll r).
                      unfold edot.
                      rewrite assoc_lookupr_drec_sort.
                      simpl.
                      refine (eq_trans (assoc_lookupr_app _ _ _ _) _).
                      simpl.
                      destruct string_eqdec;[apply eq_refl| ].
                      destruct (c eq_refl).
                    }
                    destruct D6 as [map_3 D6].
                    (* rewrite D6. *)
                    assert (Aux1 : forall x : data, In x map_3 -> exists y , x = dcoll (listT_to_listD y)).
                    { intros d Hd1.
                      destruct (lift_map_some_in_2 _ _ D6 _ Hd1) as [d2 [Hd3 Hd4]].
                      destruct d2;try discriminate.
                      destruct (lift_map_some_in_2 _ _ D5 _ Hd3) as [d2 [Hd5 Hd6]].
                      apply In_bdistinct in Hd5.
                      destruct (lift_map_some_in_2 _ _ D4 _ Hd5) as [d3 [Hd7 Hd8]].
                      destruct d3;try discriminate.
                      inversion Hd8;subst d2;clear Hd8.
                      destruct lift_filter eqn:G1 in Hd6 ;try discriminate.
                      inversion Hd6;subst l0;clear Hd6.                      
                      (* assert (A1 := fresh_is_fresh (map attribute_to_string la)). *)
                      assert (A2 := edot_fresh_concat_right_single (find_fresh_string (map attribute_to_string la)) (dcoll l2) ( (rproject l1 (map attribute_to_string la)))).
                      unfold rec_concat_sort in A2.
                      rewrite A2 in Hd4.
                      inversion Hd4;subst d.
                      apply lift_filter_Some_filter in G1.
                      rewrite G1.
                      unfold bagT_to_listD in Pq.
                      rewrite Permutation_listT_spec in Pq.                      
                      destruct Pq as [lt' [Pq <-]].
                      unfold listT_to_listD.
                      rewrite ListFacts.filter_map.
                      exists    (filter
                              (fun x : tuple =>
                                 match
                                   match
                                     unbdata (fun x0 y0 : data => if data_eq_dec x0 y0 then true else false)
                                             (drec (rproject (tuple_as_dpairs x) (map attribute_to_string la))) (drec (rproject l1 (map attribute_to_string la)))
                                   with
                                   | Some (dbool b) => Some b
                                   | _ => None
                                   end
                                 with
                                 | Some b => b
                                 | None => false
                                 end) lt');apply eq_refl.
                    }
                    assert (D66 :
                              forall lt,
                                In (dcoll (listT_to_listD lt)) map_3 ->
                                forall x,
                                  In x lt ->
                                  Oeset.mem_bool OTuple x (Febag.elements BTupleT ((eval_query i) env q)) = true).
                    {
                      intros lt Hlt x Hx.
                      refine (proj2 (inBE_tuple_as_pairs_bagT _ _) _).
                      destruct (lift_map_some_in_2 _ _ D6 _ Hlt) as [[ ] [B1 B2]];try discriminate.
                      destruct (lift_map_some_in_2 _ _ D5 _ B1) as [ d [B3 B4]].
                      apply In_bdistinct in B3.
                      destruct (lift_map_some_in_2 _ _ D4 _ B3) as [ [ ] [B5 B6]];try discriminate.
                      inversion B6;subst;clear B6.
                      destruct lift_filter eqn:B6 in B4;[|discriminate].
                      inversion B4;subst;clear B4.
                      unfold edot in B2.
                      rewrite assoc_lookupr_drec_sort in B2.
                      rewrite (assoc_lookupr_app (rproject l1 (map attribute_to_string la))) in B2.
                      simpl in B2.
                      destruct string_eqdec;[|destruct (c eq_refl)].
                      inversion B2;subst;clear B2.
                      destruct (lift_filter_some_in _ _ B6 _ (In_tuple_as_pairs_eq_listT _ _ Hx)).
                      apply (Permutation_in _ (Permutation_sym Pq) H).
                      (* apply inBE_tuple_as_pairs_bagT in G1. *)
                      (* rewrite Febag.mem_unfold in G1. *)
                      (* rewrite Oeset.mem_bool_true_iff in G1. *)
                      (* destruct G1 as [x2 [G1 G2]]. *)
                      (* apply tuple_as_dpairs_eq in G1. *)
                      (* rewrite G1 in *. *)
                      (* clear x G1 Hx. *)
                      (* assert (I3 := proj1 (in_permut_in O1 _) G2). *)
                      (* rewrite in_flat_map in I3. *)
                      (* destruct I3 as [[lv1 lt1] [I3 I4]]. *)
                    }
                    assert (A2 := ws_instance_tuples _ env q WI).
                    assert
                      (A3 :
                         forall t : tuple,
                           Oeset.mem_bool OTuple
                                       t (Febag.elements
                                            BTupleT
                                            ((eval_query i) env q)) = true ->
                           Fset.mk_set A la subS labels t).
                    {
                      intros t Ht.
                      rewrite <- Febag.mem_unfold in Ht.
                      refine (eq_trans
                                (Fset.subset_eq_2
                                   _ _ _ _ (A2 _ Ht)) _).                      
                      apply Ds.
                    }                    
                    assert
                    (NN1 := fun did =>
                             @group_by_translation_is_sound_permut_permut_aggterm
                               g br i q   la env did lq map_3).
                    assert (NN1 := fun did => NN1 did A3 D1  (Hq did) Pq).
                    unfold nraenv_eval in NN1.
                    simpl in NN1.
                    unfold olift in NN1.
                    simpl in NN1.
                    assert (NN1 := NN1 dunit).
                    simpl in Hq;rewrite (Hq dunit) in NN1.
                    simpl in NN1.
                    unfold lift in NN1.
                    rewrite D4 in NN1.
                    simpl in NN1.
                    unfold olift2 in NN1.
                    simpl in NN1,D5.
                    destruct lift_map  eqn:G1 in NN1;swap 1 2;
                      apply eq_sym in D5;assert (E1 := eq_trans D5 G1);[discriminate| ].
                    inversion E1;subst l0;clear E1.
                    simpl in NN1.
                    rewrite D6 in NN1.
                    assert (NN1 := NN1 eq_refl).                    
                    assert (Aux01 : forall lt, In (dcoll (listT_to_listD lt)) map_3 ->
                                          exists lt',
                                            In lt'
                                               (map snd
                                                    (Partition.partition (mk_olists (OVal _)) (fun t : tuple => map (fun f : aggterm => interp_aggterm _ (env_t _ env t) f) g)
                                                                         (Febag.elements BTupleT ((eval_query i) env q)))) /\
                                            lt =PE= lt').
                    {
                      intros lt Hlt.
                      apply _permut_sym in NN1;
                        [|do 4 intro;apply Permutation_sym].
                      assert (KK1 := _permut_in NN1 (listT_to_listD lt)).
                      rewrite in_map_iff in KK1.
                      assert (KK2 : (exists x : data, match x with
                                                 | dcoll ld => ld
                                                 | _ => nil
                                                 end = listT_to_listD lt /\ In x map_3) ) by
                          (exists (dcoll (listT_to_listD lt));split;[apply eq_refl|apply Hlt]).
                      assert (KK1 := KK1 KK2).
                      clear KK2.
                      destruct KK1 as [lb [KK1 KK2]].
                      rewrite in_map_iff in KK1.
                      destruct KK1 as [lt2 [<- KK1]].
                      rewrite <- Permutation_listT_eq in KK2.
                      exists lt2;split;[apply KK1|apply KK2].
                    }
                    assert (Aux0 :
                              forall lt,
                                (*     In lt *)
                                (* (map snd *)
                                (*  (Partition.partition (mk_olists (OVal T)) (fun t : tuple => map (fun f : aggterm => interp_aggterm T (env_t T env t) f) g) *)
                                (*             (Febag.elements BTupleT ((eval_query i) env q)))) -> *)
                                In (dcoll (listT_to_listD lt)) map_3 ->
                                well_formed_e _ ((sort basesort q, Group_By _ g, lt) :: env) = true).
                    { intros lt Hlt.
                      destruct ( Aux01 _ Hlt) as [lt2 [Hlt2 Glt2]].
                      refine (eq_trans (well_formed_e_eq (equiv_env_cons_eq_3 Glt2 _ _ _) ) _).
                      simpl.
                      repeat rewrite andb_true_iff.
                      repeat split.
                      + apply Wq.
                      + destruct lt2.
                        * destruct (Partition.in_map_snd_partition_diff_nil _ _ _ Hlt2 eq_refl).
                        * simpl;rewrite andb_true_iff;split.
                          -- rewrite forallb_forall.
                             intros.
                             apply Oset.eq_bool_refl.
                          -- rewrite forallb_forall.
                             intros y Hy.
                             rewrite forallb_forall.
                             intros e He.
                             rewrite Oset.eq_bool_true_iff.
                             rewrite in_map_iff in Hlt2.
                             destruct Hlt2 as [ [lv4 lt4] [P2 P3]].
                             simpl in P2.
                             subst lt4.
                             assert (P5 := Partition.partition_homogeneous_values _ _ _ _ _ P3).
                             assert (P6 := P5 _ (or_intror Hy)).
                             simpl in P6.
                             assert (P5 := P5 _ (or_introl eq_refl)).
                             simpl in P5.
                             rewrite <- P5 in P6.
                             rewrite <- ListFacts.map_eq in P6.
                             apply (P6 _ He).
                      + rewrite forallb_forall;intros.
                        apply (ws_instance_tuples _ env q WI).
                        rewrite Febag.mem_unfold.
                        apply Oeset.in_mem_bool.
                        refine (Partition.in_map_snd_partition _ _ _ _ _ Hlt2 H).
                      (* refine (D66 _ Hlt _ H). *)
                      + rewrite Fset.is_empty_spec,Fset.equal_spec.
                        intros a.
                        rewrite Fset.is_empty_spec in D0.
                        rewrite Fset.equal_spec in D0.
                        assert (D0 := D0 a).
                        rewrite Fset.empty_spec,Fset.mem_inter in D0.
                        rewrite Fset.empty_spec,Fset.mem_inter.
                        rewrite andb_false_iff in D0.
                        rewrite andb_false_iff.
                        destruct D0;[left;apply H|right].
                        rewrite mem_Union_false.
                        intros s Hs.
                        rewrite mem_Union_false in H.
                        apply H.
                        rewrite map_map.
                        rewrite in_map_iff in Hs.
                        destruct Hs as [[[]] [ -> Hs]].
                        rewrite in_map_iff;exists (s, g0,l0);repeat split.
                        apply Hs.
                      + apply WS.
                    }
                    set (nf := (formula_to_nraenv_gen query_to_nraenv
                                                     ((sort basesort q, Group_By TRcd g) :: map fst env) f)).
                    assert (D7 : exists filter_ldq1 ,
                               lift_filter
                                 (fun d1 : data =>
                                    match
                                      nraenv_core_eval
                                        br (rec_sort (instance_to_bindings i))
                                        (nraenv_to_nraenv_core (NRAEnvIsTrueB nf))
                                        (drec (("slc"%string, d1) :: ("tl"%string, env_to_data env) :: nil)) d1
                                    with
                                    | Some (dbool b) => Some b
                                    | _ => None
                                    end) map_3 = Some filter_ldq1).
                    {
                      rewrite lift_filter_some_filter_alt.
                      exists   (filter
                             (fun x : data =>
                                match
                                  match
                                    nraenv_core_eval br (rec_sort (instance_to_bindings i)) (nraenv_to_nraenv_core (NRAEnvIsTrueB nf))
                                                     (drec (("slc"%string, x) :: ("tl"%string, env_to_data env) :: nil)) x                                                     
                                  with
                                  | Some (dbool b) => Some b
                                  | _ => None
                                  end
                                with
                                | Some b => b
                                | None => false
                                end) map_3);apply eq_refl.
                      intros d Hd.
                      destruct (Aux1 _ Hd) as [lt ->].
                      unfold nraenv_eval in IHf1.
                      assert (Hfe := IHf1 ((sort basesort q, Group_By _ g , lt) :: env) ).
                      simpl env_to_data in Hfe.
                      subst nf.
                      assert (Typ1 :
                                well_typed_e ((sort basesort q, Group_By TRcd g, lt) :: env) = true).
                      {
                        refine (well_typed_e_env_g _ _ i _ _ _ Typ Ti (proj1 Tc) (proj2 (proj1 (proj1 (proj1 Tq))))).
                        intros.
                        destruct ( Aux01 _ Hd) as [lt2 [Hlt2 Glt2]].
                        destruct lt2.
                        * destruct (Partition.in_map_snd_partition_diff_nil _ _ _ Hlt2 eq_refl).
                        * rewrite <- mem_tuple_as_pairs_listT in H.                          
                          rewrite (Oeset.permut_mem_bool_eq _ Glt2) in H.
                          apply  mem_tuple_as_pairs_listT.                          
                          rewrite Oeset.mem_bool_true_iff in H.
                          destruct H as [t1 [Ht1 Ht2]].
                          rewrite Oeset.mem_bool_true_iff.
                          exists t1;split;[apply Ht1| ].
                          refine (Partition.in_map_snd_partition _ _ _ _ _ Hlt2 Ht2).
                      }
                      assert (Hfe := Hfe Typ1 (Aux0 _ Hd)).
                      assert (Hfe := Hfe  (extract_grouping_criteria_env_g_is_translatable
                                            _ _ _ _ D1 Te)).
                      
                      assert (Wf :  well_formed_f basesort ((sort basesort q, Group_By TRcd g, lt) :: env) f = true).
                      {
                        rewrite <- (proj2 Wq).
                        apply well_formed_f_eq.
                        constructor;[|apply weak_equiv_env_refl].
                        repeat split.
                        unfold default_tuple.
                        simpl.
                        rewrite (Fset.equal_eq_2 _ _ _ _ (labels_mk_tuple _ _ _)).
                        apply Fset.equal_refl.
                      }
                      assert (Wf2 :
                                more_well_formed_f
                                  ((sort basesort q, Group_By TRcd g, lt) :: env) f = true).
                      {
                        rewrite <- (proj2 (proj1 W2q)).
                        apply more_well_formed_f_eq.
                        constructor;[|apply weak_equiv_env_refl].
                        repeat split.
                        cbn.
                        unfold default_tuple.
                        rewrite (Fset.equal_eq_2 _ _ _ _ (labels_mk_tuple _ _ _)).
                        apply Fset.equal_refl.
                      }                      
                      assert (Hfe := Hfe Wf Wf2).
                      assert(G2 := NRAEnvIsTrueB_eq _ _ _ _ _ Hfe).
                      unfold nraenv_eval in G2.
                      rewrite G2.
                      exists
                        (Bool3.Bool.is_true (B TRcd)
                                           (eval_sql_formula (eval_query i) ((sort basesort q, Group_By TRcd g, lt) :: env) f));apply eq_refl.
                    }
                    destruct D7 as [filter_ldq1 D7].
                    assert (X1 := D7).
                    assert (Aux2 : forall x : data, In x filter_ldq1 -> exists y : list data, x = dcoll y).
                    { intros d Hd.
                      apply lift_filter_Some_filter in D7.
                      subst filter_ldq1.
                      rewrite filter_In in Hd.
                      destruct Hd as [Hd1 Hd2].
                      destruct (Aux1 _ Hd1);subst.
                      exists (listT_to_listD x) ;apply eq_refl. }
                    set (nsl := (select_list_to_nraenv ((sort basesort q, Group_By TRcd g) :: map fst env)
                                                      (_Select_List _  l))).
                    assert (D8 :
                              exists map_4, lift_map
                                         (fun din : data =>
                                            nraenv_core_eval
                                              br (rec_sort (instance_to_bindings i))
                                              (nraenv_to_nraenv_core nsl)
                                              (drec (("slc"%string, din)
                                                       :: ("tl"%string, env_to_data env)
                                                       :: nil)) din) filter_ldq1 = Some map_4).
                    {
                      apply lift_map_Some_spec.
                      intros d Hd.
                      clear G1.
                      assert (G1 := lift_filter_some_in _ _ X1 _ Hd).
                      destruct G1 as [G1 G2].
                      destruct (Aux1 _ G1);subst.
                      assert (A1 := select_list_to_nraenv_is_sound br (rec_sort (instance_to_bindings i))).
                      assert (A1 := A1 ((sort basesort q, Group_By _ g, x) :: env) (dcoll (listT_to_listD x))).
                      assert (Typ1 :
                                well_typed_e ((sort basesort q, Group_By TRcd g, x) :: env) = true).
                      {
                        refine (well_typed_e_env_g _ _ i _ _ _ Typ Ti (proj1 Tc) (proj2 (proj1 (proj1 (proj1 Tq))))).
                        intros.
                        destruct ( Aux01 _ G1) as [lt2 [Hlt2 Glt2]].
                        destruct lt2.
                        * destruct (Partition.in_map_snd_partition_diff_nil _ _ _ Hlt2 eq_refl).
                        * rewrite <- mem_tuple_as_pairs_listT in H.                          
                          rewrite (Oeset.permut_mem_bool_eq _ Glt2) in H.
                          apply  mem_tuple_as_pairs_listT.                          
                          rewrite Oeset.mem_bool_true_iff in H.
                          destruct H as [t1 [Ht1 Ht2]].
                          rewrite Oeset.mem_bool_true_iff.
                          exists t1;split;[apply Ht1| ].
                          refine (Partition.in_map_snd_partition _ _ _ _ _ Hlt2 Ht2).
                      }                      
                      assert (A1 :=
                                fun H =>
                                  A1 Typ1 H
                                     (extract_grouping_criteria_env_g_is_translatable
                                        _ _ _ _ D1 Te)).
                      unfold nraenv_eval in A1.
                      simpl env_to_data in A1.
                      subst nsl.
                      rewrite A1.
                      - refine (ex_intro _ _ eq_refl).
                      - apply (Aux0 _ G1).
                      - rewrite <- (proj2 W2q).
                        apply well_formed_s_eq.
                        constructor.
                        + repeat split.
                          simpl.
                          unfold default_tuple.
                          rewrite (Fset.equal_eq_2 _ _ _ _ (labels_mk_tuple _ _ _)).
                          apply Fset.equal_refl.
                        + apply weak_equiv_env_refl.
                    }
                    destruct D8 as [map_4 D8].                 
                    {
                      exists map_4;split.
                      - destruct (proj1 (Permutation_listT_spec _ _) Pq) as [ltq [Bq <-]].                        
                        refine(Permutation_trans (permutation_bagT_listT_map _ _) _).
                        refine (_permut_Permutation_listT_lift_map_list_tuple _ _ _ _ Aux2 _ _ _ D8 _);swap 2 4.
                        + intros l1 l2 Hl1 Hl.
                          apply mk_tuple_eq;[apply Fset.equal_refl| ].
                          intros a Ha1 Ha2.
                          destruct Oset.find;[|apply eq_refl].
                          apply interp_aggterm_eq.
                          apply equiv_env_cons_eq;[|apply eq_refl|apply Hl].
                          apply env_slice_eq_1.
                          apply Partition.compare_list_t.
                          apply Hl.
                        + apply lift_filter_Some_filter in D7.
                          rewrite D7.
                          assert (F1 := @group_by_translation_is_sound_permut_permut_aggterm g br i q   la _ dunit _ map_3 A3 D1   (Hq dunit)).
                          unfold nraenv_eval in F1;simpl in F1.
                          unfold olift in F1.
                          rewrite Hq in F1.
                          unfold edot,lift in F1;simpl in F1.
                          rewrite D4 in F1;simpl in F1.
                          destruct (lift_map _ (bdistinct map_ldq1) ) as [pp| ] eqn:Dpp;[|discriminate].
                          inversion D5;subst;clear D5.
                          assert (D5 := Dpp);clear Dpp.
                          simpl in F1.
                          unfold edot in D6;simpl in D6;rewrite D6 in F1.
                          assert (F1 := F1  Pq eq_refl).
                          assert (Aux11 : forall d : data, In d map_3 -> exists ld : list data, d = dcoll ld).
                          { intros.
                            apply Aux1 in H.
                            destruct H;subst.
                            exists (listT_to_listD x);apply eq_refl.
                          }
                          refine (_permut_Perm_filter_inside _ _ _ _ Aux11 _ _ );swap 1 2.
                          * refine (_permut_trans _ _ F1);[do 6 intro;apply Permutation_trans| ].
                            apply _permut_refl;intros;apply Permutation_refl.
                          * intros lt ld Hlt Hld Hx.
                            destruct (lift_filter_some_in_bool _ _ X1 _ Hld) as [z Hz].
                            rewrite Hz.
                            clear G1.
                            destruct nraenv_core_eval eqn:Hd in Hz;try discriminate.
                            destruct d;try discriminate.
                            inversion Hz.
                            assert (G5 := Hx).
                            rewrite Permutation_listT_spec in G5.
                            destruct G5 as [lt' [G5 <-]].
                            assert (Aux3 : well_formed_e _ ((sort basesort q, Group_By _ g , lt') :: env) = true).
                            {
                              apply Oeset.permut_sym in G5.
                              rewrite (well_formed_e_eq (equiv_env_cons_eq_3 G5 _ _ _) ).
                              simpl.
                              repeat rewrite andb_true_iff.
                              repeat split.
                              - apply Wq.
                              - destruct lt.
                                + destruct (Partition.in_map_snd_partition_diff_nil _ _ _ Hlt eq_refl).
                                + simpl;rewrite andb_true_iff;split.
                                  * rewrite forallb_forall.
                                    intros.
                                    apply Oset.eq_bool_refl.
                                  * rewrite forallb_forall.
                                    intros y Hy.
                                    rewrite forallb_forall.
                                    intros e He.
                                    rewrite Oset.eq_bool_true_iff.
                                    assert (Hlt2 := Hlt).
                                    rewrite in_map_iff in Hlt.
                                    destruct Hlt as [ [lv4 lt4] [P2 P3]].
                                    simpl in P2.
                                    subst lt4.
                                    assert (P5 := Partition.partition_homogeneous_values _ _ _ _ _ P3).
                                    assert (P6 := P5 _ (or_intror Hy)).
                                    simpl in P6.
                                    assert (P5 := P5 _ (or_introl eq_refl)).
                                    simpl in P5.
                                    rewrite <- P5 in P6.
                                    rewrite <- ListFacts.map_eq in P6.
                                    apply (P6 _ He).
                              - rewrite forallb_forall;intros.
                                apply A2.
                                rewrite Febag.mem_unfold.
                                apply Oeset.in_mem_bool.
                                refine (Partition.in_map_snd_partition _ _ _ _ _ Hlt H).
                              - rewrite Fset.is_empty_spec,Fset.equal_spec.
                                intros a.
                                rewrite Fset.is_empty_spec in D0.
                                rewrite Fset.equal_spec in D0.
                                assert (D0 := D0 a).
                                rewrite Fset.empty_spec,Fset.mem_inter in D0.
                                rewrite Fset.empty_spec,Fset.mem_inter.
                                rewrite andb_false_iff in D0.
                                rewrite andb_false_iff.
                                destruct D0;[left;apply H|right].
                                rewrite mem_Union_false.
                                intros s Hs.
                                rewrite mem_Union_false in H.
                                apply H.
                                rewrite map_map.
                                rewrite in_map_iff in Hs.
                                destruct Hs as [[[]] [ -> Hs]].
                                rewrite in_map_iff;exists (s, g0,l0);repeat split.
                                apply Hs.
                              - apply WS.
                            }
                            assert (G3 : well_formed_f basesort ((sort basesort q, Group_By _ g, lt') :: env) f = true).
                            {
                              rewrite <- (proj2 Wq).
                              apply well_formed_f_eq.
                              constructor;[|apply weak_equiv_env_refl].
                              repeat split.
                              unfold default_tuple.
                              simpl.
                              rewrite (Fset.equal_eq_2 _ _ _ _ (labels_mk_tuple _ _ _)).
                              apply Fset.equal_refl.
                            }
                            assert (G4 : is_a_translatable_e ((sort basesort q, Group_By _ g, lt') :: env) = true).
                            {
                              refine (extract_grouping_criteria_env_g_is_translatable _ _ _ _ D1 Te).
                            }
                            assert (GG : well_typed_e ((sort basesort q, Group_By TRcd g, lt') :: env) = true).
                            {
                              refine (well_typed_e_env_g _ _ i _ _ _ Typ Ti (proj1 Tc) (proj2 (proj1 (proj1 (proj1 Tq))))).
                              intros.
                              destruct ( Aux01 _ Hld) as [lt2 [Hlt2 Glt2]].
                              destruct lt2.
                              * destruct (Partition.in_map_snd_partition_diff_nil _ _ _ Hlt2 eq_refl).
                              * rewrite <- mem_tuple_as_pairs_listT in H.                          
                                rewrite (Oeset.permut_mem_bool_eq _ Glt2) in H.
                                apply  mem_tuple_as_pairs_listT.                          
                                rewrite Oeset.mem_bool_true_iff in H.
                                destruct H as [t1 [Ht1 Ht2]].
                                rewrite Oeset.mem_bool_true_iff.
                                exists t1;split;[apply Ht1| ].
                                refine (Partition.in_map_snd_partition _ _ _ _ _ Hlt2 Ht2).
                            }
                            assert (Wf2 :
                                      more_well_formed_f
                                        
                                        ((sort basesort q, Group_By TRcd g, lt') :: env) f = true).
                            {
                              rewrite <- (proj2 (proj1 W2q)).
                              apply more_well_formed_f_eq.
                              constructor;[|apply weak_equiv_env_refl].
                              repeat split.
                              cbn.
                              unfold default_tuple.
                              rewrite (Fset.equal_eq_2 _ _ _ _ (labels_mk_tuple _ _ _)).
                              apply Fset.equal_refl.
                            }

                            assert (Hf2 := IHf1 ((sort basesort q, Group_By _ g , lt') :: env) GG Aux3 G4 G3  Wf2).
                            unfold nraenv_eval in Hf2;simpl in Hf2.
                            subst;clear Hz.
                            assert(G2 := NRAEnvIsTrueB_eq _ _ _ _ _ Hf2  (dcoll (listT_to_listD lt')) ).
                            unfold nraenv_eval in G2.
                            subst nf.
                            rewrite Hd in G2.
                            inversion G2.
                            assert (G6 : equiv_env _ (env_g _ env (Group_By _ g) lt) ((sort basesort q, Group_By _ g , lt') :: env)).
                            {
                              apply equiv_env_cons_eq;[|apply eq_refl| apply G5].
                              destruct (ListSort.quicksort OTuple lt) eqn:P1.
                              - rewrite quicksort_nil in P1;subst.
                                destruct (Partition.in_map_snd_partition_diff_nil _ _ _ Hlt eq_refl).
                              - apply (ws_instance_tuples _ env q WI).
                                rewrite Febag.mem_unfold.
                                rewrite Oeset.mem_bool_true_iff.
                                exists t;split;[apply Oeset.compare_eq_refl| ].
                                refine (Partition.in_map_snd_partition _ _ _ _ _ Hlt _).
                                rewrite (ListSort.In_quicksort OTuple),P1;left;apply eq_refl.
                            }
                            rewrite eval_sql_formula_eq with (env2 := ((sort basesort q, Group_By _ g , lt') :: env));
                              [|apply G6].
                            apply eq_refl.
                        + intros.
                          destruct (select_list_eval_is_drec _ _ _ ((sort basesort q, Group_By _ g,a) :: env) _  H0) as [r ->].
                          apply f_equal.
                          clear G1.
                          assert (G1 : equiv_env _ (env_g _ env (Group_By _ g) a) ((sort basesort q, Group_By _ g,a) :: env)).
                          {
                            apply equiv_env_cons_eq_1.
                            rewrite filter_In in H.
                            destruct (ListSort.quicksort OTuple a) eqn:F1.
                            - rewrite quicksort_nil in F1;subst.
                              destruct (Partition.in_map_snd_partition_diff_nil _ _ _ (proj1 H) eq_refl).
                            - apply (ws_instance_tuples _ env q WI).
                              rewrite Febag.mem_unfold.
                              rewrite Oeset.mem_bool_true_iff.
                              exists t;split;[apply Oeset.compare_eq_refl| ].
                              refine (Partition.in_map_snd_partition _ _ _ _ _ (proj1 H) _).
                              rewrite (ListSort.In_quicksort OTuple),F1;left;apply eq_refl.
                          }
                          (* rewrite <- (select_list_to_nraenv_equiv_env_eq (equiv_env_equiv_nenv G1)) in Dsl. *)
                          assert (J1 : is_a_translatable_e ((sort basesort q, Group_By _ g, a) :: env) = true)
                            by refine (extract_grouping_criteria_env_g_is_translatable _ _ _ _ D1 Te).
                          rewrite <- ( is_a_translatable_e_eq (equiv_env_weak_equiv_env _ _ _ G1)) in J1.
                          assert (T1 : well_formed_s _ (env_g _ env (Group_By _ g) a) l = true).
                          {
                            rewrite <- (proj2 W2q).
                          apply well_formed_s_eq.
                          { constructor.
                            - repeat split.
                              rewrite filter_In in H.
                              destruct (ListSort.quicksort OTuple a ) eqn:N1.
                              + rewrite quicksort_nil in N1;subst a.
                                destruct (Partition.in_map_snd_partition_diff_nil _ _ _ (proj1 H) eq_refl).
                              + clear A2;assert (A2 : t inBE (eval_query i) env q).
                                { rewrite Febag.mem_unfold.
                                  rewrite Oeset.mem_bool_true_iff.
                                  exists t;split;[apply Oeset.compare_eq_refl| ].
                                  refine (Partition.in_map_snd_partition _ _ _ _ _ (proj1 H) _).
                                  rewrite (ListSort.In_quicksort OTuple),N1;left;apply eq_refl.
                                }
                                simpl.
                                unfold default_tuple.
                                rewrite (Fset.equal_eq_2 _ _ _ _ (labels_mk_tuple _ _ _)).
                                apply (ws_instance_tuples _ _ q WI _ A2).
                            - apply weak_equiv_env_refl.
                            }
                          }
                          assert (T2 : well_formed_e _ (env_g _ env (Group_By _ g) a) = true).
                          {
                            simpl.
                            rewrite filter_In in H.
                            assert (K0 : match ListSort.quicksort OTuple a with
                                         | nil => emptysetS
                                         | t :: _ => labels t
                                         end =S= sort basesort q).
                            {
                              destruct (ListSort.quicksort OTuple a ) eqn:N1.
                              + rewrite quicksort_nil in N1;subst a.
                                destruct (Partition.in_map_snd_partition_diff_nil _ _ _ (proj1 H) eq_refl).
                              + apply (ws_instance_tuples _ env q WI).
                                rewrite Febag.mem_unfold.
                                rewrite Oeset.mem_bool_true_iff.
                                exists t;split;[apply Oeset.compare_eq_refl| ].
                                refine (Partition.in_map_snd_partition _ _ _ _ _ (proj1 H) _).
                                rewrite (ListSort.In_quicksort OTuple),N1;left;apply eq_refl.
                            }
                            assert (K1 : labels (default_tuple _ match ListSort.quicksort OTuple a with
                                                                 | nil => emptysetS
                                                                 | t :: _ => labels t
                                                                 end) =S= labels (default_tuple _ (sort basesort q))).
                            {
                              unfold default_tuple.
                              rewrite (Fset.equal_eq_1 _ _ _ _ (labels_mk_tuple _ _ _)).
                              rewrite (Fset.equal_eq_2 _ _ _ _ (labels_mk_tuple _ _ _)).
                              apply K0.
                            }
                            repeat rewrite andb_true_iff.
                            repeat split.
                            - assert (Y1 := proj2 (proj1 (proj1 Wq))).
                              rewrite forallb_forall.
                              rewrite forallb_forall in Y1.
                              intros y Hy.
                              rewrite <- (Y1 _ Hy).
                              apply well_formed_ag_eq.
                              constructor;[|apply weak_equiv_env_refl].
                              repeat split.
                              apply K1.
                            - destruct a.
                              + destruct (Partition.in_map_snd_partition_diff_nil _ _ _ (proj1 H) eq_refl).
                              + simpl;rewrite andb_true_iff;split.
                                * rewrite forallb_forall.
                                  intros.
                                  apply Oset.eq_bool_refl.
                                * rewrite forallb_forall.
                                  intros y Hy.
                                  rewrite forallb_forall.
                                  intros e He.
                                  rewrite Oset.eq_bool_true_iff.
                                  assert (Hlt2 := (proj1 H)).
                                  rewrite in_map_iff in H.
                                  destruct H as [[ [lv4 lt4] [P2 P3]] _].
                                  simpl in P2.
                                  subst lt4.
                                  assert (P5 := Partition.partition_homogeneous_values _ _ _ _ _ P3).
                                  assert (P6 := P5 _ (or_intror Hy)).
                                  simpl in P6.
                                  assert (P5 := P5 _ (or_introl eq_refl)).
                                  simpl in P5.
                                  rewrite <- P5 in P6.
                                  rewrite <- ListFacts.map_eq in P6.
                                  apply (P6 _ He).
                            - rewrite forallb_forall;intros.
                              destruct (ListSort.quicksort OTuple a ) eqn:N1.
                              + rewrite quicksort_nil in N1;subst a.
                                destruct (Partition.in_map_snd_partition_diff_nil _ _ _ (proj1 H) eq_refl).
                              + assert (A1 : x inBE (eval_query i) env q).
                                { rewrite Febag.mem_unfold.
                                  rewrite Oeset.mem_bool_true_iff.
                                  exists x;split;[apply Oeset.compare_eq_refl| ].
                                  refine (Partition.in_map_snd_partition _ _ _ _ _ (proj1 H) H1).
                                }
                                clear A2;assert (A2 : t inBE (eval_query i) env q).
                                { rewrite Febag.mem_unfold.
                                  rewrite Oeset.mem_bool_true_iff.
                                  exists t;split;[apply Oeset.compare_eq_refl| ].
                                  refine (Partition.in_map_snd_partition _ _ _ _ _ (proj1 H) _).
                                  rewrite (ListSort.In_quicksort OTuple),N1;left;apply eq_refl.
                                }
                                rewrite (Fset.equal_eq_1 _ _ _ _ (ws_instance_tuples _ env q WI _ A1)).
                                rewrite (Fset.equal_eq_2 _ _ _ _ (ws_instance_tuples _ env q WI _ A2)).
                                apply Fset.equal_refl.
                            - rewrite Fset.is_empty_spec,Fset.equal_spec.
                              intros e.
                              rewrite Fset.is_empty_spec in D0.
                              rewrite Fset.equal_spec in D0.
                              assert (D0 := D0 e).
                              rewrite Fset.empty_spec,Fset.mem_inter in D0.
                              rewrite Fset.empty_spec,Fset.mem_inter.
                              rewrite andb_false_iff in D0.
                              rewrite andb_false_iff.
                              rewrite (Fset.mem_eq_2 _ _ _ K0).
                              destruct D0;[left;apply H1|right].
                              rewrite mem_Union_false.
                              intros s Hs.
                              rewrite mem_Union_false in H1.
                              apply H1.
                              rewrite map_map.
                              rewrite in_map_iff in Hs.
                              destruct Hs as [[[]] [ -> Hs]].
                              rewrite in_map_iff;exists (s, g0,l0);repeat split.
                              apply Hs.
                            - apply WS.
                          }
                          assert (K0 := select_list_to_nraenv_is_sound
                                         br  (rec_sort (instance_to_bindings i)) (env_g TRcd env (Group_By TRcd g) a)).
                          assert (K1 : well_typed_e (env_g TRcd env (Group_By TRcd g) a) = true ).
                          {
                            rewrite (well_typed_e_eq  G1).                            
                            refine (well_typed_e_env_g _ _ i _ _ _ Typ Ti (proj1 Tc) (proj2 (proj1 (proj1 (proj1 Tq))))).                            
                            intros.
                            rewrite <- mem_tuple_as_pairs_listT
                              in H1.                                                      
                            apply  mem_tuple_as_pairs_listT.                          
                            rewrite Oeset.mem_bool_true_iff in H1.
                            destruct H1 as [t1 [Ht1 Ht2]].
                        rewrite Oeset.mem_bool_true_iff.
                        exists t1;split;[apply Ht1| ].
                        rewrite filter_In in H.
                        refine (Partition.in_map_snd_partition _ _ _ _ _ (proj1 H) Ht2).
                          }
                          assert (K0 := K0 
                                         (dcoll (map (fun x : tuple => drec (tuple_as_dpairs x)) a))
                                         K1 T2 J1 _ T1).
                          rewrite ((select_list_to_nraenv_is_sound_equiv_env br _ _ G1) K1 T2 J1 _ T1) in K0.
                          rewrite (select_list_to_nraenv_equiv_env_eq (equiv_env_equiv_nenv G1)) in K0.
                          subst nsl.
                          simpl map in K0.
                          simpl env_to_data in K0.
                          unfold listT_to_listD in K0.
                          unfold nraenv_eval in K0.
                          rewrite K0 in H0.
                          inversion H0.
                          apply tuple_as_dpairs_eq.
                          apply mk_tuple_eq.
                          unfold sort_of_select_list.
                          rewrite map_map.
                          * rewrite Fset.equal_spec.
                            intro e.
                            rewrite (Fset.mem_eq_2 _ _ _ (Fset.mk_set_idem _ _ )).
                            do 2 apply f_equal;apply map_ext;intros [ ];apply eq_refl.
                          * intros e.
                            rewrite (Fset.mem_eq_2 _ _ _ (Fset.mk_set_idem _ _ )).
                            intros He _.
                            apply eq_refl.
                        + intros.
                          assert (J1 : is_a_translatable_e ((sort basesort q, Group_By _ g, l1) :: env) = true)
                            by refine (extract_grouping_criteria_env_g_is_translatable _ _ _ _ D1 Te).
                          refine
                            (eq_trans
                               (select_list_to_nraenv_ignores_did _ _ _ _  _ _ (dcoll (listT_to_listD l2)))
                               _).
                          rewrite filter_In in H.
                          assert (J3 :  well_formed_e _ ((sort basesort q, Group_By _ g, l1) :: env) = true).
                          { simpl;repeat rewrite andb_true_iff;repeat split.
                            - apply Wq.
                            - destruct l1;
                                [destruct (Partition.in_map_snd_partition_diff_nil _ _ _ (proj1 H) eq_refl)| ].
                              simpl.
                              rewrite andb_true_iff;split.
                              + rewrite forallb_forall;intros;apply Oset.eq_bool_refl.
                              + rewrite forallb_forall.
                                intros y Hy.
                                rewrite forallb_forall.
                                intros e He.
                                rewrite Oset.eq_bool_true_iff.
                                assert (Hlt := proj1 H).
                                rewrite in_map_iff in Hlt.
                                destruct Hlt as [ [lv4 lt4] [P2 P3]].
                                simpl in P2.
                                subst lt4.
                                assert (P5 := Partition.partition_homogeneous_values _ _ _ _ _ P3).
                                assert (P6 := P5 _ (or_intror Hy)).
                                simpl in P6.
                                assert (P5 := P5 _ (or_introl eq_refl)).
                                simpl in P5.
                                rewrite <- P5 in P6.
                                rewrite <- ListFacts.map_eq in P6.
                                apply (P6 _ He).
                            - rewrite forallb_forall.
                              intros.
                              apply A2.
                              rewrite Febag.mem_unfold.
                              rewrite Oeset.mem_bool_true_iff.
                              exists x;split;[apply Oeset.compare_eq_refl| ].
                              refine (Partition.in_map_snd_partition _ _ _ _ _ (proj1 H) H2).
                            - replace (map (fun slc : nslice * list tuple => let (y, _) := slc in let (sa, _) := y in sa) env)
                                with (map fst (map fst env));[apply D0| ].
                              rewrite map_map;apply map_ext;intros [ []];apply eq_refl.
                            - apply WS. }
                          rewrite <- H1.
                          assert (GG : well_typed_e ((sort basesort q, Group_By TRcd g, l1) :: env) = true).
                      {
                        refine (well_typed_e_env_g _ _ i _ _ _ Typ Ti (proj1 Tc) (proj2 (proj1 (proj1 (proj1 Tq))))).                        
                        intros.
                        rewrite <- mem_tuple_as_pairs_listT
                          in H2.                                                      
                        apply  mem_tuple_as_pairs_listT.                          
                        rewrite Oeset.mem_bool_true_iff in H2.
                        destruct H2 as [t1 [Ht1 Ht2]].
                        rewrite Oeset.mem_bool_true_iff.
                        exists t1;split;[apply Ht1| ].
                        refine (Partition.in_map_snd_partition _ _ _ _ _ (proj1 H) Ht2).
                      }        
                          refine (select_list_to_nraenv_is_sound_equiv_env br  _ _ (equiv_env_cons_eq_3 H0 _ _ _)  GG J3  J1 _ _).
                          rewrite <- (proj2 W2q).
                          apply well_formed_s_eq.
                          rewrite weak_equiv_env_sym.
                          { constructor.
                            - repeat split;simpl.
                              unfold default_tuple.
                              rewrite (Fset.equal_eq_1 _ _ _ _ (labels_mk_tuple _ _ _)).
                              apply Fset.equal_refl.
                            - apply weak_equiv_env_refl.
                      }
                      - intros.
                        destruct f;simpl;rewrite Hq;simpl;rewrite D4;simpl;
                        unfold olift,lift,olift2 in *;unfold  lift_oncoll in *;
                          cbn in G1;cbn;rewrite G1;simpl;rewrite D6;simpl;
                        subst nf;
                        try (simpl in D7;rewrite D7;
                        simpl;
                        subst nsl;
                        unfold select_list_to_nraenv in D8;
                        rewrite D8;apply eq_refl).
                        subst nsl.
                        unfold select_list_to_nraenv in D8.
                        simpl in D7.
                        apply lift_filter_Some_filter in D7.
                        rewrite D7 in D8.
                        rewrite ListFacts.filter_true in D8.
                        + simpl;simpl in D8;rewrite D8.
                          apply eq_refl.
                        + intros x Hx.
                          assert (J1 := NRAEnvIsTrueB_eq).
                          unfold nraenv_eval in J1.
                          rewrite J1 with (b1:= Bool3.Bool.true (B TRcd)).
                          * apply Bool3.Bool.true_is_true_alt.
                          *  intros did1.
                             cbn.
                             apply f_equal.
                             rewrite boolB_to_data_true.
                             apply normalize_data_dtrue.        
                    }
              }
        Qed.
        
        Lemma query_to_nraenv_is_sound_beautiful :
          forall br i did,
            well_sorted_instance i = true ->
            well_typed_instance i = true ->
            forall env,     
              well_typed_e env = true ->
              well_formed_e _ env = true ->
              is_a_translatable_e env = true ->                   
              forall q,
                is_complete_instance_q i q = true ->
                is_translatable_q q = true ->
                well_formed_q basesort env q = true ->                
                more_well_formed_q env q = true ->      
                match nraenv_eval br (rec_sort (instance_to_bindings i)) (query_to_nraenv (map fst env) q) (env_to_data env) did with
                | Some (dcoll dq) => Permutation (bagT_to_listD (eval_query i env q )) dq
                | _ => False
                end.
        Proof.
          intros br i did Wi Ti env Typ We Te q Tc Tq Wq W2q.
          destruct (query_to_nraenv_is_sound br _ Wi Ti q Tc Tq env Typ We Te Wq W2q)
            as [lq [H1 H2]].
          rewrite H2;apply H1.
        Qed.

        Definition query_to_nraenv_top := query_to_nraenv nil.

        Lemma query_to_nraenv_top_is_sound :
          forall br i,
            well_sorted_instance i = true ->
            well_typed_instance i = true ->
            forall q,              
              is_complete_instance_q i q = true ->
              is_translatable_q q = true ->
              well_formed_q basesort nil q = true ->              
              more_well_formed_q nil q = true ->
              match nraenv_eval_top br (query_to_nraenv_top q) (instance_to_bindings i) with
              | Some (dcoll dq) => Permutation (bagT_to_listD (eval_query i nil q )) dq
              | _ => False
              end.
        Proof.
          intros br i Wi Ti q Tc Tq Wq W2q.
          destruct (query_to_nraenv_is_sound br _ Wi Ti q Tc Tq nil eq_refl eq_refl eq_refl Wq W2q)
            as [lq [H1 H2]].
          simpl in *. unfold nraenv_eval_top, query_to_nraenv_top. rewrite H2.
          apply H1.
        Qed.

      End Lemmas.

    End Sec.

End QueryToNRAEnv.
