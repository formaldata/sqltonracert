(************************************************************************************)
(**                                                                                 *)
(**                          The SQLToNRACert Library                               *)
(**                                                                                 *)
(**                       LMF, CNRS & Université Paris-Saclay                       *)
(**                                                                                 *)
(**                        Copyright 2016-2022 : FormalData                         *)
(**                                                                                 *)
(**         Authors: Véronique Benzaken                                             *)
(**                  Évelyne Contejean                                              *)
(**                  Mohammed Hachmaoui                                             *)
(**                  Chantal Keller                                                 *)
(**                                                                                 *)
(************************************************************************************)

Require Import ZArith.
From SQLFS Require Import FlatData SqlAlgebra Formula.
Require Export TnullBD TnullTD TnullEN TnullATN TnullIN TnullFN.
From SQLFS Require Import Bool3 Formula GenericInstance.

Require Import QueryToNRAEnv FormulaToNRAEnv TupleToData MoreSugar AxiomFloat.

Section Sec.

  Variable faac : float_add_assoc_comm.

  Global Instance tnull_QN basesort :
    @QueryToNRAEnv.QueryToNRAEnv 
      tnull_frt tnull_TD tnull_SN  tnull_EN tnull_FTN
      (tnull_AN faac) (tnull_ATN faac) relname ORN  (tnull_IN basesort)
      tnull_BD tnull_PN (tnull_FN faac basesort).
  Qed.
  
  Definition eval_sql_query_in_state (db : SqlSyntax.db_state) q :=
    (@eval_query
       TNull relname (_basesort TNull db) (_instance TNull db) 
       Bool3.unknown3 SqlSyntax.contains_nulls nil q).

End Sec.
