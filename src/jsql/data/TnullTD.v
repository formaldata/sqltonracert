(************************************************************************************)
(**                                                                                 *)
(**                          The SQLToNRACert Library                               *)
(**                                                                                 *)
(**                       LMF, CNRS & Université Paris-Saclay                       *)
(**                                                                                 *)
(**                        Copyright 2016-2022 : FormalData                         *)
(**                                                                                 *)
(**         Authors: Véronique Benzaken                                             *)
(**                  Évelyne Contejean                                              *)
(**                  Mohammed Hachmaoui                                             *)
(**                  Chantal Keller                                                 *)
(**                                                                                 *)
(************************************************************************************)

(* Require Import Bool. *)
(* Require Export ExtendedForeignRT. *)

Require Import TupleToData.

Require Export Qcert.Compiler.Enhanced.EnhancedModel.
Require Import String NArith ZArith.
From SQLFS Require Import TuplesImpl ListFacts.

From SQLFS Require Import GenericInstance SqlSyntax Bool3 Formula.

Section Sec.

  Export TupleToData.
  Import Tuple Values.
  
  Notation tnull_frt := enhanced_foreign_runtime.
    
  Definition attribute_to_string (a:attribute TNull) : string :=
    match a with
    | Attr_string s => "0" ++ s
    | Attr_Z s => "1" ++ s
    | Attr_bool s => "2" ++ s
    | Attr_float s => "3" ++ s
    end.
  
  Lemma attribute_to_string_compare_lt :
    forall a1 a2 : attribute TNull,
      Oset.compare (OAtt TNull) a1 a2 = Lt ->
      Oset.compare
        Ostring (attribute_to_string a1)
        (attribute_to_string a2) = Lt.
  Proof.
    intros [s1|s1|s1|s1] [s2|s2|s2|s2];auto.
  Qed.
  
  Definition value_to_data : NullValues.value -> data.
    intros [[s | ] | [z | ] | [b | ] | [f | ]];
      [exact (dleft (dstring s))|exact (dright (dstring ""))
       |exact (dleft (dnat z))|exact (dright (dnat 0))
       |exact (dleft (dbool b))| exact (dright (dbool true))
       |exact (dleft (dfloat f))| exact (dright (dfloat float_zero))].
  Defined.
  
  Lemma value_to_data_inj :
    forall a1 a2, value_to_data a1 = value_to_data a2 -> a1 = a2.
  Proof.
    intros [[s1| ]|[z1| ]|[b1| ]|[f1| ]][[s2| ]|[z2| ]|[b2| ]|[f2| ]] H1;
      inversion H1;apply eq_refl.
  Qed.
  
  Lemma value_to_data_normalize :
    forall br v,
      value_to_data v = normalize_data br (value_to_data v).
  Proof.
    intros br.
    intros [[ ]|[ ]|[ ]|[ ]];try apply eq_refl.
  Qed.

    Lemma type_of_default_value_eq : forall t : type ,
      Tuple.type_of_value TNull 
                          (Tuple.default_value  TNull t) = t.
  Proof.
    intros [ ];try apply eq_refl.
  Qed.

  
  Global Instance tnull_TD : TupleToData.TupleToData :=
    TupleToData.mk_C
      tnull_frt TNull OT
      type_of_default_value_eq attribute_to_string attribute_to_string_compare_lt _
      value_to_data_inj value_to_data_normalize _
      SqlSyntax.contains_nulls_eq.

   Lemma find_tuple_as_pairs_TNull_idem :
    forall t a,
      Oset.find OAN a (tuple_as_pairs TNull t) =
      Oset.find OAN a t.
  Proof.
    intros.
    destruct (a inS? labels TNull t) eqn:D1.
    - refine (eq_trans
                (tuple_tuple_as_pairs_dot_in TNull _ _ D1)
                _).
      apply eq_sym.
      simpl dot.      
      unfold TuplesImpl.Tuple2.dot.
      simpl labels in D1.
      unfold TuplesImpl.Tuple2.support in D1.
      rewrite Fset.mem_mk_set in D1.
      rewrite Oset.mem_bool_true_iff in D1.
      destruct (Oset.find OAN a t) eqn:D2;
        [apply eq_refl|destruct (Oset.find_none_alt _ _ _ D2  D1) ].    
    - simpl labels in D1.
      unfold TuplesImpl.Tuple2.support in D1.
      rewrite Fset.mem_mk_set in D1.     
      assert (D2 := Oset.not_mem_find_none _ _ _ D1).
      rewrite D2.  
      apply Oset.not_mem_find_none.
      unfold tuple_as_pairs.
      rewrite map_map.
      simpl fst.
      rewrite map_id.
      rewrite <- Fset.mem_elements.
      simpl labels.
      unfold TuplesImpl.Tuple2.support.
      rewrite Fset.mem_mk_set.
      apply D1.
      intros;apply eq_refl.
  Qed.
  

  
  Definition is_dnull (d:data) :=
    match d with
    | dright (dstring "") =>  true
    | dright (dnat 0) => true
    | dright (dbool true) => true
    | dright (dfloat float_zero) => true
    | _ => false
    end.

  Lemma filter_is_dnull_map_value_to_data :
    forall vl,
      filter is_dnull (map value_to_data vl) =
      map value_to_data (filter
                           (fun v =>
                              match v with
                              | NullValues.Value_string (Some _)
                              | NullValues.Value_Z (Some _)
                              | NullValues.Value_bool (Some _)
                              | NullValues.Value_float (Some _) => false
                              | _ => true
                              end) vl).
  Proof.
    induction vl;[apply eq_refl| ].
    destruct a;destruct o;simpl in *;rewrite IHvl;apply eq_refl.
  Qed.                   

  
  Lemma lift_filter_is_dnull_on_value_list :
    forall dl,
      lift_filter
        (fun d1 : data =>
           match match d1 with
                 | dleft _ => Some (dbool false)
                 | dright _ => Some (dbool true)
                 | _ => None
                 end with
           | Some (dbool b) => Some b
           | _ => None
           end) (map value_to_data dl) = Some (filter is_dnull (map value_to_data dl)).
  Proof.
    induction dl;[apply eq_refl| ].
    simpl.
    destruct a;destruct o;simpl in *;try rewrite IHdl;apply eq_refl.
  Qed.
  
End Sec.

Notation tnull_frt := enhanced_foreign_runtime.
Notation tnull_fdata := (@foreign_runtime_data tnull_frt).
Notation tnull_fops := (@foreign_runtime_operators tnull_frt).
