(************************************************************************************)
(**                                                                                 *)
(**                          The SQLToNRACert Library                               *)
(**                                                                                 *)
(**                       LMF, CNRS & Université Paris-Saclay                       *)
(**                                                                                 *)
(**                        Copyright 2016-2022 : FormalData                         *)
(**                                                                                 *)
(**         Authors: Véronique Benzaken                                             *)
(**                  Évelyne Contejean                                              *)
(**                  Mohammed Hachmaoui                                             *)
(**                  Chantal Keller                                                 *)
(**                                                                                 *)
(************************************************************************************)

Set Implicit Arguments.

Require Import ZArith NArith.

Require Export List String Permutation.

Require Export Qcert.Utils.Utils Qcert.NRAEnv.NRAEnvRuntime.

From SQLFS Require Import ListPermut ListSort.
From SQLFS Require Export OrderedSet FiniteSet FiniteBag FiniteCollection.
From SQLFS Require Export FlatData.

Require Export Aux.

Module TupleToData.

  Export Tuple.
  
  Class TupleToData `{frt : foreign_runtime} : Type :=
    mk_C
      {
        TRcd : Rcd;
        OType : Oset.Rcd (type TRcd);
        type_of_default_value_eq :
          forall t,
            type_of_value TRcd (default_value TRcd t) = t;
        attribute_to_string : attribute TRcd -> string;
        attribute_to_string_compare_lt :
          forall a1 a2, 
            Oset.compare (OAtt TRcd) a1 a2 = Lt ->
            Oset.compare Ostring
                         (attribute_to_string a1)
                         (attribute_to_string a2) = Lt;
        value_to_data : value TRcd  -> data;
        value_to_data_inj :
          forall a1 a2, value_to_data a1 = value_to_data a2 ->  a1 = a2;
        value_to_data_normalize : forall br v,
            value_to_data v = normalize_data br (value_to_data v);
        contains_nulls : tuple TRcd -> bool;
        contains_nulls_eq :
          forall t1 t2, t1 =t= t2 -> contains_nulls t1 = contains_nulls t2;
      }.
  
  Notation tuple := (tuple TRcd).
  Notation labels := (labels TRcd).
  Notation dot := (dot TRcd).
  Notation mk_tuple := (mk_tuple TRcd).
  Notation attribute := (attribute TRcd).
  Notation OAtt := (OAtt TRcd).
  Notation A := (A TRcd).
  Notation value := (value TRcd).    
  Notation OTuple := (OTuple TRcd).
  Notation attribute_Lt :=
    (fun x y : attribute => Oset.compare OAtt x y = Lt).
  Notation BTupleT := (Fecol.CBag (CTuple TRcd)).
  Notation bagT := (Febag.bag BTupleT).
  Notation tuple_as_pairs := (tuple_as_pairs TRcd).
  Arguments F_Dot {T}.
  Arguments A_Expr {T}.

  
  Section Translation.

    Context {frt : foreign_runtime}.
    Context {TD : TupleToData}.

    Global Instance attribute_eq_dec : EquivDec.EqDec attribute eq.
    Proof.
      red; unfold Equivalence.equiv,RelationClasses.complement.
      change (forall x y : attribute  , {x = y} + {x <> y}).
      intros x y.
      destruct (Oset.eq_bool OAtt x y) eqn:D1.
      - rewrite Oset.eq_bool_true_iff in D1;left;apply D1.
      - right;intro N1.
        rewrite N1 in D1.
        rewrite Oset.eq_bool_refl in D1.
        discriminate.
    Defined.
    
    Definition tuple_as_dpairs t :=
      map
        (fun x => (attribute_to_string (fst x), value_to_data (snd x)))
        (tuple_as_pairs t).

    Definition listT_to_listD lt :=
      map (fun x =>  (drec (tuple_as_dpairs x))) lt.

    Definition bagT_to_listD b :=
      listT_to_listD (Febag.elements BTupleT b).

  End Translation.

  Section Lemmas.
    
    Context {frt : foreign_runtime}.
    Context {TD : TupleToData}.
    
    Section Attributes.
      
      Definition attribute_lt_dec  a a' :
        {Oset.compare OAtt a a' = Lt} +
        {~ Oset.compare OAtt a a' = Lt}.
      Proof.
        destruct (Oset.compare OAtt a a');
          try (right;discriminate).
        left;apply eq_refl.
      Defined.

      Lemma  attribute_to_string_compare_same :
        forall a1 a2,
          Oset.compare
            Ostring (attribute_to_string a1)
            (attribute_to_string a2) =
          Oset.compare OAtt a1 a2.
      Proof.
        intros a1 a2.
        destruct (Oset.compare OAtt a1 a2) eqn:D1.
        - rewrite Oset.compare_eq_iff in D1;subst.
          apply Oset.compare_eq_refl.
        - apply (attribute_to_string_compare_lt _ _ D1).
        - rewrite Oset.compare_lt_gt in D1.
          rewrite Oset.compare_lt_gt.
          rewrite <- CompOpp_involutive in D1.
          apply CompOpp_inj in D1.
          rewrite <- CompOpp_involutive;apply CompOpp_inj.
          do 2 apply f_equal.
          apply (attribute_to_string_compare_lt _ _ D1).
      Qed.

      Lemma  attribute_to_StringOrder_compare_same :
        forall a1 a2,
          StringOrder.compare
            (attribute_to_string a1)
            (attribute_to_string a2) =
          Oset.compare OAtt a1 a2.
      Proof.
        intros a1 a2.
        rewrite <- attribute_to_string_compare_same.
        rewrite ostring_string_order_compare.
        apply eq_refl.
      Qed.
      
      Lemma attribute_to_string_inj :
        forall a1 a2,
          attribute_to_string a1 =
          attribute_to_string a2 ->  a1 = a2.
      Proof.
        intros a1 a2 H.
        rewrite <- (Oset.compare_eq_iff OAtt).
        rewrite <- attribute_to_string_compare_same.
        rewrite H.
        apply Oset.compare_eq_refl.
      Qed.

      Lemma in_map_attribute_to_string_iff: forall (a:attribute) l,
          In (attribute_to_string a) (map attribute_to_string l) <->
          In a l.
      Proof.
        intros a; induction l;
          [exact (conj (fun x => x) (fun x => x))|idtac].
        simpl;rewrite IHl.
        exact
          (conj
             (fun H =>
                or_ind
                  (fun K => (or_introl (attribute_to_string_inj a0 a K)))
                  (fun K => (or_intror K)) H)
             (fun H =>
                or_ind
                  (fun K => (or_introl (f_equal _ K)))
                  (fun K => (or_intror K)) H)).
      Qed.

      Lemma in_string_map_attribute_to_string_iff : forall s l,
          In s (insertion_sort ODT_lt_dec (map attribute_to_string l)) <->
          In s (map attribute_to_string ({{{Fset.mk_set A l}}})).
      Proof.
        intros  s l.
        split;intro H.
        - apply in_insertion_sort in H.
          rewrite in_map_iff in H.
          destruct H as [b [<- H]].
          rewrite in_map_iff;exists b;split;[apply eq_refl| ].
          apply Fset.mem_in_elements.
          rewrite Fset.mem_mk_set,Oset.mem_bool_true_iff.
          apply H.
        - rewrite in_map_iff in H.
          destruct H as [a [<- H]].
          apply Fset.in_elements_mem in H.
          rewrite Fset.mem_mk_set,Oset.mem_bool_true_iff in H.
          apply insertion_sort_in_strong.
          + apply asymmetric_asymmetric_over.
            apply lt_not_not.
          + rewrite in_map_iff;exists a;split;trivial.
      Qed.
      
      Lemma NoDup_map_attribute_to_string_iff :
        forall l, NoDup l <-> NoDup (map attribute_to_string l).
      Proof.
        intros l.
        exact
          (conj
             (fun H => map_inj_NoDup _ attribute_to_string_inj _ H)
             (NoDup_map_inv _ _)).
      Qed.
      
      Lemma HdRel_map_attribute_to_string_iff :
        forall l a,
          HdRel attribute_Lt a l <->
          HdRel StringOrder.lt (attribute_to_string a) (map attribute_to_string l).
      Proof.
        induction l as [|a1 l];
          [intuition;constructor|idtac].
        intros a;split;intro Ha.
        - inversion Ha;subst.
          constructor.
          rewrite <- attribute_to_StringOrder_compare_same in H0.
          apply H0.
        - constructor.
          inversion Ha;subst.
          rewrite <- attribute_to_StringOrder_compare_same.
          apply H0.
      Qed.
      
      Lemma Sorted_map_attribute_to_string_iff :
        forall l, Sorted attribute_Lt l <-> Sorted ODT_lt  (map attribute_to_string l).
      Proof.
        induction l as [|a l];
          [intuition;constructor|idtac].
        split;intro H.
        - inversion H;subst.
          simpl;constructor.
          + apply IHl;apply H2.
          + apply HdRel_map_attribute_to_string_iff.
            apply H3.
        - inversion H;subst.
          simpl;constructor.
          + apply IHl;apply H2.
          + apply HdRel_map_attribute_to_string_iff.
            apply H3.
      Qed.

      Lemma Sorted_listA_is_Sorted_domain :
        forall B l,
          Sorted
            rec_field_lt
            (map
               (fun x : attribute * B =>
                  (attribute_to_string (fst x), snd x)) l) <->
          Sorted
            (fun x y : attribute =>
               Oset.compare OAtt x y = Lt) (map fst l).
      Proof.
        intros B l.
        rewrite Sorted_map_attribute_to_string_iff.
        rewrite <- (is_list_sorted_Sorted_iff ODT_lt_dec).
        rewrite <- (is_list_sorted_Sorted_iff rec_field_lt_dec).
        rewrite is_list_sorted_domain_rec_field.
        unfold domain.
        rewrite 2 map_map;simpl.
        refine (conj (fun x => x) (fun x => x)).
      Qed.
      
      Lemma insertion_sort_set_elements_eq :
        forall l,
          insertion_sort ODT_lt_dec (map attribute_to_string l) =
          map attribute_to_string ({{{Fset.mk_set A l}}}).
      Proof.
        intro l.
        apply (ListSort.sorted_list_eq Ostring).
        - intro s.
          apply Bool.eq_true_iff_eq.
          do 2 rewrite Oset.mem_bool_true_iff.
          apply in_string_map_attribute_to_string_iff.
        - refine  (ListSort.Sorted_incl _ _ (insertion_sort_Sorted _ _)).
          intros.
          rewrite ostring_string_order_compare.
          apply H1.
        - refine
            (ListSort.Sorted_incl
               _ _
               (proj1 (Sorted_map_attribute_to_string_iff _ )
                      (Fset.elements_spec3 _ _))).
          intros.
          rewrite ostring_string_order_compare.
          apply H1.
      Qed.
      
    End Attributes.

    Section Values.

      Lemma map_value_to_data_Permutation_eq :
        forall l1 l2 ,
          Oeset.permut (oeset_of_oset (OVal TRcd)) l1 l2 ->
          Permutation (map value_to_data l1) (map value_to_data l2).
      Proof.
        intros l1 l2.
        induction 1 as [|a b l l1 l2 H1 H2 IH];[constructor| ].
        simpl.
        rewrite map_app.
        rewrite map_app in IH.
        apply Oset.compare_eq_iff in H1.
        subst.
        apply Permutation_cons_app.
        apply IH.
      Qed.
      
    End Values.
    
    Section Tuples.
      
      Definition record_compare (r1 r2: list (string*data)) :=
        comparelA
          (Oset.compare Ostring)
          (map fst (rec_sort r1)) (map fst (rec_sort r2)).

      Definition OeRec : Oeset.Rcd (list (string*data)).
      Proof.
        split with record_compare;unfold record_compare.
        - do 3 intro;apply comparelA_eq_trans.
          do 6 intro;apply Oset.compare_eq_trans.
        - do 3 intro;apply comparelA_le_lt_trans.
          + do 6 intro;apply Oset.compare_eq_trans.
          + do 6 intro;apply Oset.compare_eq_lt_trans.
        - do 3 intro;apply comparelA_lt_le_trans.
          + do 6 intro;apply Oset.compare_eq_trans.
          + do 6 intro;apply Oset.compare_lt_eq_trans.
        - do 3 intro;apply comparelA_lt_trans.
          + do 6 intro;apply Oset.compare_eq_trans.
          + do 6 intro;apply Oset.compare_eq_lt_trans.
          + do 6 intro;apply Oset.compare_lt_eq_trans.
          + do 6 intro;apply Oset.compare_lt_trans.
        - do 2 intro;apply comparelA_lt_gt.
          do 4 intro;apply Oset.compare_lt_gt.
      Qed.
      
      Lemma tuple_as_dpairs_eq_alt :
        forall t1 t2, tuple_as_pairs  t1 = tuple_as_pairs  t2 <->
                 tuple_as_dpairs t1 = tuple_as_dpairs t2.
      Proof.
        intros t1 t2;split.
        - unfold tuple_as_pairs.
          refine  (fun H =>   (f_equal _ H)).
        - apply map_inj.
          intros [a1 v1] [a2 v2] H;inversion H.
          rewrite  (value_to_data_inj _ _ H2).
          rewrite  (attribute_to_string_inj _ _ H1).
          apply eq_refl.
      Qed.
      
      Lemma tuple_as_dpairs_eq :
        forall t1 t2,
          t1 =t= t2 <->
          tuple_as_dpairs t1 = tuple_as_dpairs t2.
      Proof.
        intros t1 t2.
        rewrite (tuple_as_pairs_canonizes TRcd).
        apply tuple_as_dpairs_eq_alt.
      Qed.
      
      Lemma domain_labels_eq :
        forall t,
          (domain (tuple_as_dpairs t)) =
          (map attribute_to_string ({{{labels t}}})).         
      Proof.
        intros t.
        unfold domain,tuple_as_dpairs,tuple_as_pairs.
        do 2 rewrite map_map;simpl.
        apply eq_refl.
      Qed.
      
      Lemma tuple_as_dpairs_labels_domain_in :
        forall t (x : string),
          In x (map attribute_to_string ({{{labels t}}})) <->
          In x (domain (tuple_as_dpairs t)).
      Proof.
        intros t s.
        rewrite domain_labels_eq.
        refine (conj (fun X => X)  (fun X => X)).
      Qed.

      Lemma  tuple_as_dpairs_inS_labels :
        forall a t,
          In (attribute_to_string a) (domain (tuple_as_dpairs t)) <->
          a inS labels t.
      Proof.
        intros a t.
        rewrite Fset.mem_elements.
        rewrite Oset.mem_bool_true_iff.
        rewrite  domain_labels_eq.
        apply in_map_attribute_to_string_iff.
      Qed.

      Lemma  tuple_as_dpairs_not_in_labels : forall a t,
          ~ In (attribute_to_string a) (domain (tuple_as_dpairs t)) <->
          a inS? labels t = false.
      Proof.
        intros a t.
        rewrite  tuple_as_dpairs_inS_labels.
        apply Bool.not_true_iff_false.
      Qed.
      
      Lemma  tuple_as_dpairs_in : forall t v a,
          In
            (attribute_to_string a,value_to_data v)
            (tuple_as_dpairs t) <->
          In (a,v) (tuple_as_pairs  t).      
      Proof.
        intros t  v a;split.
        - unfold tuple_as_dpairs,tuple_as_pairs.
          rewrite map_map;simpl.
          do 2 rewrite in_map_iff.
          intro Ht.
          destruct Ht as [x [A1 A2]];inversion A1;subst;clear A1.
          exists x.
          rewrite  (value_to_data_inj _ _ H1).
          rewrite  (attribute_to_string_inj _ _ H0) in *.
          split;[apply eq_refl|apply A2].
        - unfold tuple_as_dpairs,tuple_as_pairs.
          rewrite map_map;simpl.
          do 2 rewrite in_map_iff.
          intro Ht.
          destruct Ht as [x [A1 A2]];inversion A1;subst;clear A1.
          exists a;split;[apply eq_refl|apply A2].
      Qed.
      
      Lemma tuple_as_dpairs_mk_tuple_empty :
        forall t,
          tuple_as_dpairs
            (mk_tuple
               (emptysetS)
               (fun a : attribute => dot t a)) =  nil.
      Proof.
        intros t.
        unfold tuple_as_dpairs,tuple_as_pairs.
        rewrite map_map.
        rewrite
          (Fset.elements_spec1
             _ _ _ (labels_mk_tuple _ _ _)).
        rewrite Fset.elements_empty.
        apply eq_refl.
      Qed.

      Lemma NoDup_domain_tuple_as_dpairs :
        forall t,  NoDup (domain (tuple_as_dpairs t)).
      Proof.
        intros t.
        rewrite domain_labels_eq.
        apply NoDup_map_attribute_to_string_iff.
        refine (@Sorted_NoDup
                  _ _ (Oset.StrictOrder _)
                  _ (Fset.elements_spec3 _ _ )).
      Qed.
      
      Lemma NoDup_tuple_as_dpairs :
        forall t,  NoDup (tuple_as_dpairs t).
      Proof.
        intros t.
        apply NoDup_domain_NoDup.
        apply NoDup_domain_tuple_as_dpairs.
      Qed.
      
      Lemma Sorted_rec_field_lt_tuple_as_dpairs :
        forall t,  Sorted rec_field_lt  (tuple_as_dpairs t).
      Proof.
        intros t.
        rewrite <- (is_list_sorted_Sorted_iff rec_field_lt_dec).
        rewrite is_list_sorted_domain_rec_field.
        rewrite is_list_sorted_Sorted_iff.
        rewrite domain_labels_eq.
        apply Sorted_map_attribute_to_string_iff.
        apply (Fset.elements_spec3 _ (labels t)).
      Qed.
      
      Lemma Sorted_ODT_lt_domain_tuple_as_dpairs :
        forall t,  Sorted ODT_lt  (domain (tuple_as_dpairs t)).
      Proof.
        intro t.
        rewrite <- (is_list_sorted_Sorted_iff ODT_lt_dec).
        rewrite <- is_list_sorted_domain_rec_field.
        rewrite is_list_sorted_Sorted_iff.
        apply Sorted_rec_field_lt_tuple_as_dpairs.
      Qed.
      
      Lemma rpoject_mk_tuple_alt : forall (l : list attribute) (t : tuple),
          (forall a, In a ({{{Fset.mk_set A l}}}) -> In a ({{{labels t}}})) ->
          tuple_as_dpairs (mk_tuple
                             (Fset.mk_set A l)
                             (fun a : attribute => dot t a)) =
          rproject (tuple_as_dpairs t) (map attribute_to_string l).
      Proof.
        intros l t Aux.
        rewrite <- rproject_sortfilter.
        rewrite insertion_sort_set_elements_eq.
        unfold tuple_as_dpairs,tuple_as_pairs.
        rewrite 2 map_map.
        unfold rproject.
        simpl.
        rewrite (Fset.elements_spec1 _ _ _ (labels_mk_tuple _ _ _)).
        rewrite ListFacts.filter_map.
        simpl fst.
        apply ListFacts.map_eq_alt.
        - apply eq_sym;simpl.
          assert (A1 := Fset.elements_spec3 _ (Fset.mk_set A l)).
          apply (ListSort.sorted_list_eq OAtt).
          + intros a.
            apply Bool.eq_true_iff_eq.
            do 2 rewrite Oset.mem_bool_true_iff.
            rewrite filter_In.
            rewrite
              <- (in_map_attribute_to_string_iff
                   _ ({{{Fset.mk_set A l}}})).
            destruct in_dec as [H1|N1].
            * refine
                (conj
                   (fun _ => H1)
                   (fun H2 =>
                      conj
                        (Aux _ _) eq_refl)).
              rewrite <- in_map_attribute_to_string_iff .
              apply H1.
            * split;[intros [H1 H2];discriminate|intro H1].
              destruct (N1 H1).
          + apply (@SetoidList.filter_sort
                     _ eq (Eqsth _) _
                     (Oset.StrictOrder _ ) (Oset_compare_lt_wd _)).
            apply (Fset.elements_spec3 _ (labels t)).
          + apply A1.
        - intros a Ha.
          rewrite dot_mk_tuple.
          apply Fset.in_elements_mem in Ha.
          rewrite Ha.
          apply eq_refl.
      Qed.
      
      Lemma rpoject_mk_tuple : forall (l : list attribute) (t : tuple),
          (Fset.mk_set A l) subS labels t ->
          tuple_as_dpairs
            (mk_tuple
               (Fset.mk_set A l)
               (fun a : attribute => dot t a)) =
          rproject (tuple_as_dpairs t) (map attribute_to_string l).
      Proof.
        intros l t Aux.
        apply rpoject_mk_tuple_alt.
        intros a Ha.
        rewrite Fset.subset_spec in Aux.
        apply Fset.mem_in_elements.
        apply (Aux _ (Fset.in_elements_mem _ _ _ Ha)).
      Qed.
      
      Lemma rpoject_mk_tuple_bdistinct : forall (l : list attribute) (t : tuple),
          (Fset.mk_set A l) subS labels t ->
          tuple_as_dpairs (mk_tuple (Fset.mk_set A l) (fun a : attribute => dot t a)) =
          rproject (bdistinct  (tuple_as_dpairs t)) (map attribute_to_string  l).
      Proof.
        intros l t H1.
        rewrite (rpoject_mk_tuple _ _ H1).
        rewrite NoDup_bdistinct;
          [apply eq_refl | ].
        apply NoDup_tuple_as_dpairs.
      Qed.
      
      Lemma list_map_dot_NoDup_domain :
        forall l, NoDup l <->
             forall t,
               NoDup
                 (domain
                    (map
                       (fun a : attribute =>
                          (attribute_to_string a,
                           value_to_data (dot t a))) l)).
      Proof.
        intros l;split;intros H;[intro t| ].
        - unfold domain;rewrite map_map;simpl.
          refine (map_inj_NoDup  _  attribute_to_string_inj _ H).
        - refine (  (NoDup_map_inv _ _ _)).
          refine (NoDup_domain_NoDup (H (Tuple.empty_tuple TRcd))).
      Qed.
      
      Lemma list_map_dot_tuple_as_dpairs_eq_Sorted :
        forall l t,
          Sorted (fun x y : attribute => Oset.compare OAtt x y = Lt) l ->
          tuple_as_dpairs (mk_tuple (Fset.mk_set A l) (dot t)) =
          map (fun a : attribute => (attribute_to_string a, value_to_data (dot t a))) l.
      Proof.
        intros l t Hl.
        unfold tuple_as_dpairs,tuple_as_pairs.
        rewrite map_map;simpl.
        rewrite (Fset.elements_spec1 _ _ _ (labels_mk_tuple _ _ _)).
        rewrite (sorted_eq_fset_mk_set _ Hl).
        apply map_ext_in.
        intros a Ha.
        apply f_equal.
        rewrite dot_mk_tuple.
        rewrite <- (Oset.mem_bool_true_iff OAtt) in Ha.
        rewrite <- (Fset.mem_mk_set A) in  Ha.
        rewrite Ha.
        apply eq_refl.
      Qed.
      
      Lemma find_assoc_lookupr___some_sorted :
        forall t,
        forall a v,
          Oset.find OAtt a (tuple_as_pairs  t) =
          Some v ->
          assoc_lookupr
            ODT_eqdec (tuple_as_dpairs t) (attribute_to_string a) =
          Some (value_to_data v).
      Proof.
        intros t a v Hat.
        - assert (A1 := Oset.find_some _ _ _ Hat).
          apply tuple_as_dpairs_in in A1.
          apply (in_assoc_lookupr_nodup
                   _ _ _ ODT_eqdec
                   (NoDup_domain_tuple_as_dpairs t ) A1).
      Qed.
      
      Lemma find_assoc_lookupr___some :
        forall t,
        forall a v,
          Oset.find OAtt a (tuple_as_pairs  t) =
          Some v ->
          assoc_lookupr
            ODT_eqdec (tuple_as_dpairs t) (attribute_to_string a) =
          Some (value_to_data v).
      Proof.
        intros t a v;intros Hat.
        - assert (A1 := Oset.find_some _ _ _ Hat).
          apply tuple_as_dpairs_in in A1.
          apply (in_assoc_lookupr_nodup
                   _ _  _ ODT_eqdec
                   (NoDup_domain_tuple_as_dpairs  _) A1).
      Qed.
      
      Lemma assoc_lookupr___find_in_eq :
        forall t,
        forall a,
          In a ({{{labels t}}}) ->
          assoc_lookupr
            ODT_eqdec (tuple_as_dpairs t) (attribute_to_string a) =
          lift value_to_data (Oset.find OAtt a (tuple_as_pairs  t)).
      Proof.
        intros t a Ha.
        unfold lift.
        unfold tuple_as_pairs.
        rewrite Oset.find_map;[|apply Ha].
        apply (find_assoc_lookupr___some _).
        apply Oset.find_map;apply Ha.
      Qed.

      Lemma assoc_lookupr___find_eq :
        forall t,
        forall a,
          assoc_lookupr
            ODT_eqdec (tuple_as_dpairs t) (attribute_to_string a) =
          lift value_to_data (Oset.find OAtt a (tuple_as_pairs  t)).
      Proof.
        intros t a.
        destruct (a inS? (labels t) ) eqn:Ha.
        - apply assoc_lookupr___find_in_eq.
          apply (Fset.mem_in_elements _ _ _ Ha).
        - unfold lift.
          unfold tuple_as_pairs.
          rewrite Oset.not_mem_find_none.
          + apply assoc_lookupr_nin_none.
            apply  tuple_as_dpairs_not_in_labels.
            apply Ha.
          + rewrite map_map,map_id;simpl.
            rewrite Fset.mem_elements in Ha.
            apply Ha.
      Qed.

      Lemma assoc_lookupr_dot_eq:
        forall t a d,
          assoc_lookupr
            ODT_eqdec
            (tuple_as_dpairs t) (attribute_to_string a) = Some d ->
          value_to_data (dot t a) = d.
      Proof.
        intros t a d Hd.
        assert (A1 := Hd);apply assoc_lookupr_in in A1.
        unfold tuple_as_dpairs,tuple_as_pairs in A1.
        rewrite map_map in A1.
        rewrite in_map_iff in A1.
        destruct A1 as [a1 [A1 A2]].
        inversion A1 as [[I1 I2]].
        rewrite <- I2 in *;clear I2 A1.
        apply Fset.in_elements_mem in A2.
        rewrite (attribute_to_string_inj _ _ I1) in *.
        apply eq_refl.
      Qed.    

      Lemma join_compatible_tuple_assoc_lookupr :
        forall t1 t2,
          join_compatible_tuple TRcd t1 t2 = true <->
          forall a, a inS (labels t1 interS labels t2) ->
               assoc_lookupr
                 ODT_eqdec
                 (tuple_as_dpairs t1) (attribute_to_string a) =
               assoc_lookupr
                 ODT_eqdec
                 (tuple_as_dpairs t2) (attribute_to_string a).
      Proof.
        intros t1 t2;split.
        - unfold join_compatible_tuple.
          rewrite Fset.for_all_spec_alt.
          intros H a Ha.
          assert (A1 := H _ Ha).
          destruct Oset.compare eqn:D1;try discriminate;clear A1.
          do 2 rewrite assoc_lookupr___find_eq.
          unfold lift.
          apply BasicFacts.match_option_eq.
          rewrite Oset.compare_eq_iff in D1.
          rewrite Fset.mem_inter in Ha.
          rewrite Bool.Bool.andb_true_iff in Ha.
          rewrite (tuple_tuple_as_pairs_dot_in _ _ _ (proj1 Ha)).
          rewrite (tuple_tuple_as_pairs_dot_in _ _ _ (proj2 Ha)).
          rewrite D1;apply eq_refl.
        - unfold join_compatible_tuple.
          rewrite Fset.for_all_spec_alt.
          intros H a Ha.
          assert (A1 := H _ Ha).
          do 2 rewrite assoc_lookupr___find_eq in A1.
          unfold lift in A1.
          rewrite Fset.mem_inter in Ha.
          rewrite Bool.Bool.andb_true_iff in Ha.
          rewrite (tuple_tuple_as_pairs_dot_in _ _ _ (proj1 Ha)) in A1.
          rewrite (tuple_tuple_as_pairs_dot_in _ _ _ (proj2 Ha)) in A1.
          inversion A1.
          rewrite (value_to_data_inj _ _  H1).
          rewrite Oset.compare_eq_refl.
          apply eq_refl.
      Qed.
      
      Lemma list_map_dot_tuple_as_dpairs_lookup_rec_sort_aux :
        forall l t,
          Fset.mk_set A l subS labels t ->
          forall a,
            lookup
              string_eqdec
              (tuple_as_dpairs
                 (mk_tuple
                    (Fset.mk_set A l)
                    (dot t))) (attribute_to_string a) =
            lookup
              string_eqdec
              (rec_sort
                 (map
                    (fun a : attribute =>
                       (attribute_to_string a,
                        value_to_data (dot t a))) l))
              (attribute_to_string a).
      Proof.
        intros l t Ht.
        intro a.
        rewrite <- (assoc_lookupr_lookup_nodup _ _ (NoDup_domain_tuple_as_dpairs _)).        
        rewrite <- assoc_lookupr_lookup_nodup.
        - refine (eq_trans _ (eq_sym (assoc_lookupr_drec_sort _ _))). 
          destruct (assoc_lookupr string_eqdec (tuple_as_dpairs (mk_tuple (Fset.mk_set A l) (dot t))) (attribute_to_string a) ) eqn:D1;
            apply eq_sym.
          + assert (A1 := assoc_lookupr_in _ _ _ _ D1).
            rewrite  rpoject_mk_tuple  in A1;[|apply Ht].
            unfold rproject in A1.
            rewrite filter_In in A1.
            destruct A1 as [A1 A2].
            destruct in_dec;[|discriminate].
            simpl in i.
            assert (A3 := in_dom_lookupr (map (fun a0 : attribute => (attribute_to_string a0, value_to_data (dot t a0))) l)).
            unfold domain in A3.
            rewrite map_map in A3;simpl fst in A3.
            assert (A3 := A3 _ ODT_eqdec i).
            destruct A3 as [v A3].
            refine (eq_trans A3 _).
            apply assoc_lookupr_in in A3.
            rewrite in_map_iff in A3.
            destruct A3 as [x [A3 A4]].
            inversion A3;subst.
            apply f_equal.
            apply assoc_lookupr_dot_eq.
            rewrite <- D1.
            rewrite H0 in *.
            do 2 rewrite assoc_lookupr___find_eq.
            rewrite <- (Oset.mem_bool_true_iff OAtt) in A4.           
            rewrite <- (Fset.mem_mk_set A) in A4.               
            apply f_equal.
            rewrite <- (attribute_to_string_inj _ _ H0) in *;clear H0.
            rewrite Fset.subset_spec in Ht.
            assert (A5 := Ht _ A4).
            rewrite tuple_tuple_as_pairs_dot_in;[|apply A5].
            rewrite tuple_tuple_as_pairs_dot_in.
            * rewrite dot_mk_tuple,A4;apply eq_refl.
            * rewrite (Fset.mem_eq_2 _ _ _ (labels_mk_tuple _ _ _)).
              apply A4.
          + apply assoc_lookupr_none_nin in D1.
            apply assoc_lookupr_nin_none.
            rewrite domain_labels_eq in D1.
            unfold domain.
            rewrite map_map;simpl.
            rewrite (Fset.elements_spec1 _ _ _ (labels_mk_tuple _ _ _)) in D1.
            intro N1.
            apply D1.
            rewrite in_map_iff in N1.
            destruct N1 as [x [<- N1]].
            rewrite in_map_iff.
            exists x;split;[apply eq_refl| ].            
            apply Fset.mem_in_elements.
            rewrite Fset.mem_mk_set.
            rewrite Oset.mem_bool_true_iff.
            apply N1.
        - apply Sorted_NoDup.
          rewrite <- (is_list_sorted_Sorted_iff ODT_lt_dec).
          apply drec_sort_sorted.
      Qed.

      Lemma list_map_dot_tuple_as_dpairs_lookup_rec_sort :
        forall l t,
          Fset.mk_set A l subS labels t ->
          forall s,
            lookup
              string_eqdec
              (tuple_as_dpairs
                 (mk_tuple
                    (Fset.mk_set A l)
                    (dot t))) s =
            lookup
              string_eqdec
              (rec_sort
                 (map
                    (fun a : attribute =>
                       (attribute_to_string a, value_to_data (dot t a))) l)) s.
      Proof.
        intros l t Ht s.
        destruct (in_dec
                    ODT_eqdec s
                    (domain
                       (tuple_as_dpairs
                          (mk_tuple (Fset.mk_set A l) (dot t))))).
        - unfold domain,tuple_as_dpairs,tuple_as_pairs in i.
          rewrite 2 map_map in i.
          rewrite in_map_iff in i.
          destruct i as [a [H1 H2]].
          simpl in H1.
          inversion H1.
          apply (list_map_dot_tuple_as_dpairs_lookup_rec_sort_aux
                   _ _ Ht).
        - rewrite lookup_nin_none;[apply eq_sym|apply n].
          apply lookup_nin_none.
          intro i;apply n.
          rewrite in_dom_rec_sort in i.
          unfold domain,tuple_as_dpairs,tuple_as_pairs in i.
          rewrite map_map in i.
          rewrite in_map_iff in i.
          destruct i as [a[H1 H2]].
          simpl in H1;subst s.
          unfold domain,tuple_as_dpairs,tuple_as_pairs.
          rewrite 2 map_map;simpl.
          rewrite (Fset.elements_spec1 _ _ _ (labels_mk_tuple _ _ _)).
          rewrite in_map_iff.
          exists a;split;[apply eq_refl| ].
          apply Fset.mem_in_elements.
          rewrite Fset.mem_mk_set.
          rewrite Oset.mem_bool_true_iff.
          apply H2.
      Qed.

      Lemma list_map_dot_tuple_as_dpairs_eq_rec_sort :
        forall l t,
          Fset.mk_set A l subS labels t ->
          tuple_as_dpairs (mk_tuple (Fset.mk_set A l) (dot t)) =
          rec_sort
            (map
               (fun a : attribute =>
                  (attribute_to_string a, value_to_data (dot t a))) l).
      Proof.
        intros l t H.
        apply drec_sorted_perm_eq.
        - rewrite is_list_sorted_Sorted_iff.
          apply Sorted_ODT_lt_domain_tuple_as_dpairs.
        - apply drec_sort_sorted.
        - apply NoDup_lookup_equiv_Permutation.
          + apply Sorted_NoDup.
            apply Sorted_ODT_lt_domain_tuple_as_dpairs.
          + apply Sorted_NoDup.
            rewrite <- (is_list_sorted_Sorted_iff ODT_lt_dec).
            apply drec_sort_sorted.
          + unfold lookup_equiv.
            apply list_map_dot_tuple_as_dpairs_lookup_rec_sort.
            apply H.
      Qed.
      
      Lemma join_tuple_rec_concat_sort_eq_if_disjoint :
        forall t1 t2,
          Fset.is_empty A (labels t1 interS labels t2) = true ->
          tuple_as_dpairs (join_tuple TRcd t1 t2) = rec_concat_sort (tuple_as_dpairs t1) (tuple_as_dpairs t2).
      Proof.
        intros t1 t2 H.
        unfold rec_concat_sort.
        unfold join_tuple.
        rewrite <- (sort_sorted_is_id _ (proj2 (is_list_sorted_Sorted_iff _ _ ) (Sorted_ODT_lt_domain_tuple_as_dpairs _ ))).
        apply drec_sort_perm_eq;[apply NoDup_domain_tuple_as_dpairs| ].
        apply NoDup_Permutation;[apply NoDup_tuple_as_dpairs| | ].
        - apply NoDup_app;[|apply NoDup_tuple_as_dpairs|apply NoDup_tuple_as_dpairs].
          unfold disjoint.
          intros [s d] H1 H2.
          unfold tuple_as_dpairs,tuple_as_pairs in H1,H2.
          rewrite map_map in H1,H2;simpl in H1,H2.
          rewrite in_map_iff in H1,H2.
          destruct H1 as [x1 [A1 A2]].
          destruct H2 as [x2 [B1 B2]].
          inversion A1;subst;clear A1.
          inversion B1 as [[I1 I2]];clear B1.
          rewrite (attribute_to_string_inj _ _ I1) in *.
          rewrite Fset.is_empty_spec in H.
          rewrite Fset.equal_spec in H.
          assert (Hx1 := H x1).
          rewrite Fset.mem_inter,Fset.empty_spec in Hx1.
          rewrite Bool.Bool.andb_false_iff in Hx1.
          apply Fset.in_elements_mem in A2.
          apply Fset.in_elements_mem in B2.
          rewrite A2,B2 in Hx1.
          destruct Hx1 as [Hx1|Hx1];discriminate.
        - intros [s d];split;intros H1.
          + unfold tuple_as_dpairs in *.
            rewrite in_map_iff in H1.
            destruct H1 as [[a1 v1] [<- H2]].
            unfold tuple_as_pairs in *.
            do 2 rewrite map_map.
            simpl.
            rewrite in_map_iff in H2.
            destruct H2 as [a2 [H1 H2]].
            inversion H1;subst;clear H1.           
            rewrite (Fset.elements_spec1 _ _ _ (labels_mk_tuple _ _ _)) in H2.
            apply Fset.in_elements_mem in H2.
            rewrite dot_mk_tuple,H2.
            apply in_or_app.
            rewrite Fset.mem_union in H2.
            rewrite Bool.Bool.orb_true_iff in H2.
            destruct (a1 inS? labels t1) eqn:D2.
            * left.
              rewrite in_map_iff.
              exists a1;split;[apply eq_refl|apply Fset.mem_in_elements;apply D2 ].
            * destruct H2 as [H2|H2];[discriminate| ].
              right.
              rewrite in_map_iff.
              exists a1;split;[apply eq_refl|apply Fset.mem_in_elements;apply H2].
          + apply app_or_in in H1.
            unfold tuple_as_dpairs,tuple_as_pairs in H1.
            unfold tuple_as_dpairs,tuple_as_pairs.
            rewrite 2 map_map in H1;simpl in H1.
            rewrite 2 in_map_iff in H1.
            rewrite map_map;simpl.
            rewrite (Fset.elements_spec1 _ _ _ (labels_mk_tuple _ _ _)).
            rewrite in_map_iff.
            destruct H1 as [H1|H1].
            * destruct H1 as [x1 [H1 H2]];inversion H1;subst;clear H1.
              exists x1.
              apply Fset.in_elements_mem in H2.
              rewrite dot_mk_tuple.
              rewrite Fset.mem_union,H2.
              split;[apply eq_refl| ].
              apply Fset.mem_in_elements.
              rewrite Fset.mem_union,Bool.Bool.orb_true_iff;left;apply H2.
            * destruct H1 as [x1 [H1 H2]];inversion H1;subst;clear H1.
              exists x1.
              apply Fset.in_elements_mem in H2.
              rewrite dot_mk_tuple.
              rewrite Fset.mem_union,H2.
              rewrite Bool.orb_true_r.          
              rewrite Fset.is_empty_spec in H.
              rewrite Fset.equal_spec in H.
              assert (Hx1 := H x1).
              rewrite Fset.mem_inter,Fset.empty_spec in Hx1.
              rewrite Bool.Bool.andb_false_iff in Hx1.
              rewrite H2 in Hx1.
              destruct Hx1 as [Hx1|Hx1];[|discriminate].
              rewrite Hx1.
              split;[apply eq_refl| ].
              apply Fset.mem_in_elements.
              rewrite Fset.mem_union,Bool.Bool.orb_true_iff;right;apply H2.
      Qed.
      
      Lemma rpoject_mk_tuple_aggterm :
        forall env (l : list attribute) (t : tuple),
          Fset.mk_set A l subS labels t ->
          tuple_as_dpairs (mk_tuple (Fset.mk_set A l) (fun a : attribute => interp_aggterm TRcd (env_t TRcd env t) (A_Expr   (F_Dot   a)))) =
          rproject (tuple_as_dpairs t) (map attribute_to_string l).
      Proof.
        intros env l t Ht.
        rewrite <- (rpoject_mk_tuple _ _ Ht).
        apply tuple_as_dpairs_eq.
        apply Oeset.compare_eq_sym.
        apply (dot_is_aggterm_subset _ _ _ Ht).
      Qed.

      Lemma list_map_aggterm_tuple_as_dpairs_lookup_rec_sort_aux :
        forall env l t,
          NoDup l ->
          forall a,
            lookup string_eqdec
                   (tuple_as_dpairs
                      (mk_tuple (Fset.mk_set A l)
                                (fun a0 : attribute => interp_aggterm TRcd (env_t TRcd env t) (A_Expr   (F_Dot    a0))))) (attribute_to_string a) =
            lookup string_eqdec
                   (rec_sort
                      (map
                         (fun a0 : attribute =>
                            (attribute_to_string a0,
                             value_to_data
                               (interp_aggterm TRcd (env_t TRcd env t) (A_Expr  (F_Dot   a0))))) l))
                   (attribute_to_string a).
      Proof.
        intros env l t Aux .
        intro a.
        rewrite <- (assoc_lookupr_lookup_nodup _ _ (NoDup_domain_tuple_as_dpairs _)).
        rewrite <- assoc_lookupr_lookup_nodup.
        - refine (eq_trans _ (eq_sym (assoc_lookupr_drec_sort _ _))).
          destruct (assoc_lookupr string_eqdec (tuple_as_dpairs (mk_tuple (Fset.mk_set A l)   (fun a0 : attribute => interp_aggterm  _ (env_t _ env t) (A_Expr   (F_Dot    a0))))) (attribute_to_string a) ) eqn:D1;
            apply eq_sym.
          + assert (A1 := assoc_lookupr_in _ _ _ _ D1).
            rewrite <- D1.
            apply assoc_lookupr_nodup_perm.
            * unfold domain;rewrite map_map;simpl.
              apply NoDup_map_attribute_to_string_iff.
              apply Aux.
            * unfold tuple_as_dpairs,tuple_as_pairs.
              rewrite map_map;simpl.
              rewrite (Fset.elements_spec1 _ _ _ (labels_mk_tuple _ _ _)).
              assert (A2 := no_dup_permutation_fset_mk_set A Aux).
              apply NoDup_Permutation.
              -- refine (map_inj_NoDup _ _ _ Aux).
                 intros.
                 inversion H.
                 apply (attribute_to_string_inj _ _ H1).
              -- refine (map_inj_NoDup _ _ _ (Permutation_NoDup (Permutation_sym A2) Aux)).
                 intros.
                 inversion H.
                 apply (attribute_to_string_inj _ _ H1).
              -- intros [s1 d1];split;intros H1.
                 ++ rewrite in_map_iff in H1.
                   destruct H1 as [a2 [H1 H2]].
                   inversion H1;subst d1.
                   rewrite in_map_iff.
                   exists a2;split.
                   ** apply f_equal.
                      rewrite dot_mk_tuple.
                      assert (G1 : a2 inS Fset.mk_set A l).
                      { rewrite Fset.mem_mk_set,Oset.mem_bool_true_iff;apply H2. }
                      rewrite G1;apply eq_refl.
                   ** apply Fset.mem_in_elements.
                      rewrite Fset.mem_mk_set,Oset.mem_bool_true_iff;apply H2.
                 ++  rewrite in_map_iff in H1.
                    destruct H1 as [a2 [H1 H2]].
                    inversion H1;subst d1.
                    rewrite in_map_iff.
                    exists a2;split.
                    ** apply f_equal.
                       rewrite dot_mk_tuple.
                       rewrite (Fset.in_elements_mem _ _ _ H2).
                       apply eq_refl.
                    ** apply Fset.in_elements_mem in H2.
                       rewrite Fset.mem_mk_set,Oset.mem_bool_true_iff in H2;apply H2.
          + apply assoc_lookupr_none_nin in D1.
            apply assoc_lookupr_nin_none.
            rewrite domain_labels_eq in D1.
            unfold domain.
            rewrite map_map;simpl.
            rewrite (Fset.elements_spec1 _ _ _ (labels_mk_tuple _ _ _)) in D1.
            intro N1.
            apply D1.
            rewrite in_map_iff in N1.
            destruct N1 as [x [<- N1]].
            rewrite in_map_iff.
            exists x;split;[apply eq_refl| ].
            apply Fset.mem_in_elements.
            rewrite Fset.mem_mk_set.
            rewrite Oset.mem_bool_true_iff.
            apply N1.
        - apply Sorted_NoDup.
          rewrite <- (is_list_sorted_Sorted_iff ODT_lt_dec).
          apply drec_sort_sorted.
      Qed.

      Lemma list_map_aggterm_tuple_as_dpairs_lookup_rec_sort :
        forall env l t,
          NoDup l ->
          forall x : string,
            lookup string_eqdec
                   (tuple_as_dpairs
                      (mk_tuple (Fset.mk_set A l)
                                (fun a : attribute => interp_aggterm TRcd (env_t _ env t) (A_Expr  (F_Dot    a)) ))) x =
            lookup string_eqdec
                   (rec_sort
                      (map
                         (fun a : attribute =>
                            (attribute_to_string a,
                             value_to_data (interp_aggterm TRcd (env_t TRcd env t) (A_Expr   (F_Dot a))))) l)) x.
      Proof.
        intros env l t Ht s.
        destruct (in_dec ODT_eqdec s (domain (tuple_as_dpairs (mk_tuple (Fset.mk_set A l) (fun a =>   interp_dot TRcd (env_t TRcd env t) a))))).
        - unfold domain,tuple_as_dpairs,tuple_as_pairs in i.
          rewrite 2 map_map in i.
          rewrite in_map_iff in i.
          destruct i as [a [H1 H2]].
          simpl in H1.
          inversion H1.
          apply (list_map_aggterm_tuple_as_dpairs_lookup_rec_sort_aux _ _ Ht).
        - rewrite lookup_nin_none;[apply eq_sym|apply n].
          apply lookup_nin_none.
          intro i;apply n.
          rewrite in_dom_rec_sort in i.
          unfold domain,tuple_as_dpairs,tuple_as_pairs in i.
          rewrite map_map in i.
          rewrite in_map_iff in i.
          destruct i as [a[H1 H2]].
          simpl in H1;subst s.
          unfold domain,tuple_as_dpairs,tuple_as_pairs.
          rewrite 2 map_map;simpl.
          rewrite (Fset.elements_spec1 _ _ _ (labels_mk_tuple _ _ _)).
          rewrite in_map_iff.
          exists a;split;[apply eq_refl| ].
          apply Fset.mem_in_elements.
          rewrite Fset.mem_mk_set.
          rewrite Oset.mem_bool_true_iff.
          apply H2.
      Qed.
      
      Lemma list_map_aggterm_tuple_as_dpairs_eq_rec_sort_ :
        forall env l t,
          NoDup l ->
          tuple_as_dpairs
            (mk_tuple (Fset.mk_set A l)
                      (fun a : attribute => interp_aggterm TRcd (env_t _ env t) (A_Expr  (F_Dot    a)))) =
          rec_sort
            (map
               (fun a : attribute =>
                  (attribute_to_string a,
                   value_to_data
                     (interp_aggterm TRcd (env_t TRcd env t) (A_Expr  (F_Dot  a)) ))) l).
      Proof.
        intros env l t H.
        apply drec_sorted_perm_eq.
        - rewrite is_list_sorted_Sorted_iff.
          apply Sorted_ODT_lt_domain_tuple_as_dpairs.
        - apply drec_sort_sorted.
        - apply NoDup_lookup_equiv_Permutation.
          + apply Sorted_NoDup.
            apply Sorted_ODT_lt_domain_tuple_as_dpairs.
          + apply Sorted_NoDup.
            rewrite <- (is_list_sorted_Sorted_iff ODT_lt_dec).
            apply drec_sort_sorted.
          + unfold lookup_equiv.
            apply list_map_aggterm_tuple_as_dpairs_lookup_rec_sort.
            apply H.
      Qed.

      Lemma tuple_as_dpairs_empty_tuple_is_nil :
        tuple_as_dpairs (empty_tuple TRcd) = nil.
      Proof.
        unfold tuple_as_dpairs,tuple_as_pairs.
        rewrite map_map;simpl.
        rewrite (Fset.elements_spec1 _ _ _ (labels_empty_tuple TRcd)).
        rewrite Fset.elements_empty.
        apply eq_refl.
      Qed.
      
    End Tuples.
    
    Section Bags.
      
      Lemma Permutation_mult_equiv :
        forall l1 l2 ,
          (forall (d:@Data.data (@foreign_runtime_data frt)), mult l1 d = mult l2 d) <->
          Permutation l1 l2.
      Proof.
        intros l1 l2;split.
        apply bags_same_mult_has_equal.
        apply bags_equal_same_mult1.
      Qed.

      Lemma comparelA_listT_eq :
        forall l1 l2 , comparelA (Oeset.compare OTuple) l1 l2 = Eq <-> listT_to_listD l1 = listT_to_listD l2.
      Proof.
        induction l1 as [|t1 l1];intros l2;split;intro H.
        - destruct l2;[apply eq_refl|discriminate].
        - destruct l2;[apply eq_refl|discriminate].
        - destruct l2 as [|t2 l2];[discriminate| ].
          simpl in H.
          destruct Oeset.compare eqn:D1;try discriminate.
          simpl.
          rewrite tuple_as_dpairs_eq in D1;rewrite D1.
          apply f_equal.
          apply IHl1;apply H.
        - destruct l2 as [|t2 l2];[discriminate| ].
          simpl in H.
          inversion H.
          simpl.
          rewrite <- tuple_as_dpairs_eq in H1;rewrite H1.
          apply IHl1;apply H2.
      Qed.

      Lemma In_tuple_as_pairs_eq_listT :
        forall lt t,
          In t lt ->
          In
            (drec (tuple_as_dpairs t))
            (listT_to_listD lt).
      Proof.
        induction lt;[intuition| intro t].
        intros [H|H].
        rewrite H;left;apply eq_refl.
        right;apply IHlt;apply H.
      Qed.

      Lemma listT_to_listD_app_distr :
        forall l1 l2,
          listT_to_listD (l1 ++ l2) = listT_to_listD l1 ++ listT_to_listD l2.
      Proof.
        intros l1 l2.
        unfold listT_to_listD.
        rewrite map_app.
        apply eq_refl.
      Qed.

      Lemma nb_occ_mult_listT_eq :
        forall l t,
          mult (listT_to_listD l)  (drec (tuple_as_dpairs t)) =
          N.to_nat (Oeset.nb_occ OTuple t l).       
      Proof.
        intros l t;apply eq_sym;revert l t.
        induction l as [|t1 l];intros t;[apply eq_refl| ].
        unfold listT_to_listD in *.
        simpl.
        destruct (Oeset.compare OTuple t t1 ) eqn:D1.
        - apply tuple_as_dpairs_eq in D1.
          rewrite D1,N2Nat.inj_add,IHl.
          unfold tuple_as_pairs.
          unfold tuple_as_pairs in D1.
          unfold mult at 2;rewrite D1.
          destruct  EquivDec.equiv_dec as [x|x];[apply eq_refl| ].
          unfold RelationClasses.complement in x.
          exfalso;apply x.
          apply eq_refl.
        - rewrite N.add_0_l,IHl.
          unfold tuple_as_pairs.
          unfold mult.
          destruct  EquivDec.equiv_dec as [x|x];[|apply eq_refl].
          unfold Equivalence.equiv in x.     
          inversion x as [X'].
          apply  (proj2 (tuple_as_dpairs_eq t  t1)) in X'.
          simpl in X';rewrite X' in D1;discriminate.
        - rewrite N.add_0_l,IHl.
          unfold tuple_as_pairs.
          unfold mult.
          destruct  EquivDec.equiv_dec as [x|x];[|apply eq_refl].
          unfold Equivalence.equiv in x.     
          inversion x as [X'].
          apply  (proj2 (tuple_as_dpairs_eq t  t1)) in X'.
          simpl in X';rewrite X' in D1;discriminate.
      Qed.

      Lemma mem_tuple_as_pairs_listT :
        forall lt t,
          Oeset.mem_bool OTuple t lt = true <->
          In
            (drec (tuple_as_dpairs t))
            (listT_to_listD lt).
      Proof.
        intros t lt;split;intro H.
        - rewrite Oeset.mem_bool_true_iff in H.
          destruct H as [x [H1 H2]].
          unfold listT_to_listD.
          rewrite in_map_iff.
          rewrite tuple_as_dpairs_eq in H1.
          exists x;split;[rewrite H1;apply eq_refl|apply H2].
        - unfold listT_to_listD in H.
          rewrite in_map_iff in H.
          destruct H as [t2 [H1 H2]].
          injection H1;intros H3;clear H1.
          apply eq_sym in H3.
          apply tuple_as_dpairs_eq in H3.
          rewrite Oeset.mem_bool_true_iff.
          exists t2;split;assumption.
      Qed.

      Lemma mult_listT_gt_0 :
        forall l d ,
          mult (listT_to_listD  l) d > 0 ->
          exists t, d = drec (tuple_as_dpairs t).
      Proof.
        fix thm 1.
        intros [ | t1 l] d  Hn;[inversion Hn| ].
        simpl mult in Hn.
        unfold EquivDec.equiv_dec in Hn.
        destruct (data_eqdec d (drec (tuple_as_dpairs t1)) ) eqn:D1;
          [exists t1;apply e| ];
          apply (thm _ _ Hn).
      Qed.
      
      Lemma in_listT_is_drec :
        forall l d ,
          In d (listT_to_listD  l) ->
          exists t, d = drec (tuple_as_dpairs t).
      Proof.
        intros l d.
        rewrite <- mult_pos_equiv_in.
        apply  mult_listT_gt_0.
      Qed.

      Lemma nb_occ_mult_listT_eq_complete :
        forall lt d,
          mult (listT_to_listD lt) d = 0 \/
          exists t, d = drec (tuple_as_dpairs t) /\ 
               mult (listT_to_listD lt) (drec (tuple_as_dpairs t)) =  N.to_nat (Oeset.nb_occ OTuple t lt)  /\ Oeset.mem_bool OTuple t lt = true .
      Proof.
        intros lt d.
        destruct (in_dec data_eq_dec d (listT_to_listD lt) ) as [H1|N1];
          [right|left;apply mult_0_equiv_nin;apply N1].
        assert (A1 := in_listT_is_drec _ _ H1).
        destruct A1;subst.
        exists x;split;[apply eq_refl| ].
        rewrite nb_occ_mult_listT_eq;split;[apply eq_refl| ].
        apply mem_tuple_as_pairs_listT.
        apply H1.
      Qed.

      Lemma Permutation_listT_is_tuple :
        forall lt l,
          Permutation (listT_to_listD lt) l -> forall d, In d l -> exists t, d = drec (tuple_as_dpairs t).
      Proof.
        intros b l H d Hd.
        apply Permutation_sym in H.
        assert (A2 := Permutation_in _ H Hd).
        unfold listT_to_listD in A2.
        rewrite in_map_iff in A2.
        destruct A2 as [x [A2 A3]].
        subst.
        exists x;apply eq_refl.
      Qed.
      
      Lemma Permutation_listT_eq :
        forall l1 l2 , _permut (fun x1 x2 => Oeset.compare OTuple x1 x2 = Eq) l1 l2  <-> Permutation (listT_to_listD l1) (listT_to_listD l2).
      Proof.
        intros l1 l2;split;intro H.
        - unfold listT_to_listD.
          apply _permut_eq_Permutation.
          refine (_permut_map _ _ _ _ H).
          intros t1 t2 H1 H2 Heq.
          apply f_equal.
          apply tuple_as_dpairs_eq.
          apply Heq.
        - rewrite <- Permutation_mult_equiv in H.
          apply Oeset.nb_occ_permut.
          intro t.
          assert (Ht := H (drec (tuple_as_dpairs t))).
          do 2 rewrite nb_occ_mult_listT_eq in Ht.
          rewrite <- N2Nat.id.
          rewrite <- (N2Nat.id (Oeset.nb_occ OTuple t l1)).
          rewrite Ht.
          apply eq_refl.
      Qed.

      Lemma Permutation_listT_eq_alt :
        forall l1 l2 , Permutation l1 l2  -> Permutation (listT_to_listD l1) (listT_to_listD l2).
      Proof.
        intros l1 l2;intro H.
        unfold listT_to_listD.
        apply _permut_eq_Permutation.
        apply _permut_eq_Permutation in H.
        refine (_permut_map _ _ _ _ H).
        intros t1 t2 H1 H2 Heq.
        apply f_equal.
        apply tuple_as_dpairs_eq.
        rewrite Heq.
        apply Oeset.compare_eq_refl.
      Qed.

      Lemma Permutation_listT_spec :
        forall lt1 ld, Permutation (listT_to_listD lt1) ld <->
                  exists lt', _permut (fun x1 x2 => Oeset.compare OTuple x1 x2 = Eq) lt1 lt' /\ listT_to_listD lt' = ld.
      Proof.
        intros lt1 ld;split.
        - revert ld.
          induction lt1 as [|t1 lt1];intros d.
          + exists nil;split;
              [apply _permut_refl;intros t2 H1;inversion H1| ].
            apply Permutation_nil in H.
            subst d;apply eq_refl.
          + intro Hd.
            simpl in Hd.
            rewrite <- _permut_eq_Permutation in Hd.
            inversion Hd;subst.
            rewrite  _permut_eq_Permutation in H3.
            assert (A1 := IHlt1 _ H3).
            destruct A1 as [lt' [A1 A2]].
            unfold listT_to_listD in A2.
            apply map_app_break in A2.
            destruct A2 as [lt2 [lt3 [A2 [A4 A3]]]].
            exists (lt2 ++ (t1::lt3));split.
            * rewrite Permutation_listT_eq.
              unfold listT_to_listD.
              rewrite map_app,2map_cons.
              subst.
              rewrite Permutation_listT_eq in A1.
              refine (Permutation_trans _ (Permutation_app_comm _ _)).
              simpl.
              apply Permutation_cons;[apply eq_refl| ].
              refine (Permutation_trans A1 _).
              refine (Permutation_trans _ (Permutation_app_comm _ _)).
              unfold listT_to_listD.
              rewrite map_app.
              apply Permutation_refl.
            * unfold listT_to_listD.
              rewrite map_app.
              simpl.
              subst.
              apply eq_refl.
        - intros [lt' [H1 H2]].
          subst ld.
          apply Permutation_listT_eq.
          apply H1.
      Qed.
      
      Lemma Permutation_listT_exists_eq :
        forall lt1 ld, Permutation (listT_to_listD lt1) ld ->
                  exists lt', Permutation lt1 lt' /\ listT_to_listD lt' = ld.
      Proof.
        intros lt1 ld.
        revert ld.
        induction lt1 as [|t1 lt1];intros d.
        + exists nil;split;
            [apply Permutation_refl;intros t2 H1;inversion H1| ].
          apply Permutation_nil in H.
          subst d;apply eq_refl.
        + intro Hd.
          simpl in Hd.
            rewrite <- _permut_eq_Permutation in Hd.
            inversion Hd;subst.
            rewrite  _permut_eq_Permutation in H3.
            clear Hd.
            destruct (IHlt1 _ H3) as [l3 [G3 A2]].
            unfold listT_to_listD in A2.
            apply map_app_break in A2.
            destruct A2 as [lt2 [lt3 [A2 [A4 A3]]]].
            exists (lt2 ++ (t1::lt3));split.
            * unfold listT_to_listD in H3.
              apply Permutation_cons_app.
              subst l3;apply G3.
            * unfold listT_to_listD.
              rewrite map_app.
              simpl.
              subst.
              apply eq_refl.
      Qed.
      
      Lemma _permut_Permut_map_listT_to_listD_eq :
        forall l1 l2,
          _permut (fun x y : list tuple => Oeset.compare (Partition.OLA OTuple) x y = Eq) l1 l2 <->
          ListPermut._permut (@Permutation _ ) (map listT_to_listD l1) (map listT_to_listD l2).
      Proof.
        intros l1 l2.
        split;intro H.
        - refine (_permut_map _ _ _ _ H).
          intros a b Ha Hb Hc.
          apply Permutation_listT_eq.
          apply Partition.compare_list_t in Hc.
          apply Hc.
        - simple refine (_permut_incl _ _ _).
          + exact  (fun x y => Permutation (listT_to_listD x) (listT_to_listD y)).
          + simpl;intros.
            rewrite <- Permutation_listT_eq in H0.
            apply Partition.compare_list_t.
            apply H0.
          + revert dependent l2.
            induction l1;intros l2 Hl.
            * inversion Hl.
              apply eq_sym in H.
              apply map_eq_nil in H.
              subst;constructor.
            * simpl in Hl;inversion Hl;subst.
              destruct (map_app_break _ (eq_sym H1)) as [m1 [m2 [-> [-> M1]]]].
              destruct (map_eq_cons _ _ _ _ (eq_sym M1)) as [m3 [m4 [-> [<- <-]]]].
              constructor;[apply H2| ].
              apply IHl1.
              rewrite map_app.
              apply H3.
      Qed.
      
      Lemma  Permutation_listT_filter :
        forall f1 f2 l lt ,
          ( forall e e' : tuple, Oeset.mem_bool OTuple e lt = true -> e =t= e' -> f1 e = f1 e') ->
          Permutation (listT_to_listD lt) l ->
          (forall t d, Oeset.mem_bool OTuple t lt = true -> f2 (drec (tuple_as_dpairs t)) = Some d -> f1 t = d ) ->
          forall l',
            lift_filter f2 l = Some l' ->
            Permutation
              (listT_to_listD
                 (filter f1
                         lt)) l'.
      Proof.
        intros f1 f2 l b Aux Hp Ht l' Hl'.
        rewrite <- Permutation_mult_equiv.
        intro d.
        destruct (in_dec  data_eq_dec d l) as [A1|N1].
        - assert (A2 := Permutation_listT_is_tuple  b Hp _ A1).
          destruct A2;subst.
          rewrite nb_occ_mult_listT_eq.
          rewrite Oeset.nb_occ_filter;[|apply Aux].
          assert (A2 : exists d, f2 (drec (tuple_as_dpairs x)) = Some d).
          { revert l' Hl' A1.
            clear.
            induction l as [|a l1];intros l2 H1 H2;[inversion H2| ].
            destruct H2.
            - subst.
              simpl in H1.
              destruct (f2 (drec (tuple_as_dpairs x)) ) eqn:D1;[|discriminate].
              exists b;apply eq_refl.
            - simpl in H1.
              destruct (f2 a ) eqn:D1;[|discriminate].
              destruct (lift_filter f2 l1) eqn:D2;[|discriminate].
              destruct b.
              + destruct l2;[discriminate| ].
                inversion H1;subst.
                apply (IHl1 l2 eq_refl H).
              +    apply (IHl1 l2 H1 H). }
          apply     lift_filter_Some_filter  in Hl'.
          rewrite Hl'.
          rewrite mult_filter.
          destruct A2 as [b2 A2].
          rewrite A2.
          apply Permutation_sym in Hp.
          assert (A3 := Permutation_in _ Hp A1).
          rewrite <- mem_tuple_as_pairs_listT in A3.
          rewrite (Ht _ _ A3 A2).
          rewrite <- Permutation_mult_equiv in Hp.
          rewrite  Hp.
          rewrite nb_occ_mult_listT_eq.
          destruct b2;intuition.
        - apply     lift_filter_Some_filter  in Hl'.
          rewrite Hl'.
          rewrite mult_filter.
          rewrite (proj2 (mult_0_equiv_nin  _ _ _) N1).
          assert (A1 : ~ In d (listT_to_listD b)).
          {  intro N2;apply N1.
             apply (Permutation_in _ Hp N2). }
          rewrite mult_0_equiv_nin.
          intro N2.
          apply A1.
          rewrite <- Permutation_mult_equiv in Hp.
          unfold listT_to_listD in N2.
          rewrite in_map_iff in N2.
          destruct N2 as [x [N2 N3]].
          subst.
          rewrite filter_In in N3.
          unfold listT_to_listD.
          rewrite in_map_iff.
          exists x;split;[apply eq_refl| apply N3].
      Qed.

      Lemma  permutation_listT_flat_map_alt :
        forall  f1 (f2:data -> list data),
          (forall t, Permutation (map (fun x : tuple => drec (tuple_as_dpairs x)) (f1 t)) (f2 (drec (tuple_as_dpairs t)))) ->
          forall l,
            Permutation  (listT_to_listD (flat_map f1 l)) (flat_map f2 (listT_to_listD l)).
      Proof.
        intros f1 f2 Aux.
        induction l as [|t l];[constructor| ].
        unfold listT_to_listD.
        simpl;rewrite map_app.
        apply Permutation_app;[apply Aux |apply IHl].
      Qed.

      Lemma  permutation_listT_map :
        forall f1 lt,
        forall f2 l1 ,
          (forall t1 , Oeset.mem_bool OTuple t1 lt = true -> drec (tuple_as_dpairs (f1 t1)) =  f2 (drec (tuple_as_dpairs t1)) ) ->
          Permutation (listT_to_listD lt) l1 ->
          Permutation  (listT_to_listD ( map f1  lt)) (map f2 l1).
      Proof.
        intros f1 lt f2 l1 H Hp.
        apply Permutation_map with (f := f2) in Hp.
        refine (Permutation_trans  _ Hp ).
        unfold listT_to_listD.
        do 2 rewrite map_map.
        rewrite <- Permutation_mult_equiv.
        intros d;apply f_equal3;[apply eq_refl| |apply eq_refl].
        apply map_ext_in.
        do 2 intro.
        apply (Oeset.in_mem_bool OTuple) in H0.
        revert H0.
        apply H.
      Qed.
      
      Lemma  permutation_listT_lift_map :
        forall f1 lt,
        forall f2 l1 l2,
          (forall a, Oeset.mem_bool OTuple a lt = true -> Some (drec (tuple_as_dpairs (f1 a))) = f2 (drec (tuple_as_dpairs a)) ) ->
          lift_map f2 l1 = Some l2 ->
          Permutation (listT_to_listD lt) l1 ->
          Permutation  (listT_to_listD (map f1 lt)) l2.
      Proof.
        intros f1 b f2 l1 l2  Hf Hl Hp.
        apply Permutation_map with (f := f2) in Hp.
        rewrite listo_to_olist_simpl_lift_map in Hl.
        apply listo_to_olist_some in Hl.
        rewrite Hl in Hp.
        apply (Permutation_map_Some_eq data_eq_dec).
        refine (Permutation_trans  _ Hp ).
        unfold listT_to_listD.
        do 3 rewrite map_map.
        apply bags_same_mult_has_equal.
        intros d;apply f_equal3;[apply eq_refl| |apply eq_refl].
        apply map_ext_in.
        do 2 intro.
        apply (Oeset.in_mem_bool OTuple) in H.
        revert H.
        apply Hf.
      Qed.

      Lemma  permutation_listT_lift_map_weak :
        forall f1 lt,
        forall f2 l1 l2,
          (forall a b, Oeset.mem_bool OTuple a lt = true -> f2 (drec (tuple_as_dpairs a)) = Some b -> b = drec (tuple_as_dpairs (f1 a))) ->
          lift_map f2 l1 = Some l2 ->
          Permutation (listT_to_listD lt) l1 ->
          Permutation  (listT_to_listD (map f1 lt)) l2.
      Proof.
        intros f1 b f2 l1 l2  Hf Hl Hp.
        refine (permutation_listT_lift_map f1 b f2 _ Hl Hp).
        intros t Ht.
        assert (A2 := Ht).
        rewrite mem_tuple_as_pairs_listT in Ht.
        assert (A1 := Permutation_in _ Hp Ht).
        rewrite listo_to_olist_simpl_lift_map in Hl.
        apply listo_to_olist_some in Hl.
        destruct (map_Some_in_1 _ _ _ Hl _ A1) as [d [A3 A4]].
        rewrite  <- (Hf _ _ A2 A4),A4.
        apply eq_refl.
      Qed.

      Lemma  permutation_listT_flat_map :
        forall f1 f2,
          (forall t, Permutation (map (fun x : tuple => drec (tuple_as_dpairs x)) (f1 t)) (f2 (drec (tuple_as_dpairs t)))) ->
          forall l1 l2,
            Permutation l1 l2 ->
            Permutation  (listT_to_listD (flat_map f1 l1)) (flat_map f2 (listT_to_listD l2)).
      Proof.
        unfold listT_to_listD.
        intros f1 f2 Aux l1 l2 H.
        rewrite <- _permut_eq_Permutation in *.
        induction H;[constructor|subst].
        simpl.
        rewrite 2 map_app.
        simpl.
        rewrite flat_map_app.
        simpl.
        rewrite  _permut_eq_Permutation in *.
        rewrite   app_assoc.
        refine (Permutation_trans _ (Permutation_app_comm _ _) ).
        refine (Permutation_trans  (Permutation_app_comm _ _) _).
        rewrite   app_assoc.
        apply Permutation_app;[|apply Aux].
        rewrite map_app,flat_map_app in IH_permut.
        refine (Permutation_trans _ (Permutation_app_comm _ _) ).
        apply IH_permut.
      Qed.
      
      Lemma permutation_listT_lift_flat_map :
        forall f1 f2,
          (forall t l,
              (f2 (drec (tuple_as_dpairs t))) =  Some l ->
              Permutation (map (fun x : tuple => drec (tuple_as_dpairs x)) (f1 t)) l) ->
          forall l1 l2 l3,
            lift_flat_map f2 l2 = Some l3 ->
            Permutation (listT_to_listD l1) l2 ->
            Permutation  (listT_to_listD (flat_map f1 l1))  l3.
      Proof.
        intros f1 f2 Aux.
        intros l1 l2 l3 H H1.
        rewrite <- _permut_eq_Permutation in *.
        assert (H2 : exists l4, l4 = listT_to_listD l1 /\ _permut eq l4 l2).
        { exists (listT_to_listD l1) ; auto. }
        destruct H2 as [l4 [H3 H4]]. clear H1.
        revert H4 l1 H3 l3 H.
        induction 1;subst.
        - intros [| ] ;[|discriminate];intros _ [| ] H1;
            [ constructor|discriminate].
        - intros [ | b' l'];[discriminate| ].
          intros Hbl l3 H3. simpl.
          unfold listT_to_listD.
          rewrite map_app.
          inversion Hbl;subst.
          assert (A1 := lift_flat_map_app_eq _ _  _ H3).
          destruct A1 as [m1 [m2 [-> [H2 H5]]]].
          simpl in H5.
          destruct (f2 (drec (tuple_as_dpairs b'))) eqn:D5;[|discriminate].
          unfold lift in H5.
          destruct ( lift_flat_map f2 l2 ) eqn:D6;[|discriminate].
          inversion H5;subst;clear H5.
          clear H3.
          rewrite  _permut_eq_Permutation.
          refine (Permutation_trans _ (Permutation_app_comm _ _)).
          rewrite <- app_assoc.
          apply Permutation_app;[apply Aux;apply D5|idtac].
          refine (Permutation_trans _ (Permutation_app_comm _ _)).
          rewrite  <- _permut_eq_Permutation.
          apply IH_permut;[apply eq_refl| ].
          apply lift_flat_map_app ;trivial.
      Qed.

      Lemma permutation_listT_lift_flat_map_in :
        forall f1 f2 l1,
          (forall t l,
              In t l1 ->
              (f2 (drec (tuple_as_dpairs t))) =  Some l ->
              Permutation (map (fun x : tuple => drec (tuple_as_dpairs x)) (f1 t)) l) ->
          forall l2 l3,
            lift_flat_map f2 l2 = Some l3 ->
            Permutation (listT_to_listD l1) l2 ->
            Permutation  (listT_to_listD (flat_map f1 l1))  l3.
      Proof.
        intros f1 f2 l1 Aux.
        intros l2 l3 H H1.
        rewrite <- _permut_eq_Permutation in *.
        assert (H2 : exists l4, l4 = listT_to_listD l1 /\ _permut eq l4 l2).
        { exists (listT_to_listD l1) ; auto. }
        destruct H2 as [l4 [H3 H4]]. clear H1.
        revert H4 l1 Aux   H3 l3 H.
        induction 1;subst.
        - intros [| ] Aux;[|discriminate];intros _ [| ] H1;
            [ constructor|discriminate].
        - intros [ | b' l'] Aux;[discriminate| ].
          intros Hbl l3 H3. simpl.
          unfold listT_to_listD.
          rewrite map_app.
          inversion Hbl;subst.
          assert (A1 := lift_flat_map_app_eq _ _  _ H3).
          destruct A1 as [m1 [m2 [-> [H2 H5]]]].
          simpl in H5.
          destruct (f2 (drec (tuple_as_dpairs b'))) eqn:D5;[|discriminate].
          unfold lift in H5.
          destruct ( lift_flat_map f2 l2 ) eqn:D6;[|discriminate].
          inversion H5;subst;clear H5.
          clear H3.
          rewrite  _permut_eq_Permutation.
          refine (Permutation_trans _ (Permutation_app_comm _ _)).
          rewrite <- app_assoc.
          apply Permutation_app;[apply Aux;[left;apply eq_refl|apply D5]|idtac].
          refine (Permutation_trans _ (Permutation_app_comm _ _)).
          rewrite  <- _permut_eq_Permutation.
          apply IH_permut;[do 3 intro;apply Aux;right;apply H|apply eq_refl | ].
          apply lift_flat_map_app ;trivial.
      Qed.

      Lemma  permutation_listT_lift_map_list_tuple :
        forall (f1:list tuple -> tuple)  lt,
        forall f2 l1 l2,
          (forall a : list tuple, In a lt -> Some (drec (tuple_as_dpairs (f1 a))) = f2 (map (fun x : tuple => drec (tuple_as_dpairs x)) a)) ->
          lift_map f2 l1 = Some l2 ->
          Permutation (map listT_to_listD lt) l1 ->
          Permutation  (listT_to_listD (map f1 lt)) l2.
      Proof.
        intros f1 b f2 l1 l2 Hf Hl Hp.
        apply Permutation_map with (f := f2) in Hp.
        rewrite listo_to_olist_simpl_lift_map in Hl.
        apply listo_to_olist_some in Hl.
        rewrite Hl in Hp.
        apply (Permutation_map_Some_eq data_eq_dec).
        refine (Permutation_trans  _ Hp ).
        unfold listT_to_listD.
        do 3 rewrite map_map.
        apply bags_same_mult_has_equal.
        intros d;apply f_equal3;[apply eq_refl| |apply eq_refl].
        apply map_ext_in.
        apply Hf.
      Qed.
      
      Lemma  permutation_listT_lift_map_list_tuple_weak :
        forall (f1:list tuple -> tuple)  lt,
        forall f2 l1 l2,
          (forall (a : list tuple) b, In a lt -> f2 (map (fun x : tuple => drec (tuple_as_dpairs x)) a) = Some b -> b = (drec (tuple_as_dpairs (f1 a))) ) ->
          lift_map f2 l1 = Some l2 ->
          Permutation (map listT_to_listD lt) l1 ->
          Permutation  (listT_to_listD (map f1 lt)) l2.
      Proof.
        do 8 intro.
        refine (permutation_listT_lift_map_list_tuple _ _ _ _ H0 H1).
        intros a Ha.
        assert (A1 : In (listT_to_listD a) (map listT_to_listD lt) ).
        { rewrite in_map_iff;exists a;split;[apply eq_refl|apply Ha]. }
        assert (A2 := Permutation_in _ H1 A1).
        rewrite listo_to_olist_simpl_lift_map in H0.
        apply listo_to_olist_some in H0.
        assert (A3 := map_Some_in_1 _ _ _ H0 _ A2).
        destruct A3 as [b [A4 A3]].
        assert (A5 := H _ _ Ha A3).
        rewrite <- A5,<- A3.
        apply eq_refl.
      Qed.

      Definition data_option_elim f :=
        fun x:data => match f x with Some y => y | None => dunit end.

      Lemma data_option_elim_some :
        forall l1,
        forall f1, forall x y, In x l1 -> f1 x = Some y -> y = (data_option_elim f1) x.
      Proof.
        intros l1 f1 x y H1 H2.
        unfold data_option_elim.
        rewrite H2;apply eq_refl.
      Qed.

      Lemma data_option_elim_permut_lift_map :
        forall f1 l1 l2,
          lift_map f1 l1 = Some l2 -> Permutation (map (data_option_elim f1) l1) l2.
      Proof.
        intros f1 l1 l2 H1.
        refine
          (Permutation_lift_map_weak
             _ _ _ (data_option_elim_some _ _)
             H1 (Permutation_refl _)).
      Qed.

      
      Definition bool_option_elim f :=
        fun x:data => match f x with Some y => y | None => false end.

      Lemma bool_option_elim_some :
        forall l1,
        forall f1, forall x y, In x l1 -> f1 x = Some y ->  (bool_option_elim f1) x = y.
      Proof.
        intros l1 f1 x y H1 H2.
        unfold bool_option_elim.
        rewrite H2;apply eq_refl.
      Qed.

      Lemma bool_option_elim_permut_lift_filter :
        forall f1 l1 l2,
          lift_filter f1 l1 = Some l2 -> Permutation (filter (bool_option_elim f1) l1) l2.
      Proof.
        intros f1 l1 l2 H1.
        refine
          (permutation_lift_filter_weak
             _ _ (bool_option_elim_some  _  _ )
             (Permutation_refl _) H1).
      Qed.

      Lemma Permutation_bdistinct_flat_map_partition_aggterm :
        forall env lt g,
          Permutation
            (bdistinct (listT_to_listD lt))
            (bdistinct (map (fun r => drec (tuple_as_dpairs r)) (flat_map (fun lt => snd lt) (  (Partition.partition (mk_olists (OVal TRcd))
                                                                                                                   (fun t : tuple => map (fun f0 : aggterm _  => interp_aggterm TRcd (env_t TRcd env t) f0) g)
                                                                                                                   lt))))).
      Proof.
        intros env lt la.
        refine (NoDup_Permutation (bdistinct_NoDup _) (bdistinct_NoDup _) _  ).
        intro d;split;intro Hd.
        - apply In_bdistinct in Hd.
          apply bdistinct_same_elemts.
          unfold listT_to_listD in Hd.
          rewrite in_map_iff in Hd.
          destruct Hd as [t [<- Hd]].
          rewrite in_map_iff.
          exists  t;split.
          apply eq_refl.
          refine  (Permutation_in _ _ Hd).
          rewrite <- _permut_eq_Permutation.
          apply (Partition.partition_permut _ _ _ ).
        - apply In_bdistinct in Hd.
          apply bdistinct_same_elemts.
          unfold listT_to_listD in Hd.
          rewrite in_map_iff in Hd.
          destruct Hd as [t [<- Hd]].
          apply In_tuple_as_pairs_eq_listT.
          refine (Permutation_in _ (Permutation_sym _) Hd).
          rewrite <- _permut_eq_Permutation.
          apply (Partition.partition_permut _ _ _ ).      
      Qed.  

      Lemma  permutation_listT_lift_map_list_tuple_weak_alt :
        forall (f1:list tuple -> tuple)  lt,
        forall f2 l1 l2,
          (forall x, In x l1 -> exists y, x = dcoll y) ->
          (forall (a : list tuple) b, In a lt -> f2 (dcoll (map (fun x : tuple => drec (tuple_as_dpairs x)) a)) = Some b -> b = (drec (tuple_as_dpairs (f1 a))) ) ->
          lift_map f2 l1 = Some l2 ->
          Permutation (map listT_to_listD lt) (map (fun x : data => match x with
                                                                   | dcoll l => l
                                                                   | _ => nil
                                                                   end) l1) ->
          Permutation  (listT_to_listD (map f1 lt)) l2.
      Proof.
        intros f1 b f2 l1 l2 Aux Hf Hl Hp.
        refine (Permutation_trans _ (@Permutation_lift_map_weak _ _   _ (fun x => data_option_elim f2 x) (map (fun x => dcoll (listT_to_listD x)) b) _ l1 _ (data_option_elim_some _ _) Hl _ ) ).
        - unfold listT_to_listD.
          assert (H: forall A (l1 l2:list A), l1 = l2 -> Permutation l1 l2) by (clear;intros;subst l1;apply Permutation_refl).
          rewrite 2 map_map;simpl.
          apply H;clear H.
          apply map_ext_in.
          intros a Ha.
          unfold data_option_elim.
          assert (Aux_0 : In (listT_to_listD a)  (map (fun x : data => match x with
                                                                      | dcoll l => l
                                                                      | _ => nil
                                                                      end) l1)).
          { refine (Permutation_in _ Hp _ ).
            rewrite in_map_iff;exists a;split;[apply eq_refl|apply Ha].
          }
          rewrite in_map_iff in Aux_0.
          destruct Aux_0 as [d [A1 A2] ].
          destruct (lift_map_some_in_1 _ _ Hl _ A2) as [d2 [A3 A4]].
          unfold listT_to_listD in A1.
          rewrite <- A1 in *.
          destruct (Aux _ A2) as [ld ->].
          rewrite A4.
          apply eq_sym.
          apply (Hf _ _ Ha).
          rewrite <- A1.
          apply A4.
        - apply Permutation_sym in Hp.
          apply Permutation_sym.
          revert dependent l2.
          revert dependent b.
          revert dependent l1.
          induction l1;intros.
          + simpl in Hp.
            apply Permutation_nil in Hp.
            apply map_eq_nil in Hp.
            rewrite Hp;constructor.
          +  destruct (Aux a (or_introl eq_refl)) as [la ->].
             simpl in Hl.
             destruct (f2 (dcoll la)) eqn:D1;[|discriminate].
             unfold lift in Hl.
             destruct (lift_map f2 l1) eqn:D2;[|discriminate].
             inversion Hl;subst l2;clear Hl.
             assert (IH := IHl1 (fun x H => Aux x (or_intror H)));clear IHl1.
             rewrite <- _permut_eq_Permutation in Hp.
             inversion Hp;subst.
             rewrite  _permut_eq_Permutation in H3;clear Hp.
             destruct (map_app_break _ (eq_sym H1)) as [m1 [m2 [-> [-> M1]]]].
             destruct (map_eq_cons _ _ _ _ (eq_sym M1)) as [m3 [m4 [-> [<- <-]]]].
             rewrite map_app.
             simpl.
             apply Permutation_cons_app.
             rewrite <- map_app.
             rewrite <- map_app in H3.
             refine (IH _ _ H3 _ eq_refl).
             intros a b G1.
             refine (Hf _ _ _ ).
             apply in_app_iff.
             apply in_app_iff in G1.
             destruct G1 as [G1|G1];[left|right;right];apply G1.
      Qed.

      Lemma _permut_Permutation_listT_to_listD_split : 
        forall f1,
        forall llt l,
          (forall l1 l2 : list tuple, In l1 llt -> l1 =PE= l2 -> f1 l1 =t= f1 l2) ->
          _permut (Permutation (A:=data)) (map listT_to_listD llt) l ->    
          exists llt', Permutation (map listT_to_listD llt') l /\
                  Permutation (listT_to_listD (map f1 llt))  (listT_to_listD (map f1 llt'))  /\
                  _permut (fun x y => Oeset.compare  (Partition.OLA OTuple) x y = Eq)    llt  llt'  .
      Proof.
        intros f1.
        induction llt as [|lt llt];intros l Aux1 Heq.
        - inversion Heq;subst l;exists nil;intuition.
          constructor.
        - simpl in Heq;inversion Heq;subst.          
          destruct (IHllt _ (fun l1 l2 H => Aux1 l1 l2 (or_intror H)) H3) as [llt' [A1 A2]].
          assert (H2 := H1).
          apply Permutation_listT_spec in H1.
          destruct H1 as [lt' [H5 H4]].
          exists (lt'::llt');repeat split.
          + refine (Permutation_trans  _ (Permutation_middle _ _ _)).
            simpl.
            apply Permutation_cons;[apply H4|apply A1].
          + simpl;apply Permutation_cons.
            * apply f_equal.
              apply tuple_as_dpairs_eq.
              apply (Aux1 _ _ (or_introl eq_refl) H5).
            * apply A2.
          + apply Oeset.permut_cons;[|apply A2].
            apply Partition.compare_list_t.
            apply H5.
      Qed.

      Notation "s1 '=PPE=' s2" := 
        (_permut (@Permutation _) s1 s2) (at level 70, no associativity).
      
      Lemma  _permut_Permutation_listT_lift_map_list_tuple :
        forall (f1:list tuple -> tuple)  lt,
        forall f2 l1 l2,
          (forall x, In x l1 -> exists y, x = dcoll y) ->
          (forall l1 l2, In l1 lt -> l1 =PE= l2 -> f1 l1 =t= f1 l2) ->          
          (forall l1 l2, In l1 lt -> l1 =PE= l2 ->
                    forall b1,
                      f2 (dcoll (listT_to_listD l2)) = Some b1 -> f2 (dcoll (listT_to_listD l1)) = Some b1) ->
          (forall (a : list tuple) b, In a lt -> f2 (dcoll (map (fun x : tuple => drec (tuple_as_dpairs x)) a)) = Some b -> b = (drec (tuple_as_dpairs (f1 a))) ) ->
          lift_map f2 l1 = Some l2 ->
          (map listT_to_listD lt) =PPE= (map (fun x : data => match x with
                                                             | dcoll l => l
                                                             | _ => nil
                                                             end) l1) ->
          Permutation  (listT_to_listD (map f1 lt)) l2.
      Proof.
        intros f1 lt f2 l1 l2 Aux1 Aux2 Aux3 Hf Hl1 Heq.
        destruct (_permut_Permutation_listT_to_listD_split _ _ Aux2 Heq)
          as [llt' [A1 [A2 A3]]].
        refine (Permutation_trans A2 _ ).
        assert (Hf' : forall (a : list tuple) (b : data),
                   In a llt' -> f2 (dcoll (map (fun x : tuple => drec (tuple_as_dpairs x)) a)) = Some b -> b = drec (tuple_as_dpairs (f1 a))).
        { do 3 intros.          
          destruct  (_permut_in (Oeset.permut_sym  A3) _ H) as [l3 [A5 A6]].
          apply Partition.compare_list_t in A6.
          unfold listT_to_listD in Aux3.
          apply _permut_sym in  A6;[|do 4 intro;apply Oeset.compare_eq_sym].
          apply  (Aux3 _ _ A5 A6) in H0.
          rewrite <- (proj1 (tuple_as_dpairs_eq _ _) (Aux2 _ _ A5 A6)).
          revert A5 H0.
          apply Hf. }
        clear dependent lt.
        refine  (permutation_listT_lift_map_list_tuple_weak_alt _ _ _ _ Aux1 Hf' Hl1 A1).
      Qed.

      
      Lemma inBE_tuple_as_pairs_bagT :
        forall (b:Febag.bag BTupleT) t,
          t inBE b <->
          In (drec (tuple_as_dpairs t)) (bagT_to_listD b).
      Proof.
        intros b t.
        apply mem_tuple_as_pairs_listT.
      Qed.

      Lemma nb_occ_mult_bag_eq :
        forall t d,
          mult (bagT_to_listD d) (drec  (tuple_as_dpairs t)) =
          N.to_nat (Febag.nb_occ _ t d).
      Proof.
        intros t d.
        unfold bagT_to_listD.
        rewrite Febag.nb_occ_elements.
        apply nb_occ_mult_listT_eq.
      Qed.    
      
      Lemma nb_occ_mult_bagT_eq_complete :
        forall b d,
          mult (bagT_to_listD b) d = 0 \/
          exists t, d = drec (tuple_as_dpairs t) /\
               mult (bagT_to_listD b) (drec (tuple_as_dpairs t)) =  N.to_nat (Febag.nb_occ _ t b) /\ t inBE b.
      Proof.
        intros b d.
        destruct (in_dec data_eq_dec d (bagT_to_listD b) ) as [H1|N1];
          [right|left;apply mult_0_equiv_nin;apply N1].
        assert (A1 := in_listT_is_drec _ _ H1).
        destruct A1;subst.
        exists x;split;[apply eq_refl| ].
        rewrite nb_occ_mult_bag_eq;split;[apply eq_refl| ].
        apply inBE_tuple_as_pairs_bagT.
        apply H1.
      Qed.
      
      Lemma bagT_to_listD_eq :
        forall b1 b2 , b1 =BE= b2 <-> bagT_to_listD b1 = bagT_to_listD b2.
      Proof.
        intros b1 b2;split;intro H.
        - unfold bagT_to_listD.
          apply Febag.elements_spec1 in H.
          apply comparelA_listT_eq.
          apply H.
        - apply comparelA_listT_eq in H.
          rewrite Febag.nb_occ_equal.
          intro t.
          rewrite 2 Febag.nb_occ_elements.
          apply (Oeset.nb_occ_eq_2 _ _ _ _ H).
      Qed.
      
      Lemma permutation_bagT_eq :
        forall b1 b2 , b1 =BE= b2 <-> Permutation (bagT_to_listD b1) (bagT_to_listD b2).
      Proof.
        intros b1 b2;split;intro H.
        - unfold bagT_to_listD.
          apply Febag.elements_spec1 in H.
          unfold listT_to_listD.
          apply Oeset.permut_refl_alt in H.
          apply _permut_eq_Permutation.
          eapply _permut_map;[|apply H].
          do 5 intro.
          apply f_equal.
          apply tuple_as_dpairs_eq.
          apply H2.
        - unfold bagT_to_listD in H;rewrite <- Permutation_listT_eq in H.
          rewrite Febag.nb_occ_equal.
          intro e.
          apply Oeset.permut_nb_occ with (x := e) in H.
          do 2 rewrite Febag.nb_occ_elements.
          apply H.
      Qed.

      Lemma bagT_to_listD_permutation_eq_same :
        forall b1 b2 , bagT_to_listD b1 = bagT_to_listD b2 <-> Permutation (bagT_to_listD b1) (bagT_to_listD b2).
      Proof.
        intros b1 b2.
        rewrite <- bagT_to_listD_eq.
        apply permutation_bagT_eq.
      Qed.
      
      Lemma mult_bagT_S :
        forall l d n,
          mult (listT_to_listD  l) d = S n ->
          exists t, d = drec (tuple_as_dpairs t).
      Proof.
        fix thm 1.
        intros [ | t1 l] d n Hn;[inversion Hn| ].
        simpl mult in Hn.
        unfold EquivDec.equiv_dec in Hn.
        destruct (data_eqdec d (drec (tuple_as_dpairs t1)) ) eqn:D1
        ;[exists t1;apply e| ];
          apply (thm _ _ _ Hn).
      Qed.

      Lemma mult_bagT_0_not_drec :
        forall x, (forall r, x <> drec r) -> forall d, (mult (bagT_to_listD d) x = 0)%nat.
      Proof.
        intros x H d.
        unfold bagT_to_listD.
        induction ((Febag.elements (Fecol.CBag (CTuple TRcd)) d)) as [|t1 l1];[apply eq_refl| ].
        simpl.
        destruct EquivDec.equiv_dec as [X|X];[|apply IHl1].
        unfold Equivalence.equiv in X.
        rewrite X in *;clear X.
        assert (N1 := H (tuple_as_dpairs t1)).
        exfalso;apply N1;apply eq_refl.
      Qed.
      
      Lemma permutation_bagT_tuple :
        forall b l,
          Permutation (bagT_to_listD b) l -> forall d, In d l -> exists t, d = drec (tuple_as_dpairs t).
      Proof.
        intros b l H d Hd.
        apply Permutation_sym in H.
        assert (A2 := Permutation_in _ H Hd).
        unfold bagT_to_listD,listT_to_listD in A2.
        rewrite in_map_iff in A2.
        destruct A2 as [x [A2 A3]].
        subst.
        exists x;apply eq_refl.
      Qed.

      Lemma permutation_bagT_singleton_empty_tuple :   
        Permutation (bagT_to_listD (Febag.singleton BTupleT (empty_tuple TRcd))) (drec nil :: nil).
      Proof.
        unfold bagT_to_listD,listT_to_listD.
        unfold tuple_as_dpairs,tuple_as_pairs.
        assert (A1 := Febag.elements_singleton BTupleT (empty_tuple TRcd)).
        destruct A1 as [t [A1 A2]].
        rewrite A2.
        simpl.
        rewrite map_map.
        apply Permutation_cons;[|constructor].
        apply tuple_eq_labels in A1.
        rewrite (Fset.equal_eq_1 _ _ _ _ (labels_empty_tuple  _ )) in A1.
        rewrite <- (Fset.elements_spec1 _ _ _ A1).
        rewrite Fset.elements_empty.
        apply eq_refl.
      Qed.

      Lemma  permutation_bagT_filter :
        forall f1 f2 l b ,
          ( forall e e' : tuple, (Febag.nb_occ BTupleT e b >= 1)%N -> e =t= e' -> f1 e = f1 e') ->
          Permutation (bagT_to_listD b) l ->
          (forall t d, t inBE b -> f2 (drec (tuple_as_dpairs t)) = Some d -> f1 t = d ) ->
          forall l',
            lift_filter f2 l = Some l' ->
            Permutation
              (bagT_to_listD
                 (Febag.filter BTupleT f1
                               b)) l'.
      Proof.
        intros f1 f2 l b Aux Hp Ht l' Hl'.
        rewrite <- Permutation_mult_equiv.
        intro d.
        destruct (in_dec  data_eq_dec d l) as [A1|N1].
        - assert (A2 := Permutation_listT_is_tuple  _ Hp _ A1).
          destruct A2;subst.
          rewrite nb_occ_mult_bag_eq.
          rewrite Febag.nb_occ_filter;[|apply Aux].
          assert (A2 : exists d, f2 (drec (tuple_as_dpairs x)) = Some d).
          { revert l' Hl' A1.
            clear.
            induction l as [|a l1];intros l2 H1 H2;[inversion H2| ].
            destruct H2.
            - subst.
              simpl in H1.
              destruct (f2 (drec (tuple_as_dpairs x)) ) eqn:D1;[|discriminate].
              exists b;apply eq_refl.
            - simpl in H1.
              destruct (f2 a ) eqn:D1;[|discriminate].
              destruct (lift_filter f2 l1) eqn:D2;[|discriminate].
              destruct b.
              + destruct l2;[discriminate| ].
                inversion H1;subst.
                apply (IHl1 l2 eq_refl H).
              +    apply (IHl1 l2 H1 H). }
          apply     lift_filter_Some_filter  in Hl'.
          rewrite Hl'.
          rewrite mult_filter.
          destruct A2 as [b2 A2].
          rewrite A2.
          apply Permutation_sym in Hp.
          assert (A3 := Permutation_in _ Hp A1).
          rewrite <- inBE_tuple_as_pairs_bagT in A3.
          rewrite (Ht _ _ A3 A2).
          rewrite <- Permutation_mult_equiv in Hp.
          rewrite  Hp.
          rewrite nb_occ_mult_bag_eq.
          rewrite N2Nat.inj_mul.
          destruct b2;apply eq_refl.
        - apply     lift_filter_Some_filter  in Hl'.
          rewrite Hl'.
          rewrite mult_filter.
          rewrite (proj2 (mult_0_equiv_nin  _ _ _) N1).
          assert (A1 : ~ In d (bagT_to_listD b)).
          {  intro N2;apply N1.
             apply (Permutation_in _ Hp N2). }
          rewrite mult_0_equiv_nin.
          intro N2.
          apply A1.
          unfold bagT_to_listD.
          rewrite <- Permutation_mult_equiv in Hp.
          unfold bagT_to_listD,listT_to_listD in N2.
          rewrite in_map_iff in N2.
          destruct N2 as [x [N2 N3]].
          subst.
          apply Febag.in_elements_mem in N3.
          rewrite Febag.mem_filter in N3.
          + unfold listT_to_listD.
            rewrite in_map_iff.
            rewrite Febag.mem_unfold in N3.
            apply andb_prop in N3.
            rewrite Oeset.mem_bool_true_iff in N3.
            destruct (proj1 N3) as [t2 [A3 A4]].
            rewrite tuple_as_dpairs_eq in A3.
            exists t2;split;[rewrite A3;apply eq_refl| apply A4].
          + do 3 intro.
            apply Aux.
            rewrite Febag.mem_nb_occ in H.
            rewrite <- BasicFacts.N_diff_0_ge_1.
            apply H.
      Qed.
      
      Lemma permutation_bagT_listT_map :
        forall X (f:X -> tuple) l,
          Permutation (bagT_to_listD  (Febag.mk_bag BTupleT (map f l))) (listT_to_listD  (map f l)).
      Proof.
        intros X f.
        induction l.
        - simpl.
          unfold bagT_to_listD.
          rewrite Febag.elements_empty.
          constructor.
        - rewrite <- Permutation_mult_equiv.
          intro d.
          rewrite <- Permutation_mult_equiv in IHl.
          assert (IHd := IHl d).
          simpl.
          destruct (EquivDec.equiv_dec d (drec (tuple_as_dpairs (f a)))) as [H1|N1].
          + inversion H1;subst.
            rewrite nb_occ_mult_bag_eq.
            rewrite nb_occ_mult_bag_eq in IHd.
            simpl.
            destruct (EquivDec.equiv_dec (drec (tuple_as_dpairs (f a))) (drec (tuple_as_dpairs (f a)))) as [_|N1];
              [|destruct (N1 eq_refl)].
            rewrite Febag.nb_occ_add.
            rewrite Oeset.eq_bool_refl.
            rewrite <- IHd,<- N2Nat.inj_succ.
            apply f_equal.
            apply N.add_1_l.
          + simpl.
            rewrite <- IHd.
            clear IHd.
            unfold RelationClasses.complement,Equivalence.equiv in N1.
            assert (A1 := nb_occ_mult_bagT_eq_complete (Febag.mk_bag BTupleT (map f l)) d).
            destruct A1 as [A1|A1].
            * rewrite A1.
              assert (A2 := nb_occ_mult_bagT_eq_complete (f a addBE Febag.mk_bag BTupleT (map f l)) d).
              destruct A2 as [A2|A2];[apply A2| ].
              destruct A2 as [t [A2 [A3 A0]]].
              subst.
              rewrite A3.
              rewrite Febag.nb_occ_add.
              destruct (Oeset.eq_bool OTuple t (f a)) eqn:A4;
                [|simpl;rewrite <- nb_occ_mult_bag_eq;apply A1].
              rewrite Oeset.eq_bool_true_compare_eq in A4.
              rewrite tuple_as_dpairs_eq in A4.
              rewrite A4 in N1.
              destruct (N1 eq_refl).
            * destruct A1 as [t [A2 A3]].
              subst.
              do 2 rewrite nb_occ_mult_bag_eq.
              apply f_equal.
              rewrite Febag.nb_occ_add.
              destruct (Oeset.eq_bool OTuple t (f a)) eqn:A4;
                [|simpl;apply eq_refl].
              rewrite Oeset.eq_bool_true_compare_eq in A4.
              rewrite tuple_as_dpairs_eq in A4.
              rewrite A4 in N1.
              destruct (N1 eq_refl).
      Qed.

      Lemma permutation_bagT_listT_map_alt :
        forall X (f:X -> tuple) l1 l2,
          Permutation l1 l2 ->
          Permutation (bagT_to_listD  (Febag.mk_bag BTupleT (map f l1))) (listT_to_listD  (map f l2)).
      Proof.
        intros X f l1 l2 H1.
        refine (Permutation_trans (permutation_bagT_listT_map f l1) _).
        do 2 apply Permutation_map.
        apply H1.
      Qed.
      
      Lemma  permutation_bagT_map :
        forall f1 lt,
        forall f2 l1 ,
          (forall t1 , In t1 (Febag.elements _ (Febag.mk_bag BTupleT lt)) -> drec (tuple_as_dpairs (f1 t1)) =  f2 (drec (tuple_as_dpairs t1)) ) ->
          Permutation (bagT_to_listD (Febag.mk_bag BTupleT lt)) l1 ->
          Permutation  (bagT_to_listD (Febag.map BTupleT BTupleT f1 (Febag.mk_bag BTupleT lt))) (map f2 l1).
      Proof.
        intros f1 lt f2 l1 H Hp.
        refine (Permutation_trans   (permutation_bagT_listT_map  _ _) _).
        unfold bagT_to_listD in Hp.
        apply Permutation_map with (f := f2) in Hp.
        refine (Permutation_trans  _ Hp ).
        unfold listT_to_listD.
        do 2 rewrite map_map.
        rewrite <- Permutation_mult_equiv.
        intros d;apply f_equal3;[apply eq_refl| |apply eq_refl].
        apply map_ext_in.
        apply H.
      Qed.

      Lemma  permutation_bagT_lift_map_mk_bag :
        forall f1 lt,
        forall f2 l1 l2,
          (  forall a : tuple,
              In a (Febag.elements BTupleT (Febag.mk_bag BTupleT lt)) -> Some (drec (tuple_as_dpairs (f1 a))) = f2 (drec (tuple_as_dpairs a))) ->
          lift_map f2 l1 = Some l2 ->
          Permutation (bagT_to_listD (Febag.mk_bag BTupleT lt)) l1 ->
          Permutation  (bagT_to_listD (Febag.map BTupleT BTupleT f1 (Febag.mk_bag BTupleT lt))) l2.
      Proof.
        intros f1 b f2 l1 l2  Hf Hl Hp.
        refine (Permutation_trans   (permutation_bagT_listT_map  _ _) _).
        unfold bagT_to_listD in Hp.
        apply Permutation_map with (f := f2) in Hp.
        rewrite listo_to_olist_simpl_lift_map in Hl.
        apply listo_to_olist_some in Hl.
        rewrite Hl in Hp.
        apply (Permutation_map_Some_eq data_eq_dec).
        refine (Permutation_trans  _ Hp ).
        unfold listT_to_listD.
        do 3 rewrite map_map.
        apply bags_same_mult_has_equal.
        intros d;apply f_equal3;[apply eq_refl| |apply eq_refl].
        apply map_ext_in.
        apply Hf.
      Qed.
      
      Lemma  permutation_bagT_lift_map :
        forall f1 b,
        forall f2 l1 l2,
          (forall a, In a  (Febag.elements _ b) -> Some (drec (tuple_as_dpairs (f1 a))) = f2 (drec (tuple_as_dpairs a)) ) ->
          lift_map f2 l1 = Some l2 ->
          Permutation (bagT_to_listD b) l1 ->
          Permutation  (bagT_to_listD (Febag.map BTupleT BTupleT f1 b)) l2.
      Proof.
        intros f1 b f2 l1 l2  Hf Hl Hp.
        refine (Permutation_trans   (permutation_bagT_listT_map  _ _) _).
        unfold bagT_to_listD in Hp.
        apply Permutation_map with (f := f2) in Hp.
        rewrite listo_to_olist_simpl_lift_map in Hl.
        apply listo_to_olist_some in Hl.
        rewrite Hl in Hp.
        apply (Permutation_map_Some_eq data_eq_dec).
        refine (Permutation_trans  _ Hp ).
        unfold listT_to_listD.
        do 3 rewrite map_map.
        apply bags_same_mult_has_equal.
        intros d;apply f_equal3;[apply eq_refl| |apply eq_refl].
        apply map_ext_in.
        apply Hf.
      Qed.

      Lemma permutation_bagT_listT_flat_map :
        forall (f:tuple -> list tuple) l,
          Permutation (bagT_to_listD  (Febag.mk_bag BTupleT (flat_map f l))) (listT_to_listD  (flat_map f l)).
      Proof.
        intros f l.
        rewrite <- Permutation_mult_equiv.
        intro d.
        assert (A1 := nb_occ_mult_bagT_eq_complete (Febag.mk_bag BTupleT (flat_map f l)) d).
        destruct A1.
        - rewrite H;apply eq_sym.
          rewrite mult_0_equiv_nin.
          rewrite mult_0_equiv_nin in H.
          intro N1;apply H;clear H.
          unfold bagT_to_listD,listT_to_listD in *.
          rewrite in_map_iff in N1.
          destruct N1 as [x [N1 N2]].
          rewrite in_map_iff.
          assert (H1: x inBE  (Febag.mk_bag BTupleT (flat_map f l))).
          {  rewrite Febag.mem_mk_bag.
             apply Oeset.in_mem_bool;apply N2. }
          rewrite Febag.mem_unfold in H1.
          rewrite Oeset.mem_bool_true_iff in H1.
          destruct H1 as [z [Hz N3]].
          exists z;split;[|apply N3].
          subst d.
          refine (eq_sym   (f_equal _ _)).
          exact (proj1 (tuple_as_dpairs_eq _ _ ) Hz).
        - destruct H as [t [H0 [H1 H_1]]];subst d.
          rewrite H1;clear H1.
          rewrite Febag.nb_occ_mk_bag.
          rewrite nb_occ_mult_listT_eq.
          apply eq_refl.
      Qed.

      Lemma  febag_is_empty_is_Z_le_bcount_0 :
        forall b (l : list data),
          (forall d,  In d (bagT_to_listD b)  <-> In d l) ->
          Febag.is_empty BTupleT b = (if ZArith_dec.Z_le_dec (BinInt.Z.of_nat (bcount l)) 0 then true else false).
      Proof.
        intros  b l.
        intros A1.
        rewrite Febag.is_empty_spec. 
        destruct (ZArith_dec.Z_le_dec (BinInt.Z.of_nat (bcount l)) 0) eqn:D3.
        - rewrite Febag.nb_occ_equal.
          intro t.
          rewrite Febag.nb_occ_empty.
          assert (N1 := A1 (drec (tuple_as_dpairs t))).
          destruct l;[|discriminate].
          apply Febag.not_mem_nb_occ.
          destruct  (t inBE? b) eqn:D5;[|apply eq_refl].
          rewrite inBE_tuple_as_pairs_bagT in D5.
          rewrite N1 in D5;destruct D5.
        - apply Bool.not_true_is_false;intro N1.
          destruct l;[apply n;apply BinInt.Z.le_refl| ].
          assert (N2 := A1 d).
          simpl in N2.
          assert (N3 :  In d (bagT_to_listD b) ).
          { apply N2;left;apply eq_refl. }
          rewrite Febag.nb_occ_equal in N1.
          assert (Aux : Febag.elements _ b = nil).
          { destruct (Febag.elements BTupleT b) eqn:D1;[apply eq_refl| ].
            assert (N1 := N1 t).
            rewrite Febag.nb_occ_elements,D1 in N1.
            rewrite Febag.nb_occ_empty in N1.
            simpl in N1.
            rewrite Oeset.compare_eq_refl in N1.
            destruct ( (Oeset.nb_occ OTuple t l0));discriminate. }
          unfold bagT_to_listD in N3;rewrite Aux in N3.
          destruct N3.
      Qed.
      
      Lemma  permutation_bagT_omap_product :
        forall l l1 l2   x,
          x = Some (dcoll l2) ->
          omap_product (fun _:data => x) l1 = Some l ->
          forall b1 b2,
            Permutation (bagT_to_listD b1) l1 ->
            Permutation (bagT_to_listD b2) l2 ->
            (forall t1 t2, t1 inBE b1 -> t2 inBE b2 ->     Fset.is_empty A (labels t1 interS labels t2) = true) ->
            Permutation
              (bagT_to_listD
                 (Febag.mk_bag BTupleT
                               (NJoinTuples.natural_join_list TRcd   (* (build_data T) *)
                                                              (Febag.elements _ b1) (Febag.elements _ b2)))) l.
      Proof.
        intros l l1 l2 x H0 H1 b1 b2 H2 H3 Aux;subst.
        unfold omap_product,oncoll_map_concat  in H1.
        refine (Permutation_trans (permutation_bagT_listT_flat_map _ _) _).
        refine (permutation_listT_lift_flat_map_in _ _ _ _   H1 H2).
        intros t lt Htin Ht.
        unfold omap_concat in Ht.
        refine (permutation_listT_lift_map _ _ _ _  Ht _).
        - intros t2 Ht2.
          simpl.
          do 2 apply f_equal.
          apply join_tuple_rec_concat_sort_eq_if_disjoint.
          apply Aux;[apply (Febag.in_elements_mem _ _ _ Htin)| ].
          rewrite Oeset.mem_bool_filter in Ht2.
          + rewrite Febag.mem_unfold.
            apply (andb_prop _ _ Ht2).
          + do 3 intro.
            apply join_compatible_tuple_eq_2.
        - simpl.
          rewrite <- Permutation_mult_equiv.
          intro d.
          assert (A1 := nb_occ_mult_listT_eq_complete (Febag.elements BTupleT b2) d).
          destruct A1 as [A1|A1].
          + rewrite <- Permutation_mult_equiv in H3.
            rewrite <- H3.
            unfold bagT_to_listD; rewrite A1.
            rewrite mult_0_equiv_nin.
            rewrite mult_0_equiv_nin in A1.
            intro N1;apply A1.
            unfold listT_to_listD in N1.
            rewrite in_map_iff in N1.
            destruct N1 as [t1 [<- N1]].
            rewrite filter_In in N1.
            apply (In_tuple_as_pairs_eq_listT _ _ (proj1 N1)).
          + destruct A1 as [t1 [-> [A1 A2]]].
            rewrite <- Permutation_mult_equiv in H3.
            rewrite <- H3.
            unfold bagT_to_listD; rewrite A1.
            rewrite nb_occ_mult_listT_eq,Oeset.nb_occ_filter;
              [|do 3 intro;apply join_compatible_tuple_eq_2].
            apply f_equal.
            replace (join_compatible_tuple TRcd t t1) with true;[apply eq_refl|apply eq_sym].
            apply inter_is_empty_join_compatible_tuple_true.
            apply Aux;[apply (Febag.in_elements_mem _ _ _ Htin)| ].
            rewrite Febag.mem_unfold.
            apply A2.
      Qed.

      Definition is_drec {fd} (d:@ Data.data fd) :=
        match d with
        |drec r => true
        | _ => false
        end.

      Lemma is_drec_false_forall_neq_drec :
        forall fd d, @is_drec fd d = false <-> (forall r, d <> drec r) .
      Proof.
        intros fd d;split;intro H.
        - intro r;destruct d;discriminate.
        - destruct d;try apply eq_refl.
          destruct (H l eq_refl).
      Qed.

      Lemma is_drec_true_exists_r :
        forall fd d, @is_drec fd d = true <-> (exists r, d = drec r) .
      Proof.
        intros fd d;split;intro H.
        - destruct d;try discriminate.
          exists l;apply eq_refl.
        - destruct H as [r H];subst;apply eq_refl.
      Qed.
      
      Lemma data_to_bag_union_tuple : forall t b1 b2,
          mult (bagT_to_listD (b1 unionBE b2)) (drec (tuple_as_dpairs t)) =
          mult ( (bunion (bagT_to_listD b1) (bagT_to_listD b2))) (drec (tuple_as_dpairs t)).
      Proof.
        intros x b1 b2.
        rewrite bunion_mult.
        rewrite  nb_occ_mult_bag_eq.
        rewrite Febag.nb_occ_union.
        rewrite N2Nat.inj_add.
        rewrite 2 nb_occ_mult_bag_eq.
        apply eq_refl.
      Qed.

      Lemma data_to_bag_union : forall d b1 b2,
          mult (bagT_to_listD (b1 unionBE b2)) d =
          mult ( (bunion (bagT_to_listD b1) (bagT_to_listD b2))) d.
      Proof.
        intros d b1 b2.
        destruct (mult (bagT_to_listD _) d) eqn:D1.
        -    rewrite bunion_mult.
             destruct (is_drec d) eqn:D2.
             + rewrite is_drec_true_exists_r  in D2.
               destruct D2 as [r D2];subst.
               destruct (mult (bagT_to_listD b1) (drec r)) eqn:D3.
               * destruct (mult (bagT_to_listD b2) (drec r)) eqn:D4;
                   [apply eq_refl| ].
                 assert (A1 := D4).
                 unfold bagT_to_listD in A1.
                 apply mult_bagT_S in A1.
                 destruct A1 as [t A1];rewrite A1 in *.
                 rewrite <- D4.
                 rewrite <- D1 at 1.
                 rewrite <- D3.
                 rewrite <- bunion_mult.
                 apply data_to_bag_union_tuple.
               * assert (A1 := D3).
                 unfold bagT_to_listD in A1.
                 apply mult_bagT_S in A1.
                 destruct A1 as [t A1];rewrite A1 in *.
                 rewrite <- D3.
                 rewrite <- D1.
                 rewrite <- bunion_mult.
                 apply data_to_bag_union_tuple.
             + rewrite is_drec_false_forall_neq_drec in D2.
               assert (A1 := mult_bagT_0_not_drec   D2).
               do 2 rewrite A1;apply eq_refl.
        -  assert (A1 := D1) .
           unfold bagT_to_listD in A1.
           apply mult_bagT_S in A1.
           destruct A1 as [t A1];subst.
           rewrite <- D1.
           apply data_to_bag_union_tuple.
      Qed.

      Lemma data_to_bag_diff_tuple : forall t b1 b2,
          mult (bagT_to_listD (Febag.diff _ b1  b2)) (drec (tuple_as_dpairs t)) =
          mult ( (bminus  (bagT_to_listD b2) (bagT_to_listD b1))) (drec (tuple_as_dpairs t)).
      Proof.
        intros x b1 b2.
        rewrite bminus_mult.
        rewrite  nb_occ_mult_bag_eq.
        rewrite Febag.nb_occ_diff.
        rewrite N2Nat.inj_sub.
        rewrite 2 nb_occ_mult_bag_eq.
        apply eq_refl.
      Qed.

      Lemma data_to_bag_diff : forall d b1 b2,
          mult (bagT_to_listD (Febag.diff _ b1  b2)) d =
          mult ( (bminus  (bagT_to_listD b2) (bagT_to_listD b1))) d.
      Proof.
        intros d b1 b2.
        destruct (mult (bagT_to_listD _) d) eqn:D1.
        -    rewrite bminus_mult.
             destruct (is_drec d) eqn:D2.
             + rewrite is_drec_true_exists_r  in D2.
               destruct D2 as [r D2];subst.
               destruct (mult (bagT_to_listD b1) (drec r)) eqn:D3.
               * destruct (mult (bagT_to_listD b2) (drec r)) eqn:D4;
                   [apply eq_refl| ].
                 assert (A1 := D4).
                 unfold bagT_to_listD in A1.
                 apply mult_bagT_S in A1.
                 destruct A1 as [t A1];rewrite A1 in *.
                 rewrite <- D4.
                 rewrite <- D1 at 1.
                 rewrite <- D3.
                 rewrite <- bminus_mult.
                 apply data_to_bag_diff_tuple.
               * assert (A1 := D3).
                 unfold bagT_to_listD in A1.
                 apply mult_bagT_S in A1.
                 destruct A1 as [t A1];rewrite A1 in *.
                 rewrite <- D3.
                 rewrite <- D1.
                 rewrite <- bminus_mult.
                 apply data_to_bag_diff_tuple.
             + rewrite is_drec_false_forall_neq_drec in D2.
               assert (A1 := mult_bagT_0_not_drec   D2).
               do 2 rewrite A1;apply eq_refl.
        -  assert (A1 := D1) .
           unfold bagT_to_listD in A1.
           apply mult_bagT_S in A1.
           destruct A1 as [t A1];subst.
           rewrite <- D1.
           apply data_to_bag_diff_tuple.
      Qed.
      
      Lemma data_to_bag_union_max_tuple : forall t b1 b2,
          mult (bagT_to_listD (Febag.union_max _ b1  b2)) (drec (tuple_as_dpairs t)) =
          mult ( (bmax  (bagT_to_listD b2) (bagT_to_listD b1))) (drec (tuple_as_dpairs t)).
      Proof.
        intros x b1 b2.
        rewrite bmax_mult.
        rewrite  nb_occ_mult_bag_eq.
        rewrite Febag.nb_occ_union_max.
        rewrite N2Nat.inj_max.
        rewrite 2 nb_occ_mult_bag_eq.
        apply Max.max_comm.
      Qed.

      Lemma data_to_bag_union_max : forall d b1 b2,
          mult (bagT_to_listD (Febag.union_max _ b1  b2)) d =
          mult ( (bmax  (bagT_to_listD b2) (bagT_to_listD b1))) d.
      Proof.
        intros d b1 b2.
        destruct (mult (bagT_to_listD _) d) eqn:D1.
        -    rewrite bmax_mult.
             destruct (is_drec d) eqn:D2.
             + rewrite is_drec_true_exists_r  in D2.
               destruct D2 as [r D2];subst.
               destruct (mult (bagT_to_listD b1) (drec r)) eqn:D3.
               * destruct (mult (bagT_to_listD b2) (drec r)) eqn:D4;
                   [apply eq_refl| ].
                 assert (A1 := D4).
                 unfold bagT_to_listD in A1.
                 apply mult_bagT_S in A1.
                 destruct A1 as [t A1];rewrite A1 in *.
                 rewrite <- D4.
                 rewrite <- D1 at 1.
                 rewrite <- D3.
                 rewrite <- bmax_mult.
                 apply data_to_bag_union_max_tuple.
               * assert (A1 := D3).
                 unfold bagT_to_listD in A1.
                 apply mult_bagT_S in A1.
                 destruct A1 as [t A1];rewrite A1 in *.
                 rewrite <- D3.
                 rewrite <- D1.
                 rewrite <- bmax_mult.
                 apply data_to_bag_union_max_tuple.
             + rewrite is_drec_false_forall_neq_drec in D2.
               assert (A1 := mult_bagT_0_not_drec   D2).
               do 2 rewrite A1;apply eq_refl.
        -  assert (A1 := D1) .
           unfold bagT_to_listD in A1.
           apply mult_bagT_S in A1.
           destruct A1 as [t A1];subst.
           rewrite <- D1.
           apply data_to_bag_union_max_tuple.
      Qed.

      Lemma data_to_bag_inter_tuple : forall t b1 b2,
          mult (bagT_to_listD (Febag.inter _ b1  b2)) (drec (tuple_as_dpairs t)) =
          mult ( (bmin  (bagT_to_listD b2) (bagT_to_listD b1))) (drec (tuple_as_dpairs t)).
      Proof.
        intros x b1 b2.
        rewrite bmin_mult.
        rewrite nb_occ_mult_bag_eq.
        rewrite Febag.nb_occ_inter.
        rewrite N2Nat.inj_min.
        rewrite 2 nb_occ_mult_bag_eq.
        apply Min.min_comm.
      Qed.
      
      Lemma data_to_bag_inter : forall d b1 b2,
          mult (bagT_to_listD (Febag.inter _ b1  b2)) d =
          mult ( (bmin  (bagT_to_listD b2) (bagT_to_listD b1))) d.
      Proof.
        intros d b1 b2.
        destruct (mult (bagT_to_listD _) d) eqn:D1.
        -    rewrite bmin_mult.
             destruct (is_drec d) eqn:D2.
             + rewrite is_drec_true_exists_r  in D2.
               destruct D2 as [r D2];subst.
               destruct (mult (bagT_to_listD b1) (drec r)) eqn:D3.
               * destruct (mult (bagT_to_listD b2) (drec r)) eqn:D4;
                   [apply eq_refl| ].
                 assert (A1 := D4).
                 unfold bagT_to_listD in A1.
                 apply mult_bagT_S in A1.
                 destruct A1 as [t A1];rewrite A1 in *.
                 rewrite <- D4.
                 rewrite <- D1 at 1.
                 rewrite <- D3.
                 rewrite <- bmin_mult.
                 apply data_to_bag_inter_tuple.
               * assert (A1 := D3).
                 unfold bagT_to_listD in A1.
                 apply mult_bagT_S in A1.
                 destruct A1 as [t A1];rewrite A1 in *.
                 rewrite <- D3.
                 rewrite <- D1.
                 rewrite <- bmin_mult.
                 apply data_to_bag_inter_tuple.
             + rewrite is_drec_false_forall_neq_drec in D2.
               assert (A1 := mult_bagT_0_not_drec   D2).
               do 2 rewrite A1;apply eq_refl.
        -  assert (A1 := D1) .
           unfold bagT_to_listD in A1.
           apply mult_bagT_S in A1.
           destruct A1 as [t A1];subst.
           rewrite <- D1.
           apply data_to_bag_inter_tuple.
      Qed.
      
      Definition set_op_to_binary_op  o :    (@binary_op (@foreign_runtime_data frt) (@foreign_runtime_operators frt)) :=
        match o with
        | Union => OpBagUnion
        | UnionMax => OpBagMax
        | Inter => OpBagMin
        | Diff => OpBagDiff
        end.


      Lemma Permutation_bagT_to_listD_set_op :
        forall br sop b1 b2 dq1 dq2 ,
          Permutation (bagT_to_listD b1) dq1 -> Permutation (bagT_to_listD b2) dq2 ->
          exists dq,
            Permutation (bagT_to_listD (Febag.interp_set_op BTupleT sop b1 b2)) dq /\
            binary_op_eval br (set_op_to_binary_op sop) (dcoll dq1) (dcoll dq2) = Some (dcoll dq).
      
      Proof.
        intros br [ ] b1 b2 dq1 dq2 H2 H3;try destruct (H1 eq_refl).
        - exists (bunion dq1 dq2).
          split;[|apply eq_refl].
          rewrite <- Permutation_mult_equiv in *.
          simpl;intro d.
          rewrite data_to_bag_union,2bunion_mult,H2,H3.
          apply eq_refl.
        -  exists (bmax dq1 dq2).
           split;[|apply eq_refl].
           rewrite <- Permutation_mult_equiv in *.
           simpl;intro d.
           rewrite data_to_bag_union_max,2bmax_mult,H2,H3.
           apply Max.max_comm.
        - exists (bmin dq1 dq2).
          split;[|apply eq_refl].
          rewrite <- Permutation_mult_equiv in *.
          simpl;intro d.
          rewrite data_to_bag_inter,2bmin_mult,H2,H3.
          apply Min.min_comm.
        - exists (bminus dq2 dq1).
          split;[| apply eq_refl].
          rewrite <- Permutation_mult_equiv in *.
          simpl;intro d.
          rewrite data_to_bag_diff,2bminus_mult,H2,H3.
          apply eq_refl.  
      Qed.

      
      (* Lemma Permutation_bagT_to_listD_set_op_not_diff : *)
      (*   forall br sop b1 b2 dq1 dq2 , *)
      (*     sop <> Diff -> *)
      (*     Permutation (bagT_to_listD b1) dq1 -> Permutation (bagT_to_listD b2) dq2 -> *)
      (*     exists dq, *)
      (*     binary_op_eval br (set_op_to_binary_op sop) (dcoll dq1) (dcoll dq2) = Some (dcoll dq) /\ *)
      (*     Permutation (bagT_to_listD (Febag.interp_set_op BTupleT sop b1 b2)) dq. *)
      (* Proof. *)
      (*   intros br [ ] b1 b2 dq1 dq2 H1 H2 H3;try destruct (H1 eq_refl). *)
      (*   - exists (bunion dq1 dq2). *)
      (*     split;[apply eq_refl| ]. *)
      (*     rewrite <- Permutation_mult_equiv in *. *)
      (*     simpl;intro d. *)
      (*     rewrite data_to_bag_union,2bunion_mult,H2,H3. *)
      (*     apply eq_refl. *)
      (*   -  exists (bmax dq1 dq2). *)
      (*     split;[apply eq_refl| ]. *)
      (*     rewrite <- Permutation_mult_equiv in *. *)
      (*     simpl;intro d. *)
      (*     rewrite data_to_bag_union_max,2bmax_mult,H2,H3. *)
      (*     apply Max.max_comm. *)
      (*   - exists (bmin dq1 dq2). *)
      (*     split;[apply eq_refl| ]. *)
      (*     rewrite <- Permutation_mult_equiv in *. *)
      (*      simpl;intro d. *)
      (*      rewrite data_to_bag_inter,2bmin_mult,H2,H3. *)
      (*      apply Min.min_comm. *)
      (* Qed. *)

      

      (* Lemma Permutation_bagT_to_listD_set_op_diff : *)
      (*   forall br b1 b2 dq1 dq2, *)
      (*     Permutation (bagT_to_listD b1) dq1 -> Permutation (bagT_to_listD b2) dq2 -> *)
      (*     exists dq, *)
      (*     binary_op_eval br (set_op_to_binary_op Diff) (dcoll dq1) (dcoll dq2) = Some (dcoll dq) /\ *)
      (*     . *)
      (* Proof. *)
      (*   intros br b1 b2 dq1 dq2 H1 H2. *)
      (*   - exists (bminus dq2 dq1). *)
      (*     split;[apply eq_refl| ]. *)
      (*     rewrite <- Permutation_mult_equiv in *. *)
      (*     simpl;intro d. *)
      (*     rewrite data_to_bag_diff,2bminus_mult,H1,H2. *)
      (*     apply eq_refl. *)
      (* Qed. *)

      

      Lemma _permut_Perm_filter_inside :
        forall f1 f2 llt lld,
          (forall d, In d lld -> exists ld, d =dcoll ld) ->
          (forall lt lx, In lt llt -> In (dcoll lx) lld -> Permutation (listT_to_listD lt) lx -> f1 lt = f2 (dcoll lx)) ->
          _permut (Permutation (A:=data))
                  (map (fun x : _ => listT_to_listD x) llt)
                  (map (fun d : data => match d with
                                       | dcoll ld => ld
                                       | _ => nil
                                       end) lld) ->        
          _permut (Permutation (A:=data))
                  (map listT_to_listD
                       (filter f1 llt))
                  (map (fun x : data => match x with
                                       | dcoll l => l
                                       | _ => nil
                                       end)
                       (filter
                          f2  lld)).
      Proof.
        intros f1 f2.
        induction llt as [|lt llt];intros lld Hlld Aux1 H.
        - inversion H.
          apply eq_sym in H0.
          apply map_eq_nil in H0.
          subst lld;constructor.
        - simpl in H;inversion H;subst.
          apply eq_sym in H2.
          destruct (map_app_break _ H2) as [ld1 [ld2 [ -> [-> H1]]]].
          apply eq_sym in H1.
          destruct (map_eq_cons _ _ _ _ H1) as [d [ld4 [-> [ <- <-]]]].
          rewrite filter_app,map_app.
          simpl filter.      
          assert (A1 : In d (ld1 ++ d :: ld4)).
          { refine (in_or_app _ _ _ (or_intror _) );left;apply eq_refl. }
          destruct  (Hlld d A1) as [ld5 ->].
          rewrite (Aux1 _ _ (or_introl eq_refl) A1 H3).
          assert (A3 :  _permut (Permutation (A:=data)) (map listT_to_listD (filter f1 llt))
                                (map (fun x : data => match x with
                                                     | dcoll l => l
                                                     | _ => nil
                                                     end) (filter f2 ld1) ++ map (fun x : data => match x with
                                                                                                | dcoll l => l
                                                                                                | _ => nil
                                                                                                end) (filter f2 ld4))).
          {
            rewrite <- map_app,<- filter_app.
            refine (IHllt _ _ _ _ ).
            * intros d Hd.
              apply Hlld.
              rewrite ListFacts.in_app_iff.
              right;apply Hd.
            * intros lt' lx' H' G'.
              apply Aux1;[right;apply H'|rewrite ListFacts.in_app_iff;right;apply G'].
            * rewrite <- map_app in H4;apply H4.
          }
          destruct (f2 (dcoll ld5));[|apply A3].
          apply Pcons;[apply H3|apply A3].
      Qed.
      
      Lemma lift_filter_group_by_neq_None_aggterm :
        forall env la lt t,
          lift_filter
            (fun d1 : data =>
               match
                 olift2 (fun d2 d3 : data => unbdata (fun x y : data => if data_eq_dec x y then true else false) d2 d3)
                        match d1 with
                        | drec r => Some (drec (rproject r (map attribute_to_string la)))
                        | _ => None
                        end (Some (drec (rec_sort (map (fun a : attribute => (attribute_to_string a, value_to_data (interp_aggterm TRcd (env_t TRcd env t) (A_Expr  (F_Dot  a))))) la))))
               with
               | Some (dbool b) => Some b
               | _ => None
               end) (listT_to_listD lt) <> None.
      Proof.
        intros env la lt t H.
        induction lt;[inversion H| ].
        simpl in H.
        apply IHlt.
        clear IHlt.
        destruct lift_filter eqn:D1;[|apply eq_refl].
        destruct   list_Forallt_eq_dec;try discriminate.
        destruct eq_rec_r;discriminate.
      Qed.

    End Bags.


      
  Lemma  lift_map_edot_domain_snd :
    forall t,
      lift_map (edot (tuple_as_dpairs t)) (domain (tuple_as_dpairs t)) =
      Some (map snd (tuple_as_dpairs t)).
  Proof.
    intros.
    assert (A1 := NoDup_domain_tuple_as_dpairs t).
    induction (tuple_as_dpairs t) as [|[s1 d1] r];
      [apply eq_refl| ].
    inversion A1;subst.
    apply IHr in H2.
    simpl.
    assert (edot ((s1, d1) :: r) s1 = Some d1).
    {
      unfold edot.
      simpl assoc_lookupr.
      rewrite (assoc_lookupr_nin_none _ _ _ H1).
      destruct string_eqdec;[apply eq_refl| ].
      destruct (c eq_refl).
    }
    simpl in H.
    rewrite H.
    unfold lift.
    assert (Hx: lift_map (edot ((s1, d1) :: r)) (domain r) = Some (map snd r)).
    { rewrite <- H2.
      apply lift_map_ext.
      intros.
      cbn.
      unfold edot.
      simpl.
      destruct assoc_lookupr;
        [apply eq_refl| ].
      destruct string_eqdec;[|apply eq_refl].
      inversion e.
      subst.
      destruct (H1 H0).
    }
    simpl in Hx.
    rewrite Hx.
    apply eq_refl.
  Qed.

  
  Lemma listT_to_listD_cons :
    forall a l,
      listT_to_listD (a::l) = drec (tuple_as_dpairs a) :: listT_to_listD l.
  Proof.
    intros;apply eq_refl.
  Qed.

  
  Section Types.

    Definition well_typed_attribute  a t :=
      Oset.eq_bool OType 
                (type_of_value TRcd (dot  t a)) 
                (type_of_attribute TRcd a).

          Lemma well_typed_attribute_spec :
            forall t a,
              well_typed_attribute a t = true <->
              type_of_value TRcd (dot t a) =
              type_of_attribute TRcd a.
          Proof.
            intros.
            unfold well_typed_attribute.
            rewrite Oset.eq_bool_true_iff.
            split;apply (fun H => H).
          Qed.
                
          Definition well_typed_list_tuple lt :=
            forallb (fun t => (forallb  (fun a => well_typed_attribute a t) ({{{labels t}}}))) lt.

          Lemma well_typed_list_tuple_eq :
            forall x y : list tuple, Oeset.permut OTuple x y   -> well_typed_list_tuple x = well_typed_list_tuple y.
          Proof.
            intros.
             apply (Oeset.forallb_eq OTuple).
            - intros.
              apply Oeset.permut_mem_bool_eq.
              apply H.
            - intros.
              apply (Oeset.forallb_eq (oeset_of_oset OAtt)).
              + intros.
                rewrite 2 mem_bool_oeset_of_oset.
                rewrite <- 2 Fset.mem_elements.
                apply Fset.equal_spec.
                apply tuple_eq_labels.
                apply H1.
              + intros.
                apply Oset.compare_eq_iff in H3.
                subst.
                unfold well_typed_attribute.
                apply f_equal2;[|apply eq_refl].
                apply f_equal.
                apply (tuple_eq_dot_alt _ _ _ H1).
                rewrite  Fset.mem_elements.
                apply H2.
          Qed.

  End Types.

  Section Dot.

    Lemma dot_OpDot :
    forall br dreg i t a (did : data),
      a inS (Tuple.labels _ t) ->
      (br
         ⊢
         NRAEnvUnop (OpDot (attribute_to_string a)) (NRAEnvConst (drec (tuple_as_dpairs t))) @ₓ did
         ⊣ i; dreg)%nraenv = Some (value_to_data (Tuple.dot _ t a)).
  Proof.
    do 6 intro;intros Ha.
    cbn.
    rewrite ListFacts.map_id.
    - unfold edot.
      assert (A1 := tuple_tuple_as_pairs_dot_in  _ _ _ Ha).
      rewrite assoc_lookupr_drec_sort.
      rewrite assoc_lookupr___find_eq.
      rewrite A1.
      apply eq_refl.
    - intros  [ ] H;apply f_equal.
      simpl.
      unfold tuple_as_dpairs in H.
      rewrite in_map_iff in H.
      destruct H as [[a1 v1] [H1 H2]].
      inversion H1;subst.
      apply eq_sym;apply value_to_data_normalize.
  Qed.

  Lemma dot_OpDot_NRAEnvID :
    forall br dreg i t a,
      a inS (Tuple.labels _ t) ->
      (br
         ⊢
         NRAEnvUnop (OpDot (attribute_to_string a)) NRAEnvID @ₓ (drec (tuple_as_dpairs t))
         ⊣ i; dreg)%nraenv = Some (value_to_data (Tuple.dot _ t a)).
  Proof.
    do 5 intro;intros Ha.
    cbn.
    refine
      (eq_trans         
         _
         ( (dot_OpDot br dreg i t a (drec (tuple_as_dpairs t)) Ha))).
    cbn.
    unfold edot.
    rewrite ListFacts.map_id.
    - rewrite assoc_lookupr_drec_sort.
      apply eq_refl.
    - intros  [ ] H;apply f_equal.
      simpl.
      unfold tuple_as_dpairs in H.
      rewrite in_map_iff in H.
      destruct H as [[a1 v1] [H1 H2]].
      inversion H1;subst.
      apply eq_sym;apply value_to_data_normalize.
  Qed.  
  

    Lemma dot_OpDot_NRAEnvID_eq :
    forall br dreg i t a did,
         (br
         ⊢
         NRAEnvUnop (OpDot (attribute_to_string a)) (NRAEnvConst (drec (tuple_as_dpairs t))) @ₓ did
         ⊣ i; dreg)%nraenv 
      =
    (br
         ⊢
         NRAEnvUnop (OpDot (attribute_to_string a)) NRAEnvID @ₓ (drec (tuple_as_dpairs t))
         ⊣ i; dreg)%nraenv.
    Proof.
      do 6 intro.
      unfold nraenv_eval.
      simpl.
      unfold edot.
      rewrite assoc_lookupr_drec_sort.
      rewrite ListFacts.map_id;[apply eq_refl| ].
       intros  [ ] H;apply f_equal.
      simpl.
      unfold tuple_as_dpairs in H.
      rewrite in_map_iff in H.
      destruct H as [[a1 v1] [H1 H2]].
      inversion H1;subst.
      apply eq_sym;apply value_to_data_normalize.
    Qed.

  End Dot.
  
  End Lemmas.
    
End TupleToData.
