(************************************************************************************)
(**                                                                                 *)
(**                          The SQLToNRACert Library                               *)
(**                                                                                 *)
(**                       LMF, CNRS & Université Paris-Saclay                       *)
(**                                                                                 *)
(**                        Copyright 2016-2022 : FormalData                         *)
(**                                                                                 *)
(**         Authors: Véronique Benzaken                                             *)
(**                  Évelyne Contejean                                              *)
(**                  Mohammed Hachmaoui                                             *)
(**                  Chantal Keller                                                 *)
(**                                                                                 *)
(************************************************************************************)

Set Implicit Arguments.

Require Import NArith List String Bool Ascii.

Require Import Permutation.
From SQLFS Require Import ListPermut SqlAlgebra Formula.

Require Import FormulaToNRAEnv Qcert.Utils.Utils.

Require Import Qcert.NRAEnv.NRAEnvRuntime Aux AxiomFloat.
From SQLFS Require Import FiniteSet OrderedSet FiniteCollection FiniteBag FlatData.

Require Import QueryToNRAEnv.

Module Decode.
  
  Export QueryToNRAEnv.

  Class Decode
        `{frt:foreign_runtime}         
        `{TD : @TupleToData frt}        
        `{SN : @SymbolToNRAEnv frt TD}       
        {EN : @EnvToNRAEnv frt TD}
        `{FTN : @FTermToNRAEnv frt TD SN EN}        
        `{AN : @AggregateToNRAEnv frt TD }
        `{ATN : @ATermToNRAEnv frt TD SN AN EN  FTN}
        {relname : Type}
        {ORN : Oset.Rcd relname}
        `{IN : @InstanceToNRAEnv frt TD _ ORN}
        `{BD : @BoolBToData  frt (B TRcd)}
        `{PN :  @PredicateToNRAEnv frt TD BD}    
        `{FN :  @FormulaToNRAEnv
                  frt TD SN EN FTN AN ATN
                  relname ORN IN BD PN}        
        `{QN :  @QueryToNRAEnv
                  frt TD SN EN FTN AN ATN
                  relname ORN IN BD PN FN}: Type :=
    mk_C
      { dcoll_to_bag :  list data -> bagT;
        dcoll_to_bag_spec :
          forall b ld, Permutation (bagT_to_listD b) ld -> b =BE= dcoll_to_bag ld;
      }.

  Section Sec.
    

    Hypothesis relname : Type.
    Hypothesis ORN : Oset.Rcd relname.
    
    Context {frt : foreign_runtime}.
    Context {TD:TupleToData}.
    Context {SN:SymbolToNRAEnv}.
    Context {EN : EnvToNRAEnv}.
    Context {AN:AggregateToNRAEnv}.
    Context {FTN:FTermToNRAEnv}.  
    Context {ATN:ATermToNRAEnv}.
    Context {IN : InstanceToNRAEnv ORN}.
    Context {BD : BoolBToData (B TRcd)}.
    Context {PN : PredicateToNRAEnv}.
    Context {FN : FormulaToNRAEnv}.
    Context {QN : QueryToNRAEnv}.
    Context {DN : Decode}.
    
    (* Notation bagT := (Febag.bag BTupleT). *)
    Notation eval_query i :=
      (eval_query  basesort
                   (fun r =>
                      match Oset.find  ORN r i with
                      | Some b => b
                      |None => Febag.empty _
                      end) unknownB
                   contains_nulls).
    
    Lemma query_to_nraenv_top_is_sound_decode :
      forall br i,
        well_sorted_instance i = true ->
        well_typed_instance i = true ->
        forall q,              
          is_complete_instance_q ORN i q = true ->
          is_translatable_q q = true ->
          well_formed_q basesort nil q = true ->              
          more_well_formed_q nil q = true ->
          match nraenv_eval_top br (query_to_nraenv_top q) (instance_to_bindings i) with
          | Some (dcoll dq) =>
            match Febag.equal _ (eval_query i nil q ) (dcoll_to_bag dq) with
            | true => True
            | _ => False
            end
          | _ => False
          end.
    Proof.
      intros br i WSi WTi q Ci Gq WFq MWFq.
      destruct (query_to_nraenv_is_sound br _ WSi WTi q Ci Gq nil eq_refl eq_refl eq_refl WFq MWFq)
        as [lq [H1 H2]].
      simpl in *. unfold nraenv_eval_top, query_to_nraenv_top. rewrite H2.
      apply dcoll_to_bag_spec in H1.
      rewrite H1;constructor.
    Qed.


  End Sec.
  
End Decode.



Require Export TnullBD TnullTD TnullEN TnullATN TnullIN TnullFN TnullQN.

Require Import QueryToNRAEnv FormulaToNRAEnv
        TupleToData MoreSugar.
From SQLFS Require Import Bool3 Formula GenericInstance.

Section Sec.
  Export QueryToNRAEnv.
  Import Values.
  
  Definition data_to_value (d:data) : NullValues.value :=
    match d with
    | dleft (dstring s) => NullValues.Value_string (Some s)
    | (dright (dstring "")) => NullValues.Value_string None
    | dleft (dnat z) => NullValues.Value_Z (Some z)
    | (dright (dnat 0)) => NullValues.Value_Z None
    | dleft (dbool b) => NullValues.Value_bool (Some b)
    | (dright (dbool true)) => NullValues.Value_bool None
    | dleft (dfloat f) => NullValues.Value_float (Some f)
    | (dright (dfloat float_zero)) => NullValues.Value_float None
    | _ => NullValues.Value_string None
    end.

  Lemma data_to_value_correct :
    forall v, data_to_value (value_to_data v) = v.
  Proof.
    intros  [ [] |[] | [ ] | []];auto.
  Qed.

  Definition string_to_att s : attribute :=
    match s with
    | String "0" s' => Attr_string s'
    | String "1" s' => Attr_Z s'
    | String "2" s' => Attr_bool s'
    | String "3" s' => Attr_float s'
    | _ => Attr_string EmptyString
    end. 


  Lemma string_to_att_correct :
    forall a, string_to_att (attribute_to_string a) = a.
  Proof.
    intros  [ [] |[] | [ ] | []];auto.
  Qed.
  
  Definition drec_to_tuple r : tuple :=
    Tuple.pairs_as_tuple
      _
      (map (fun x => (string_to_att (fst x),(data_to_value (snd x)))) r).
  
  
  Definition data_to_tuple d : tuple :=
    match d with      
    | drec r =>  drec_to_tuple r
    | _ => empty_tuple _
    end.

  Lemma data_to_tuple_correct :
    forall t, data_to_tuple (drec (tuple_as_dpairs t)) =t= t.
  Proof.
    intros  t.
    unfold data_to_tuple,drec_to_tuple.
    unfold pairs_as_tuple.
    unfold tuple_as_dpairs,tuple_as_pairs.
    repeat rewrite map_map.
    unfold fst,snd.
    simple refine (Oeset.compare_eq_trans _ _ _ _ _ ((mk_tuple_idem _ _ _(Fset.equal_refl _ _) )) ).
    apply mk_tuple_eq.
    - rewrite ListFacts.map_id;[|intros;apply string_to_att_correct].
      apply Fset.mk_set_idem.
    - intros a Ha1 Ha2.
      apply tuple_tuple_as_pairs_dot_in in Ha2.      
      unfold tuple_as_pairs in Ha2.
      replace    (map (fun x : attribute => (string_to_att (attribute_to_string x), data_to_value (value_to_data (dot t x))))
                      ({{{labels t}}})) with (map (fun a : attribute => (a, dot t a)) ({{{labels t}}})).
      + rewrite Ha2;apply eq_refl.
      + apply map_ext.
        intros.
        rewrite string_to_att_correct,data_to_value_correct.
        apply eq_refl.
  Qed.
  
  
  Definition dcoll_to_bag (ld: list data): bagT :=
    Febag.mk_bag _ (map data_to_tuple ld).

  Lemma dcoll_to_bag_spec :
    forall b ld,   Permutation (bagT_to_listD b) ld -> b =BE= dcoll_to_bag ld.
    intros b ld H.      
    rewrite permutation_bagT_eq.
    refine (Permutation_trans H _).      
    refine (Permutation_trans _ (Permutation_sym (permutation_bagT_listT_map _ _))).
    unfold listT_to_listD;rewrite map_map.
    assert (H2 := permutation_bagT_tuple _ H).
    clear H.      
    unfold dcoll_to_bag,bagT_to_listD,listT_to_listD.
    induction ld.
    - constructor.
    - assert ( IH := IHld (fun d H =>  H2 d (or_intror H))).
      destruct (H2 a (or_introl eq_refl)) as [t Ht].
      simpl.
      subst a.
      assert (X1 :=  data_to_tuple_correct t).
      rewrite tuple_as_dpairs_eq in X1.
      rewrite X1.
      apply perm_skip.
      apply IH.
  Qed.

  Variable faac : float_add_assoc_comm.

  Global Instance tnull_DN basesort :
    @Decode.Decode
      tnull_frt tnull_TD tnull_SN  tnull_EN tnull_FTN
      (tnull_AN faac) (tnull_ATN faac) relname ORN  (tnull_IN basesort)
      tnull_BD tnull_PN (tnull_FN faac basesort) (tnull_QN faac basesort) :=
    Decode.mk_C      
      (tnull_QN faac basesort)
      dcoll_to_bag
      dcoll_to_bag_spec.
  
End Sec.
