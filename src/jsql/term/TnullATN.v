(************************************************************************************)
(**                                                                                 *)
(**                          The SQLToNRACert Library                               *)
(**                                                                                 *)
(**                       LMF, CNRS & Université Paris-Saclay                       *)
(**                                                                                 *)
(**                        Copyright 2016-2022 : FormalData                         *)
(**                                                                                 *)
(**         Authors: Véronique Benzaken                                             *)
(**                  Évelyne Contejean                                              *)
(**                  Mohammed Hachmaoui                                             *)
(**                  Chantal Keller                                                 *)
(**                                                                                 *)
(************************************************************************************)

Require Export TnullFTN Psatz ZArith Floats.

Require Import ATermToNRAEnv.
From SQLFS Require Import TuplesImpl GenericInstance.

Require Import TupleToData MoreSugar Aux NRAEnvRefine AxiomFloat.

Section Sec.

  Import TupleToData Tuple  Values.

    Definition NRAEnvNullAgg n1 n2 :=
    NRAEnvApp
      (NRAEnvEither
         n1
         (NRAEnvConst (dleft (dnat 0))))
      n2.
  
  Definition aggregate_to_nraenv (f:Tuple.aggregate TNull) ty n :=
    match f, ty with
    | Aggregate "count",_ =>
       NRAEnvUnop OpLeft ((NRAEnvUnop OpCount) n)
    | Aggregate "sum",type_Z =>
      let without_nulls :=
          NRAEnvMap
            (NRAEnvEither NRAEnvID NRAEnvID)
            (NRAEnvSelect
               (NRAEnvEither
                  (NRAEnvConst (dbool true))
                  (NRAEnvConst (dbool false)))
               n) in
      NRAEnvUnop OpLeft (NRAEnvUnop OpNatSum without_nulls)
    | Aggregate "sum",_ =>
      NRAEnvConst (dleft (dnat 0))
    | Aggregate "sum.",type_float =>
      let without_nulls :=
          NRAEnvMap
            (NRAEnvEither NRAEnvID NRAEnvID)
            (NRAEnvSelect
               (NRAEnvEither
                  (NRAEnvConst (dbool true))
                  (NRAEnvConst (dbool false)))
               n) in
      NRAEnvUnop OpLeft (NRAEnvUnop OpFloatSum without_nulls)
    | Aggregate "sum.",_ =>
      NRAEnvConst (dleft (dfloat float_zero))
    | Aggregate "max",type_Z =>
    (* | Aggregate "MAX",type_Z => *)
      let without_nulls :=
          NRAEnvMap
            (NRAEnvEither NRAEnvID NRAEnvID)
            (NRAEnvSelect
               (NRAEnvEither
                  (NRAEnvConst (dbool true))
                  (NRAEnvConst (dbool false)))
               n) in
      NRAEnvUnop OpLeft (NRAEnvUnop OpNatMax without_nulls)
    | Aggregate "max",_ (* | Aggregate "MAX",_ *) =>
      NRAEnvUnop OpLeft
                  (NRAEnvConst (dnat 0))
    | Aggregate "max.",type_float =>
      let without_nulls :=
          NRAEnvMap
            (NRAEnvEither NRAEnvID NRAEnvID)
            (NRAEnvSelect
               (NRAEnvEither
                  (NRAEnvConst (dbool true))
                  (NRAEnvConst (dbool false)))
               n) in
      NRAEnvUnop OpLeft (NRAEnvUnop OpFloatBagMax without_nulls)
    | Aggregate "max.",_ =>
      NRAEnvUnop OpLeft
                  (NRAEnvConst (dfloat JsNumber.neg_infinity))
    | Aggregate "avg",type_Z =>
      let without_nulls :=
          NRAEnvMap
            (NRAEnvEither NRAEnvID NRAEnvID)
            (NRAEnvSelect
               (NRAEnvEither
                  (NRAEnvConst (dbool true))
                  (NRAEnvConst (dbool false)))
               n) in
      NRAEnvUnop OpLeft (NRAEnvBinop
                           (OpNatBinary NatDiv)
                           (NRAEnvUnop OpNatSum without_nulls) (NRAEnvUnop OpCount n))
    | Aggregate "avg",_ =>
      NRAEnvUnop OpLeft
                 (NRAEnvBinop (OpNatBinary NatDiv)
                              (NRAEnvConst (dnat 0))
                              (NRAEnvUnop OpCount n))
    | Aggregate "avg.",type_float =>
      let without_nulls :=
          NRAEnvMap
            (NRAEnvEither NRAEnvID NRAEnvID)
            (NRAEnvSelect
               (NRAEnvEither
                  (NRAEnvConst (dbool true))
                  (NRAEnvConst (dbool false)))
               n) in
      NRAEnvUnop OpLeft
                 (ITE (NRAEnvBinop OpEqual NRAEnvID (NRAEnvConst (dnat 0%Z)))
                      (NRAEnvUnop OpCount n)
                      (NRAEnvConst (dfloat float_zero))
                      (NRAEnvBinop
                         (OpFloatBinary FloatDiv)
                         (NRAEnvUnop OpFloatSum without_nulls) (NRAEnvUnop OpFloatOfNat (NRAEnvUnop OpCount n)))
                 )
    | Aggregate "avg.",_ =>
      NRAEnvUnop OpLeft (NRAEnvConst (dfloat float_zero))
    | Aggregate _,_ => NRAEnvConst (dright (dnat 0))
    end.

  Definition type_of_aggregate (a: SqlSyntax.aggregate) :
    Tuple.type TRcd :=
    match a with
    | Aggregate "sum." | Aggregate "max." | Aggregate "avg." => type_float
    | _ => type_Z
    end.
  
  (* Definition well_typed_aggregate *)
  (*            (a: aggregate) *)
  (*            (t :  (Tuple.type TNull)) : bool := *)
  (*   match a with *)
  (*   | Aggregate "sum" => *)
  (*     Oset.eq_bool (Tuple.OVal TNull) (Tuple.default_value _ t) (Tuple.default_value TNull type_Z) *)
  (*   | Aggregate "count" => true *)
  (*   | _ => false *)
  (*   end. *)

  Lemma value_to_data_is_dleft_or_dright :
    forall v, exists d, value_to_data v = dleft d \/ value_to_data v = dright d.
  Proof.
    intros [[s| ]|[z| ]|[b| ]|[f| ]];simpl.
    - exists (dstring s);left;apply eq_refl.
    - exists (dstring "");right;apply eq_refl.
    - exists (dnat z);left;apply eq_refl.
    - exists (dnat 0);right;apply eq_refl.
    - exists (dbool b);left;apply eq_refl.
    - exists (dbool true);right;apply eq_refl.
    - exists (dfloat f);left;apply eq_refl.
    - exists (dfloat float_zero);right;apply eq_refl.
  Qed.
  
  Lemma lift_filter_value_to_data_Some :
    forall l,
    exists m,
      lift_filter
        (fun d : data =>
           match match d with
                 | dleft _ => Some (dbool false)
                 | dright _ => Some (dbool true)
                 | _ => None
                 end with
           | Some (dbool b) => Some b
           | _ => None
           end) (map value_to_data l) = Some  m.
  Proof.
    induction l as [|v l [m IH]];
      [exists nil;apply eq_refl| ].
    rewrite map_cons.
    destruct (value_to_data_is_dleft_or_dright v) as [d [Hd|Hd]].
    - rewrite Hd.
      exists m;simpl in *.
      rewrite IH;apply eq_refl.
    - exists (dright d::m);rewrite Hd.
      simpl in *;rewrite IH.
      apply eq_refl.
  Qed.        

    Lemma lift_filter_value_to_data_Some_true_false :
    forall l,
    exists m,
      lift_filter
        (fun d : data =>
           match match d with
                 | dleft _ => Some (dbool true)
                 | dright _ => Some (dbool false)
                 | _ => None
                 end with
           | Some (dbool b) => Some b
           | _ => None
           end) (map value_to_data l) = Some  m.
  Proof.
    induction l as [|v l [m IH]];
      [exists nil;apply eq_refl| ].
    rewrite map_cons.
    destruct (value_to_data_is_dleft_or_dright v) as [d [Hd|Hd]].    
    - exists (dleft d::m);rewrite Hd.
      simpl in *;rewrite IH.
      apply eq_refl.
    - rewrite Hd.
      exists m;simpl in *.
      rewrite IH;apply eq_refl.
  Qed.        

  Lemma nraenv_eval_aggregate_id_eq :
    forall s  n br i reg,
      (forall id1 id2,
          nraenv_eval br i  n reg id1 =
          nraenv_eval br i  n reg id2) ->
      forall id1 id2 t,
        nraenv_eval br i  (aggregate_to_nraenv s t n) reg id1 =
        nraenv_eval br i  (aggregate_to_nraenv s  t n) reg id2.
  Proof.
    unfold nraenv_eval in *.
    intros  [ ].
    do 6 (try (destruct s as [ |[[ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ]] s];try (intros;apply eq_refl)));
      intros;destruct t;try apply eq_refl;cbn;
        unfold olift,olift2;rewrite (H _ id2);
          apply eq_refl.
  Qed.

  Lemma Permutation_aggregate_sum_aux :
    forall l1 l2,
      Permutation l1 l2 ->      
      forall acc,
      fold_left
    (fun (acc : Z) (x : NullValues.value) =>
     match x with
     | NullValues.Value_Z (Some x0) => (acc + x0)%Z
     | _ => acc
     end) l1 acc =
  fold_left
    (fun (acc : Z) (x : NullValues.value) =>
     match x with
     | NullValues.Value_Z (Some x0) => (acc + x0)%Z
     | _ => acc
     end) l2 acc.
  Proof.
    intros l1 l2.
    rewrite <- _permut_eq_Permutation.
    induction 1 as [ |v _ l3 l4  l5 <- Hp IH];intros acc;[apply eq_refl| ].
    cbn.
    rewrite fold_left_app.
    simpl.
    rewrite IH.
    rewrite fold_left_app.
    destruct v;destruct o;try apply eq_refl.
    simpl.
    apply f_equal.
    revert z acc.
    clear.
    induction l4;intros;[apply eq_refl| ].
    cbn.
    rewrite <- IHl4.
    destruct a;destruct o;try apply eq_refl.    
    apply f_equal.
    lia.
  Qed.

  Lemma Permutation_aggregate_sum_left_float_aux
    (faac : float_add_assoc_comm) :
    forall l1 l2,
      Permutation l1 l2 ->
      forall acc,
  fold_left
    (fun (acc : float) (x : NullValues.value) =>
     match x with
     | NullValues.Value_float (Some x0) => float_add acc x0
     | _ => acc
     end) l1 acc =
  fold_left
    (fun (acc : float) (x : NullValues.value) =>
     match x with
     | NullValues.Value_float (Some x0) => float_add acc x0
     | _ => acc
     end) l2 acc.
  Proof.
    intros l1 l2.
    rewrite <- _permut_eq_Permutation.
    induction 1 as [ |v _ l3 l4  l5 <- Hp IH];intros acc;[apply eq_refl| ].
    cbn.
    rewrite fold_left_app.
    simpl.
    rewrite IH.
    rewrite fold_left_app.
    destruct v;destruct o;try apply eq_refl.
    simpl.
    apply f_equal.
    revert f acc.
    clear -faac.
    induction l4;intros;[apply eq_refl| ].
    cbn.
    rewrite <- IHl4.
    destruct a;destruct o;try apply eq_refl.
    apply f_equal.
    rewrite <- (float_add_assoc faac).
    rewrite (float_add_comm faac f).
    now rewrite (float_add_assoc faac).
  Qed.

  Lemma Permutation_aggregate_sum_right_float_aux
    (faac : float_add_assoc_comm) :
    forall l1 l2,
      Permutation l1 l2 ->
      forall acc,
  fold_right
    (fun (x : NullValues.value) (acc : float) =>
     match x with
     | NullValues.Value_float (Some x0) => float_add x0 acc
     | _ => acc
     end) acc l1 =
  fold_right
    (fun (x : NullValues.value) (acc : float) =>
     match x with
     | NullValues.Value_float (Some x0) => float_add x0 acc
     | _ => acc
     end) acc l2.
  Proof.
    intros l1 l2.
    rewrite <- _permut_eq_Permutation.
    induction 1 as [ |v _ l3 l4  l5 <- Hp IH];intros acc;[apply eq_refl| ].
    cbn.
    rewrite fold_right_app.
    simpl.
    rewrite IH.
    rewrite fold_right_app.
    destruct v;destruct o;try apply eq_refl.
    simpl.
    revert f acc.
    clear -faac.
    induction l4;intros;[apply eq_refl| ].
    cbn.
    rewrite <- IHl4.
    destruct a;destruct o;try apply eq_refl.
    rewrite (float_add_assoc faac).
    rewrite (float_add_comm faac f).
    now rewrite <- (float_add_assoc faac).
  Qed.

  Lemma Permutation_aggregate_max_aux :
    forall l1 l2,
      Permutation l1 l2 ->      
      forall acc,        
   fold_left
    (fun (acc : Z) (x : NullValues.value) =>
     match x with
     | NullValues.Value_Z (Some x0) => Z.max acc x0
     | _ => acc
     end) l1 acc =
  fold_left
    (fun (acc : Z) (x : NullValues.value) =>
     match x with
     | NullValues.Value_Z (Some x0) => Z.max acc x0
     | _ => acc
     end) l2 acc.
  Proof.
    intros l1 l2.
    rewrite <- _permut_eq_Permutation.
    induction 1 as [ |v _ l3 l4  l5 <- Hp IH];intros acc;[apply eq_refl| ].
    cbn.
    rewrite fold_left_app.
    simpl.
    rewrite IH.
    rewrite fold_left_app.
    destruct v;destruct o;try apply eq_refl.
    simpl.
    apply f_equal.
    revert z acc.
    clear.
    induction l4;intros;[apply eq_refl| ].
    cbn.
    rewrite <- IHl4.
    destruct a;destruct o;try apply eq_refl.
    f_equal.
    lia.
  Qed.

  Lemma Permutation_aggregate_max_float_aux :
    forall l1 l2,
      Permutation l1 l2 ->
      forall acc,
          fold_right
             (fun (x0 : NullValues.value) (y : float) =>
              match x0 with
              | NullValues.Value_float (Some x1) => float_max x1 y
              | _ => y
              end) acc l1 =
          fold_right
             (fun (x0 : NullValues.value) (y : float) =>
              match x0 with
              | NullValues.Value_float (Some x1) => float_max x1 y
              | _ => y
              end) acc l2.
  Proof.
    intros l1 l2.
    rewrite <- _permut_eq_Permutation.
    induction 1 as [ |v _ l3 l4  l5 <- Hp IH];intros acc;[apply eq_refl| ].
    cbn.
    rewrite fold_right_app.
    simpl.
    rewrite IH.
    rewrite fold_right_app.
    destruct v;destruct o;try apply eq_refl.
    simpl.
    revert f acc.
    clear.
    induction l4;intros;[apply eq_refl| ].
    cbn.
    rewrite <- IHl4.
    destruct a;destruct o;try apply eq_refl.
    rewrite float_max_assoc.
    rewrite (float_max_comm f f0).
    now rewrite <- float_max_assoc.
  Qed.

  (* TODO: generalize *)
  (*
  Lemma Permutation_aggregate :
    forall A (agg_acc:A -> A -> A),
      (forall x y z, agg_acc x (agg_acc y z) = agg_acc y (agg_acc x z)) ->
    forall (f:NullValues.value -> A -> A),
    forall l1 l2,
      Permutation l1 l2 ->
    forall acc,
      fold_left
    (fun (acc : A) (x : NullValues.value) =>
       agg_acc (f x acc) acc) l1 acc =
  fold_left
    (fun (acc : A) (x : NullValues.value) =>
       agg_acc (f x acc) acc) l2 acc.
  Proof.
    intros A agg_acc Hagg_acc f l1 l2.
    rewrite <- _permut_eq_Permutation.
    induction 1 as [ |v _ l3 l4  l5 <- Hp IH];intros acc;[apply eq_refl| ].
    cbn.
    rewrite fold_left_app.
    simpl.
    rewrite IH.
    rewrite fold_left_app.
    apply f_equal.
    revert acc.
    clear -Hagg_acc.
    induction l4;intros;[apply eq_refl| ].
    cbn.
    rewrite <- IHl4.
    apply f_equal.
  Qed.
  *)

  Open Scope Z_scope.

  Lemma forallb_Permutation :
    forall A (P : A -> bool),
    forall m1 m2,
      Permutation m1 m2 ->
      forallb
        P m1 =
      forallb
        P m2.
    Proof.
      induction 1 as [ | v l1 l2 Heq IH| | l1 l2 l3 Heq1 IH1 Heq2  IH2].
      - apply eq_refl.
      - cbn;rewrite IH;apply eq_refl.
      - cbn.
        rewrite 2 Bool.andb_assoc.
        apply f_equal2;[|apply eq_refl].
        apply Bool.andb_comm.
      - rewrite IH1;apply IH2.
    Qed.
    
    Lemma existsb_Permutation_Valuz_Z :
    forall m1 m2,
      Permutation m1 m2 ->
      existsb
        (fun x : NullValues.value =>
           match x with
           | NullValues.Value_Z _ => false
           | _ => true
           end) m1 =
      existsb
        (fun x : NullValues.value =>
           match x with
           | NullValues.Value_Z _ => false
           | _ => true
           end) m2.
   induction 1 as [ | v l1 l2 Heq IH| | l1 l2 l3 Heq1 IH1 Heq2  IH2].
      - apply eq_refl.
      - cbn;rewrite IH;apply eq_refl.
      - cbn.
        rewrite 2 Bool.orb_assoc.
        apply f_equal2;[|apply eq_refl].
        apply Bool.orb_comm.
      - rewrite IH1;apply IH2.
    Qed.
    
  Lemma  fold_left_sum_aux :
    forall  l z acc,
    z +
  fold_left
    (fun (acc : Z) (x : NullValues.value) =>
     match x with
     | NullValues.Value_Z (Some x0) => acc + x0
     | _ => acc
     end) l acc =
  fold_left
    (fun (acc : Z) (x : NullValues.value) =>
     match x with
     | NullValues.Value_Z (Some x0) => acc + x0
     | _ => acc
     end) l (acc + z).
  Proof.
    induction l;intros;
      [apply Z.add_comm| ].
    cbn.
    rewrite IHl.
    apply f_equal2;[apply eq_refl| ].
    destruct a;destruct o;try apply eq_refl.
    rewrite <- 2 Z.add_assoc.
    apply f_equal2;try apply eq_refl.
    apply  Z.add_comm.
  Qed.
    
  Lemma  fold_left_sum_float_aux
    (faac : float_add_assoc_comm) :
    forall  l f acc,
  float_add f
    (fold_left
       (fun (acc : float) (x : NullValues.value) =>
        match x with
        | NullValues.Value_float (Some x0) => float_add acc x0
        | _ => acc
        end) l acc) =
  fold_left
    (fun (acc : float) (x : NullValues.value) =>
     match x with
     | NullValues.Value_float (Some x0) => float_add acc x0
     | _ => acc
     end) l (float_add acc f).
  Proof.
    induction l;intros;
      [apply (float_add_comm faac)| ].
    cbn.
    rewrite IHl.
    apply f_equal2;[apply eq_refl| ].
    destruct a;destruct o;try apply eq_refl.
    rewrite <- 2 (float_add_assoc faac).
    apply f_equal2;try apply eq_refl.
    apply (float_add_comm faac).
  Qed.

    Lemma  fold_left_max_aux :
    forall  l z acc,
    Z.max z 
  (fold_left
    (fun (acc : Z) (x : NullValues.value) =>
     match x with
     | NullValues.Value_Z (Some x0) => Z.max acc  x0
     | _ => acc
     end) l acc) =
  fold_left
    (fun (acc : Z) (x : NullValues.value) =>
     match x with
     | NullValues.Value_Z (Some x0) => Z.max acc  x0
     | _ => acc
     end) l (Z.max acc  z).
  Proof.
    induction l;intros;
      [apply Z.max_comm| ].
    cbn.
    rewrite IHl.
    apply f_equal2;[apply eq_refl| ].
    destruct a;destruct o;try apply eq_refl.
    rewrite <- 2 Z.max_assoc.
    apply f_equal2;try apply eq_refl.
    apply  Z.max_comm.
  Qed.

  Lemma interp_aggregate_permut_aux1 :
    forall l, exists m,      
        (filter
           (fun x : NullValues.value => match x with
                                       | NullValues.Value_Z (Some _) => true
                                       | _ => false
                                       end) l) =
        map (fun x => NullValues.Value_Z (Some x) ) m.
  Proof.
    induction l;[exists nil;apply eq_refl| ].
    destruct IHl.
    simpl.
    rewrite H.
    destruct a;destruct o;try (exists x;apply eq_refl).
    exists (z::x);apply eq_refl.
  Qed.

  Lemma interp_aggregate_float_permut_aux1 :
    forall l, exists m,
        (filter
           (fun x : NullValues.value => match x with
                                       | NullValues.Value_float (Some _) => true
                                       | _ => false
                                       end) l) =
        map (fun x => NullValues.Value_float (Some x) ) m.
  Proof.
    induction l;[exists nil;apply eq_refl| ].
    destruct IHl.
    simpl.
    rewrite H.
    destruct a;destruct o;try (exists x;apply eq_refl).
    exists (f::x);apply eq_refl.
  Qed.

  Lemma interp_aggregate_permut
    (faac : float_add_assoc_comm) :
    forall ag l1 l2 ,
      Permutation l1 l2 ->
      (* (exists t, forall v, In v l1 -> Tuple.type_of_value _ v = t) -> *)
      interp_aggregate TNull ag l1 = interp_aggregate TNull ag l2.
  Proof.
    unfold nraenv_eval in *.
    intros  [ ].
    do 6 (try (destruct s as [ |[[ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ]] s];try (intros;apply eq_refl))).
    - (* sum *)
      intros;cbn.
      do 2 apply f_equal.
      apply (Permutation_aggregate_sum_aux _ _ H).
    - (* sum. *)
      intros;cbn.
      do 2 apply f_equal.
      apply (Permutation_aggregate_sum_left_float_aux faac _ _ H).
    - (* count *)
      intros;cbn;do 3 apply f_equal.
      apply (Permutation_length H).
    - (* max *)
      intros.
      simpl.
      rewrite <- (forallb_Permutation _ _ _ _ H).
      destruct forallb eqn:D1;
        [|apply eq_refl].
      destruct (interp_aggregate_permut_aux1 l1) as [l3 Hl3].        
      destruct (interp_aggregate_permut_aux1 l2) as [l4 Hl4].      
      assert (A1 :=
                Permutation_filter
                  (fun x : NullValues.value =>
                     match x with
                     | NullValues.Value_Z (Some _) => true
                     | _ => false
                     end) _ _ H).
      rewrite Hl3,Hl4 in *.
      apply Permutation_map_inj_Permutation in A1;
        [|intros x1 x2 Hx1x2;inversion Hx1x2;apply eq_refl].
      revert  A1;clear.
      induction 1.
      + apply eq_refl.
      + simpl in *.        
        rewrite (Permutation_aggregate_max_aux _ _ (Permutation_map _ A1)).
        apply eq_refl.
      + simpl.
        rewrite Z.max_comm.
        apply eq_refl.
      + rewrite   IHA1_1,IHA1_2.
        apply eq_refl.
    - (* max. *)
      intros.
      simpl.
      rewrite <- (forallb_Permutation _ _ _ _ H).
      destruct forallb eqn:D1;
        [|apply eq_refl].
      destruct (interp_aggregate_float_permut_aux1 l1) as [l3 Hl3].
      destruct (interp_aggregate_float_permut_aux1 l2) as [l4 Hl4].
      assert (A1 :=
                Permutation_filter
                  (fun x : NullValues.value =>
                     match x with
                     | NullValues.Value_float (Some _) => true
                     | _ => false
                     end) _ _ H).
      rewrite Hl3,Hl4 in *.
      apply Permutation_map_inj_Permutation in A1;
        [|intros x1 x2 Hx1x2;inversion Hx1x2;apply eq_refl].
      revert  A1;clear -faac.
      induction 1.
      + apply eq_refl.
      + simpl in *.
        rewrite (Permutation_aggregate_max_float_aux _ _ (Permutation_map _ A1)).
        apply eq_refl.
      + simpl.
        do 2 apply f_equal.
        rewrite float_max_assoc.
        rewrite (float_max_comm y x).
        now rewrite <- float_max_assoc.
      + rewrite   IHA1_1,IHA1_2.
        apply eq_refl.
    - (* avg *)
      intros;cbn.      
      do 2 apply f_equal.
      apply f_equal2.
      apply (Permutation_aggregate_sum_aux _ _ H).
      apply f_equal;apply (Permutation_length H).
    - (* avg. *)
      intros;cbn.
      do 2 apply f_equal.
      change ((fun x => match Z.of_nat x with
  | 0 => float_zero
  | _ =>
      float_div
        (fold_right
           (fun (x : NullValues.value) (acc : float) =>
            match x with
            | NullValues.Value_float (Some x0) => float_add x0 acc
            | _ => acc
            end) float_zero l1) (float_of_int (Z.of_nat x))
  end =
  match Z.of_nat (Datatypes.length l2) with
  | 0 => float_zero
  | _ =>
      float_div
        (fold_right
           (fun (x : NullValues.value) (acc : float) =>
            match x with
            | NullValues.Value_float (Some x0) => float_add x0 acc
            | _ => acc
            end) float_zero l2) (float_of_int (Z.of_nat (Datatypes.length l2)))
  end) (Datatypes.length l1)).
      rewrite (Permutation_length H).
      change ((fun x => match Z.of_nat x with
  | 0 => float_zero
  | _ =>
      float_div
        (fold_right
           (fun (x : NullValues.value) (acc : float) =>
            match x with
            | NullValues.Value_float (Some x0) => float_add x0 acc
            | _ => acc
            end) float_zero l1) (float_of_int (Z.of_nat x))
  end =
  match Z.of_nat x with
  | 0 => float_zero
  | _ =>
      float_div
        (fold_right
           (fun (x : NullValues.value) (acc : float) =>
            match x with
            | NullValues.Value_float (Some x0) => float_add x0 acc
            | _ => acc
            end) float_zero l2) (float_of_int (Z.of_nat x))
  end) (Datatypes.length l2)).
      case (Datatypes.length l2); auto.
      intro n. simpl. apply f_equal2; auto.
      now apply (Permutation_aggregate_sum_right_float_aux faac _ _ H).
  Qed.

  Lemma aggregate_to_nraenv_eq_aux1 :
    forall lf l1,
      lift_filter
        (fun d : data =>
           match match d with
                 | dleft _ => Some (dbool true)
                 | dright _ => Some (dbool false)
                 | _ => None
                 end with
           | Some (dbool b) => Some b
           | _ => None
           end) (map value_to_data l1) = Some lf
      ->
      lift_map (fun din : data => match din with
                                 | dleft d1 => Some d1
                                 | dright d2 => Some d2
                                 | _ => None
                                   end) lf =
      Some (map
              (fun x =>
                 match x with
                 | dleft d1 => d1
                 | _ => dnat 0
                 end) lf).
  Proof.
    intros.
    apply lift_filter_Some_filter in H.
    subst.
    rewrite ListFacts.filter_map.
    rewrite map_map.
    rewrite lift_map_map_fusion.
    induction l1;
      [apply eq_refl| ].
    simpl.    
    destruct a;destruct o;try apply IHl1.
    + simpl.
      unfold lift.
      destruct lift_map;[|discriminate].
      inversion IHl1.
      apply eq_refl.
     + simpl.
      unfold lift.
      destruct lift_map;[|discriminate].
      inversion IHl1.
      apply eq_refl.
    + simpl.
      unfold lift.
      destruct lift_map;[|discriminate].
      inversion IHl1.
      apply eq_refl.
    + simpl.
      unfold lift.
      destruct lift_map;[|discriminate].
      inversion IHl1.
      apply eq_refl.
  Qed.  
      
  Lemma aggregate_to_nraenv_eq
    (faac : float_add_assoc_comm) :
    forall  ag (a:funterm TRcd),
    forall ft_to_nraenv ft_to_lval type_of_ft br i dreg,
      (forall did, nraenv_eval br i (ft_to_nraenv a)  dreg did =
              Some
                (dcoll (map value_to_data  (ft_to_lval a)))) ->
      (forall ft, In ft (ft_to_lval a) -> type_of_ft a = Tuple.type_of_value TNull ft) ->      
      forall did,
        nraenv_eval br i (aggregate_to_nraenv ag (type_of_ft a) (ft_to_nraenv a)) dreg did =
        Some
          (value_to_data  (interp_aggregate _ ag (ft_to_lval a))).
  Proof.
    unfold nraenv_eval in *.
    intros  [ ].
    repeat (destruct s as [ |[[ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ]] s];try (intros;apply eq_refl)).
    - (* sum *)
      do 7 intro.
      intros Heq Ht did.
      simpl interp_aggregate.
      simpl.
      unfold olift.
      destruct (type_of_ft a) eqn:D1.
      + simpl;refine (eq_sym (f_equal _ _)).
        do 2 apply f_equal.
        revert Ht;clear.
        set (l := (ft_to_lval a)).
        generalize l.
        clear.
        induction l;intros;
          [apply eq_refl| ].
        cbn.
        assert (Ha := Ht _ (or_introl eq_refl)).
        destruct a;destruct o;try discriminate;
          apply (IHl (fun v H => Ht v (or_intror H))).
      + simpl.
        unfold olift.
        rewrite Heq.
        simpl;unfold lift.
        revert Ht.
        set (l := (ft_to_lval a)).
        generalize l.
        clear.
        induction l;intros;[apply eq_refl| ].
        assert (IH := IHl (fun v H => Ht v (or_intror H))).
        destruct lift_filter eqn:D1;[|discriminate].
        simpl in IH;unfold lift in IH.
        destruct lift_map eqn:D2;[|discriminate].
        simpl in IH.
        simpl.
        assert (Ha := Ht _ (or_introl eq_refl)).
        destruct a;try discriminate.
        destruct o.
        * simpl;rewrite D1;simpl.
          unfold lift.
          rewrite D2.
          simpl.
          unfold lift.
          destruct (dsum l1);try discriminate.
          inversion IH.
          do 3 apply f_equal.
          apply fold_left_sum_aux.
        * simpl;rewrite D1;simpl;rewrite D2.
          simpl;apply IH.
      + simpl;refine (eq_sym (f_equal _ _)).
        do 2 apply f_equal.
        revert Ht;clear.
        set (l := (ft_to_lval a)).
        generalize l.
        clear.
        induction l;intros;
          [apply eq_refl| ].
        cbn.
        assert (Ha := Ht _ (or_introl eq_refl)).
        destruct a;destruct o;try discriminate;
          apply (IHl (fun v H => Ht v (or_intror H))).
      + simpl;refine (eq_sym (f_equal _ _)).
        do 2 apply f_equal.
        revert Ht;clear.
        set (l := (ft_to_lval a)).
        generalize l.
        clear.
        induction l;intros;
          [apply eq_refl| ].
        cbn.
        assert (Ha := Ht _ (or_introl eq_refl)).
        destruct a;destruct o;try discriminate;
          apply (IHl (fun v H => Ht v (or_intror H))).
    - (* sum. *)
      do 7 intro.
      intros Heq Ht did.
      simpl interp_aggregate.
      simpl.
      unfold olift.
      destruct (type_of_ft a) eqn:D1.
      + simpl;refine (eq_sym (f_equal _ _)).
        do 2 apply f_equal.
        revert Ht;clear.
        set (l := (ft_to_lval a)).
        generalize l.
        clear.
        induction l;intros;
          [apply eq_refl| ].
        cbn.
        assert (Ha := Ht _ (or_introl eq_refl)).
        destruct a;destruct o;try discriminate;
          apply (IHl (fun v H => Ht v (or_intror H))).
      + simpl;refine (eq_sym (f_equal _ _)).
        do 2 apply f_equal.
        revert Ht;clear.
        set (l := (ft_to_lval a)).
        generalize l.
        clear.
        induction l;intros;
          [apply eq_refl| ].
        cbn.
        assert (Ha := Ht _ (or_introl eq_refl)).
        destruct a;destruct o;try discriminate;
          apply (IHl (fun v H => Ht v (or_intror H))).
      + simpl;refine (eq_sym (f_equal _ _)).
        do 2 apply f_equal.
        revert Ht;clear.
        set (l := (ft_to_lval a)).
        generalize l.
        clear.
        induction l;intros;
          [apply eq_refl| ].
        cbn.
        assert (Ha := Ht _ (or_introl eq_refl)).
        destruct a;destruct o;try discriminate;
          apply (IHl (fun v H => Ht v (or_intror H))).
      + simpl.
        unfold olift.
        rewrite Heq.
        simpl;unfold lift.
        revert Ht.
        set (l := (ft_to_lval a)).
        generalize l.
        clear -faac.
        induction l;intros;[apply eq_refl| ].
        assert (IH := IHl (fun v H => Ht v (or_intror H))).
        destruct lift_filter eqn:D1;[|discriminate].
        simpl in IH;unfold lift in IH.
        destruct lift_map eqn:D2;[|discriminate].
        simpl in IH.
        simpl.
        assert (Ha := Ht _ (or_introl eq_refl)).
        destruct a;try discriminate.
        destruct o.
        * simpl;rewrite D1;simpl.
          unfold lift.
          rewrite D2.
          unfold lifted_fsum, lifted_fbag in *. simpl.
          rewrite lift_lift in *.
          rewrite lift_lift.
          case_eq (lift_map (ondfloat (fun x : float => x)) l1); [intro d| ]; intro Heq; rewrite Heq in IH; try discriminate.
          simpl in *.
          inversion IH.
          do 3 apply f_equal.
          rewrite H0.
          apply (fold_left_sum_float_aux faac).
        * simpl;rewrite D1;simpl;rewrite D2.
          simpl;apply IH.
    - (* count *)
      intros.
      simpl interp_aggregate.
      simpl nraenv_core_eval.
      unfold olift.
      rewrite H;simpl.
      unfold bcount.
      rewrite map_length.
      apply eq_refl.
    - (* max *)
      do 7 intro.
      intros Heq Ht did.
      simpl interp_aggregate.
      simpl.
      unfold olift.
      destruct (type_of_ft a) eqn:D1;swap 2 3.
      + simpl;refine (eq_sym (f_equal _ _)).
        revert Ht;clear.
        set (l := (ft_to_lval a)).
        generalize l.
        clear.
        induction l;intros;
          [apply eq_refl| ].
        cbn.
        assert (Ha := Ht _ (or_introl eq_refl)).
        destruct a;destruct o;try discriminate;apply eq_refl.        
      + simpl;refine (eq_sym (f_equal _ _)).
        revert Ht;clear.
        set (l := (ft_to_lval a)).
        generalize l.
        clear.
        induction l;intros;
          [apply eq_refl| ].
        cbn.
        assert (Ha := Ht _ (or_introl eq_refl)).
        destruct a;destruct o;try discriminate;apply eq_refl.
      +
        assert (H1 :   forallb (fun x : NullValues.value => match x with
                                                 | NullValues.Value_Z _ => true
                                                 | _ => false
                                                            end) (ft_to_lval a) =true).
        { rewrite forallb_forall.
          intros.
          apply Ht in H.
          destruct x;destruct o;try discriminate;apply eq_refl.
        }
        rewrite H1;clear H1.
        refine (NRAEnvUnOpLeft_refine _ _ _ _ (NRAEnvUnop OpNatMax
              (NRAEnvMap (NRAEnvEither NRAEnvID NRAEnvID)
                 (NRAEnvSelect (NRAEnvEither (NRAEnvConst (dbool true)) (NRAEnvConst (dbool false)))
                               (ft_to_nraenv a)))) _ _ _ ).
        intros did1.
        simple refine
          (eq_trans
             (NRAEnvUnOpNatMax_refine_alt _ _ _ _ _ _ _ 
                                            (fun d =>
                                               match d with
                                               | dleft d => d
                                               | dright d =>  d
                                               | _ => dunit
                                               end ) _ _)
             _).
        * exact (
       (map value_to_data
          (filter
             (fun d : NullValues.value => match TnullTD.value_to_data d with
                                          | dleft _ => true
                                          | _ => false
                                          end) (ft_to_lval a)))).
        * simple refine (NRAEnvMap_eval_map _ _ _
                                            (fun d =>
                                               match d with
                                               | dleft d => d
                                               | dright d =>  d
                                               | _ => dunit
                                               end ) _ _ _ _ _ _  ).
          -- refine
               (NRAEnvSelect_eval_filter_alt
                  _ _ _ _
                  (fun d =>
                     match TnullTD.value_to_data d with
                     | dleft _ => true
                     | dright _ =>  false
                     | _ => false
                     end ) _ _ _ _ _ Heq _).
             intros;unfold nraenv_eval;simpl.
             destruct d;destruct o;apply eq_refl.
          -- intros.
             unfold nraenv_eval.
             rewrite in_map_iff in H.
             destruct H as [v [Hv Iv]].
             subst d.
             destruct v;destruct o;apply eq_refl.
        * revert Ht;clear.
          set (l := (ft_to_lval a) ).
          intros.
          unfold lifted_max,lift,lifted_zbag,ondnat.
          rewrite 2 lift_map_map_fusion.
          assert ( Heq : filter (fun x : NullValues.value => match x with
                                             | NullValues.Value_Z (Some _) => true
                                             | _ => false
                                             end) l =   (filter
           (fun d : NullValues.value => match TnullTD.value_to_data d with
                                        | dleft _ => true
                                        | _ => false
                                       end) l)).
          { apply filter_ext.
            intros  [ [ ] | [] | [ ] | [ ] ] H;try apply eq_refl;
              apply Ht in H;discriminate.
          }
          rewrite <- Heq.
          clear Heq.
          destruct (interp_aggregate_permut_aux1 l) as [l1 H].
          rewrite H.
          rewrite lift_map_map_fusion.
          simpl.
          rewrite lift_map_map.
          do 2 apply  f_equal.
          unfold bnummax.
          rewrite map_id.
          clear.
          induction l1;
            [apply eq_refl| ].
          simpl.
          { refine
            (eq_trans
               (eq_sym (fold_symmetric _ _ _ _  _))
               _).
            - intros;apply Z.max_assoc.
            - apply Z.max_comm.
            - rewrite fold_left_map.
              apply eq_refl.
          }
      + simpl;refine (eq_sym (f_equal _ _)).
        revert Ht;clear.
        set (l := (ft_to_lval a)).
        generalize l.
        clear.
        induction l;intros;
          [apply eq_refl| ].
        cbn.
        assert (Ha := Ht _ (or_introl eq_refl)).
        destruct a;destruct o;try discriminate;apply eq_refl.
    - (* max. *)
      do 7 intro.
      intros Heq Ht did.
      simpl interp_aggregate.
      simpl.
      unfold olift.
      destruct (type_of_ft a) eqn:D1.
      + simpl;refine (eq_sym (f_equal _ _)).
        revert Ht;clear.
        set (l := (ft_to_lval a)).
        generalize l.
        clear.
        induction l;intros;
          [apply eq_refl| ].
        cbn.
        assert (Ha := Ht _ (or_introl eq_refl)).
        destruct a;destruct o;try discriminate;apply eq_refl.
      + simpl;refine (eq_sym (f_equal _ _)).
        revert Ht;clear.
        set (l := (ft_to_lval a)).
        generalize l.
        clear.
        induction l;intros;
          [apply eq_refl| ].
        cbn.
        assert (Ha := Ht _ (or_introl eq_refl)).
        destruct a;destruct o;try discriminate;apply eq_refl.
      + simpl;refine (eq_sym (f_equal _ _)).
        revert Ht;clear.
        set (l := (ft_to_lval a)).
        generalize l.
        clear.
        induction l;intros;
          [apply eq_refl| ].
        cbn.
        assert (Ha := Ht _ (or_introl eq_refl)).
        destruct a;destruct o;try discriminate;apply eq_refl.
      +
        assert (H1 :   forallb (fun x : NullValues.value => match x with
                                                 | NullValues.Value_float _ => true
                                                 | _ => false
                                                            end) (ft_to_lval a) =true).
        { rewrite forallb_forall.
          intros.
          apply Ht in H.
          destruct x;destruct o;try discriminate;apply eq_refl.
        }
        rewrite H1;clear H1.
        refine (NRAEnvUnOpLeft_refine _ _ _ _ (NRAEnvUnop OpFloatBagMax
              (NRAEnvMap (NRAEnvEither NRAEnvID NRAEnvID)
                 (NRAEnvSelect (NRAEnvEither (NRAEnvConst (dbool true)) (NRAEnvConst (dbool false)))
                               (ft_to_nraenv a)))) _ _ _ ).
        intros did1.
        simple refine
          (eq_trans
             (NRAEnvUnOpFloatBagMax_refine_alt _ _ _ _ _ _ _ 
                                            (fun d =>
                                               match d with
                                               | dleft d => d
                                               | dright d =>  d
                                               | _ => dunit
                                               end ) _ _)
             _).
        * exact (
       (map value_to_data
          (filter
             (fun d : NullValues.value => match TnullTD.value_to_data d with
                                          | dleft _ => true
                                          | _ => false
                                          end) (ft_to_lval a)))).
        * simple refine (NRAEnvMap_eval_map _ _ _
                                            (fun d =>
                                               match d with
                                               | dleft d => d
                                               | dright d =>  d
                                               | _ => dunit
                                               end ) _ _ _ _ _ _  ).
          -- refine
               (NRAEnvSelect_eval_filter_alt
                  _ _ _ _
                  (fun d =>
                     match TnullTD.value_to_data d with
                     | dleft _ => true
                     | dright _ =>  false
                     | _ => false
                     end ) _ _ _ _ _ Heq _).
             intros;unfold nraenv_eval;simpl.
             destruct d;destruct o;apply eq_refl.
          -- intros.
             unfold nraenv_eval.
             rewrite in_map_iff in H.
             destruct H as [v [Hv Iv]].
             subst d.
             destruct v;destruct o;apply eq_refl.
        * revert Ht;clear.
          set (l := (ft_to_lval a) ).
          intros.
          unfold lifted_fmax, lift_oncoll, lifted_fbag, ondfloat.
          rewrite 2 lift_map_map_fusion.
          assert ( Heq : filter (fun x : NullValues.value => match x with
                                             | NullValues.Value_float (Some _) => true
                                             | _ => false
                                             end) l =   (filter
           (fun d : NullValues.value => match TnullTD.value_to_data d with
                                        | dleft _ => true
                                        | _ => false
                                       end) l)).
          { apply filter_ext.
            intros  [ [ ] | [] | [ ] | [ ] ] H;try apply eq_refl;
              apply Ht in H;discriminate.
          }
          rewrite <- Heq.
          clear Heq.
          destruct (interp_aggregate_float_permut_aux1 l) as [l1 H].
          rewrite H.
          rewrite lift_map_map_fusion.
          simpl.
          rewrite lift_map_map.
          simpl.
          do 2 apply  f_equal.
          unfold float_list_max.
          rewrite map_id.
          clear.
          induction l1;
            [apply eq_refl| ].
          simpl.
          apply f_equal.
          now rewrite fold_right_map.
    - (* avg *)
      do 7 intro.
      intros Heq Ht did.
      simpl interp_aggregate.
      simpl.
      unfold olift.
      destruct (type_of_ft a) eqn:D1;swap 2 3.
      + simpl.
        unfold bcount,olift.
        rewrite Heq.
        simpl.
        rewrite map_length.
        apply f_equal.
        do 2 apply f_equal.
        apply f_equal2;[apply eq_sym|apply eq_refl].
        revert Ht;clear.
        set (l := (ft_to_lval a)).
        generalize l.
        clear.
        induction l;intros;
          [apply eq_refl| ].
        cbn.
        assert (Ha := Ht _ (or_introl eq_refl)).
        destruct a;try discriminate.
        clear Ha.
        apply eq_sym.
        assert (IH := eq_sym (IHl (fun v H => Ht v (or_intror H)))).
        apply IH.
      + simpl.
        unfold bcount,olift,lift.
        rewrite Heq.
        simpl.
        rewrite map_length.
        apply f_equal.
        do 2 apply f_equal.
        apply f_equal2;[apply eq_sym|apply eq_refl].
        revert Ht;clear.
        set (l := (ft_to_lval a)).
        generalize l.
        clear.
        induction l;intros;
          [apply eq_refl| ].
        cbn.
        assert (Ha := Ht _ (or_introl eq_refl)).
        destruct a;try discriminate.
        clear Ha.
        apply eq_sym.
        assert (IH := eq_sym (IHl (fun v H => Ht v (or_intror H)))).
        apply IH.
      + 
        refine (NRAEnvUnOpLeft_refine tnull_frt br i _
           (NRAEnvBinop (OpNatBinary NatDiv)
              (NRAEnvUnop OpNatSum
                 (NRAEnvMap (NRAEnvEither NRAEnvID NRAEnvID)
                    (NRAEnvSelect (NRAEnvEither (NRAEnvConst (dbool true)) (NRAEnvConst (dbool false)))
                                  (ft_to_nraenv a)))) (NRAEnvUnop OpCount (ft_to_nraenv a))) _ _ _ ).
        refine (NRAEnvBinOpNatDivt_refine  tnull_frt br i  _ _ _ _ _ _ _ ).
        * intros.          
          unfold nraenv_eval.
          simpl.
          rewrite Heq.
          simpl;unfold olift,lift.
        revert Ht.
        set (l := (ft_to_lval a)).
        generalize l.
        clear.
        induction l;intros;[apply eq_refl| ].
        assert (IH := IHl (fun v H => Ht v (or_intror H))).
        destruct lift_filter eqn:D1;[|discriminate].
        simpl in IH;unfold lift in IH.
        destruct lift_map eqn:D2;[|discriminate].
        simpl in IH.
        simpl.
        assert (Ha := Ht _ (or_introl eq_refl)).
        destruct a;try discriminate.
        destruct o.
          -- simpl;rewrite D1;simpl.
          unfold lift.
          rewrite D2.
          simpl.
          unfold lift.
          destruct (dsum l1);try discriminate.
          inversion IH.
          do 2 apply f_equal.
          apply fold_left_sum_aux.
          -- simpl;rewrite D1;simpl;rewrite D2.
             simpl;apply IH.
        *  intros.
           unfold nraenv_eval.
           simpl interp_aggregate.
           simpl nraenv_core_eval.
           unfold olift.
           rewrite Heq;simpl.
           unfold bcount.
           rewrite map_length.
           apply eq_refl.
      + simpl.
        unfold bcount,olift,lift.
        rewrite Heq.
        simpl.
        rewrite map_length.
        apply f_equal.
        do 2 apply f_equal.
        apply f_equal2;[apply eq_sym|apply eq_refl].
        revert Ht;clear.
        set (l := (ft_to_lval a)).
        generalize l.
        clear.
        induction l;intros;
          [apply eq_refl| ].
        cbn.
        assert (Ha := Ht _ (or_introl eq_refl)).
        destruct a;try discriminate.
        clear Ha.
        apply eq_sym.
        assert (IH := eq_sym (IHl (fun v H => Ht v (or_intror H)))).
        apply IH.
    - (* avg. *)
      intros a ft_to_nraenv ft_to_lval type_of_ft br i dreg.
      intros Heq Ht did.
      simpl interp_aggregate.
      simpl.
      unfold olift.
      destruct (type_of_ft a) eqn:D1.
      4:{
        refine (NRAEnvUnOpLeft_refine tnull_frt br i _
          (ITE (NRAEnvBinop OpEqual NRAEnvID (NRAEnvConst (dnat 0)))
              (NRAEnvUnop OpCount (ft_to_nraenv a)) (NRAEnvConst (dfloat float_zero))
              (NRAEnvBinop (OpFloatBinary FloatDiv)
                 (NRAEnvUnop OpFloatSum
                    (NRAEnvMap (NRAEnvEither NRAEnvID NRAEnvID)
                       (NRAEnvSelect
                          (NRAEnvEither (NRAEnvConst (dbool true))
                             (NRAEnvConst (dbool false)))
                          (ft_to_nraenv a))))
                 (NRAEnvUnop OpFloatOfNat (NRAEnvUnop OpCount (ft_to_nraenv a))))) _ _ _ ).
        assert (Hif: forall z r0 rr,
                   Some (dfloat (match z with 0 => r0 | _ => rr end)) =
                   if Z.eqb z 0 then Some (dfloat r0) else Some (dfloat rr))
          by now intros [ | | ].
        rewrite Hif; clear Hif.
        refine (NRAEnvITE_refine _ _ _
       (NRAEnvBinop OpEqual NRAEnvID (NRAEnvConst (dnat 0)))
       (NRAEnvUnop OpCount (ft_to_nraenv a))
       (NRAEnvConst (dfloat float_zero))
       (NRAEnvBinop (OpFloatBinary FloatDiv)
          (NRAEnvUnop OpFloatSum
             (NRAEnvMap (NRAEnvEither NRAEnvID NRAEnvID)
                (NRAEnvSelect
                   (NRAEnvEither (NRAEnvConst (dbool true))
                      (NRAEnvConst (dbool false))) (ft_to_nraenv a))))
          (NRAEnvUnop OpFloatOfNat (NRAEnvUnop OpCount (ft_to_nraenv a))))
       _ _ _ _ _ _ _).
        + intro did0. cbn. unfold olift. rewrite Heq. simpl.
          unfold unbdata, bcount. do 2 apply f_equal. rewrite map_length.
          simpl. case_eq (Z.eq_dec (Z.of_nat (Datatypes.length (ft_to_lval a))) 0); simpl.
          * now intros -> ->.
          * intros e ->. symmetry. now rewrite Z.eqb_neq.
        + reflexivity.
        + intro Hlength.
          refine (NRAEnvBinOpFloatDivt_refine tnull_frt br i _ _ _ _ _ _ _).
          * intro did0.
            unfold nraenv_eval.
            simpl.
            rewrite Heq.
            simpl;unfold olift,lift.
            revert Ht.
            set (l := (ft_to_lval a)).
            generalize l.
            clear.
            induction l;intros;[apply eq_refl| ].
            assert (IH := IHl (fun v H => Ht v (or_intror H))).
            destruct lift_filter eqn:D1;[|discriminate].
            simpl in IH;unfold lift in IH.
            destruct lift_map eqn:D2;[|discriminate].
            simpl in IH.
            simpl.
            assert (Ha := Ht _ (or_introl eq_refl)).
            destruct a;try discriminate.
            destruct o.
            -- simpl;rewrite D1;simpl.
               unfold lift.
               rewrite D2.
               simpl.
               unfold lifted_fsum, lifted_fbag in *. simpl.
               rewrite lift_lift in *. rewrite lift_lift.
               simpl. unfold lift in *.
               revert IH. case_eq (lift_map (ondfloat (fun x : float => x)) l1); try discriminate.
               intros l2 Hl2 IH. now inversion IH.
            -- simpl;rewrite D1;simpl;rewrite D2.
               simpl;apply IH.
          * intros.
            unfold nraenv_eval.
            simpl interp_aggregate.
            simpl nraenv_core_eval.
            unfold olift.
            rewrite Heq;simpl.
            unfold bcount.
            rewrite map_length.
            apply eq_refl.
      }
      + simpl.
        change ((fun x => Some (dleft (dfloat float_zero)) =
  Some
    (dleft
       (dfloat
          match x with
          | 0 => float_zero
          | _ =>
              float_div
                (fold_right
                   (fun (x : NullValues.value) (acc : float) =>
                    match x with
                    | NullValues.Value_float (Some x0) => float_add x0 acc
                    | _ => acc
                    end) float_zero (ft_to_lval a))
                (float_of_int (Z.of_nat (Datatypes.length (ft_to_lval a))))
          end))) (Z.of_nat (Datatypes.length (ft_to_lval a)))).
        case_eq (Datatypes.length (ft_to_lval a)); auto.
        intros n Hn. simpl.
        replace (fold_right
                (fun (x : NullValues.value) (acc : float) =>
                 match x with
                 | NullValues.Value_float (Some x0) => float_add x0 acc
                 | _ => acc
                 end) float_zero (ft_to_lval a)) with float_zero;
          simpl.
        * destruct (float_of_int_pos (Pos.of_succ_nat n)) as [->|[m [e [-> H1]]]]; auto.
          rewrite <- (SF2Prim_Prim2SF (float_div float_zero (SF2Prim (S754_finite false m e)))).
          now rewrite div_spec, Prim2SF_SF2Prim.
        * revert Ht; clear.
          set (l := (ft_to_lval a)).
          generalize l.
          clear.
          induction l;intros;
            [apply eq_refl| ].
          cbn.
          assert (Ha := Ht _ (or_introl eq_refl)).
          destruct a;try discriminate.
          clear Ha.
          apply eq_sym.
          assert (IH := eq_sym (IHl (fun v H => Ht v (or_intror H)))).
          apply IH.
      + simpl.
        change ((fun x => Some (dleft (dfloat float_zero)) =
  Some
    (dleft
       (dfloat
          match x with
          | 0 => float_zero
          | _ =>
              float_div
                (fold_right
                   (fun (x : NullValues.value) (acc : float) =>
                    match x with
                    | NullValues.Value_float (Some x0) => float_add x0 acc
                    | _ => acc
                    end) float_zero (ft_to_lval a))
                (float_of_int (Z.of_nat (Datatypes.length (ft_to_lval a))))
          end))) (Z.of_nat (Datatypes.length (ft_to_lval a)))).
        case_eq (Datatypes.length (ft_to_lval a)); auto.
        intros n Hn. simpl.
        replace (fold_right
                (fun (x : NullValues.value) (acc : float) =>
                 match x with
                 | NullValues.Value_float (Some x0) => float_add x0 acc
                 | _ => acc
                 end) float_zero (ft_to_lval a)) with float_zero;
          simpl.
        * destruct (float_of_int_pos (Pos.of_succ_nat n)) as [->|[m [e [-> H1]]]]; auto.
          rewrite <- (SF2Prim_Prim2SF (float_div float_zero (SF2Prim (S754_finite false m e)))).
          now rewrite div_spec, Prim2SF_SF2Prim.
        * revert Ht; clear.
          set (l := (ft_to_lval a)).
          generalize l.
          clear.
          induction l;intros;
            [apply eq_refl| ].
          cbn.
          assert (Ha := Ht _ (or_introl eq_refl)).
          destruct a;try discriminate.
          clear Ha.
          apply eq_sym.
          assert (IH := eq_sym (IHl (fun v H => Ht v (or_intror H)))).
          apply IH.
      + simpl.
        change ((fun x => Some (dleft (dfloat float_zero)) =
  Some
    (dleft
       (dfloat
          match x with
          | 0 => float_zero
          | _ =>
              float_div
                (fold_right
                   (fun (x : NullValues.value) (acc : float) =>
                    match x with
                    | NullValues.Value_float (Some x0) => float_add x0 acc
                    | _ => acc
                    end) float_zero (ft_to_lval a))
                (float_of_int (Z.of_nat (Datatypes.length (ft_to_lval a))))
          end))) (Z.of_nat (Datatypes.length (ft_to_lval a)))).
        case_eq (Datatypes.length (ft_to_lval a)); auto.
        intros n Hn. simpl.
        replace (fold_right
                (fun (x : NullValues.value) (acc : float) =>
                 match x with
                 | NullValues.Value_float (Some x0) => float_add x0 acc
                 | _ => acc
                 end) float_zero (ft_to_lval a)) with float_zero;
          simpl.
        * destruct (float_of_int_pos (Pos.of_succ_nat n)) as [->|[m [e [-> H1]]]]; auto.
          rewrite <- (SF2Prim_Prim2SF (float_div float_zero (SF2Prim (S754_finite false m e)))).
          now rewrite div_spec, Prim2SF_SF2Prim.
        * revert Ht; clear.
          set (l := (ft_to_lval a)).
          generalize l.
          clear.
          induction l;intros;
            [apply eq_refl| ].
          cbn.
          assert (Ha := Ht _ (or_introl eq_refl)).
          destruct a;try discriminate.
          clear Ha.
          apply eq_sym.
          assert (IH := eq_sym (IHl (fun v H => Ht v (or_intror H)))).
          apply IH.
  Qed.  
  
  Lemma type_of_aggregate_eq :
    forall (s : Tuple.aggregate TRcd) (l : list TupleToData.value),
      Tuple.type_of_value _ (interp_aggregate _ s l) =
      type_of_aggregate s.
  Proof.
    intros [ag] l.
    repeat (destruct ag as [ |[[ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ]] ag];auto).
  Qed.      

  Variable faac : float_add_assoc_comm.
  
  Instance tnull_AN :
       AggregateToNRAEnv.AggregateToNRAEnv :=
    ATermToNRAEnv.AggregateToNRAEnv.mk_C
        tnull_TD type_of_aggregate 
      aggregate_to_nraenv  nraenv_eval_aggregate_id_eq (aggregate_to_nraenv_eq faac)
      (interp_aggregate_permut faac)
      type_of_aggregate_eq.

  Global Instance tnull_ATN : ATermToNRAEnv.ATermToNRAEnv.
  Qed.

End Sec.
