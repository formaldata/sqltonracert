(************************************************************************************)
(**                                                                                 *)
(**                          The SQLToNRACert Library                               *)
(**                                                                                 *)
(**                       LMF, CNRS & Université Paris-Saclay                       *)
(**                                                                                 *)
(**                        Copyright 2016-2022 : FormalData                         *)
(**                                                                                 *)
(**         Authors: Véronique Benzaken                                             *)
(**                  Évelyne Contejean                                              *)
(**                  Mohammed Hachmaoui                                             *)
(**                  Chantal Keller                                                 *)
(**                                                                                 *)
(************************************************************************************)

Set Implicit Arguments.

Require Export TupleToData EnvToNRAEnv.

Module SymbolToNRAEnv.

  Import TupleToData.
  
  Class SymbolToNRAEnv `{frt : foreign_runtime} `{TD : @TupleToData frt} : Type :=
    mk_C
      {
        type_of_symbol : symbol TRcd -> type TRcd;
        (* well_typed_symbol : symbol T -> list (type T) -> bool; *)
        symbol_to_nraenv : symbol TRcd -> list (type TRcd) -> list nraenv -> nraenv;
        symbol_to_nraenv_ignores_did :
          forall s  lnra lty,
            (forall n br i reg id1 id2, In n lnra ->
                                   nraenv_eval br i  n reg id1 =
                                   nraenv_eval br i  n reg id2) ->
            forall br i reg id1 id2,
              nraenv_eval br i  (symbol_to_nraenv s  lty lnra) reg id1 =
              nraenv_eval br i  (symbol_to_nraenv s  lty lnra) reg id2;
        symbol_to_nraenv_eq :
          forall X s (l:list X),
          forall f1 f2 f3 br i dreg,(forall (x: X) ,
                                   In x l -> forall did,
                                     nraenv_eval br i (f1 x) dreg did =
                                     Some
                                       (value_to_data  (f2 x))) ->
                               (forall ft, f3 ft = Tuple.type_of_value TRcd (f2 ft)) ->            
                               forall did ,
                                 nraenv_eval br i (symbol_to_nraenv s (map f3 l) (map f1 l)) dreg did =
                                 Some
                                   (value_to_data  (interp_symbol TRcd s (map f2 l)));
        (* type_of_dot_eq : *)
        (*   forall t a, *)
        (*     a inS labels t -> *)
        (*     type_of_value TRcd (dot t a) = *)
        (*     type_of_attribute TRcd a; *)
        type_of_symbol_eq :
          forall s l,
            type_of_value TRcd (interp_symbol TRcd s l) = type_of_symbol s;
      }.  
  
End SymbolToNRAEnv.

Module FTermToNRAEnv.

  Export SymbolToNRAEnv EnvToNRAEnv.
  
  Class FTermToNRAEnv
        `{frt : foreign_runtime}
        `{TD : @TupleToData frt}
        `{SN : @SymbolToNRAEnv frt TD}
        `{EN : @EnvToNRAEnv frt TD} : Type.

    Notation funterm := (@funterm TRcd).
    Notation symbol := (symbol TRcd).
    
    Section Sec.

      Context {frt : foreign_runtime}.
      Context {TD:TupleToData }.
      Context {SN:SymbolToNRAEnv}.
      Context {EN : EnvToNRAEnv.EnvToNRAEnv }.
      Context {TN:FTermToNRAEnv}.

      Section Translation.
        
        Definition type_of_funterm f :=
          match f with
          | F_Constant _ c => type_of_value TRcd c
          | F_Dot  a =>   type_of_attribute TRcd a
          | F_Expr _ f l => type_of_symbol f
          end.

        Fixpoint funterm_to_nraenv (ne:nenv)
                 (f : funterm) : nraenv :=
          match f with
          | F_Constant _ c =>
            NRAEnvConst (value_to_data c)
          | F_Dot  a =>  env_dot_to_nraenv ne a
          | F_Expr _ f l =>
            symbol_to_nraenv
              f
              (map type_of_funterm l)
              (map (funterm_to_nraenv ne) l)
          end.

        Global Instance funterm_eq_dec : EquivDec.EqDec funterm eq.
        Proof.
          red; unfold Equivalence.equiv,RelationClasses.complement.
          change (forall x y : funterm  , {x = y} + {x <> y}).
          intros x y.
          destruct (Oset.eq_bool (OFun TRcd) x y) eqn:D1.
          - rewrite Oset.eq_bool_true_iff in D1;left;apply D1.
          - right;intro N1.
            rewrite N1 in D1.
            rewrite Oset.eq_bool_refl in D1.
            discriminate.
        Defined.

        (* Fixpoint well_typed_ft  f := *)
        (*   match f with *)
        (*   | F_Constant _ c => true *)
        (*   | F_Dot  a =>   true *)
        (*   | F_Expr _ f l => *)
        (*     well_typed_symbol f (map type_of_funterm l) && forallb well_typed_ft l *)
        (*   end. *)

        
      End Translation.

      Section Lemmas.
        Lemma funterm_to_nraenv_equiv_env_eq :
          forall e1 e2,
            equiv_nenv e1 e2 ->
            forall ft ,
              funterm_to_nraenv  e1 ft  =
              funterm_to_nraenv  e2 ft.
        Proof.
          intros e1 e2 Heq.
          fix Thm 1.
          destruct ft as [ | |s ].
          - apply eq_refl.
          - apply (env_dot_to_nraenv_equiv_nenv_eq _ Heq).         
          - simpl.
            apply f_equal.
            induction l;[apply eq_refl| ].
            simpl.
            rewrite IHl.
            rewrite Thm.
            apply eq_refl.
        Qed.


        Lemma funterm_to_nraenv_ignores_did :
          forall ft  aenv,
          forall br i reg id1 id2,
            nraenv_eval br i  (funterm_to_nraenv aenv ft) reg id1 =
            nraenv_eval br i  (funterm_to_nraenv aenv ft) reg id2.
        Proof.
          intros ft.
          set (n := size_funterm TRcd ft).
          assert (Hn := le_n n);unfold n at 1 in Hn.
          clearbody n; revert ft Hn.
          induction n as [ | n];intros ft Hn aenv;
            [destruct ft;inversion Hn| intros br i reg id1 id2].
          destruct ft.
          - apply eq_refl.
          - simpl.
            apply (env_dot_to_nraenv_ignores_did i br aenv a  reg id1 id2).
          - simpl.
            apply symbol_to_nraenv_ignores_did.
            intros n1 br1 i1 reg1 id3 id4 H.
            rewrite in_map_iff in H.
            destruct H as [f1 [H1 H2]].
            subst.
            apply IHn.
            apply (size_funterm_F_Expr Hn _ H2).
        Qed.
        
        (* Lemma type_of_interp_dot_eq : *)
        (*   forall e a, *)
        (*     type_of_value TRcd (interp_dot TRcd e a) = *)
        (*     type_of_attribute TRcd a. *)
        (* Proof. *)
        (*   induction e as [|[[s1 g1] lt1] e IH];intros a. *)
        (*   - simpl. *)
        (*     apply type_of_default_value_eq. *)
        (*   - simpl. *)
        (*     destruct (quicksort OTuple lt1) eqn:D1; *)
        (*       [apply IH| ]. *)
        (*     destruct (a inS? labels t ) eqn:D2; *)
        (*       [ |apply IH]. *)
        (*     apply (type_of_dot_eq _ _ D2). *)
        (* Qed. *)

        Lemma type_of_funterm_eq :
          forall ft e,
            well_typed_e e = true ->
            type_of_value TRcd (interp_funterm TRcd e ft) =
            type_of_funterm ft.
        Proof.    
          intros [c|a|s l] e He.
          - apply eq_refl.
          - apply (type_of_interp_dot_eq _ He).
          - simpl.
            apply type_of_symbol_eq.
        Qed.
        
        (* Fixpoint well_typed_ft  f := *)
        (*   match f with *)
        (*   | F_Constant _ c => true *)
        (*   | F_Dot  a =>   true *)
        (*   | F_Expr _ f l => *)
        (*     well_typed_symbol f (map type_of_funterm l) && forallb well_typed_ft l *)
        (*   end. *)  
        
        Lemma funterm_to_nraenv_is_sound :
          forall ft br i e did   ,
            is_a_translatable_e e = true ->
            well_formed_e TRcd  e = true ->
            well_typed_e e = true ->
            well_formed_ft TRcd e ft = true ->
            (* well_typed_ft ft = true -> *)
            nraenv_eval br i (funterm_to_nraenv (map fst  e)  ft) (env_to_data   e) did = Some (value_to_data (interp_funterm TRcd e ft)).
        Proof.
          intros ft.
          set (n := size_funterm TRcd ft).
          assert (Hn := le_n n);unfold n at 1 in Hn.
          clearbody n; revert ft Hn.
          induction n as [ | n];intros ft Hn br;
            [destruct ft;inversion Hn| intros  i e did].
          destruct ft as [ | |s ];intros  Wt WS Te Wft.
          - unfold nraenv_eval.
            simpl.
            rewrite <- value_to_data_normalize.
            apply eq_refl.
          -  apply (env_dot_to_nraenv_is_sound    br i _ _ WS Wft did).
          - simpl.
            refine (symbol_to_nraenv_eq _ _  _ _ _ _   _ _ _ _ _);
              [|intros;rewrite (type_of_funterm_eq _ _ Te);apply eq_refl].        
            intros.
            refine (IHn _ (size_funterm_F_Expr Hn _ H) _  _ _ _ Wt WS Te _).
            unfold well_formed_ft in *.
            simpl in Wft.
            rewrite orb_true_iff in Wft.
            destruct Wft as [Wft|Wft].
            + rewrite Oset.mem_bool_true_iff in Wft.
              rewrite in_extract_funterms in Wft.
              apply (is_a_translatable_e_groups_are_attributes _ Wt) in Wft.
              destruct Wft;discriminate.
            + rewrite forallb_forall in Wft.
              apply (Wft _ H).
        Qed.
        
        Lemma eval_funterm_to_nraenv_equiv_env_eq :
          forall br i,
          forall e1 e2,
            well_formed_e TRcd e1 = true ->
            well_typed_e e1 = true ->
            equiv_env _ e1 e2 ->
            is_a_translatable_e  e1 = true ->          
            forall ft  did,
              well_formed_ft  _ e1 ft  = true ->              
              nraenv_eval br i (funterm_to_nraenv  (map fst e1) ft) (env_to_data e1)   did =
              nraenv_eval br i (funterm_to_nraenv  (map fst e1) ft) (env_to_data e2)  did.
        Proof.
          unfold nraenv_eval.
          intros br i e1 e2 We1 Te1 Heq T1.
          fix Thm 1.
          destruct ft as [ | |s ];intros  did Wft1 .
          - apply eq_refl.
          - simpl.
            rewrite  (env_dot_to_nraenv_equiv_nenv_eq a ( (equiv_env_equiv_nenv   Heq))) at 2.
            refine (eval_env_dot_to_nraenv_equiv_env_eq _ _ _ _ Heq We1  Wft1).
          - cbn.
            rewrite  (map_ext _ _ (funterm_to_nraenv_equiv_env_eq (equiv_env_equiv_nenv Heq))) at 2.
            refine (eq_trans
                      (symbol_to_nraenv_eq _ _  _ _ _ _    _ _ _ (fun ft  => eq_sym (type_of_funterm_eq ft e1 Te1)) _   )
                      _).
            + intros.
              apply (funterm_to_nraenv_is_sound _ _ _ _ _ T1 We1 Te1).
              unfold well_formed_ft in *.
              simpl in Wft1.
              rewrite orb_true_iff in Wft1.
              destruct Wft1 as [Wft|Wft].
              * rewrite Oset.mem_bool_true_iff in Wft.
                rewrite in_extract_funterms in Wft.
                apply (is_a_translatable_e_groups_are_attributes _ T1) in Wft.
                destruct Wft;discriminate.
              * rewrite forallb_forall in Wft.
                apply (Wft _ H).
            + apply eq_sym.
              rewrite (well_typed_e_eq Heq) in Te1.
              refine (eq_trans
                        (symbol_to_nraenv_eq _ _  _ _ _ _    _ _ _ (fun ft  => eq_sym (type_of_funterm_eq ft e2 Te1)) _   )
                        _).
              *  intros.
                 rewrite (is_a_translatable_e_eq (equiv_env_weak_equiv_env _ _ _ Heq)) in T1.
                 rewrite (well_formed_e_eq Heq) in We1.
                 apply (funterm_to_nraenv_is_sound _ _ _ _ _ T1 We1 Te1).
                 rewrite <- (well_formed_e_eq Heq) in We1.
                 rewrite <- (well_formed_ft_eq  _ _ _ _ (equiv_env_weak_equiv_env _ _ _ Heq)).
                 rewrite <- (is_a_translatable_e_eq (equiv_env_weak_equiv_env _ _ _ Heq)) in T1.
                 unfold well_formed_ft in *.
                 simpl in Wft1.
                 rewrite orb_true_iff in Wft1.
                 { destruct Wft1 as [Wft|Wft].
                   - rewrite Oset.mem_bool_true_iff in Wft.
                     rewrite in_extract_funterms in Wft.            
                     apply (is_a_translatable_e_groups_are_attributes _ T1) in Wft.
                     destruct Wft;discriminate.
                   - rewrite forallb_forall in Wft.
                     apply (Wft _ H).
                 }
              * refine (f_equal _ (f_equal _ (eq_sym (f_equal _ _)))).
                apply map_ext_in.
                intros.
                apply interp_funterm_eq.
                apply Heq.
        Qed.
        

      End Lemmas.

    End Sec.
    
End FTermToNRAEnv.
