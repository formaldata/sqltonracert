(************************************************************************************)
(**                                                                                 *)
(**                          The SQLToNRACert Library                               *)
(**                                                                                 *)
(**                       LMF, CNRS & Université Paris-Saclay                       *)
(**                                                                                 *)
(**                        Copyright 2016-2022 : FormalData                         *)
(**                                                                                 *)
(**         Authors: Véronique Benzaken                                             *)
(**                  Évelyne Contejean                                              *)
(**                  Mohammed Hachmaoui                                             *)
(**                  Chantal Keller                                                 *)
(**                                                                                 *)
(************************************************************************************)

Require Import ZArith.
Require Export TnullTD TnullEN.

Require Import FTermToNRAEnv.
From SQLFS Require Import TuplesImpl GenericInstance.

Section Sec.

  Import TupleToData TnullTD Tuple  Values.
  
  Definition type_of_symbol (f:Tuple.symbol TNull) :=
    match f with
    | Symbol _ "plus" => type_Z
    | Symbol _ "plus." => type_float
    | Symbol _ "mult" => type_Z
    | Symbol _ "mult." => type_float
    | Symbol _ "minus" => type_Z
    | Symbol _ "minus." => type_float
    | Symbol _ "opp"%string => type_Z
    | Symbol _ "opp."%string => type_float
    | CstVal _ v =>  NullValues.type_of_value  v
    | _ => type_Z
    end.
    
  Definition NRAEnvNullApp n1 n2 :=
    NRAEnvApp
      (NRAEnvEither
         n1
         (NRAEnvUnop OpRight  NRAEnvID))
      n2.

  Definition NRAEnvNullBinOp op n1 n2 :=
    let
      dsome_result :=
      (NRAEnvNullApp
         (NRAEnvUnop
            OpLeft
            (NRAEnvBinop
               op
               NRAEnvID (NRAEnvNullApp NRAEnvID  n1))) n2)
    in
    (NRAEnvNullApp (NRAEnvNullApp dsome_result  n2)  n1).
  
  (* TODO: factorize *)
  (* TODO: opp and opp. should be interpreted by a unary symbol instead of `0 - ...` *)
  Definition symbol_to_nraenv (f:Tuple.symbol TNull) lty lnra :=
    match f,lty,lnra with
    | CstVal _ v,nil,nil => NRAEnvConst (value_to_data v)
    | CstVal _ v,_,_ => NRAEnvConst (value_to_data (Tuple.default_value TNull (Tuple.type_of_value TNull v)))
    | Symbol _ "opp",type_Z::nil,n1::nil =>                                 
      let
        dsome_result :=
        (NRAEnvNullApp
           (NRAEnvUnop
              OpLeft
              (NRAEnvBinop
                 (OpNatBinary NatMinus)
                 NRAEnvID (NRAEnvNullApp NRAEnvID  n1))) (NRAEnvConst (dleft (dnat 0))))
      in
      (NRAEnvNullApp (NRAEnvNullApp dsome_result  (NRAEnvConst (dleft (dnat 0))))  n1)        
    | Symbol _ "opp.",type_float::nil,n1::nil =>
      let
        dsome_result :=
        (NRAEnvNullApp
           (NRAEnvUnop
              OpLeft
              (NRAEnvBinop
                 (OpFloatBinary FloatMinus)
                 NRAEnvID (NRAEnvNullApp NRAEnvID  n1))) (NRAEnvConst (dleft (dfloat float_zero))))
      in
      (NRAEnvNullApp (NRAEnvNullApp dsome_result (NRAEnvConst (dleft (dfloat float_zero)))) n1)
    | Symbol _ "opp.",_,_ => NRAEnvConst (value_to_data (Tuple.default_value TNull (type_float)))
    | Symbol _ "plus",type_Z::type_Z::nil,n1::n2::nil =>
      let
        dsome_result :=
        (NRAEnvNullApp
           (NRAEnvUnop
              OpLeft
              (NRAEnvBinop
                 (OpNatBinary NatPlus)
                 NRAEnvID (NRAEnvNullApp NRAEnvID  n2))) n1)
      in
      (NRAEnvNullApp (NRAEnvNullApp dsome_result  n2)  n1)
    | Symbol _ "plus.",type_float::type_float::nil,n1::n2::nil =>
      let
        dsome_result :=
        (NRAEnvNullApp
           (NRAEnvUnop
              OpLeft
              (NRAEnvBinop
                 (OpFloatBinary FloatPlus)
                 NRAEnvID (NRAEnvNullApp NRAEnvID  n2))) n1)
      in
      (NRAEnvNullApp (NRAEnvNullApp dsome_result  n2)  n1)
    | Symbol _ "plus.",_,_ => NRAEnvConst (value_to_data (Tuple.default_value TNull (type_float)))
    | Symbol _ "mult",type_Z::type_Z::nil,n1::n2::nil =>                                            
      let
        dsome_result :=
        (NRAEnvNullApp
           (NRAEnvUnop
              OpLeft
              (NRAEnvBinop
                 (OpNatBinary NatMult)
                 NRAEnvID (NRAEnvNullApp NRAEnvID  n2))) n1)
      in
      (NRAEnvNullApp (NRAEnvNullApp dsome_result  n2)  n1)        
    | Symbol _ "mult.",type_float::type_float::nil,n1::n2::nil =>
      let
        dsome_result :=
        (NRAEnvNullApp
           (NRAEnvUnop
              OpLeft
              (NRAEnvBinop
                 (OpFloatBinary FloatMult)
                 NRAEnvID (NRAEnvNullApp NRAEnvID  n2))) n1)
      in
      (NRAEnvNullApp (NRAEnvNullApp dsome_result  n2)  n1)
    | Symbol _ "mult.",_,_ => NRAEnvConst (value_to_data (Tuple.default_value TNull (type_float)))
    | Symbol _ "minus",type_Z::type_Z::nil,n1::n2::nil =>
      let
        dsome_result :=
        (NRAEnvNullApp
           (NRAEnvUnop
              OpLeft
              (NRAEnvBinop
                 (OpNatBinary NatMinus)
                 NRAEnvID (NRAEnvNullApp NRAEnvID  n2))) n1)
      in
      (NRAEnvNullApp (NRAEnvNullApp dsome_result  n2)  n1)              
    | Symbol _ "minus.",type_float::type_float::nil,n1::n2::nil =>
      let
        dsome_result :=
        (NRAEnvNullApp
           (NRAEnvUnop
              OpLeft
              (NRAEnvBinop
                 (OpFloatBinary FloatMinus)
                 NRAEnvID (NRAEnvNullApp NRAEnvID  n2))) n1)
      in
      (NRAEnvNullApp (NRAEnvNullApp dsome_result  n2)  n1)
    | Symbol _ "minus.",_,_ => NRAEnvConst (value_to_data (Tuple.default_value TNull (type_float)))
    | _,_,_ =>  NRAEnvConst (value_to_data (Tuple.default_value TNull (type_Z)))
    end.

  Local Notation type_of_funterm := (type_of_funterm type_of_symbol).

  Lemma symbol_to_nraenv_eq :
    forall X s (l:list X),
    forall f1 f2 f3 br i dreg,(forall x,
                             In x l -> forall did,
                               nraenv_eval br i (f1 x)  dreg did =
                               Some
                                 (value_to_data  (f2 x))) ->
                         (forall ft, f3 ft = Tuple.type_of_value TNull (f2 ft)) ->            
                         forall did,
                           nraenv_eval br i (symbol_to_nraenv s (map f3 l) (map f1 l)) dreg did =
                           Some
                             (value_to_data  (interp_symbol  _ s (map f2 l))).
  Proof.
    destruct s;intros.
    repeat (destruct s as [ |[[ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ]] s];try apply eq_refl).
    - (* opp *)
      destruct l as [|a1 l];[apply eq_refl| ].
      simpl.
      assert (H := H _ (or_introl eq_refl)).
      assert (Ha := H did).
      rewrite H0.
      destruct  (f2 a1);[apply eq_refl| |apply eq_refl|apply eq_refl].
      destruct l;[|destruct o;apply eq_refl].
      simpl in Ha.
      simpl.
      cbn.
      unfold olift.
      unfold nraenv_eval in Ha.
      simpl in Ha;simpl.
      rewrite Ha.
      destruct o;try apply eq_refl.
      assert (Ga := H  (dnat 0)).
      unfold nraenv_eval in Ga.
      simpl in Ga.
      rewrite Ga.
      simpl.
      apply eq_refl.
    - (* opp. *)
      destruct l as [|a1 l];[apply eq_refl| ].
      simpl.
      assert (H := H _ (or_introl eq_refl)).
      assert (Ha := H did).
      rewrite H0.
      destruct  (f2 a1);[apply eq_refl|apply eq_refl|apply eq_refl| ].
      destruct l;[|destruct o;apply eq_refl].
      simpl in Ha.
      simpl.
      cbn.
      unfold olift.
      unfold nraenv_eval in Ha.
      simpl in Ha;simpl.
      rewrite Ha.
      destruct o;try apply eq_refl.
      assert (Ga := H (dfloat float_zero)).
      unfold nraenv_eval in Ga.
      simpl in Ga.
      rewrite Ga.
      simpl.
      apply eq_refl.
    - (* mult *)
      destruct l as [|a1 l];[apply eq_refl| ].
      simpl.
      assert (Ha := H _ (or_introl eq_refl)).
      assert (Ha := Ha did).
      rewrite H0.
      destruct  (f2 a1) eqn:D1;[apply eq_refl| |apply eq_refl|apply eq_refl].
      { destruct l as [|a2 l];[destruct o;apply eq_refl| ].
      destruct l as [|a3 l];[| ].
      + simpl in *.
        rewrite H0.
        destruct  (f2 a2) eqn:D2;try (destruct o;apply eq_refl).
        destruct o;cbn.
        * unfold olift.
          unfold nraenv_eval in H.
          rewrite H;[rewrite D1|left;apply eq_refl].
          simpl.
          rewrite H;[rewrite D2|right;left;apply eq_refl].
          simpl.
          destruct o0;[|apply eq_refl].
          rewrite H;[rewrite D1|left;apply eq_refl].
          simpl.
          rewrite H;[rewrite D2|right;left;apply eq_refl].
          apply eq_refl.
        * unfold olift.
          unfold nraenv_eval in H.
          rewrite H;[rewrite D1;apply eq_refl|left;apply eq_refl].         
      + destruct o;cbn;
          destruct (f3 a2),(f2 a2),o;try apply eq_refl.
      }
    - (* mult. *)
      destruct l as [|a1 l];[apply eq_refl| ].
      simpl.
      assert (Ha := H _ (or_introl eq_refl)).
      assert (Ha := Ha did).
      rewrite H0.
      destruct  (f2 a1) eqn:D1;[apply eq_refl|apply eq_refl|apply eq_refl| ].
      { destruct l as [|a2 l];[destruct o;apply eq_refl| ].
      destruct l as [|a3 l];[| ].
      + simpl in *.
        rewrite H0.
        destruct  (f2 a2) eqn:D2;try (destruct o;apply eq_refl).
        destruct o;cbn.
        * unfold olift.
          unfold nraenv_eval in H.
          rewrite H;[rewrite D1|left;apply eq_refl].
          simpl.
          rewrite H;[rewrite D2|right;left;apply eq_refl].
          simpl.
          destruct o0;[|apply eq_refl].
          rewrite H;[rewrite D1|left;apply eq_refl].
          simpl.
          rewrite H;[rewrite D2|right;left;apply eq_refl].
          apply eq_refl.
        * unfold olift.
          unfold nraenv_eval in H.
          rewrite H;[rewrite D1;apply eq_refl|left;apply eq_refl].
      + destruct o;cbn;
          destruct (f3 a2),(f2 a2),o;try apply eq_refl.
      }
    - (* minus *)
      destruct l as [|a1 l];[apply eq_refl| ].
      simpl.
      assert (Ha := H _ (or_introl eq_refl)).
      assert (Ha := Ha did).
      rewrite H0.
      destruct  (f2 a1) eqn:D1;[apply eq_refl| |apply eq_refl|apply eq_refl].
      destruct l as [|a2 l];[destruct o;apply eq_refl| ].
      destruct l as [|a3 l];[| ].
      + simpl in *.
        rewrite H0.
        destruct  (f2 a2) eqn:D2;try (destruct o;apply eq_refl).
        destruct o;cbn.
        * unfold olift.
          unfold nraenv_eval in H.
          rewrite H;[rewrite D1|left;apply eq_refl].
          simpl.
          rewrite H;[rewrite D2|right;left;apply eq_refl].
          simpl.
          destruct o0;[|apply eq_refl].
          rewrite H;[rewrite D1|left;apply eq_refl].
          simpl.
          rewrite H;[rewrite D2|right;left;apply eq_refl].
          apply eq_refl.
        * unfold olift.
          unfold nraenv_eval in H.
          rewrite H;[rewrite D1;apply eq_refl|left;apply eq_refl].         
      + destruct o;cbn;
          destruct (f3 a2),(f2 a2),o;try apply eq_refl.
    - (* minus. *)
      destruct l as [|a1 l];[apply eq_refl| ].
      simpl.
      assert (Ha := H _ (or_introl eq_refl)).
      assert (Ha := Ha did).
      rewrite H0.
      destruct  (f2 a1) eqn:D1;[apply eq_refl|apply eq_refl|apply eq_refl| ].
      destruct l as [|a2 l];[destruct o;apply eq_refl| ].
      destruct l as [|a3 l];[| ].
      + simpl in *.
        rewrite H0.
        destruct  (f2 a2) eqn:D2;try (destruct o;apply eq_refl).
        destruct o;cbn.
        * unfold olift.
          unfold nraenv_eval in H.
          rewrite H;[rewrite D1|left;apply eq_refl].
          simpl.
          rewrite H;[rewrite D2|right;left;apply eq_refl].
          simpl.
          destruct o0;[|apply eq_refl].
          rewrite H;[rewrite D1|left;apply eq_refl].
          simpl.
          rewrite H;[rewrite D2|right;left;apply eq_refl].
          apply eq_refl.
        * unfold olift.
          unfold nraenv_eval in H.
          rewrite H;[rewrite D1;apply eq_refl|left;apply eq_refl].
      + destruct o;cbn;
          destruct (f3 a2),(f2 a2),o;try apply eq_refl.
    - (* plus *)
      destruct l as [|a1 l];[apply eq_refl| ].
      simpl.
      assert (Ha := H _ (or_introl eq_refl)).
      assert (Ha := Ha did).
      rewrite H0.
      destruct  (f2 a1) eqn:D1;[apply eq_refl| |apply eq_refl|apply eq_refl].
      destruct l as [|a2 l];[destruct o;apply eq_refl| ].
      destruct l as [|a3 l];[| ].
      + simpl in *.
        rewrite H0.
        destruct  (f2 a2) eqn:D2;try (destruct o;apply eq_refl).
        destruct o;cbn.
        * unfold olift.
          unfold nraenv_eval in H.
          rewrite H;[rewrite D1|left;apply eq_refl].
          simpl.
          rewrite H;[rewrite D2|right;left;apply eq_refl].
          simpl.
          destruct o0;[|apply eq_refl].
          rewrite H;[rewrite D1|left;apply eq_refl].
          simpl.
          rewrite H;[rewrite D2|right;left;apply eq_refl].
          apply eq_refl.
        * unfold olift.
          unfold nraenv_eval in H.
          rewrite H;[rewrite D1;apply eq_refl|left;apply eq_refl].         
      + destruct o;cbn;
          destruct (f3 a2),(f2 a2),o;try apply eq_refl.
    - (* plus. *)
      destruct l as [|a1 l];[apply eq_refl| ].
      simpl.
      assert (Ha := H _ (or_introl eq_refl)).
      assert (Ha := Ha did).
      rewrite H0.
      destruct  (f2 a1) eqn:D1;[apply eq_refl|apply eq_refl|apply eq_refl| ].
      destruct l as [|a2 l];[destruct o;apply eq_refl| ].
      destruct l as [|a3 l];[| ].
      + simpl in *.
        rewrite H0.
        destruct  (f2 a2) eqn:D2;try (destruct o;apply eq_refl).
        destruct o;cbn.
        * unfold olift.
          unfold nraenv_eval in H.
          rewrite H;[rewrite D1|left;apply eq_refl].
          simpl.
          rewrite H;[rewrite D2|right;left;apply eq_refl].
          simpl.
          destruct o0;[|apply eq_refl].
          rewrite H;[rewrite D1|left;apply eq_refl].
          simpl.
          rewrite H;[rewrite D2|right;left;apply eq_refl].
          apply eq_refl.
        * unfold olift.
          unfold nraenv_eval in H.
          rewrite H;[rewrite D1;apply eq_refl|left;apply eq_refl].         
      + destruct o;cbn;
          destruct (f3 a2),(f2 a2),o;try apply eq_refl.
    - (* Constants *)
      cbn.
      destruct l;cbn;rewrite <- value_to_data_normalize;apply eq_refl.
  Qed.

  
  Lemma nraenv_eval_symbol_id_eq :
    forall s  lnra lty,
      (forall n br i reg id1 id2, In n lnra ->
                             nraenv_eval br i  n reg id1 =
                             nraenv_eval br i  n reg id2) ->
      forall br i reg id1 id2,
        nraenv_eval br i  (symbol_to_nraenv s  lty lnra) reg id1 =
        nraenv_eval br i  (symbol_to_nraenv s  lty lnra) reg id2.
  Proof.
    destruct s;intros.
    repeat (destruct s as [ |[[ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ]] s];try apply eq_refl).
    - (* opp *)
      destruct lnra as [|n1 lnra],lty;try apply eq_refl.
      + destruct t;try apply eq_refl.
        destruct lty;apply eq_refl.
      + destruct lnra as [|n2 lnra],lty,t;try apply eq_refl.
        cbn.
        apply BasicFacts.match_option_eq.
        apply (H _ br i reg id1 id2 (or_introl eq_refl)).
    - (* opp. *)
      destruct lnra as [|n1 lnra],lty;try apply eq_refl.
      + destruct t;try apply eq_refl.
        destruct lty;apply eq_refl.
      + destruct lnra as [|n2 lnra],lty,t;try apply eq_refl.
        cbn.
        apply BasicFacts.match_option_eq.
        apply (H _ br i reg id1 id2 (or_introl eq_refl)).
    - (* mult *)
      destruct lnra as [|n1 lnra],lty;try apply eq_refl.
      + destruct t;try apply eq_refl.
        destruct lty;try apply eq_refl.
        destruct t;try apply eq_refl.
        destruct lty;apply eq_refl.
      + destruct lnra as [|n2 [ ]],lty as [|[] [ ]],t;try apply eq_refl.
        apply BasicFacts.match_option_eq.
        apply (H _ br i reg id1 id2 (or_introl eq_refl)).
    - (* mult. *)
      destruct lnra as [|n1 lnra],lty;try apply eq_refl.
      + destruct t;try apply eq_refl.
        destruct lty;try apply eq_refl.
        destruct t;try apply eq_refl.
        destruct lty;apply eq_refl.
      + destruct lnra as [|n2 [ ]],lty as [|[] [ ]],t;try apply eq_refl.
        apply BasicFacts.match_option_eq.
        apply (H _ br i reg id1 id2 (or_introl eq_refl)).
    - (* minus *)
      destruct lnra as [|n1 lnra],lty;try apply eq_refl.
      + destruct t;try apply eq_refl.
        destruct lty;try apply eq_refl.
        destruct t;try apply eq_refl.
        destruct lty;apply eq_refl.
      + destruct lnra as [|n2 [ ]],lty as [|[] [ ]],t;try apply eq_refl.
        apply BasicFacts.match_option_eq.
        apply (H _ br i reg id1 id2 (or_introl eq_refl)).
    - (* minus. *)
      destruct lnra as [|n1 lnra],lty;try apply eq_refl.
      + destruct t;try apply eq_refl.
        destruct lty;try apply eq_refl.
        destruct t;try apply eq_refl.
        destruct lty;apply eq_refl.
      + destruct lnra as [|n2 [ ]],lty as [|[] [ ]],t;try apply eq_refl.
        apply BasicFacts.match_option_eq.
        apply (H _ br i reg id1 id2 (or_introl eq_refl)).
    - (* plus *)
      destruct lnra as [|n1 lnra],lty;try apply eq_refl.
      + destruct t;try apply eq_refl.
        destruct lty;try apply eq_refl.
        destruct t;try apply eq_refl.
        destruct lty;apply eq_refl.
      + destruct lnra as [|n2 [ ]],lty as [|[] [ ]],t;try apply eq_refl.
        apply BasicFacts.match_option_eq.
        apply (H _ br i reg id1 id2 (or_introl eq_refl)).
    - (* plus. *)
      destruct lnra as [|n1 lnra],lty;try apply eq_refl.
      + destruct t;try apply eq_refl.
        destruct lty;try apply eq_refl.
        destruct t;try apply eq_refl.
        destruct lty;apply eq_refl.
      + destruct lnra as [|n2 [ ]],lty as [|[] [ ]],t;try apply eq_refl.
        apply BasicFacts.match_option_eq.
        apply (H _ br i reg id1 id2 (or_introl eq_refl)).
    -  destruct lnra as [|n1 lnra],lty;apply eq_refl.
  Qed.
  
  
  Lemma type_of_symbol_eq :
    forall (s : Tuple.symbol TRcd) (l : list TupleToData.value),
      Tuple.type_of_value _ (interp_symbol _ s l) =
      type_of_symbol s.
  Proof.
    intros  [ ] l.
    destruct s;[ apply eq_refl| ].
    -  destruct a;destruct b,b0,b1,b2,b3,b4,b5,b6,s;try apply eq_refl.
       + destruct a;destruct b,b0,b1,b2,b3,b4,b5,b6,s;try apply eq_refl.
         destruct a;destruct b,b0,b1,b2,b3,b4,b5,b6,s;try apply eq_refl.
         * (* opp *)
           destruct l;try destruct v;try apply eq_refl.
           simpl.
           destruct o;try apply eq_refl.
           simpl.
           destruct l;apply eq_refl.
         * (* opp. *)
           destruct a;destruct b,b0,b1,b2,b3,b4,b5,b6,s;try apply eq_refl.
           destruct l;try destruct v;try apply eq_refl.
           simpl.
           destruct o;try apply eq_refl.
           simpl.
           destruct l;apply eq_refl.
       + destruct a;destruct b,b0,b1,b2,b3,b4,b5,b6,s;try apply eq_refl.
         destruct a;destruct b,b0,b1,b2,b3,b4,b5,b6,s;try apply eq_refl.
         *  destruct a;destruct b,b0,b1,b2,b3,b4,b5,b6,s;try apply eq_refl.
            { (* mult *)
              destruct l;try destruct v;try apply eq_refl.
              destruct o;try apply eq_refl.
              simpl.
              destruct l;try destruct l;try apply eq_refl.
              -- destruct v,z,o;apply eq_refl.
              -- destruct v,z,v0,o;apply eq_refl.
            }
            { (* mult. *)
              destruct a;destruct b,b0,b1,b2,b3,b4,b5,b6,s;try apply eq_refl.
              destruct l;try destruct v;try apply eq_refl.
              destruct o;try apply eq_refl.
              simpl.
              destruct l;try destruct l;try apply eq_refl.
              -- destruct v,o;apply eq_refl.
              -- destruct v,v0,o;apply eq_refl.
            }
         *  destruct a;destruct b,b0,b1,b2,b3,b4,b5,b6,s;try apply eq_refl.
            destruct a;destruct b,b0,b1,b2,b3,b4,b5,b6,s;try apply eq_refl.
            destruct a;destruct b,b0,b1,b2,b3,b4,b5,b6,s;try apply eq_refl.
            { (* minus *)
              destruct l;try destruct l;try apply eq_refl.
              -- destruct v,o;try apply eq_refl.
              -- destruct v,v0,o,o0;try apply eq_refl.
                 destruct l,z,z0;try apply eq_refl.
            }
            { (* minus. *)
              destruct a;destruct b,b0,b1,b2,b3,b4,b5,b6,s;try apply eq_refl.
              destruct l;try destruct l;try apply eq_refl.
              -- destruct v,o;try apply eq_refl.
              -- destruct v,v0,o,o0;try apply eq_refl.
                 destruct l;try apply eq_refl.
            }
       + destruct a;destruct b,b0,b1,b2,b3,b4,b5,b6,s;try apply eq_refl.
         destruct a;destruct b,b0,b1,b2,b3,b4,b5,b6,s;try apply eq_refl.
         destruct a;destruct b,b0,b1,b2,b3,b4,b5,b6,s;try apply eq_refl.
         * (* plus *)
           destruct l;try destruct v;try apply eq_refl.
           destruct o;try apply eq_refl.
           simpl.
           destruct l;try destruct l;try apply eq_refl.
           -- destruct v,z,o;apply eq_refl.
           -- destruct v,z,v0,o;apply eq_refl.
         * (* plus. *)
           destruct a;destruct b,b0,b1,b2,b3,b4,b5,b6,s;try apply eq_refl.
           destruct l;try destruct v;try apply eq_refl.
           destruct o;try apply eq_refl.
           simpl.
           destruct l;try destruct l;try apply eq_refl.
           -- destruct v,o;apply eq_refl.
           -- destruct v,v0,o;apply eq_refl.
    - destruct l;[apply eq_refl| ].
      simpl.
      rewrite type_of_default_value_eq.
      apply eq_refl.
  Qed.  
  

  Global Instance tnull_SN :SymbolToNRAEnv.SymbolToNRAEnv :=
    FTermToNRAEnv.SymbolToNRAEnv.mk_C
      tnull_TD  type_of_symbol  symbol_to_nraenv
        nraenv_eval_symbol_id_eq symbol_to_nraenv_eq 
       type_of_symbol_eq.

  Global Instance tnull_FTN :
    FTermToNRAEnv.FTermToNRAEnv.
  Qed.

End Sec.
