(************************************************************************************)
(**                                                                                 *)
(**                          The SQLToNRACert Library                               *)
(**                                                                                 *)
(**                       LMF, CNRS & Université Paris-Saclay                       *)
(**                                                                                 *)
(**                        Copyright 2016-2022 : FormalData                         *)
(**                                                                                 *)
(**         Authors: Véronique Benzaken                                             *)
(**                  Évelyne Contejean                                              *)
(**                  Mohammed Hachmaoui                                             *)
(**                  Chantal Keller                                                 *)
(**                                                                                 *)
(************************************************************************************)

Set Implicit Arguments.

Require Import SetoidList.
Require Export FTermToNRAEnv.

Module AggregateToNRAEnv.

  Export FTermToNRAEnv.
  
  Class AggregateToNRAEnv
        `{frt : foreign_runtime}
        `{TD : @TupleToData frt}
    (* `{EN : @EnvToNRAEnv frt TD}  *) : Type :=
    mk_C
      {
        type_of_aggregate : aggregate TRcd -> type TRcd;
        aggregate_to_nraenv : aggregate TRcd -> (type TRcd) ->nraenv -> nraenv;
        aggregate_to_nraenv_ignores_did :
          forall s  n br i reg,
            (forall id1 id2,
                nraenv_eval br i  n reg id1 =
                nraenv_eval br i  n reg id2) ->
            forall id1 id2 t,
              nraenv_eval br i  (aggregate_to_nraenv s t n) reg id1 =
              nraenv_eval br i  (aggregate_to_nraenv s t  n) reg id2;
        aggregate_to_nraenv_eq :
          forall  ag (a:funterm ),
          forall ft_to_nraenv ft_to_lval type_of_ft br i dreg,
            (forall did, nraenv_eval br i (ft_to_nraenv a)  dreg did =
                    Some
                      (dcoll (map value_to_data  (ft_to_lval a)))) ->
            (forall v, In v (ft_to_lval a) -> type_of_ft a = Tuple.type_of_value TRcd v) ->            
            forall did,
              (* forall l, Permutation l (ft_to_lval a) -> *)
              nraenv_eval br i (aggregate_to_nraenv ag (type_of_ft a) (ft_to_nraenv a)) dreg did =
              Some
                (value_to_data  (interp_aggregate TRcd ag (ft_to_lval a)));
        interp_aggregate_permut :
          forall ag l1 l2 ,
            Permutation l1 l2 ->
            interp_aggregate TRcd ag l1 = interp_aggregate _ ag l2;        
        type_of_aggregate_eq :
          forall s l,
            type_of_value TRcd (interp_aggregate TRcd s l) = type_of_aggregate s;
      }.
  
End AggregateToNRAEnv.

Module ATermToNRAEnv.

  Export FTermToNRAEnv AggregateToNRAEnv .
  
  
  Class ATermToNRAEnv
        `{frt : foreign_runtime}
        `{TD : @TupleToData frt}
        `{SN : @SymbolToNRAEnv frt TD}                
        `{AN : @AggregateToNRAEnv frt TD}
        `{EN : @EnvToNRAEnv frt TD}
        `{FN : @FTermToNRAEnv frt TD SN EN}
    : Type.


    Notation aggregate := (aggregate TRcd).
    Notation aggterm := (@aggterm TRcd).
    
    Section Sec.

      Context {frt : foreign_runtime}.
      Context {TD:TupleToData}.
      Context {SN:SymbolToNRAEnv}.
      Context {AN:AggregateToNRAEnv}.
      Context {EN : EnvToNRAEnv}.
      Context {FTN:FTermToNRAEnv}.    
      Context {ATN:ATermToNRAEnv}.

      Section Translation.

        
        Definition translate_unfold_nslice s  nenv ft : nraenv :=
          let tail := NRAEnvUnop  (OpDot "tl") NRAEnvEnv in
          let head := NRAEnvUnop  (OpDot "slc") NRAEnvEnv in
          (NRAEnvMap
             (NRAEnvAppEnv (funterm_to_nraenv ((s,Group_Fine _)::nenv)  ft)
                           (NRAEnvBinop 
                              OpRecConcat
                              (NRAEnvUnop (OpRec "tl") tail)
                              (NRAEnvUnop
                                 (OpRec "slc")
                                 (NRAEnvUnop OpBag NRAEnvID))))
             head).          

        Open Scope string_scope.
        Fixpoint translate_eval_nenv
                 (nenv : list ((Fset.set A) * group_by))
                 ft (n:nraenv): (option (list (Fset.set A * group_by)) * nraenv) :=
          match nenv with
          | nil =>
            if is_built_upon_ag TRcd  nil ft
            then (Some nil,@NRAEnvAppEnv    frt (NRAEnvConst  (dcoll  nil))   (NRAEnvConst  (drec (("slc",dcoll nil)::("tl",drec nil)::nil))))
            else (None,NRAEnvAppEnv  (NRAEnvConst (dcoll nil))   (NRAEnvConst (drec (("slc",dcoll nil)::("tl",drec nil)::nil))))
          | slc1 :: nenv' =>
            match fst (translate_eval_nenv   nenv' ft n)  with
            | Some l =>
              (Some l,NRAEnvAppEnv (snd (translate_eval_nenv   nenv' ft n))   (NRAEnvUnop  (OpDot "tl") NRAEnvEnv) )
            | None =>
              if is_a_suitable_nenv     (fst slc1) nenv' ft
              then (Some nenv,n)
                     (* nenv like in find_eval_env  *)
                     (* change n to (snd (translate_eval_nenv   nenv' ft n)) ?? pbs in  equiv forall id *)
              else (None,(snd (translate_eval_nenv   nenv' ft n))  )
            end
          end.

        Global Instance aggterm_eq_dec : EquivDec.EqDec aggterm eq.
        Proof.
          red; unfold Equivalence.equiv,RelationClasses.complement.
          change (forall x y : aggterm  , {x = y} + {x <> y}).
          intros x y.
          destruct (Oset.eq_bool (OAggT TRcd) x y) eqn:D1.
          - rewrite Oset.eq_bool_true_iff in D1;left;apply D1.
          - right;intro N1.
            rewrite N1 in D1.
            rewrite Oset.eq_bool_refl in D1.
            discriminate.
        Defined.

        
        Definition funterm_lt_dec  a a' :
          {Oset.compare (OFun TRcd) a a' = Lt} + {~ Oset.compare (OFun TRcd)  a a' = Lt}.
        Proof.
          destruct (Oset.compare _ a a');
            try (right;discriminate).
          left;apply eq_refl.
        Defined.



        
        Definition aggterm_lt_dec  a a' :
          {Oset.compare (OAggT TRcd) a a' = Lt} + {~ Oset.compare (OAggT TRcd)  a a' = Lt}.
        Proof.
          destruct (Oset.compare _ a a');
            try (right;discriminate).
          left;apply eq_refl.
        Defined.
        
        Definition extract_grouping_criteria_ (g : list aggterm) : option (list attribute) :=
          lift_map (fun e => match e with  (A_Expr  (F_Dot  a))  => Some a | _ => None end) g.
        
        Definition extract_grouping_criteria g : option (list attribute) :=
          if @ListAdd.NoDup_dec  aggterm aggterm_eq_dec  g then
            (extract_grouping_criteria_ g)
          else None.
        

        Definition type_of_aggterm ag :=
          match ag with
          | A_Expr  ft => type_of_funterm ft
          | A_fun _ f l => type_of_symbol f
          | A_agg _ ag ft => type_of_aggregate ag
          end.
        
        Fixpoint aggterm_to_nraenv   (nenv : list ((Fset.set A) * group_by))   (a : aggterm) :  nraenv :=
          match a with
          | A_Expr  ft =>  funterm_to_nraenv nenv  ft
          | A_fun _ f l => (symbol_to_nraenv f) (map type_of_aggterm l) (map (aggterm_to_nraenv nenv) l)
          | A_agg _ ag ft => 
            aggregate_to_nraenv ag (type_of_funterm ft)
                                (if Fset.is_empty A (variables_ft TRcd ft) then 
                                   match nenv with
                                   | nil => 
                                     (NRAEnvAppEnv
                                        (NRAEnvConst (dcoll nil))
                                        (NRAEnvConst (drec (("slc",dcoll nil)::("tl",drec nil)::nil))))
                                   | (slc1 :: nenv') =>
                                     (translate_unfold_nslice (fst slc1)  nenv' ft)
                                   end
                                 else
                                   match  find_eval_nenv  nenv a  with
                                   | (None) | (Some nil) =>
                                              
                                              (NRAEnvAppEnv
                                                 (NRAEnvConst (dcoll nil))
                                                 (NRAEnvConst (   drec (("slc",dcoll nil)::("tl",drec nil)::nil))))
                                   | (Some (slc1 :: nenv')) =>
                                     snd
                                       (
                                         (translate_eval_nenv nenv a)
                                           ( (translate_unfold_nslice (fst slc1) nenv' ft)))
                                   end)  
          end.

        Fixpoint select_list_to_nraenv_  nenv   s : nraenv :=
          match s with
          | nil => NRAEnvConst (drec nil)
          | (a,e) :: s =>
            NRAEnvBinop
              OpRecConcat
              (NRAEnvUnop
                 (OpRec (attribute_to_string a))
                 (aggterm_to_nraenv   nenv   e))
              (select_list_to_nraenv_  nenv   s)
          end.
        
        Definition select_list_to_nraenv nenv (s : _select_list TRcd) : nraenv :=
          match s with
          | _Select_List _ s =>
            let s' := (map
                        (fun x =>
                           match x with
                           | Select_As _ e a => (a,e)
                           end)
                        s) in
            let l := Fset.elements
                      _ (Fset.mk_set A (map fst s') ) in
            let s' :=  (map
                         (fun x =>
                            match x with
                            | a => (a,match Oset.find OAtt a s' with
                                     | Some e => e
                                     | None =>
                                       A_Expr
                                         (F_Constant
                                            _ (default_value
                                                 _ (type_of_attribute _ a)))
                                     end)
                            end)
                         l) in
            select_list_to_nraenv_  nenv  s'
          end.

      End Translation.

      Section Lemmas.

        Section ExtractGroupingCrit.



          Lemma extract_grouping_criteria_some_split :
            forall g l,
              extract_grouping_criteria g = Some l ->
              (forall a : attribute, In (A_Expr (F_Dot  a)) g <-> In a l) /\
              NoDup g /\ NoDup l.
          Proof.
            intros g l H.
            unfold extract_grouping_criteria,extract_grouping_criteria_ in H.
            destruct (NoDup_dec g ) as [H1|N1];
              [|discriminate].
            refine (conj _ (conj H1 _ )).
            - intros a;split;intro Ha.
              + assert (A1 := lift_map_some_in_1 _ _ H _ Ha).
                destruct A1 as [x [A1 A2]].
                inversion A2;apply A1.
              + assert (A1 := lift_map_some_in_2 _ _ H _ Ha).
                destruct A1 as [x [A1 A2]].
                destruct x;try destruct f;try discriminate.
                inversion A2;subst.
                apply A1.
            - refine (NoDup_lift_map _ _ H1 H).
              intros a b Ha Hb Heq.
              destruct (lift_map_some_in_1 _ _ H _ Ha) as [e1 [A1 B1]].
              destruct (lift_map_some_in_1 _ _ H _ Hb) as [e2 [A2 B2]].
              destruct a,b;try discriminate.
              destruct f,f0;try discriminate.
              inversion Heq.
              apply eq_refl.
          Qed.
          
          Lemma extract_grouping_criteria_some_split_alt_2 :
            forall g l,
              extract_grouping_criteria g = Some l ->
              (forall ft, In ft g -> exists a, In a l /\ ft = (A_Expr (F_Dot a))) /\
              NoDup g /\ NoDup l.
          Proof.
            intros g l H.
            assert (H' := H);apply extract_grouping_criteria_some_split in H'.
            repeat split;try apply H'.
            intros ft Hft.
            clear H'.
            revert l H ft Hft.
            induction g;intros;[inversion Hft| ].
            unfold extract_grouping_criteria in H.
            destruct NoDup_dec;[|discriminate].
            simpl in H;destruct a;try destruct f;try discriminate.
            inversion n;subst;clear n.
            unfold extract_grouping_criteria_ in H.
            simpl in H;unfold lift in H.
            destruct lift_map eqn:D1;[|discriminate].
            inversion H;subst;clear H.
            assert (IH := IHg l0).
            unfold extract_grouping_criteria in IH.
            destruct NoDup_dec;[|destruct (n H3)].
            assert (IH := IH D1).
            destruct Hft.
            - exists a;split;[left;apply eq_refl|subst ft;apply eq_refl].
            - destruct (IH _ H) as [a3 [H4 ->]].
              exists a3;split;[right;apply H4|apply eq_refl].
          Qed.

          
          Lemma extract_grouping_criteria_some_split_sorted :
            forall g l,
              extract_grouping_criteria g = Some l ->
              (forall a : attribute,  In (A_Expr (F_Dot a)) (insertion_sort (aggterm_lt_dec ) g) <-> In a l) /\
              NoDup g /\ NoDup l.
          Proof.
            intros g l H.
            refine (conj _ (proj2 (extract_grouping_criteria_some_split _ H))).
            assert (A1 := proj1 (extract_grouping_criteria_some_split _ H)).
            intro a.
            rewrite <- A1.
            split.
            - apply in_insertion_sort.
            - apply  (@insertion_sort_in _ _ (aggterm_lt_dec  )).
              intros x y Hx Hy.
              rewrite Oset.compare_lt_gt in Hy.
              rewrite <- (Oset.compare_eq_iff (OAggT TRcd)).
              destruct Oset.compare;
                [apply eq_refl|destruct (Hx eq_refl)|destruct (Hy eq_refl)].
          Qed.

          Lemma extract_grouping_criteria_rproject_aggterm :
            forall  env g l lt l1 l2,
              (forall t,
                  Oeset.mem_bool OTuple t lt = true ->
                  Fset.mk_set A l subS labels t) ->
              Permutation (listT_to_listD lt) l1 ->
              extract_grouping_criteria  g = Some l ->
              lift_map
                (fun din : data => match din with
                                  | drec r => Some (drec (rproject r (map attribute_to_string l)))
                                  | _ => None
                                  end) l1 = Some l2 ->
              Permutation
                (listT_to_listD
                   (map (fun t:tuple => mk_tuple (Fset.mk_set A l) (fun a => interp_aggterm TRcd (env_t TRcd env t) (A_Expr (F_Dot   a)))) lt))  l2.
          Proof.
            intros  env.
            intros g l b1 l1 l2 Aux H1 H2 H3.
            refine (permutation_listT_lift_map_weak _ _ _ _ H3 H1).
            intros t d Ht Hd.
            inversion Hd.
            assert (A1 := extract_grouping_criteria_some_split _ H2).
            assert (A2 := map_inj_NoDup _ attribute_to_string_inj _
                                       ( (proj2 (proj2 A1)))).
            rewrite <- (NoDup_bdistinct   A2).
            rewrite (NoDup_bdistinct A2).
            apply f_equal.
            rewrite rpoject_mk_tuple_aggterm;[apply eq_refl| ].
            apply Aux;apply Ht.
          Qed.
          
          Lemma NoDup_partition_translation_inv_project_rec_sort_aggterm :
            forall g l env lt,
              extract_grouping_criteria g = Some l ->
              NoDup (map
                       (fun p : _ =>
                          match snd p with
                          | nil => dunit
                          | t1 :: _ => drec (rec_sort (map (fun a : attribute => (attribute_to_string a, value_to_data (interp_aggterm TRcd (env_t   TRcd env t1) (A_Expr (F_Dot   a))))) l))
                          end)    (Partition.partition (mk_olists (OVal TRcd))
                                                       (fun t : tuple => map (fun f0 : aggterm => interp_aggterm TRcd (env_t TRcd env t) f0) g) lt)).
          Proof.
            intros g l env lt Hg.
            assert (A1 := Partition.partition_all_diff_values(mk_olists (OVal TRcd))
                                                            (fun t : tuple => map (fun f0 : aggterm => interp_aggterm TRcd (env_t TRcd env t) f0) g) lt).
            assert (A2 := Partition.in_partition_diff_nil (mk_olists (OVal TRcd))
                                                         (fun t : tuple => map (fun f0 : aggterm => interp_aggterm TRcd (env_t TRcd env t) f0) g) lt).
            assert (A3 := Partition.partition_homogeneous_values (mk_olists (OVal TRcd))
                                                                (fun t : tuple => map (fun f0 : aggterm => interp_aggterm TRcd (env_t TRcd env t) f0) g) lt).
            rewrite all_diff_NoDup_same in A1.
            set (llt :=    (Partition.partition (mk_olists (OVal TRcd))
                                               (fun t : tuple => map (fun f0 : aggterm => interp_aggterm TRcd (env_t TRcd env t) f0) g) lt)) in *.
            revert dependent llt.
            intros llt A1 A2 A3.
            induction llt.
            - constructor.
            - rewrite map_cons.
              inversion A1;subst.
              simpl in A3,A2.
              assert (A5 := fun v k H => A3 v k (or_intror H)).
              assert (A4 := fun v k H => A2 v k (or_intror H)).
              assert (IH := IHllt H2 A4 A5).
              constructor;[|apply IH].
              intros N1.
              apply H1.
              destruct a.
              simpl in N1.
              destruct l1;[destruct (A2 _ _ (or_introl eq_refl) eq_refl)| ].
              assert (N3 := N1).
              rewrite in_map_iff in N1.
              destruct N1 as [[l2 l3] [N1 N2]].
              rewrite in_map_iff.
              destruct l3;[destruct (A2 _ _ (or_intror N2) eq_refl)| ].
              exists  (l2, t0 :: l3);split;[|apply N2].
              simpl in N1.
              rewrite <- (A5  _ _ N2 t0 (or_introl eq_refl)).
              rewrite <- (A3  _ _ (or_introl eq_refl) t  (or_introl eq_refl)).
              inversion N1.
              rewrite <- rec_sort_perm_eq_iff in H0.
              apply map_ext_in.
              intros a Ha.
              destruct (proj1 (extract_grouping_criteria_some_split_alt_2 g Hg) _ Ha) as [a1 [G1 ->]].
              assert (G2 := Permutation_in _ H0 (in_map _ _ _ G1)).
              simpl in G2.
              rewrite in_map_iff in G2.
              destruct G2 as [a3 [G2 G4]].
              inversion G2.
              apply value_to_data_inj in H4.
              apply attribute_to_string_inj in H3.
              subst.
              apply (eq_sym H4).
              unfold domain;rewrite map_map.
              apply NoDup_map_attribute_to_string_iff.
              apply extract_grouping_criteria_some_split in Hg;apply Hg.
              unfold domain;rewrite map_map.
              apply NoDup_map_attribute_to_string_iff.
              apply extract_grouping_criteria_some_split in Hg;apply Hg.
          Qed.
          
          Lemma Permutation_bdistinct_flat_map_partition_projection_aggterm :
            forall env lt g la,
              extract_grouping_criteria g = Some la ->
              Permutation
                (bdistinct
                   (listT_to_listD
                      (map (fun t : tuple => mk_tuple (Fset.mk_set A la) (fun a  => interp_aggterm TRcd (env_t TRcd env t) (A_Expr  (F_Dot  a))))
                           lt)))
                (bdistinct
                   (map
                      (fun r => drec (tuple_as_dpairs (mk_tuple (Fset.mk_set A la) (fun a : attribute => interp_aggterm TRcd (env_t TRcd env r) (A_Expr (F_Dot a))))))
                      (flat_map (fun lt => snd lt) (Partition.partition (mk_olists (OVal TRcd))
                                                                       (fun t : tuple => map (fun f0 : aggterm => interp_aggterm TRcd (env_t TRcd env t) f0) g)
                                                                       lt)))).
          Proof.
            intros env lt g la Hg.
            refine (NoDup_Permutation (bdistinct_NoDup _) (bdistinct_NoDup _) _  ).
            intro d;split;intro Hd.
            - apply In_bdistinct in Hd.
              apply bdistinct_same_elemts.
              unfold listT_to_listD in Hd.
              rewrite map_map in Hd.
              rewrite in_map_iff in Hd.
              destruct Hd as [t [<- Hd]].
              rewrite in_map_iff.
              exists  t;split.
              apply eq_refl.
              refine  (Permutation_in _ _ Hd).
              rewrite <- _permut_eq_Permutation.
              apply (Partition.partition_permut _ _ _ ).
            - apply In_bdistinct in Hd.
              apply bdistinct_same_elemts.
              unfold listT_to_listD in Hd.
              rewrite in_map_iff in Hd.
              destruct Hd as [t [<- Hd]].
              apply In_tuple_as_pairs_eq_listT.
              rewrite in_map_iff.
              exists t;split;[apply eq_refl| ].
              refine (Permutation_in _ (Permutation_sym _) Hd).
              rewrite <- _permut_eq_Permutation.
              apply (Partition.partition_permut _ _ _ ).
          Qed.
          
          Lemma list_map_aggterm_tuple_as_dpairs_eq_rec_sort :
            forall env g l t,
              extract_grouping_criteria g = Some l ->
              tuple_as_dpairs
                (mk_tuple (Fset.mk_set A l)
                          (fun a : attribute =>
                             interp_aggterm TRcd (env_t _ env t) (A_Expr (F_Dot a)))) =
              rec_sort
                (map
                   (fun a : attribute =>
                      (attribute_to_string a,
                       value_to_data
                         (interp_aggterm _  (env_t TRcd env t) (A_Expr (F_Dot a))))) l).
          Proof.
            intros env g l t H.
            apply drec_sorted_perm_eq.
            - rewrite is_list_sorted_Sorted_iff.
              apply Sorted_ODT_lt_domain_tuple_as_dpairs.
            - apply drec_sort_sorted.
            - apply NoDup_lookup_equiv_Permutation.
              + apply Sorted_NoDup.
                apply Sorted_ODT_lt_domain_tuple_as_dpairs.
              + apply Sorted_NoDup.
                rewrite <- (is_list_sorted_Sorted_iff ODT_lt_dec).
                apply drec_sort_sorted.
              + unfold lookup_equiv.
                apply list_map_aggterm_tuple_as_dpairs_lookup_rec_sort.
                apply extract_grouping_criteria_some_split in H;apply H.
          Qed.

          Lemma Permutation_bdistinct_partition_projection_aggterm :
            forall  env g l lt  ,
              extract_grouping_criteria  g = Some l ->
              Permutation
                (map
                   (fun p : _ =>
                      match  p with
                      | nil => dunit
                      | t1 :: _ => drec (rec_sort (map (fun a : attribute => (attribute_to_string a, value_to_data (interp_aggterm TRcd (env_t _ env t1) (A_Expr (F_Dot   a))))) l))
                      end) (map snd
                                (Partition.partition (mk_olists (OVal TRcd))
                                                     (fun t : tuple => map (fun f0 : aggterm => interp_aggterm TRcd (env_t TRcd env t) f0) g) lt))
                )
                (bdistinct
                   (listT_to_listD
                      (map
                         (fun t:tuple => mk_tuple (Fset.mk_set A l) (fun a => interp_aggterm TRcd (env_t _ env t) (A_Expr (F_Dot  a))))
                         lt))).
          Proof.
            intros env g l lt H1.
            refine (Permutation_trans _ (Permutation_sym (Permutation_bdistinct_flat_map_partition_projection_aggterm  _ _ _ H1 ))).
            refine (NoDup_Permutation  _ (bdistinct_NoDup _) _);swap 1 2.
            intros x;split;intros Hx.
            +  rewrite in_map_iff in Hx.
               destruct Hx as [x1 [<- Hx1]].
               apply bdistinct_same_elemts.
               rewrite in_map_iff.
               destruct x1;[destruct (Partition.in_map_snd_partition_diff_nil _ _ _ Hx1 eq_refl)| ].
               exists t;split.
               * rewrite (list_map_aggterm_tuple_as_dpairs_eq_rec_sort _ g _ H1).
                 apply eq_refl.
               * rewrite in_flat_map.
                 rewrite in_map_iff in Hx1.
                 destruct Hx1 as [ltv [A1 A2]].
                 exists ltv;split;[apply A2|rewrite A1;left;apply eq_refl].
            + apply In_bdistinct in Hx.
              rewrite in_map_iff in Hx.
              destruct Hx as [x1 [<- Hx1]].
              rewrite in_flat_map in Hx1.
              destruct Hx1 as [x2 [Hx2 Hx1]].
              rewrite in_map_iff.
              destruct x2.
              exists l1.
              destruct l1;[destruct (Partition.in_partition_diff_nil _ _ _ _ Hx2 eq_refl)| ].
              split;swap 1 2.
              * rewrite in_map_iff.
                exists (l0,t::l1);split;[apply eq_refl| apply Hx2].
              * rewrite <- (list_map_aggterm_tuple_as_dpairs_eq_rec_sort  env g t H1).
                apply f_equal.
                apply tuple_as_dpairs_eq.
                apply mk_tuple_eq_2.
                intros a Ha.
                destruct Hx1.
                -- subst;apply interp_funterm_eq.
                   apply env_t_eq_2;apply Oeset.compare_eq_refl.
                -- assert (G1 := Partition.partition_homogeneous_values   _ _ _ l0 _ Hx2 _ (or_introl eq_refl)).
                   assert (G2 := Partition.partition_homogeneous_values   _ _ _ l0 _ Hx2 _ (or_intror H)).
                   simpl in G1,G2.
                   rewrite <- G1 in G2.
                   rewrite <- ListFacts.map_eq in G2.
                   apply eq_sym.
                   apply G2.
                   apply extract_grouping_criteria_some_split in H1.
                   rewrite (proj1 H1).
                   rewrite Fset.mem_mk_set,Oset.mem_bool_true_iff in Ha.
                   apply Ha.
            + rewrite map_map.
              refine (NoDup_partition_translation_inv_project_rec_sort_aggterm _ _ _  H1 ) .
          Qed.


          
          Lemma extract_grouping_criteria_env_g_is_translatable :
            forall e s lt g l,
              extract_grouping_criteria g = Some l ->
              is_a_translatable_e e = true ->
              is_a_translatable_e ((s, Group_By TRcd g, lt) :: e) = true.
          Proof.
            unfold is_a_translatable_e.
            intros e s lt g l Hg.
            do 2 rewrite forallb_forall.
            intros H x Hx.
            simpl in Hx.
            apply in_app_or in Hx.
            destruct Hx as [Hx|Hx];[|apply (H _ Hx)].
            destruct (proj1 (extract_grouping_criteria_some_split_alt_2 _ Hg) _ Hx)
              as [a [_ Hx1]].
            subst ;apply eq_refl.
          Qed.
        End ExtractGroupingCrit.


        Section ATermToNRAEnv.


          Lemma translate_unfold_nslice_cons :
            forall br i e ft s1 g1 lt1  did ,
              well_typed_e ((s1,g1,lt1)::e)  = true ->
              (* well_typed_ft ft = true -> *)
              well_formed_e _  ((s1,g1,lt1)::e) = true ->
              is_a_translatable_e  ((s1,g1,lt1)::e) = true ->          
              (forall t, In t lt1 -> well_formed_ft TRcd  ((s1,Group_Fine _,t::nil)::e) ft = true) ->
              nraenv_core_eval br  i (nraenv_to_nraenv_core (translate_unfold_nslice s1 (map fst e) ft)) (env_to_data    ((s1, g1, lt1) :: e)) did =
              Some (
                  dcoll
                    (map value_to_data
                         (map
                            (fun e : env   =>
                               interp_funterm  TRcd e ft)
                            (map (fun slc : nslice * list tuple => slc :: e) (map (fun t : tuple  => (s1, Group_Fine _, t :: nil)) lt1))))).
          Proof.
            intros br i env ft s1 g1 lt1 did  Typ WS Te Wft.
            unfold translate_unfold_nslice,lift.
            simpl;unfold lift.        
            rewrite
              (@BasicFacts.match_option_eq
                 _ _ _
                 (Some
                    (map
                       value_to_data
                       (map (fun e : _  => interp_funterm _ e ft)
                            (map (fun slc : nslice * list tuple => slc :: env)
                                 (map (fun t : tuple => (s1, Group_Fine _, t :: nil)) lt1))))));
              [apply eq_refl| ].
            repeat rewrite map_map.
            rewrite <- lift_map_id;simpl.
            unfold listT_to_listD;simpl.
            rewrite 2 lift_map_map_fusion.
            apply lift_map_ext.
            intros t Ht.
            assert (Aux : well_formed_e  _ ((s1, Group_Fine _, t::nil) :: env) = true)
              by apply (well_formed_e_cons_tuple _ _ _ _ WS _ Ht).
            assert (Aux1 : is_a_translatable_e ((s1, Group_Fine _, t :: nil) :: env) =true)
              by apply (is_a_translatable_e_cons_tuple _ _ _ _ Te _ Ht).
            refine (funterm_to_nraenv_is_sound  ft br i   _ _  Aux1  Aux  _  _).
            - simpl.
              apply well_typed_e_cons in Typ.
              rewrite (proj1 Typ),andb_true_r.
              apply forallb_forall.
              intros.
              apply forallb_forall.
              intros.
              simpl in H.
              destruct H;[|destruct H].
              subst.
              assert (T1 := proj2 Typ ).
              unfold well_typed_slice in T1.
              unfold well_typed_list_tuple in T1.
              rewrite forallb_forall in T1.
              assert (T1 := T1 _ Ht).
              rewrite forallb_forall in T1.
              assert (T1 := T1 _ H0).
              apply T1.
            - apply (Wft _ Ht).
          Qed.

          
          Open Scope string_scope.
          
          Lemma fst_translate_eval_nenv :
            forall n aenv f,
              fst (translate_eval_nenv aenv f n) =
              find_eval_nenv aenv f.
          Proof.
            intro n.
            induction aenv;intros f;cbn.
            - destruct is_built_upon_ag;apply eq_refl.
            - destruct (translate_eval_nenv aenv f  ) as (env',n') eqn:D1.
              assert (A1 := IHaenv f).
              rewrite D1 in A1;simpl in A1.
              rewrite A1.
              clear.
              destruct a.
              destruct find_eval_nenv ;[apply eq_refl| ].
              destruct is_a_suitable_nenv;apply eq_refl.
          Qed.

          Lemma snd_translate_eval_nenv_eq :
            forall e1 e2 f,
              equiv_nenv e1 e2 ->
              forall n,  snd (translate_eval_nenv e1 f n)  = snd (translate_eval_nenv e2 f n).
          Proof.
            intros e1 e2 f.
            induction 1 as [|s1 s2 e1 e2 H1 Heq IH];intros n;cbn;
              [apply eq_refl| ].
            - do 2 rewrite fst_translate_eval_nenv.
              assert (A1 := find_eval_nenv_eq f  Heq).
              simpl  in *.
              destruct (find_eval_nenv  e1 f) eqn:B1;
                destruct (find_eval_nenv  e2  f) eqn:B2.
              + rewrite IH;apply eq_refl.
              + destruct A1.
              + destruct A1.
              + rewrite (is_a_suitable_nenv_eq _ _ _ (proj1 H1) Heq).
                destruct is_a_suitable_nenv;[apply eq_refl|apply IH].
          Qed.
          
          
          (* Fixpoint well_typed_ag  ag := *)
          (*   match ag with *)
          (*   | A_Expr  ft => well_typed_ft ft *)
          (*   | A_fun _ f l =>     well_typed_symbol f (map type_of_aggterm l) && forallb well_typed_ag l *)
          (*   | A_agg _ op ft => well_typed_aggregate op (type_of_funterm ft) && well_typed_ft ft *)
          (*   end. *)

          Close Scope string_scope.
          
          Lemma aggterm_to_nraenv_equiv_env_eq :
            forall e1 e2,
              equiv_nenv  e1 e2 ->
              forall ag ,
                aggterm_to_nraenv  e1 ag  =
                aggterm_to_nraenv  e2 ag .
          Proof.
            intros e1 e2 Heq ag.
            set (n := size_aggterm _ ag).
            assert (Hn: size_aggterm _ ag <= n) by (apply le_n).
            revert dependent n.
            intros n Hn;revert n ag Hn.
            induction n as [|n].
            - intros;destruct ag;inversion Hn.
            - destruct ag as [ |u ft | ];intros;swap 2 3.
              + apply (funterm_to_nraenv_equiv_env_eq Heq).
              + simpl;apply f_equal.
                apply map_ext_in.
                intros a Ha.
                apply (IHn _ (size_aggterm_F_Expr  Hn _ Ha)).
              + cbn.
                destruct Fset.is_empty eqn:D2.
                * destruct e1,e2;
                    [apply eq_refl|inversion Heq|inversion Heq| ].
                  unfold translate_unfold_nslice,lift;simpl.
                  assert (G1 : equiv_nenv ((fst p, Group_Fine _) :: e1) ((fst p0, Group_Fine _) :: e2)).
                  { inversion Heq;subst;constructor;
                      [|apply H4].
                    repeat split;apply H2.
                  }
                  rewrite  (funterm_to_nraenv_equiv_env_eq  G1).
                  destruct funterm_to_nraenv;try apply eq_refl.
                * assert (A1 := find_eval_nenv_eq  (A_agg _ u ft)   Heq).
                  destruct
                    (find_eval_nenv  e1 (A_agg _ u ft)) eqn:B1,
                                                            (find_eval_nenv  e2 (A_agg _ u ft)) eqn:B2;
                    [|destruct A1 |destruct A1 |apply eq_refl].
                  inversion A1;[apply eq_refl|subst].
                  unfold translate_unfold_nslice,lift.
                  simpl.
                  assert (A2  : equiv_nenv ((fst x, Group_Fine _) :: l1)  ((fst y, Group_Fine _) :: l') ).
                  {   constructor;[|apply H0].
                      destruct x,y.                  
                      repeat split;simpl;apply H.
                  }
                  rewrite (funterm_to_nraenv_equiv_env_eq A2).
                  apply f_equal.
                  refine (snd_translate_eval_nenv_eq _ Heq _).
          Qed.
          
          Lemma aggterm_to_nraenv_ignores_did  :
            forall a  aenv,
            forall  br i  reg id1 id2,
              nraenv_eval br  i  (aggterm_to_nraenv aenv a) reg id1 =
              nraenv_eval br  i  (aggterm_to_nraenv aenv a)  reg id2.
          Proof.
            intros  a.
            set (n := size_aggterm _ a).
            assert (Hn := le_n n);unfold n at 1 in Hn.
            clearbody n; revert a Hn.
            induction n as [ | n];intros a Hn  aenv ;
              [destruct a;inversion Hn| intros br i reg id1 id2].
            destruct a.
            - destruct aenv as [|[s1 g1] aenv].
              +  apply  (funterm_to_nraenv_ignores_did  f nil br i   reg id1 id2).
              + unfold translate_unfold_nslice,lift.
                apply (funterm_to_nraenv_ignores_did _ _  _ _  reg id1 id2).
            - simpl.
              apply aggregate_to_nraenv_ignores_did;intros.      
              destruct (Fset.is_empty A (variables_ft _ f)) eqn:D0.
              + destruct aenv as [| [s1 g1]  aenv];apply eq_refl.
              + destruct find_eval_nenv as [[| [s1 g1]  env]| ] eqn:D2;
                  [apply eq_refl| |apply eq_refl].
                rewrite <- (fst_translate_eval_nenv (translate_unfold_nslice (fst (s1, g1)) env f)) in D2.
                unfold translate_unfold_nslice.
                destruct ((translate_eval_nenv aenv _ _)) as [etr ntr] eqn:D4.
                simpl in D2;subst.
                simpl in *.
                generalize dependent reg.
                generalize dependent ntr;generalize dependent aenv.
                fix thm 1.
                destruct aenv.
                * cbn in *.
                  destruct (is_built_upon_ft  _ nil f);discriminate.
                * intros ntr H.
                  cbn in H.
                  destruct ( translate_eval_nenv aenv (A_agg _ a f) ) eqn:D1.
                  destruct o.
                  -- inversion H.
                     apply eq_sym in H2.
                     subst;clear H.
                     cbn.
                     unfold olift.
                     destruct reg;try apply eq_refl.
                     destruct (edot l "tl") eqn:D2;[|apply eq_refl].
                     eapply thm.
                     apply D1.
                  -- simpl in H.
                     destruct orb eqn:D3;[|discriminate].
                     inversion H.
                     intros;apply eq_refl.
            - cbn.
              simpl.
              refine (symbol_to_nraenv_ignores_did _ _ _ _ _ _ _ _ _).
              intros.
              rewrite in_map_iff in H.
              destruct H as [x [H1 H2]].
              subst.
              apply IHn.
              apply (size_aggterm_F_Expr Hn _ H2).
          Qed.
          
          Lemma  nraenv_eval_snd_translate_eval_nenv_eq :
            forall br i did u ft nft,
            forall env : list (env_slice _),
              is_a_translatable_e env = true ->
              well_formed_e _ env = true ->
              forall (l : list (env_slice _)) s g lt,
                find_eval_env _ env (A_agg _ u ft) = Some ((s, g, lt) :: l) ->
                fst (translate_eval_nenv (map fst env) (A_agg _ u ft) nft) = Some (map fst ((s,g,lt)::l)) ->
                nraenv_eval br i (snd (translate_eval_nenv (map fst env) (A_agg _ u ft) nft))
                            (env_to_data env) did =
                nraenv_eval br i nft (env_to_data ((s,g,lt)::l)) did.
          Proof.
            intros br i did u ft nft.
            fix thm 1.
            destruct env as [|[[s3 g3]lt3] env];intros Wt WS l s g lt H1 H2.
            - cbn in H2;destruct is_built_upon_ft;discriminate.
            - simpl in *.
              assert ( A1:= fst_translate_eval_nenv nft (map fst env)  (A_agg _ u ft)).
              rewrite  find_eval_env_nenv_eq in A1.
              simpl in A1;rewrite A1 in *.
              destruct find_eval_env eqn:D1.
              + inversion H1;subst.
                refine (thm _  (is_a_translatable_e_cons _ _ Wt) _ _ _ _ _ D1  A1).
                rewrite andb_true_iff in WS.
                apply WS.
              + simpl in *.
                rewrite is_a_suitable_env_nenv_eq in *.
                destruct is_a_suitable_env eqn:D3;[|discriminate].
                cbn in *.
                inversion H1.
                apply eq_refl.
          Qed.

          Lemma find_eval_env_well_formed_ft_Fine_tuple :
            forall env (u : aggregate ) (ft : funterm) (l : list (env_slice _)) (s2 : Fset.set A) (g2 : group_by) (lt2 : list tuple),
              is_a_translatable_e env = true ->
              find_eval_env _ env (A_agg _ u ft) = Some ((s2, g2, lt2) :: l) ->
              forall t : tuple, In t lt2 -> well_formed_ft _ ((s2, Group_Fine _, t :: nil) :: l) ft = true.
          Proof.
            unfold well_formed_ft.
            induction env as [|[[s3 g3] lt3] env IH];intros u ft l s2 g2 lt2 Wt D6 t H.
            - simpl in D6.
              destruct (is_built_upon_ft _ nil _) eqn:D1;discriminate.
            - simpl in D6.
              destruct find_eval_env eqn:D2.
              + inversion D6;subst;clear D6.
                refine (IH _ _ _ _ _ _ _ D2 _ H).
                unfold is_a_translatable_e in *.
                simpl;simpl in Wt.
                rewrite forallb_app in Wt.
                apply Wt.                                     
              + destruct is_a_suitable_env eqn:D3;[|discriminate].
                inversion D6;subst;clear D6.
                unfold is_a_suitable_env in D3.
                simpl in D3;simpl.
                assert (G1 : Oset.mem_bool (OAggT _) (A_agg _ u ft)
                                        (map (fun a : attribute => A_Expr (F_Dot a)) ({{{s2}}}) ++ flat_map (groups_of_env_slice _) l) = false).
                {
                  apply not_true_iff_false.
                  intro N1.
                  rewrite Oset.mem_bool_true_iff in N1.
                  rewrite in_app_iff in N1.
                  rewrite in_map_iff in N1.
                  destruct N1 as [[ x [ ]] | N1];[discriminate| ].
                  unfold is_a_translatable_e in Wt.
                  simpl in Wt.
                  rewrite  forallb_app in Wt.
                  destruct Wt as [ _ Wt].
                  rewrite forallb_forall in Wt.
                  apply Wt in N1.
                  discriminate.
                }
                rewrite G1 in D3.
                apply D3.
          Qed.

          Lemma type_of_aggterm_eq :
            forall ft e,
              well_typed_e e = true ->
              type_of_value _ (interp_aggterm _ e ft) =
              type_of_aggterm ft.
          Proof.
            fix thm 1.
            intros [ft|agg ft| f l] e.
            - apply type_of_funterm_eq.
            - intros;apply type_of_aggregate_eq.
            - intros;apply type_of_symbol_eq.
          Qed.
          
          Lemma aggterm_to_nraenv_is_sound :
            forall br i ag env did  ,
              well_typed_e env = true ->
              is_a_translatable_e env = true ->
              well_formed_e _  env = true ->
              well_formed_ag _ env ag = true ->
              nraenv_eval br  i (aggterm_to_nraenv   (map fst  env)  ag ) (env_to_data env) did =
              Some (value_to_data (interp_aggterm _ env ag)).
          Proof.
            intros br i ag.
            set (n := size_aggterm _ ag).
            assert (Hn: size_aggterm _ ag <= n) by (apply le_n).
            revert dependent n.
            intros n Hn;revert n ag Hn.
            induction n as [|n];
              [intros;destruct ag;inversion Hn| ].
            destruct ag as [ |u ft | ];intros  Hn env did  Typ Wt WS Wag ;swap 2 3.
            - apply (funterm_to_nraenv_is_sound f _ _ _ _  Wt WS Typ Wag).
            - cbn.
              refine (symbol_to_nraenv_eq _ _  _ _ _ _   _ _ _ _ _);
                [|intros;rewrite (type_of_aggterm_eq _ _ Typ);apply eq_refl].
              intros.       
              refine (IHn _  (size_aggterm_F_Expr Hn _ H)  _ _ Typ Wt WS _).      
              simpl in Wag.
              rewrite forallb_forall in Wag.
              apply (Wag _ H).
            - cbn.
              unfold interp_aggterm,unfold_env_slice.
              unfold nraenv_eval in *.
              simpl;unfold olift.
              unfold unfold_env_slice.
              cbn.
              refine
                (eq_trans
                   _
                   (f_equal
                      _
                      (f_equal
                         _
                         (@interp_aggregate_permut
                            _ _ _ _
                            ((map
                                (fun e : EnvToNRAEnv.EnvToNRAEnv.env =>
                                   interp_funterm TRcd e ft)
                                match
                                  (if Fset.is_empty A (variables_ft TRcd ft)
                                   then Some env
                                   else find_eval_env TRcd env (A_agg TRcd u ft))
                                with
                                | Some (slc1 :: env'') =>
                                  map
                                    (fun slc : env_slice TRcd => slc :: env'')
                                    (let (p, l) := slc1 in
                                     let (sa, _) := p in
                                     map
                                       (fun t : tuple => (sa, Group_Fine TRcd, t :: nil))
                                       l)
                                | _ => nil
                                end)) _ _))));swap 1 2.
              + destruct Fset.is_empty eqn:D1.
                * apply _permut_eq_Permutation.
                  refine (@ListPermut._permut_map _ _ _ _ (equiv_env TRcd)  _ _ _ _ _ _ _);
                    [do 4 intro;apply interp_funterm_eq| ].
                  destruct env;[constructor| ].
                  refine (@ListPermut._permut_map _ _ _ _ (equiv_env_slice TRcd)  _ _ _ _ _ _ _).
                  intros;constructor;[assumption|apply equiv_env_refl].
                  destruct e as [[sa p] l].
                  refine (@ListPermut._permut_map _ _ _ _ (fun x y => Oeset.compare OTuple x y = Eq)  _ _ _ _ _ _ _);
                    [|apply quick_permut].
                  intros.
                  repeat constructor.
                  apply Fset.equal_refl.
                  apply Oeset.permut_refl_alt.
                  cbn;rewrite H1;apply eq_refl.
                * destruct find_eval_env;[|constructor].
                  apply _permut_eq_Permutation.
                  refine (@ListPermut._permut_map _ _ _ _ (equiv_env TRcd)  _ _ _ _ _ _ _);
                    [do 4 intro;apply interp_funterm_eq| ].
                  destruct l;[constructor| ].
                  refine (@ListPermut._permut_map _ _ _ _ (equiv_env_slice TRcd)  _ _ _ _ _ _ _).
                  intros;constructor;[assumption|apply equiv_env_refl].
                  destruct e as [[sa p] l1].
                  refine (@ListPermut._permut_map _ _ _ _ (fun x y => Oeset.compare OTuple x y = Eq)  _ _ _ _ _ _ _);
                    [|apply quick_permut].
                  intros.
                  repeat constructor.
                  apply Fset.equal_refl.
                  apply Oeset.permut_refl_alt.
                  cbn;rewrite H1;apply eq_refl.
              + { set (ft_to_l :=
                         fun ft =>
                           (map
                              (fun e : EnvToNRAEnv.EnvToNRAEnv.env => interp_funterm TRcd e ft)
                              match
                                (if Fset.is_empty A (variables_ft TRcd ft)
                                 then Some env
                                 else find_eval_env TRcd env (A_agg TRcd u ft))
                              with
                              | Some (slc1 :: env'') =>
                                map
                                  (fun slc : env_slice TRcd => slc :: env'')
                                  (let (p, l0) := slc1 in
                                   let (sa, _) := p in
                                   map (fun t : tuple => (sa, Group_Fine TRcd, t :: nil)) l0)
                              | _ => nil
                              end)).      
                  set (ft_to_nraenv :=
                         fun ft =>
                           (if Fset.is_empty A (variables_ft TRcd ft)
                            then
                              match map fst env with
                              | nil =>
                                NRAEnvAppEnv (NRAEnvConst (dcoll nil))
                                             (NRAEnvConst
                                                (drec
                                                   (("slc"%string, dcoll nil)
                                                      :: ("tl"%string, drec nil)
                                                      :: nil)))
                              | slc1 :: nenv' =>
                                translate_unfold_nslice (fst slc1) nenv' ft
                              end
                            else
                              match find_eval_nenv (map fst env) (A_agg TRcd u ft) with
                              | Some (slc1 :: nenv') =>
                                snd
                                  (translate_eval_nenv
                                     (map fst env)
                                     (A_agg TRcd u ft)
                                     (translate_unfold_nslice (fst slc1) nenv' ft))
                              | _ =>
                                NRAEnvAppEnv
                                  (NRAEnvConst (dcoll nil))
                                  (NRAEnvConst
                                     (drec
                                        (("slc"%string, dcoll nil)
                                           :: ("tl"%string, drec nil)
                                           :: nil)))
                              end)).      
                  refine (aggregate_to_nraenv_eq
                            _ _ ft_to_nraenv ft_to_l type_of_funterm
                            _ _ _ _ _ _ );swap 1 2.
                  - subst ft_to_l.
                    revert Typ.
                    clear.
                    intros Typ v Hv.
                    simpl in Hv.
                    rewrite in_map_iff in Hv.
                    destruct Hv as [e [H1 H2]].
                    subst.
                    apply eq_sym.
                    refine (type_of_funterm_eq _ _  _).
                    destruct Fset.is_empty.
                    + destruct env as [|[[slc0 g0] lt0] env];[destruct H2| ].
                      rewrite map_map in H2.
                      rewrite in_map_iff in H2.
                      destruct H2 as [t [H2 H3]].
                      subst.
                      refine (well_typed_e_cons_tuple _ _ _ _ _ _ H3).
                      apply Typ.
                    + destruct find_eval_env eqn:D1;
                        [|destruct H2].
                      destruct l;[destruct H2| ].
                      destruct e0 as [[slc0 g0] lt0].
                      rewrite map_map in H2.
                      rewrite in_map_iff in H2.
                      destruct H2 as [t [H2 H3]].
                      subst.
                      refine (well_typed_e_cons_tuple _ _ _ _ _ _ H3).
                      refine (find_eval_env_well_typed_e_if
                                (A_agg _ u ft)  _ Typ D1).
                  - subst ft_to_nraenv.
                    subst ft_to_l.
                    intros;cbn.
                    destruct Fset.is_empty eqn:D1.
                    + cbn.
                      destruct env as [|[[s1 g1]lt1] env];
                        [apply eq_refl| ].
                      assert (X1 := is_a_translatable_e_groups_are_attributes _ Wt).
                      simpl in Wag.
                      simpl in X1.
                      assert (X2 : well_formed_ft _ ((s1, g1, lt1) :: env) ft = true)
                        by apply (well_formed_ft_variables_ft_empty  _ _ D1).
                      assert
                        (X3 :
                           forall t : tuple,
                             In t lt1 ->
                             well_formed_ft
                               _ ((s1, Group_Fine _, t :: nil) :: env) ft =
                             true).
                      {
                        intros.
                        apply (well_formed_ft_variables_ft_empty _ _ D1).
                      }
                      rewrite (translate_unfold_nslice_cons _ _ _ _ _ _ _ _   Typ WS Wt X3  ).
                      rewrite 5 map_map;simpl.
                      simpl.
                      rewrite map_map.
                      apply eq_refl.
                    + rewrite find_eval_env_nenv_eq.
                      unfold lift in *.
                      destruct find_eval_env eqn:D6;[|apply eq_refl].
                      { revert dependent env.
                        destruct env as [|[[s1 g1]lt1] env];intros Typ Wt WS H1 H2 .
                        - cbn in *;destruct is_built_upon_ft;
                            apply eq_sym in H1;inversion H1;subst;
                              apply eq_sym in H2;inversion H2;subst;apply eq_refl.
                        -  destruct l as [|[[s2 g2]lt2] l].
                           +  cbn in H1.
                              destruct find_eval_env eqn:D6.
                              * inversion H1;subst.
                                apply eq_sym in H2;inversion H2;subst;
                                  inversion H3;apply eq_refl.
                              * subst;apply eq_refl.
                           +  simpl in H1,H2.
                              destruct find_eval_env eqn:D6.
                              * inversion H2;subst;clear H2 .
                                cbn.                      
                                assert
                                  (A1 :=
                                     fst_translate_eval_nenv
                                       (translate_unfold_nslice s2 (map fst l) ft)
                                       (map fst  env) (A_agg _ u ft)).
                                rewrite  find_eval_env_nenv_eq,D6 in A1.
                                simpl in A1.
                                rewrite A1.
                                assert
                                  (JJ1 :
                                     forall t : tuple,
                                       In t lt2 ->
                                       well_formed_ft
                                         _ ((s2, Group_Fine _, t :: nil) :: l) ft =
                                       true).
                                {
                                  apply is_a_translatable_e_cons in Wt.
                                  revert Wt D6.
                                  clear.
                                  revert u ft l s2 g2 lt2.
                                  apply find_eval_env_well_formed_ft_Fine_tuple.
                                }
                                assert (JJ2 : is_a_translatable_e ((s2, g2, lt2) :: l) = true).
                                {
                                  refine
                                    (find_eval_env_is_a_translatable_e_if
                                       (A_agg _ u ft)  _ (is_a_translatable_e_cons _ _ Wt) D6).
                                }
                                assert (JJ3 : well_formed_e _ ((s2, g2, lt2) :: l) = true).
                                {
                                  refine (find_eval_env_well_formed_e_if
                                            (A_agg _ u ft)  _ (well_formed_e_cons _ _ WS) D6).
                                }
                                assert (J5 : well_typed_e ((s2, g2, lt2) :: l) = true).
                                {
                                  refine (find_eval_env_well_typed_e_if
                                            (A_agg _ u ft)  _ (proj1 (proj1 (well_typed_e_cons _ _) Typ)) D6).
                                }
                                assert (JJ4 := translate_unfold_nslice_cons
                                                br i l ft s2 g2 lt2 did  J5 JJ3 JJ2 JJ1 ).
                                apply eq_sym.
                                simpl.
                                apply well_formed_e_cons in WS.
                                apply is_a_translatable_e_cons in Wt.
                                assert (D5 := H1).
                                clear JJ1 JJ2 JJ3 H1.
                                revert dependent l.
                                revert dependent env.
                                fix thm 1.
                                {  destruct env as [|[[s3 g3]lt3] env];intros Typ Wt WS l H1 H2 Typ2.
                                   - cbn in *;destruct is_built_upon_ft;discriminate.
                                   - simpl in *.
                                     assert ( A1:= fst_translate_eval_nenv (translate_unfold_nslice s2 (map fst l) ft) (map fst env)  (A_agg _ u ft)).
                                     rewrite  find_eval_env_nenv_eq in A1.
                                     simpl in *;unfold lift in *;rewrite A1 in *.
                                     destruct find_eval_env eqn:D11.
                                     + apply eq_sym in H1;inversion H1;subst.
                                       refine (thm _  _ (is_a_translatable_e_cons _ _ Wt) _ _ D11 A1 Typ2).
                                       * rewrite 2 andb_true_iff in Typ.
                                         rewrite andb_true_iff.                                         
                                         split;apply Typ.
                                       * rewrite andb_true_iff in WS.
                                         apply WS.
                                     + simpl in *.
                                       rewrite is_a_suitable_env_nenv_eq in *.
                                       destruct is_a_suitable_env eqn:D3;[|discriminate].
                                       inversion H1;simpl;intros;subst.
                                       apply eq_sym;apply JJ4.
                                }
                              * apply eq_sym in H1.
                                rewrite D1 in H1.
                                destruct is_a_suitable_env eqn:D9;[|discriminate].
                                inversion H2;subst;clear H2 H1.
                                simpl.                      
                                assert ( A1:= fst_translate_eval_nenv ((translate_unfold_nslice s2 (map fst l) ft)) (map fst l)  (A_agg _ u ft)).
                                rewrite  find_eval_env_nenv_eq,D6 in A1.
                                rewrite A1 in *.
                                rewrite is_a_suitable_env_nenv_eq.
                                simpl.
                                rewrite D9.
                                assert (JJ2 : forall t : tuple, In t lt2 -> well_formed_ft _ ((s2, Group_Fine _, t :: nil) :: l) ft = true).
                                {
                                  intros t Ht.
                                  unfold well_formed_ft.
                                  assert (G1 : Oset.mem_bool (OAggT _) (A_agg _ u ft)
                                                          (map (fun a : attribute => A_Expr (F_Dot a)) ({{{s2}}}) ++ flat_map (groups_of_env_slice _) l) = false).
                                  {
                                    apply not_true_iff_false.
                                    intro N1.
                                    rewrite Oset.mem_bool_true_iff in N1.
                                    rewrite in_app_iff in N1.
                                    rewrite in_map_iff in N1.
                                    destruct N1 as [[ x [ ]] | N1];[discriminate| ].
                                    unfold is_a_translatable_e in Wt.
                                    simpl in Wt.
                                    rewrite forallb_app in Wt.
                                    destruct Wt as [_ Wt].
                                    rewrite forallb_forall in Wt.
                                    apply Wt in N1.
                                    discriminate.
                                  }
                                  unfold is_a_suitable_env in D9.
                                  simpl in D9.
                                  rewrite G1 in D9.
                                  apply D9.
                                }                                
                                rewrite (translate_unfold_nslice_cons  _ _ _ _ _ _ _ _  Typ WS Wt JJ2   ).
                                apply eq_refl.
                      }
                }
          Qed.
          
          Lemma eval_aggterm_to_nraenv_equiv_env_eq :
            forall br i  ,
            forall e1 e2,
              well_formed_e _ e1 = true ->
              equiv_env _ e1 e2 ->
              is_a_translatable_e e1= true ->
              well_typed_e e1 = true ->
              forall ag  did,
                well_formed_ag _ e1 ag = true ->
                nraenv_eval br i (aggterm_to_nraenv (map fst e1) ag) (env_to_data e1)   did =
                nraenv_eval br i (aggterm_to_nraenv (map fst e1) ag) (env_to_data e2)  did.
          Proof.
            unfold nraenv_eval.
            intros br i e1 e2 We1 Heq F1 T1 ag did G1.
            assert (Y1 := aggterm_to_nraenv_equiv_env_eq (equiv_env_equiv_nenv Heq)  ag).
            rewrite Y1 at 2.
            refine (eq_trans
                      (aggterm_to_nraenv_is_sound _ _ _ _ _ T1 F1 We1 G1)
                      _).
            apply eq_sym.
            rewrite (is_a_translatable_e_eq (equiv_env_weak_equiv_env _ _ _ Heq) ) in F1.
            rewrite  (well_formed_e_eq Heq) in We1.
            rewrite  (well_formed_ag_eq  _ _ _ _ (equiv_env_weak_equiv_env _ _ _ Heq)) in G1.
            rewrite (well_typed_e_eq Heq) in T1.
            refine (eq_trans
                      (aggterm_to_nraenv_is_sound _ _ _ _ _ T1 F1 We1 G1)
                      _).
            do 2 apply f_equal.
            apply eq_sym.
            refine (interp_aggterm_eq _ _ _  _ Heq).
          Qed.

        End ATermToNRAEnv.

        Section ProjToNRAEnv.

          Lemma select_list_to_nraenv_equiv_env_eq_ :
            forall e1 e2,
              equiv_nenv  e1 e2 ->
              forall sl,
                select_list_to_nraenv_ e1 sl  =
                select_list_to_nraenv_  e2 sl .
          Proof.
            intros e1 e2 Heq.
            induction sl as [|[s g]];[apply eq_refl| ].
            simpl.
            rewrite (aggterm_to_nraenv_equiv_env_eq Heq).   
            rewrite IHsl.
            apply eq_refl.
          Qed.

          Lemma select_list_to_nraenv_ignores_did_ :
            forall br i l aenv ,
            forall reg id1 id2,
              nraenv_core_eval br i (nraenv_to_nraenv_core (select_list_to_nraenv_ aenv l)) reg id1 =
              nraenv_core_eval br i (nraenv_to_nraenv_core (select_list_to_nraenv_ aenv l)) reg id2.
          Proof.
            intros br i.
            induction l;intros aenv  reg id1 id2;[apply eq_refl| ].
            destruct a.
            cbn.
            apply f_equal2.
            - apply f_equal.
              assert (A1 := aggterm_to_nraenv_ignores_did);unfold nraenv_eval in A1;apply A1.
            -  simple refine (IHl aenv _   _ _ ).
          Qed.

          
          Lemma select_list_eval_is_sorted_ :
            forall br i l env did r,
              well_formed_e  _ env = true ->
              nraenv_eval br i (select_list_to_nraenv_   (map fst  env)  l) (env_to_data   env) did = Some (drec r) ->
              Sorted rec_field_lt  r.
          Proof.
            intros br i.
            fix thm 1.
            intros [|[a1 e1] l] env did  r WS Hr.
            - inversion Hr;subst.
              constructor.
            - cbn in  Hr;unfold olift2 in Hr.
              destruct  (nraenv_core_eval _ _ (_ _) _ _) as [dag| ] eqn:D4;
                [|discriminate].
              destruct  (nraenv_core_eval _ _ (_ (select_list_to_nraenv_ (map fst env) l)) _ _) as [dl| ] eqn:D3;
                [unfold olift in Hr|discriminate].
              destruct dl as [| | | | | | r'| | | | ];try discriminate.
              inversion Hr;subst;clear Hr.
              apply insertion_sort_insert_Sorted.
              rewrite <- is_list_sorted_Sorted_iff.
              rewrite is_list_sorted_domain_rec_field.
              apply rec_sort_pf.
          Qed.

          Lemma select_list_eval_is_drec_ :
            forall br i l env did  d,
              nraenv_eval br i (select_list_to_nraenv_   (map fst  env)  l) (env_to_data   env) did = Some d ->
              exists  r, d = drec r.
          Proof.
            intros br i.
            fix thm 1.
            intros [ | [ ] l] env did d Hd.
            - inversion Hd;subst.
              exists nil;apply eq_refl.
            -  cbn in  Hd;unfold olift2,olift in Hd.
               destruct  (nraenv_core_eval _ _ (_ _) _ _) as [dag| ] eqn:D4;
                 [|discriminate].
               destruct  (nraenv_core_eval _ _ (_  (select_list_to_nraenv_ (map fst env) l)) _ _) as [dl| ] eqn:D3;
                 [ |discriminate].
               destruct dl as [| | | | | | r'| | | | ];try discriminate.
               inversion Hd.
               exists ( (insertion_sort_insert rec_field_lt_dec (attribute_to_string a, dag) (rec_sort r'))).
               apply eq_refl.
          Qed.

          Lemma select_list_eval_in_1 :
            forall br i env did, 
              well_formed_e _ env = true ->
              forall l r,
                nraenv_eval br i (select_list_to_nraenv_   (map fst  env)  l) (env_to_data   env) did = Some (drec r) ->
                (forall x, In x r -> exists a, attribute_to_string a = fst x /\
                                    (exists ag, Oset.find OAtt a l = Some ag)).
          Proof.
            intros br i env did WS.
            fix thm 1.
            intros [|[s1 e1] l] r Hr [s d] Hin.
            - inversion Hr;subst;inversion Hin.
            - cbn in Hr;unfold olift2,olift in Hr.
              cbn;unfold olift2,olift.
              destruct nraenv_core_eval as [de1| ] eqn:D3;[|discriminate].
              destruct (nraenv_core_eval _ _ (nraenv_to_nraenv_core (select_list_to_nraenv_ (map fst env) l)) _ _ ) as [dl| ] eqn:D4;[|discriminate].
              destruct dl as [ | | | | | |r' | | | | ];try discriminate.
              inversion Hr;subst;clear Hr.
              apply in_insertion_sort_insert in Hin.
              destruct Hin as [H1|H1].
              + inversion H1;subst.
                exists s1;split;[|rewrite Oset.eq_bool_refl;exists e1];apply eq_refl.
              + apply rec_sort_in in H1.
                assert (IH := thm l r'  D4).
                assert (IH2 := (IH _) H1).
                destruct IH2 as [a [IH1 [ag IH2]]].
                simpl in IH1.
                exists a;split;[apply IH1| ].
                destruct ( Oset.eq_bool OAtt a s1 ).
                exists e1;apply eq_refl.
                exists ag;apply IH2.
          Qed.

          (* Definition well_typed_s s := *)
          (*   forallb (fun x => match x with Select_As _  e a => well_typed_ag e end) s.     *)
          
          Lemma select_list_to_nraenv_is_sound_ :
            forall br i env did,
              well_typed_e env = true ->              
              well_formed_e _  env = true ->
              is_a_translatable_e env = true ->
              forall l  ,
                Sorted rec_field_lt   (map (fun x => (attribute_to_string (fst x), snd x) ) l) ->
                (* well_typed_s  (map (fun x => Select_As T (snd x) (fst x)) l)  = true -> *)
                well_formed_s _ env (map (fun x => Select_As _ (snd x) (fst x)) l) = true ->
                nraenv_eval br i (select_list_to_nraenv_  (map fst  env)  l) (env_to_data   env) did = Some (drec 
                                                                                                               ( ((tuple_as_dpairs (mk_tuple (Fset.mk_set A (map fst l))
                                                                                                                                             (fun a : attribute  =>
                                                                                                                                                match Oset.find OAtt  a l with
                                                                                                                                                | Some e => interp_aggterm _  env e
                                                                                                                                                | None => dot  (default_tuple _ (emptysetS)) a
                                                                                                                                                end)))))).
          Proof.
            intros br i env did Typ WS Wt.
            fix thm 1.
            intros  [|[a1 e1] l]   Hl  Ws.
            - unfold tuple_as_dpairs,tuple_as_pairs .
              rewrite (Fset.elements_spec1 _ _ _ (labels_mk_tuple _ _ _)).
              rewrite Fset.elements_empty.
              apply eq_refl.
            - cbn;unfold olift2,olift.
              (* destruct (nraenv_core_eval _ _ (nraenv_to_nraenv_core ne1) _ _ ) as [de1| ] eqn:D3;[|discriminate]. *)
              (* destruct (nraenv_core_eval _ _ (nraenv_to_nraenv_core nl) _ _ ) as [dl| ] eqn:D4;[|discriminate]. *)
              (* destruct dl;try discriminate;inversion Hr;subst;clear Hr. *)
              assert (F1 : well_formed_ag _ env e1 = true).
              {
                simpl in Ws.
                unfold well_formed_s in Ws.
                rewrite 2 andb_true_iff,forallb_forall in Ws.
                destruct Ws as [[Ws _] _].
                refine (Ws _ (or_introl eq_refl)).
              }
              (* simpl in Typ;rewrite  andb_true_iff in Typ.       *)
              assert (A2 :=  aggterm_to_nraenv_is_sound br i e1  _ did  (* (proj1 Typ) *) Typ Wt WS F1 ).
              unfold nraenv_eval in A2;rewrite A2.
              
              (* apply eq_sym in A2;subst. *)
              inversion Hl;subst.
              assert (F2 : well_formed_s _ env (map (fun x : attribute * aggterm => Select_As _ (snd x) (fst x)) l) = true).
              {
                apply (well_formed_s_cons _ _ _ Ws).
              }
              
              assert (A1 := thm  l    H1  F2  ).
              unfold nraenv_eval in A1.
              rewrite A1.
              simpl.      
              rewrite sort_sorted_is_id;
                [|rewrite is_list_sorted_Sorted_iff;apply Sorted_ODT_lt_domain_tuple_as_dpairs].
              unfold tuple_as_dpairs,tuple_as_pairs.
              do 2 rewrite (Fset.elements_spec1 _ _ _ (labels_mk_tuple _ _ _)).
              rewrite fset_sorted_eq;[| apply (Sorted_listA_is_Sorted_domain _  );assumption].
              replace (a1 addS Fset.mk_set A (map fst l)) with (Fset.mk_set A (map fst ((a1,e1)::l))) by apply eq_refl.
              rewrite fset_sorted_eq;[| apply (Sorted_listA_is_Sorted_domain   _ );assumption].
              rewrite insertion_sort_insert_forall_lt.
              + do 4 rewrite map_map;rewrite 2 map_cons.
                do 2 apply f_equal.
                apply f_equal2.
                * apply f_equal.
                  rewrite dot_mk_tuple;simpl.
                  rewrite Fset.add_spec,Oset.eq_bool_refl.
                  simpl.
                  apply eq_refl.
                * apply map_ext_in.
                  intros a Ha.
                  apply f_equal.
                  rewrite dot_mk_tuple.
                  assert (B1 : fst a inS Fset.mk_set A (map fst l)).
                  { rewrite Fset.mem_mk_set,Oset.mem_bool_true_iff.
                    apply (in_map _ _ _ Ha). }
                  rewrite B1.
                  rewrite dot_mk_tuple.
                  simpl.
                  rewrite Fset.add_spec.
                  rewrite B1,Bool.orb_true_r.
                  apply f_equal.
                  clear A2.
                  assert (A2 : Oset.eq_bool OAtt (fst a) a1 = false) .
                  {
                    rewrite <- Bool.not_true_iff_false.
                    intro N1.
                    assert (N2:SetoidList.InA eq (attribute_to_string (fst a),(snd a)) (map (fun x : attribute * aggterm => (attribute_to_string (fst x), snd x)) l)).
                    {
                      apply (SetoidList.In_InA eq_equivalence ).
                      apply (in_map  (fun x : attribute * aggterm => (attribute_to_string (fst x), snd x))).
                      apply Ha.
                    }
                    assert (N3 := SetoidList.SortA_InfA_InA eq_equivalence rec_field_lt_strict _ H1 H2 N2).
                    rewrite Oset.eq_bool_true_iff in N1;simpl in N1;subst.
                    inversion N3.
                    rewrite StringOrder.compare_refl_eq in H0;discriminate.
                  } 
                  rewrite A2;apply eq_refl.
              + rewrite Forall_rec_field_lt.
                rewrite Forall_forall.
                intros a Ha.
                cbn.
                inversion Hl;subst.
                clear A1.
                assert ( A1 : Sorted StringOrder.lt (map (fun x => attribute_to_string (fst x)) l)).
                { revert H1;clear.
                  induction l as [|[a1 e1] l];[constructor| ];intro H.
                  inversion H;subst.
                  apply IHl in H2.
                  apply Sorted_cons;[apply H2| ].
                  revert H3;clear.
                  induction l as [|[a2 e2] l];[constructor| ];intro H.
                  inversion H;subst.
                  simpl in H1.
                  simpl.
                  simpl in H.
                  constructor;apply H1. }
                clear A2.
                assert (A2 : HdRel StringOrder.lt (attribute_to_string a1) (map (fun x : attribute * aggterm => attribute_to_string (fst x)) l)).
                { revert H2;clear.
                  induction l as [|[a2 e2] l];[constructor| ];intro H.
                  inversion H;subst.
                  simpl in H1.
                  simpl.
                  simpl in H.
                  constructor;apply H1. }
                refine (SetoidList.SortA_InfA_InA eq_equivalence StringOrder.lt_strorder StringOrder.lt_compat  A1  A2 _).
                apply (SetoidList.In_InA eq_equivalence ).
                unfold domain in Ha.
                do 3 rewrite map_map in Ha.
                apply Ha.
          Qed.      
          
          
          Lemma select_list_to_nraenv_ignores_did :
            forall br i l aenv,
            forall reg id1 id2,
              nraenv_core_eval br i (nraenv_to_nraenv_core (select_list_to_nraenv aenv l)) reg id1 =
              nraenv_core_eval br i (nraenv_to_nraenv_core (select_list_to_nraenv aenv l)) reg id2.
          Proof.
            intros br i [ ] aenv.
            apply (select_list_to_nraenv_ignores_did_ _ _ _ ).
          Qed.

          Lemma select_list_to_nraenv_equiv_env_eq :
            forall e1 e2,
              equiv_nenv  e1 e2 ->
              forall sl,
                select_list_to_nraenv e1 sl  =
                select_list_to_nraenv  e2 sl .
          Proof.
            intros e1 e2 Heq [ ].
            refine (select_list_to_nraenv_equiv_env_eq_  Heq _).
          Qed.
          
          Lemma select_list_eval_is_drec :
            forall br i l env did  d,
              nraenv_eval br i (select_list_to_nraenv   (map fst  env)  l) (env_to_data   env) did = Some d ->
              exists  r, d = drec r.
          Proof.
            intros br i [ ];apply select_list_eval_is_drec_.
          Qed.

          Lemma select_list_eval_is_sorted :
            forall br i l env did  r,
              well_formed_e _  env = true ->
              nraenv_eval br i (select_list_to_nraenv   (map fst  env)  l) (env_to_data   env) did = Some (drec r) ->
              Sorted rec_field_lt  r.
          Proof.
            intros br i [ ] env did r WI.
            refine ( select_list_eval_is_sorted_ _ i _  _ did WI).
          Qed.

          
          Lemma select_list_to_nraenv_is_sound :
            forall br i env did,
              well_typed_e env = true ->
              well_formed_e  _ env = true ->
              is_a_translatable_e env = true ->
              forall s ,
                (* well_typed_s s = true -> *)
                well_formed_s _ env s = true ->
                nraenv_eval br i (select_list_to_nraenv  (map fst  env)  (_Select_List _ s)) (env_to_data   env) did = Some (drec 
                                                                                                                               (let l := map (fun x : select _  => match x with
                                                                                                                                                                 | Select_As _ e a => (a, e)
                                                                                                                                                                 end) s in
                                                                                                                                ( ((tuple_as_dpairs (mk_tuple (Fset.mk_set A ({{{Fset.mk_set A (map fst l)}}}) )
                                                                                                                                                              (fun a : attribute =>
                                                                                                                                                                 match Oset.find OAtt a l with
                                                                                                                                                                 | Some e => interp_aggterm _  env e
                                                                                                                                                                 | None => dot  (default_tuple _ (emptysetS)) a
                                                                                                                                                                 end))))))).
          Proof.
            intros br i env did Typ Wt WS s Ws.
            unfold select_list_to_nraenv.
            rewrite map_map.
            cbv zeta.
            assert (Aux : Sorted rec_field_lt
                                 (map (fun x : attribute * aggterm => (attribute_to_string (fst x), snd x))
                                      (map
                                         (fun x : attribute =>
                                            (x,
                                             match
                                               Oset.find OAtt  x (map (fun x0 : select _  => match x0 with
                                                                                            | Select_As _ e a => (a, e)
                                                                                            end) s)
                                             with
                                             | Some e => e
                                             | None => A_Expr   (F_Constant _  (default_value _ (type_of_attribute _ x)))
                                             end))
                                         ({{{Fset.mk_set A (map (fun x : select _  => fst match x with
                                                                                        | Select_As _ e a => (a, e)
                                                                                        end) s)}}})))).
            {
              rewrite <- is_list_sorted_Sorted_iff.
              rewrite is_list_sorted_domain_rec_field.
              unfold domain.
              do 2 rewrite map_map;simpl.      
              rewrite is_list_sorted_Sorted_iff.
              apply Sorted_map_attribute_to_string_iff.
              apply Fset.elements_spec3.
            }
            assert (F3 :well_formed_s _ env
                                      (map (fun x : attribute * aggterm => Select_As _ (snd x) (fst x))
                                           (map
                                              (fun x : attribute =>
                                                 (x,
                                                  match Oset.find OAtt x (map (fun x0 : select _ => match x0 with
                                                                                                   | Select_As _ e a => (a, e)
                                                                                                   end) s) with
                                                  | Some e => e
                                                  | None => A_Expr  (F_Constant _ (default_value _ (type_of_attribute _ x)))
                                                  end)) ({{{Fset.mk_set A (map (fun x : select _ => fst match x with
                                                                                                      | Select_As _ e a => (a, e)
                                                                                                      end) s)}}}))) = true).
            {
              revert Ws.
              clear.
              unfold well_formed_s.
              rewrite 4 andb_true_iff.
              rewrite  2 forallb_forall.
              rewrite  <- 2 Oset.all_diff_bool_ok.
              intros [[H1 H2] H3];repeat split.
              - intros x Hx.
                rewrite map_map in Hx.
                rewrite in_map_iff in Hx.
                destruct Hx as [x1 [Hx1 Hx2]].
                simpl in Hx1.
                apply Fset.in_elements_mem in Hx2.
                rewrite Fset.mem_mk_set in Hx2.
                rewrite Oset.mem_bool_true_iff in Hx2.
                rewrite in_map_iff in Hx2.
                destruct Hx2 as [[ ] [Hx3 Hx2]].
                simpl in Hx3;subst.
                assert (I1 := H1 _ Hx2);simpl in I1.
                replace (
                    Oset.find OAtt
                              x1 (map
                                    (fun x0 :
                                           select _ =>
                                       match x0 with
                                       | Select_As _ e a0 => (a0, e)
                                       end) s)) with (Some a);[apply I1|apply eq_sym].
                apply Oset.all_diff_fst_find;
                  [rewrite map_map;apply H2| ].
                rewrite in_map_iff.
                exists (Select_As _ a x1);split;[apply eq_refl|apply Hx2].
              - rewrite 2 map_map,map_id.
                apply Fset.all_diff_elements.
              - rewrite <- H3.
                apply Fset.is_empty_eq.
                rewrite 2 map_map.
                apply Fset.inter_eq_1.        
                simpl;rewrite map_id.
                apply Fset.mk_set_idem.
            }
            (* assert *)
            (* (Aux2 : *)
            (*    well_typed_s *)
            (* (map (fun x : attribute * aggterm => Select_As T (snd x) (fst x)) *)
            (*    (map *)
            (*       (fun x : attribute => *)
            (*        (x, *)
            (*        match Oset.find OAtt x (map (fun x0 : select T => match x0 with *)
            (*                                                          | Select_As _ e a => (a, e) *)
            (*                                                          end) s) with *)
            (*        | Some e => e *)
            (*        | None => A_Expr (F_Constant T (default_value T (type_of_attribute T x))) *)
            (*        end)) ({{{Fset.mk_set A (map (fun x : select T => fst match x with *)
            (*                                                              | Select_As _ e a => (a, e) *)
            (*                                                            end) s)}}}))) = true). *)
            (* { *)
            (*   revert Typ. *)
            (*   rewrite map_map. *)
            (*   apply forallb_incl. *)
            (*   unfold incl. *)
            (*   intros [ ] Hx. *)
            (*   simpl in Hx. *)
            (*   rewrite in_map_iff in Hx. *)
            (*   destruct Hx as [x1 [Hx1 Hx2]]. *)
            (*   inversion Hx1;subst;clear Hx1. *)
            (*   apply Fset.in_elements_mem in Hx2. *)
            (*   rewrite Fset.mem_mk_set in Hx2. *)
            (*   rewrite Oset.mem_bool_true_iff in Hx2. *)
            (*   rewrite in_map_iff in Hx2. *)
            (*   destruct Hx2 as [[ ] [Hx3 Hx2]]. *)
            (*   simpl in Hx3;subst.       *)
            (*   replace (Oset.find OAtt a0 (map (fun x0 : select T => match x0 with *)
            (*                                                       | Select_As _ e a1 => (a1, e) *)
            (*                                                        end) s))  with (Some a);[ apply Hx2|apply eq_sym]. *)
            (*   revert Ws. *)
            (*   unfold well_formed_s. *)
            (*   rewrite 2 andb_true_iff. *)
            (*   rewrite   forallb_forall. *)
            (*   rewrite  <-  Oset.all_diff_bool_ok. *)
            (*   intros [[H1 H2] H3];repeat split. *)
            (*   apply Oset.all_diff_fst_find; *)
            (*       [rewrite map_map;apply H2| ]. *)
            (*     rewrite in_map_iff. *)
            (*     exists (Select_As T a a0);split;[apply eq_refl|apply Hx2]. *)
            (* } *)
            rewrite ( select_list_to_nraenv_is_sound_  br i  _ did Typ Wt WS  _  Aux  F3  ).
            do 2 apply f_equal.
            apply (tuple_as_dpairs_eq ).
            repeat rewrite map_map.
            simpl.
            rewrite map_id.
            apply mk_tuple_eq_2.
            intros a Ha.
            rewrite Oset.find_map;
              [|rewrite Fset.mem_mk_set,Oset.mem_bool_true_iff in Ha;apply Ha].
            destruct Oset.find eqn:D1;[apply eq_refl| ].
            rewrite Fset.mem_mk_set,<- Fset.mem_elements in Ha.
            rewrite  Fset.mem_mk_set,Oset.mem_bool_true_iff in Ha.
            assert (A1 := Oset.find_none_alt _ _ _ D1 ).
            rewrite map_map in A1.
            destruct (A1 Ha).
          Qed.
          
          Lemma select_list_eval_is___tuple :
            forall br i env did,
              
              well_typed_e env = true ->
              well_formed_e _ env = true ->
              is_a_translatable_e env = true ->
              forall s d,
                well_formed_s _ env s = true ->
                (* well_typed_s s = true -> *)
                nraenv_eval br i (select_list_to_nraenv  (map fst  env)  (_Select_List _ s)) (env_to_data   env) did = Some d ->
                exists  t, d = drec (tuple_as_dpairs t).
          Proof.
            intros br i env did Typ WS Wt s r  Ws Hr.
            destruct (select_list_eval_is_drec br i _ env did   Hr) as [r' Ar'].
            subst.    
            rewrite (select_list_to_nraenv_is_sound br i  _ did Typ WS Wt s     Ws) in Hr.
            inversion Hr;subst;clear Hr.
            cbv zeta.
            exists ((mk_tuple
                  (Fset.mk_set A
                               ({{{Fset.mk_set A
                                              (map fst (map (fun x : select _  => match x with
                                                                                 | Select_As _ e a => (a, e)
                                                                                 end) s))}}}))
                  (fun a : attribute =>
                     match
                       Oset.find OAtt a (map (fun x : select _   => match x with
                                                                   | Select_As _ e a0 => (a0, e)
                                                                   end) s)
                     with
                     | Some e => interp_aggterm _ env e
                     | None => dot (default_tuple _ (emptysetS)) a
                     end)));apply eq_refl.
          Qed.
          
          Lemma select_list_to_nraenv_is_sound_equiv_env_ :
            forall br i e1 e2  did,
              equiv_env _ e1 e2 ->              
              well_typed_e  e1 = true ->
              well_formed_e _ e1 = true ->
              is_a_translatable_e e1 = true ->
              forall l ,
                well_formed_s _ e1 (map (fun x => Select_As _ (snd x) (fst x)) l) = true ->
                nraenv_eval br i (select_list_to_nraenv_  (map fst  e1)  l) (env_to_data   e1) did =   
                nraenv_eval br i (select_list_to_nraenv_  (map fst  e1)  l) (env_to_data   e2) did .
          Proof.
            unfold nraenv_eval.
            intros br i e1 e2 did Heq Typ We1 Te1.
            induction l as [|[a ag] s];intros Ws1.
            - apply eq_refl.
            - simpl;unfold olift2,olift.
              rewrite (IHs (well_formed_s_cons _  _ _ Ws1) ).
              do 2 apply BasicFacts.match_option_eq.
              refine (eval_aggterm_to_nraenv_equiv_env_eq _ _  We1  Heq Te1 Typ   _  _ _).
              unfold well_formed_s in Ws1.
              rewrite 2 andb_true_iff in Ws1.
              rewrite forallb_forall in Ws1.
              refine (proj1 (proj1 Ws1)  (Select_As _ ag a) _).
              rewrite in_map_iff;exists (a,ag);split;[|left];apply eq_refl.
          Qed.
          
          Lemma select_list_to_nraenv_is_sound_equiv_env :
            forall br i e1 e2  did,
              equiv_env _ e1 e2 ->
              well_typed_e  e1 = true ->
              well_formed_e _ e1 = true ->
              is_a_translatable_e e1 = true ->
              forall s,
                well_formed_s _ e1  s = true ->
                nraenv_eval br i (select_list_to_nraenv  (map fst  e1)  (_Select_List _ s)) (env_to_data   e1) did =   
                nraenv_eval br i (select_list_to_nraenv  (map fst  e1)  (_Select_List _ s)) (env_to_data   e2) did .
          Proof.
            unfold nraenv_eval.
            intros br i e1 e2 did Heq Typ We1 Te1.
            intros s Ws1.
            assert (F3 :well_formed_s _ e1
                                      (map (fun x : attribute * aggterm => Select_As _ (snd x) (fst x))
                                           (map
                                              (fun x : attribute =>
                                                 (x,
                                                  match Oset.find OAtt x (map (fun x0 : select _ => match x0 with
                                                                                                   | Select_As _ e a => (a, e)
                                                                                                   end) s) with
                                                  | Some e => e
                                                  | None => A_Expr  (F_Constant _ (default_value _ (type_of_attribute _ x)))
                                                  end)) ({{{Fset.mk_set A (map (fun x : select _ => fst match x with
                                                                                                      | Select_As _ e a => (a, e)
                                                                                                      end) s)}}}))) = true).
            {
              revert Ws1.
              clear.
              unfold well_formed_s.
              rewrite 4 andb_true_iff.
              rewrite 2 forallb_forall.
              rewrite  <- 2 Oset.all_diff_bool_ok.
              intros [[H1 H2] H3];repeat split.
              - intros x Hx.
                rewrite map_map in Hx.
                rewrite in_map_iff in Hx.
                destruct Hx as [x1 [Hx1 Hx2]].
                simpl in Hx1.
                apply Fset.in_elements_mem in Hx2.
                rewrite Fset.mem_mk_set in Hx2.
                rewrite Oset.mem_bool_true_iff in Hx2.
                rewrite in_map_iff in Hx2.
                destruct Hx2 as [[ ] [Hx3 Hx2]].
                simpl in Hx3;subst.
                assert (I1 := H1 _ Hx2);simpl in I1.
                replace (
                    Oset.find OAtt
                              x1 (map
                                    (fun x0 :
                                           select _ =>
                                       match x0 with
                                       | Select_As _ e a0 => (a0, e)
                                       end) s)) with (Some a);[apply I1|apply eq_sym].
                apply Oset.all_diff_fst_find;
                  [rewrite map_map;apply H2| ].
                rewrite in_map_iff.
                exists (Select_As _ a x1);split;[apply eq_refl|apply Hx2].
              - rewrite 2 map_map,map_id.
                apply Fset.all_diff_elements.
              - rewrite <- H3.
                apply Fset.is_empty_eq.
                rewrite 2 map_map.
                apply Fset.inter_eq_1.        
                simpl;rewrite map_id.
                apply Fset.mk_set_idem.
            }    
            rewrite <- (map_map _ fst) in F3.
            refine (select_list_to_nraenv_is_sound_equiv_env_  br i   did Heq Typ We1  Te1    _  F3 ).
          Qed.    

          Lemma domain_projection_select_list_eq :
            forall sl e,
              (let s' := map (fun x : select TRcd => match x with
                                                   | Select_As _ e0 a => (a, e0)
                                                   end) sl in
               map attribute_to_string ({{{Fset.mk_set A (map fst s')}}})) =
              domain
                (tuple_as_dpairs (projection TRcd e (Select_List  _ (_Select_List  _ sl)))).
          Proof.
            intros.
            rewrite domain_labels_eq.
            rewrite (Fset.elements_spec1 _ _ _ (labels_projection _ _ _)).
            do 3 apply f_equal.
            rewrite map_map.
            apply map_ext.
            intros;destruct a;apply eq_refl.
          Qed.
          
        End ProjToNRAEnv.
        
      End  Lemmas.
      
    End Sec.
    
End ATermToNRAEnv.
