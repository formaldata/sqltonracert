(************************************************************************************)
(**                                                                                 *)
(**                          The SQLToNRACert Library                               *)
(**                                                                                 *)
(**                       LMF, CNRS & Université Paris-Saclay                       *)
(**                                                                                 *)
(**                        Copyright 2016-2022 : FormalData                         *)
(**                                                                                 *)
(**         Authors: Véronique Benzaken                                             *)
(**                  Évelyne Contejean                                              *)
(**                  Mohammed Hachmaoui                                             *)
(**                  Chantal Keller                                                 *)
(**                                                                                 *)
(************************************************************************************)

Require Import ZArith.
From SQLFS Require Import FlatData SqlAlgebra Formula.
Require Export TnullBD TnullTD TnullEN TnullATN TnullIN.

Require Import FormulaToNRAEnv
        Qcert.NRAEnv.NRAEnvRuntime Qcert.Utils.Utils TupleToData MoreSugar NRAEnvRefine EnvToNRAEnv AxiomFloat.
From SQLFS Require Import Bool3 Formula GenericInstance.

Section Sec.

  Export TupleToData BoolBToData InstanceToNRAEnv.
  Import Tuple SqlSyntax Values.
  
  Definition NRAEnvNot3 nf :=
    NRAEnvApp
      (NRAEnvEither
         (NRAEnvUnop OpLeft (NRAEnvUnop OpNeg NRAEnvID))
         (NRAEnvConst (dright dunit)))
      nf.
  
  Lemma NRAEnvNot3_ignores_did :
    forall  nf,
    forall did1 did2 br i dreg,        
      nraenv_eval br i nf dreg did1 = nraenv_eval br i nf dreg did2 ->      
      nraenv_eval br i (NRAEnvNot3 nf) dreg did1 =
      nraenv_eval br i (NRAEnvNot3 nf) dreg did2.
  Proof.
    unfold nraenv_eval.
    do 6 intro;intros  H1.
    simpl.
    rewrite H1.
    apply eq_refl.
  Qed.
  
  Lemma NRAEnvNot3_spec :
    forall nf,
    forall b1 br i dreg did,        
      (nraenv_eval br i nf dreg did = Some (bool3_to_data b1)) ->
        nraenv_eval br i (NRAEnvNot3 nf) dreg did =
        Some
          (bool3_to_data   (Bool.negb Bool3.Bool3 b1)).
  Proof.      
    unfold nraenv_eval in *.
    intros n1 b1 br i dreg did Hn1.
    cbn.
    unfold olift.
    rewrite Hn1.
    destruct b1;apply eq_refl.
  Qed.

  Lemma NRAEnvNot3_eq :
    forall nf,
    forall b1 br i dreg,        
      (forall did,nraenv_eval br i nf dreg did = Some (bool3_to_data b1)) ->
      forall did,
        nraenv_eval br i (NRAEnvNot3 nf) dreg did =
        Some
          (bool3_to_data   (Bool.negb Bool3.Bool3 b1)).
  Proof.
    intros.
    exact (NRAEnvNot3_spec nf _ _ _ _ _ (H did)).
  Qed.
  
  Definition NRAEnvIsTrue3 nf :=
    (NRAEnvApp (NRAEnvEither NRAEnvID (NRAEnvConst (dbool false))) nf).

    Lemma NRAEnvIsTrue3_spec :
    forall nf,
    forall b1 br i dreg did,        
      (nraenv_eval br i nf dreg did = Some (bool3_to_data b1)) ->
        nraenv_eval br i (NRAEnvIsTrue3 nf) dreg did =
        Some
          (dbool  (Bool.is_true Bool3.Bool3 b1)).
  Proof.      
    unfold nraenv_eval in *.
    intros n1 b1 br i dreg did Hn1.
    cbn.
    unfold olift.
    rewrite Hn1.
    destruct b1;apply eq_refl.
  Qed.

  Lemma NRAEnvIsTrue3_eq :
    forall nf,
    forall b1 br i dreg,        
      (forall did,nraenv_eval br i nf dreg did = Some (bool3_to_data b1)) ->
      forall did,
        nraenv_eval br i (NRAEnvIsTrue3 nf) dreg did =
        Some
          (dbool  (Bool.is_true Bool3.Bool3 b1)).
  Proof.      
    intros n1 b1 br i dreg Hn1 did.
    exact (NRAEnvIsTrue3_spec n1 _ _ _ _ _ (Hn1 did)).
  Qed.


  Definition NRAEnvOr3 nf1 nf2 :=
    let if_no_unkown :=
        (NRAEnvNullApp
           (NRAEnvUnop
              OpLeft
              (NRAEnvBinop
                 OpOr
                 NRAEnvID (NRAEnvNullApp NRAEnvID  nf2))) nf1)
    in
    let if_only_n2_unknown :=
        NRAEnvHdDef (dright dunit) 
                    (NRAEnvSelect (NRAEnvEither NRAEnvID (NRAEnvConst (dbool false))) (NRAEnvUnop OpBag nf1))
    in
    let if_only_n1_unknown :=
        NRAEnvHdDef (dright dunit)        
                    (NRAEnvSelect (NRAEnvEither NRAEnvID (NRAEnvConst (dbool false))) (NRAEnvUnop OpBag nf2))
    in
    let if_two_unkown := (NRAEnvConst (dright dunit)) in
    NRAEnvApp
      (NRAEnvEither
         (NRAEnvApp (NRAEnvEither if_no_unkown if_only_n2_unknown) nf2)
         (NRAEnvApp (NRAEnvEither if_only_n1_unknown if_two_unkown) nf2))
      nf1.

  Lemma NRAEnvOr3_eq :
    forall n1 n2,
    forall b1 b2 br i dreg,        
      (forall did,nraenv_eval br i n1 dreg did = Some (bool3_to_data b1)) ->
      (forall did, nraenv_eval br i n2 dreg did = Some (bool3_to_data b2)) ->
      forall did,
        nraenv_eval br i (NRAEnvOr3 n1 n2) dreg did =
        Some
          (bool3_to_data   (Bool.orb Bool3.Bool3 b1 b2)).
  Proof.
    unfold nraenv_eval in *.
    intros n1 n2 b1 b2 br i dreg Hn1 Hn2 did.    
    cbn.
    unfold olift.
    rewrite Hn1.
    destruct b1;simpl.
    + (* [|n1|] = true3 *)
      rewrite Hn2;destruct b2.
      * simpl;rewrite Hn1;simpl;rewrite Hn2;apply eq_refl.             
      * simpl;rewrite Hn1;simpl;rewrite Hn2;apply eq_refl.        
      * (* [|n2|] = unknown3 : expected true instead of unknown *)
        cbn.
        rewrite Hn1.
        apply eq_refl.
    + (* [|n1|] = false3 *)
      rewrite Hn2;destruct b2.
      * simpl;rewrite Hn1;simpl;rewrite Hn2;apply eq_refl.             
      * simpl;rewrite Hn1;simpl;rewrite Hn2;apply eq_refl.        
      * simpl.
        rewrite Hn1.
        simpl.
        apply eq_refl.
    + (* [|n1|] = unkown3 *)
      rewrite Hn2.
      destruct b2.
      * simpl.
        rewrite Hn2.
        simpl.
        apply eq_refl.
      * (* [|n2|] = false3 : expected unkown instead of false *)
        simpl.
        rewrite Hn2;simpl.
        apply eq_refl.
      * apply eq_refl.
  Qed.

  Definition NRAEnvAnd3 nf1 nf2 :=
    let if_no_unkown :=
        (NRAEnvNullApp
           (NRAEnvUnop
              OpLeft
              (NRAEnvBinop
                 OpAnd
                 NRAEnvID (NRAEnvNullApp NRAEnvID  nf2))) nf1)
    in
    let if_only_n2_unknown := NRAEnvHdDef (dright dunit)        
                                         (NRAEnvSelect (NRAEnvEither (NRAEnvUnop OpNeg NRAEnvID) (NRAEnvConst (dbool false))) (NRAEnvUnop OpBag nf1))
    in
    let if_only_n1_unknown :=
        NRAEnvHdDef (dright dunit)        
                    (NRAEnvSelect (NRAEnvEither (NRAEnvUnop OpNeg NRAEnvID) (NRAEnvConst (dbool false))) (NRAEnvUnop OpBag nf2))
    in
    let if_two_unkown := (NRAEnvConst (dright dunit)) in
    NRAEnvApp
      (NRAEnvEither
         (NRAEnvApp (NRAEnvEither if_no_unkown if_only_n2_unknown) nf2)
         (NRAEnvApp (NRAEnvEither if_only_n1_unknown if_two_unkown) nf2))
      nf1.

  Lemma NRAEnvAnd3_eq :
    forall n1 n2,
    forall b1 b2 br i dreg,        
      (forall did,nraenv_eval br i n1 dreg did = Some (bool3_to_data b1)) ->
      (forall did, nraenv_eval br i n2 dreg did = Some (bool3_to_data b2)) ->
      forall did,
        nraenv_eval br i (NRAEnvAnd3 n1 n2) dreg did =
        Some
          (bool3_to_data   (Bool.andb Bool3.Bool3 b1 b2)).
  Proof.
    unfold nraenv_eval in *.
    intros n1 n2 b1 b2 br i dreg Hn1 Hn2 did.    
    cbn.
    unfold olift.
    rewrite Hn1.
    destruct b1;simpl.
    + rewrite Hn2;destruct b2.
      * simpl;rewrite Hn1;simpl;rewrite Hn2;apply eq_refl.             
      * simpl;rewrite Hn1;simpl;rewrite Hn2;apply eq_refl.        
      * simpl;rewrite Hn1;apply eq_refl.
    + rewrite Hn2;destruct b2.
      * simpl;rewrite Hn1;simpl;rewrite Hn2;apply eq_refl.             
      * simpl;rewrite Hn1;simpl;rewrite Hn2;apply eq_refl.        
      * simpl;rewrite Hn1;apply eq_refl.
    + rewrite Hn2.
      destruct b2;[| |apply eq_refl].
      * simpl;rewrite Hn2;apply eq_refl.        
      * simpl;rewrite Hn2;apply eq_refl.
  Qed.

  (* TODO: complete *)
  Definition well_typed_predicate
             (p: Tuple.predicate TRcd)
             (l: list (Tuple.type TRcd)) : bool :=
    match p with
    | Predicate "=" =>
      match l with
      (* | type_bool :: type_bool :: nil => true *)
      | type_Z :: type_Z :: nil => true
      | type_string :: type_string :: nil => true
      | type_float :: type_float :: nil => true
      | _ => false
      end
    | Predicate "<" =>
      match l with
      | type_Z :: type_Z :: nil => true
      | _ => false
      end
    | Predicate "<." =>
      match l with
      | type_float :: type_float :: nil => true
      | _ => false
      end
    | Predicate "<=" =>
      match l with
      | type_Z :: type_Z :: nil => true
      | _ => false
      end
    | Predicate "<=." =>
      match l with
      | type_float :: type_float :: nil => true
      | _ => false
      end
    | _ => false
    end.
  
  Definition NRAEnvIsUnknown3 nb :=
    NRAEnvApp
      (NRAEnvEither
         (NRAEnvConst (dbool false))         
         (NRAEnvConst (dbool true)))
      nb.

   Lemma NRAEnvIsUnknown3_spec :
    forall nf,
    forall b1 br i dreg did,        
      (nraenv_eval br i nf dreg did = Some (bool3_to_data b1)) ->
        nraenv_eval br i (NRAEnvIsUnknown3 nf) dreg did =
        Some
          (dbool  match b1 with
                  | unknown3 => true
                  | _ => false
                  end).
  Proof.      
    unfold nraenv_eval in *.
    intros n1 b1 br i dreg did Hn1.
    cbn.
    unfold olift.
    rewrite Hn1.
    destruct b1;apply eq_refl.
  Qed.

  Lemma NRAEnvIsUnknown3_eq :
    forall nf,
    forall b1 br i dreg,        
      (forall did,nraenv_eval br i nf dreg did = Some (bool3_to_data b1)) ->
      forall did,
        nraenv_eval br i (NRAEnvIsUnknown3 nf) dreg did =
     Some
          (dbool  match b1 with
                  | unknown3 => true
                  | _ => false
                  end).
  Proof.      
    intros n1 b1 br i dreg Hn1 did.
    exact (NRAEnvIsUnknown3_spec n1 _ _ _ _ _ (Hn1 did)).
  Qed.

  Definition NRAEnvBool3App ne nb :=
    NRAEnvApp
      (NRAEnvEither
         (NRAEnvApp ne (NRAEnvUnop OpLeft NRAEnvID))
         (NRAEnvConst (dright dunit)))
      nb.

    Lemma NRAEnvBool3App_spec :
    forall ne nb,
    forall d b br i dreg did,        
      (nraenv_eval br i ne dreg (bool3_to_data b) = Some d) ->      
      (nraenv_eval br i nb dreg did = Some (bool3_to_data b)) ->
        nraenv_eval br i (NRAEnvBool3App ne nb) dreg did =
        Some (match b with
              | unknown3 => dright dunit
              | _ => d
              end).
  Proof.      
    unfold nraenv_eval in *.
    intros n1 n2 d b2 br i dreg did H1 H2.
    cbn.
    unfold olift.
    rewrite H2.
    destruct b2;try apply eq_refl;apply H1.
  Qed.
  
  Definition NRAEnvNullPred pred nb1 nb2 :=
    let rec12 :=        
        (NRAEnvBinop 
           OpRecConcat
           (NRAEnvUnop  (OpRec "a#") nb1)
           (NRAEnvUnop  (OpRec "b#") nb2))
    in
    let ne :=
        NRAEnvBool3App
          (NRAEnvBool3App
             ((NRAEnvUnop
                OpLeft(
               NRAEnvBinop
                 pred
                 (NRAEnvApp (NRAEnvEither NRAEnvID NRAEnvID) (NRAEnvUnop  (OpDot "a#") NRAEnvEnv))             
                 (NRAEnvApp (NRAEnvEither NRAEnvID NRAEnvID) (NRAEnvUnop  (OpDot "b#") NRAEnvEnv)))
             ))
             (NRAEnvUnop  (OpDot "b#") NRAEnvEnv))
          (NRAEnvUnop  (OpDot "a#") NRAEnvEnv)
          in
    NRAEnvAppEnv
      ne       
      rec12.
  
  Definition predicate_to_nraenv_bin (p:Tuple.predicate TRcd)
             (t1 t2 : (Tuple.type TRcd)) (n1 n2 : nraenv) :=
    match p,t1,t2 with
    | Predicate "<", type_Z, type_Z =>
      NRAEnvNullPred OpLt n1 n2
    | Predicate "<.", type_float, type_float =>
      NRAEnvNullPred (OpFloatCompare FloatLt) n1 n2
    | Predicate "<=", type_Z,type_Z =>
      NRAEnvNullPred OpLe n1 n2
    | Predicate "<=.", type_float, type_float =>
      NRAEnvNullPred (OpFloatCompare FloatLe) n1 n2
    | Predicate ">", type_Z,type_Z =>
      NRAEnvNullPred OpLt n2 n1
    | Predicate ">.", type_float, type_float =>
      NRAEnvNullPred (OpFloatCompare FloatLt) n2 n1
    | Predicate ">=", type_Z,type_Z =>
      NRAEnvNullPred OpLe n2 n1
    | Predicate ">=.", type_float, type_float =>
      NRAEnvNullPred (OpFloatCompare FloatLe) n2 n1
    | Predicate "=" , _, _ =>
      match t1, t2 with
      | type_Z, type_Z
      | type_string, type_string
      | type_float, type_float => NRAEnvNullPred OpEqual n1 n2
      | _, _ => NRAEnvConst (dright dunit)
      end
    | Predicate "<>", _, _ =>
      NRAEnvNot3
        match t1, t2 with
        | type_Z, type_Z
        | type_string, type_string
        | type_float, type_float => NRAEnvNullPred OpEqual n1 n2
        | _, _ => NRAEnvConst (dright dunit)
        end
    | _,_,_ =>
      NRAEnvConst (dright dunit)
    end.
  
  Lemma predicate_to_nraenv_bin_ignores_did :
    forall X pred (x1 x2 :X),
    forall f1  f3 br i dreg did1 did2 ,
      (nraenv_eval br i (f1 x1)  dreg did1 =
       nraenv_eval br i (f1 x1)  dreg did2) ->      
      (nraenv_eval br i (f1 x2)  dreg did1 =
       nraenv_eval br i (f1 x2)  dreg did2) ->
      (* (forall ft, In ft l -> f3 ft = t) -> *)
      nraenv_eval br i (predicate_to_nraenv_bin pred (f3 x1) (f3 x2) (f1 x1) (f1 x2)) dreg did1 =
      nraenv_eval br i (predicate_to_nraenv_bin pred (f3 x1) (f3 x2) (f1 x1) (f1 x2)) dreg did2.
  Proof.
    unfold nraenv_eval.
    destruct pred;intros.
    repeat (destruct s as [ |[[ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ]] s];try apply eq_refl).
    - (* = *)
      cbn.
      destruct  (f3 x1),(f3 x2) eqn:D1;try apply eq_refl.
      + cbn.
        unfold olift,olift2.
        rewrite H,H0;apply eq_refl.
      + cbn;rewrite H,H0;apply eq_refl.
      + cbn;rewrite H,H0;apply eq_refl.
    - (* > *)
      simpl.
      destruct  (f3 x1) eqn:D1;[apply eq_refl| |apply eq_refl|apply eq_refl].
      destruct  (f3 x2) eqn:D2;[apply eq_refl| |apply eq_refl|apply eq_refl].      
      simpl.
      unfold olift.
      rewrite H0,H;apply eq_refl.
    - (* >= *)
      simpl.
      destruct  (f3 x1) eqn:D1;[apply eq_refl| |apply eq_refl|apply eq_refl].
      destruct  (f3 x2) eqn:D2;[apply eq_refl| |apply eq_refl|apply eq_refl].      
      simpl.
      unfold olift.
      rewrite H0,H;apply eq_refl.
    - (* >=. *)
      simpl.
      destruct (f3 x1) eqn:D1;[apply eq_refl|apply eq_refl|apply eq_refl| ].
      destruct (f3 x2) eqn:D2;[apply eq_refl|apply eq_refl|apply eq_refl| ].
      simpl.
      unfold olift.
      rewrite H0,H;apply eq_refl.
    - (* >. *)
      simpl.
      destruct (f3 x1) eqn:D1;[apply eq_refl|apply eq_refl|apply eq_refl| ].
      destruct (f3 x2) eqn:D2;[apply eq_refl|apply eq_refl|apply eq_refl| ].
      simpl.
      unfold olift.
      rewrite H0,H;apply eq_refl.
    - (* < *)
      simpl.
      destruct  (f3 x1) eqn:D1;[apply eq_refl| |apply eq_refl|apply eq_refl].
      destruct  (f3 x2) eqn:D2;[apply eq_refl| |apply eq_refl|apply eq_refl].
      cbn.
      rewrite H0,H;apply eq_refl.
    - (* <= *)
      simpl.
      destruct  (f3 x1) eqn:D1;[apply eq_refl| |apply eq_refl|apply eq_refl].
      destruct  (f3 x2) eqn:D2;[apply eq_refl| |apply eq_refl|apply eq_refl].
      cbn.
      rewrite H0,H;apply eq_refl.
    - (* <=. *)
      simpl.
      destruct (f3 x1) eqn:D1;[apply eq_refl|apply eq_refl|apply eq_refl| ].
      destruct (f3 x2) eqn:D2;[apply eq_refl|apply eq_refl|apply eq_refl| ].
      simpl.
      unfold olift.
      rewrite H0,H;apply eq_refl.
    - (* <> *)
      cbn.
      destruct  (f3 x1),(f3 x2) eqn:D1;try apply eq_refl.
      + cbn;rewrite H,H0;apply eq_refl.
      + cbn;rewrite H,H0;apply eq_refl.         
      + cbn;rewrite H,H0;apply eq_refl.
    - (* <. *)
      simpl.
      destruct (f3 x1) eqn:D1;[apply eq_refl|apply eq_refl|apply eq_refl| ].
      destruct (f3 x2) eqn:D2;[apply eq_refl|apply eq_refl|apply eq_refl| ].
      simpl.
      unfold olift.
      rewrite H0,H;apply eq_refl.
  Qed.
    
  Lemma predicate_to_nraenv_bin_eq :
    forall X pred (x1 x2 :X),
    forall f1 f2 f3 br i dreg did,
      (
          nraenv_eval br i (f1 x1)  dreg did =
          Some
            (value_to_data  (f2 x1))) ->      
      (
          nraenv_eval br i (f1 x2)  dreg did =
          Some
            (value_to_data  (f2 x2))) ->
      (f3 x1 = Tuple.type_of_value TNull (f2 x1)) ->                         
      (f3 x2 = Tuple.type_of_value TNull (f2 x2)) ->                  
        nraenv_eval br i (predicate_to_nraenv_bin pred (f3 x1) (f3 x2) (f1 x1) (f1 x2)) dreg did =
        Some
          (bool3_to_data   (interp_predicate TRcd pred ((f2 x1)::(f2 x2)::nil))).
  Proof.
    destruct pred;intros a1 a2;intros.
    repeat (destruct s as [ |[[ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ]] s];try apply eq_refl).
    - (* = *)
      cbn;rewrite H1,H2.
      destruct  (f2 a1) eqn:D1;destruct o;try apply eq_refl.
      + cbn.
        destruct  (f2 a2) eqn:D2;try apply eq_refl.
        simpl;unfold olift,olift2.
        unfold nraenv_eval in H,H0.
        rewrite H;simpl;rewrite H0.
        simpl.
        destruct o;[|apply eq_refl].        
        simpl.
        { destruct string_dec.
          - subst.
            rewrite string_compare_refl.
            apply eq_refl.
          - destruct string_compare eqn:D3;try apply eq_refl.
            rewrite string_string_order_compare in D3.
            rewrite StringOrder.compare_eq_iff in D3.
            destruct (n D3).
        }
      + cbn.
        destruct  (f2 a2) eqn:D2;try apply eq_refl.
        simpl;unfold olift,olift2.
        unfold nraenv_eval in H,H0.
        rewrite H,H0;apply eq_refl.
      +cbn.
       destruct  (f2 a2) eqn:D2;try apply eq_refl.
       simpl;unfold olift,olift2.
       unfold nraenv_eval in H,H0.
       rewrite H;simpl;rewrite H0.
       destruct o;[|apply eq_refl].
       simpl.
       { destruct Z.eq_dec.
         - subst.
           rewrite Z.compare_refl.
           apply eq_refl.
         - destruct Z.compare eqn:D3;try apply eq_refl.
           rewrite Z.compare_eq_iff in D3.
           destruct (n D3).
       }
      + cbn.
        destruct  (f2 a2) eqn:D2;try apply eq_refl.
        simpl;unfold olift2,olift.
        unfold nraenv_eval in H,H0.
        rewrite H,H0;simpl;apply eq_refl.
      + cbn.
        destruct  (f2 a2) eqn:D2;try apply eq_refl.
        simpl;unfold olift,olift2.
        unfold nraenv_eval in H,H0.
        rewrite H;simpl;rewrite H0.
        simpl.
        destruct o;[|apply eq_refl].
        simpl. now destruct float_eq_dec.
      + cbn.
        destruct  (f2 a2) eqn:D2;try apply eq_refl.
        simpl;unfold olift,olift2.
        unfold nraenv_eval in H,H0.
        rewrite H,H0;apply eq_refl.
    - (* > *)
      simpl.
      rewrite H1,H2;clear H1 H2.
      destruct  (f2 a1) eqn:D1;[apply eq_refl| |apply eq_refl|apply eq_refl].
      simpl.
      destruct  (f2 a2) eqn:D2;
        [destruct o;apply eq_refl
        | |destruct o;apply eq_refl|destruct o;apply eq_refl].
      cbn.
      unfold nraenv_eval in H,H0.
      rewrite H0;simpl.
      unfold olift,olift2.
      rewrite H.
      vm_compute assoc_lookupr.
      destruct o,o0;try apply eq_refl;simpl.
      destruct Z_lt_dec.
        * rewrite <- Z.compare_gt_iff in l.
          rewrite l;apply eq_refl.
        * apply Z.compare_ngt_iff in n.
          destruct Z.compare;try apply eq_refl; (destruct (n eq_refl)).
    - (* >= *)
      cbn.
      unfold nraenv_eval in H,H0.      
      rewrite H1,H2.
      destruct  (f2 a1) eqn:D1;[apply eq_refl| |apply eq_refl|apply eq_refl].
      simpl.
      destruct  (f2 a2) eqn:D2;
        [destruct o;apply eq_refl
        | |destruct o;apply eq_refl|destruct o;apply eq_refl].
      simpl.
      unfold olift,olift2.
      rewrite H0,H;simpl.
      destruct o,o0;try apply eq_refl;simpl.
        destruct Z_le_dec.
      + apply Z.compare_ge_iff in l.
           destruct Z.compare;try apply eq_refl; (destruct (l eq_refl)).
      + apply Z.compare_nge_iff in n.
        rewrite n;apply eq_refl.
    - (* >=. *)
      cbn.
      unfold nraenv_eval in H,H0.
      rewrite H1,H2.
      destruct  (f2 a1) eqn:D1;[apply eq_refl|apply eq_refl|apply eq_refl| ].
      simpl.
      destruct  (f2 a2) eqn:D2;
        [destruct o;apply eq_refl
        |destruct o;apply eq_refl|destruct o;apply eq_refl| ].
      simpl.
      unfold olift,olift2.
      rewrite H0,H;simpl.
      destruct o,o0;try apply eq_refl;simpl.
      now destruct float_le.
    - (* >. *)
      cbn.
      unfold nraenv_eval in H,H0.
      rewrite H1,H2.
      destruct  (f2 a1) eqn:D1;[apply eq_refl|apply eq_refl|apply eq_refl| ].
      simpl.
      destruct  (f2 a2) eqn:D2;
        [destruct o;apply eq_refl
        |destruct o;apply eq_refl|destruct o;apply eq_refl| ].
      simpl.
      unfold olift,olift2.
      rewrite H0,H;simpl.
      destruct o,o0;try apply eq_refl;simpl.
      now destruct float_lt.
    - (* < *)
      cbn.
      unfold nraenv_eval in H,H0.      
      rewrite H1,H2.
      destruct  (f2 a1) eqn:D1;[apply eq_refl| |apply eq_refl|apply eq_refl].
      simpl.
      destruct  (f2 a2) eqn:D2;
        [destruct o;apply eq_refl
        | |destruct o;apply eq_refl|destruct o;apply eq_refl].
      simpl.
      unfold olift,olift2.
      rewrite H,H0;simpl.
      destruct o,o0;try apply eq_refl;simpl.
        destruct Z_lt_dec.
        + rewrite <- Z.compare_gt_iff in l.
          rewrite Z.compare_antisym.
          rewrite l;apply eq_refl.
        + apply Z.compare_nlt_iff in n.
          destruct Z.compare;try apply eq_refl; (destruct (n eq_refl)).
    - (* <= *)
      cbn.
      unfold nraenv_eval in H,H0.      
      rewrite H1,H2.
      destruct  (f2 a1) eqn:D1;[apply eq_refl| |apply eq_refl|apply eq_refl].
      simpl.
      destruct  (f2 a2) eqn:D2;
        [destruct o;apply eq_refl
        | |destruct o;apply eq_refl|destruct o;apply eq_refl].
      simpl.
      unfold olift.
      rewrite H,H0;simpl.
      destruct o,o0;try apply eq_refl;simpl.
        destruct Z_le_dec.
        + apply Z.compare_le_iff in l.
          destruct Z.compare;try apply eq_refl; (destruct (l eq_refl)).
        + apply Z.compare_nle_iff in n.
          rewrite n;apply eq_refl.      
    - (* <=. *)
      cbn.
      unfold nraenv_eval in H,H0.
      rewrite H1,H2.
      destruct  (f2 a1) eqn:D1;[apply eq_refl|apply eq_refl|apply eq_refl| ].
      simpl.
      destruct  (f2 a2) eqn:D2;
        [destruct o;apply eq_refl
        |destruct o;apply eq_refl|destruct o;apply eq_refl| ].
      simpl.
      unfold olift,olift2.
      rewrite H0,H;simpl.
      destruct o,o0;try apply eq_refl;simpl.
      now destruct float_le.
    - (* <> *)
      cbn.
      rewrite H1,H2.
      destruct  (f2 a1) eqn:D1;destruct o;try apply eq_refl.
      + cbn.
        destruct  (f2 a2) eqn:D2;try apply eq_refl.
        simpl;unfold olift.
        unfold nraenv_eval in H,H0.
        rewrite H;simpl.
        rewrite H0;simpl.
        destruct o;[|apply eq_refl].
        simpl.
        { destruct string_dec.
          - subst.
            rewrite string_compare_refl.
            apply eq_refl.
          - destruct string_compare eqn:D3;try apply eq_refl.
            rewrite string_string_order_compare in D3.
            rewrite StringOrder.compare_eq_iff in D3.
            destruct (n D3).
        }
      + cbn.
        destruct  (f2 a2) eqn:D2;try apply eq_refl.
        simpl;unfold olift,olift2.
        unfold nraenv_eval in H,H0.
        rewrite H,H0;apply eq_refl.
      + cbn.
        destruct  (f2 a2) eqn:D2;try apply eq_refl.
        simpl;unfold olift,olift2.
        unfold nraenv_eval in H,H0.
        rewrite H.
        simpl.
        rewrite H0;simpl.
        destruct o;[|apply eq_refl].
        simpl.
        { destruct Z.eq_dec.
          - subst.
            rewrite Z.compare_refl.
            apply eq_refl.
          - destruct Z.compare eqn:D3;try apply eq_refl.
            rewrite Z.compare_eq_iff in D3.
            destruct (n D3).
        }
      + cbn.
        destruct  (f2 a2) eqn:D2;try apply eq_refl.
        simpl;unfold olift,olift2.
        unfold nraenv_eval in H,H0.
        rewrite H,H0;apply eq_refl.        
      + cbn.
        destruct  (f2 a2) eqn:D2;try apply eq_refl.
        simpl;unfold olift,olift2.
        unfold nraenv_eval in H,H0.
        rewrite H.
        simpl.
        rewrite H0;simpl.
        destruct o;[|apply eq_refl].
        simpl.
        now destruct float_eq_dec.
      + cbn.
        destruct  (f2 a2) eqn:D2;try apply eq_refl.
        simpl;unfold olift,olift2.
        unfold nraenv_eval in H,H0.
        rewrite H,H0;apply eq_refl.
    - (* <. *)
      cbn.
      unfold nraenv_eval in H,H0.
      rewrite H1,H2.
      destruct  (f2 a1) eqn:D1;[apply eq_refl|apply eq_refl|apply eq_refl| ].
      simpl.
      destruct  (f2 a2) eqn:D2;
        [destruct o;apply eq_refl
        |destruct o;apply eq_refl|destruct o;apply eq_refl| ].
      simpl.
      unfold olift,olift2.
      rewrite H0,H;simpl.
      destruct o,o0;try apply eq_refl;simpl.
      now destruct float_lt.
  Qed.
  
  Lemma predicate_to_nraenv_bin_eq_2 :
    forall X Y pred (lx:X) (ly:  Y),
    forall nraenv_of_X value_of_X type_of_X,
    forall nraenv_of_Y value_of_Y type_of_Y,
    forall br i dreg did,
      (
          nraenv_eval br i (nraenv_of_X lx)  dreg did =
          Some (value_to_data  (value_of_X lx))) ->
      ( type_of_X lx = Tuple.type_of_value TRcd (value_of_X lx)) ->
      (nraenv_eval br i (nraenv_of_Y ly)  dreg did =
          Some (value_to_data  (value_of_Y ly))) ->
      (type_of_Y ly = Tuple.type_of_value TRcd (value_of_Y ly)) ->
      
        nraenv_eval
          br i
          (predicate_to_nraenv_bin
             pred (type_of_X lx) (type_of_Y ly) (nraenv_of_X lx) (nraenv_of_Y ly)) dreg did =
        Some(boolB_to_data   (interp_predicate TRcd pred (value_of_X lx :: value_of_Y ly :: nil))).
  Proof.
    unfold nraenv_eval.
    simpl TRcd.
    destruct pred;intros a1 a2;intros.
    repeat (destruct s as [ |[[ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ]] s];try apply eq_refl).
    - (* = *)
      cbn;rewrite H0,H2.
      destruct  (value_of_X a1) eqn:D1;destruct o;try apply eq_refl.
      + cbn.
        destruct  (value_of_Y a2) eqn:D2;try apply eq_refl.
        simpl;unfold olift.
        unfold nraenv_eval in *.
        rewrite H;simpl;rewrite H1.
        simpl.
        destruct o;[|apply eq_refl].        
        simpl.
        { destruct string_dec.
          - subst.
            rewrite string_compare_refl.
            apply eq_refl.
          - destruct string_compare eqn:D3;try apply eq_refl.
            rewrite string_string_order_compare in D3.
            rewrite StringOrder.compare_eq_iff in D3.
            destruct (n D3).
        }
      + cbn.
        destruct  (value_of_Y a2) eqn:D2;try apply eq_refl.
        simpl;unfold olift.
        unfold nraenv_eval in *.
        rewrite H,H1;simpl;apply eq_refl.
      +cbn.
       destruct  (value_of_Y a2) eqn:D2;try apply eq_refl.
       simpl;unfold olift.
       unfold nraenv_eval in H,H0.
       rewrite H;simpl;rewrite H1.
       destruct o;[|apply eq_refl].
       simpl.
       { destruct Z.eq_dec.
         - subst.
           rewrite Z.compare_refl.
           apply eq_refl.
         - destruct Z.compare eqn:D3;try apply eq_refl.
           rewrite Z.compare_eq_iff in D3.
           destruct (n D3).
       }
      + cbn.
        destruct  (value_of_Y a2) eqn:D2;try apply eq_refl.
        simpl;unfold olift.
        unfold nraenv_eval in H.
        rewrite H,H1;simpl;apply eq_refl.
      + cbn.
        destruct  (value_of_Y a2) eqn:D2;try apply eq_refl.
        simpl;unfold olift.
        unfold nraenv_eval in H,H0.
        rewrite H;simpl;rewrite H1.
        destruct o;[|apply eq_refl].
        simpl.
        now destruct float_eq_dec.
      + cbn.
        destruct  (value_of_Y a2) eqn:D2;try apply eq_refl.
        simpl;unfold olift.
        unfold nraenv_eval in H.
        rewrite H,H1;simpl;apply eq_refl.
    - (* > *)
      simpl.
      rewrite H0,H2;clear H0 H2.
      destruct  (value_of_X a1) eqn:D1;[apply eq_refl| |apply eq_refl|apply eq_refl].
      simpl.
      destruct  (value_of_Y  a2) eqn:D2;
        [destruct o;apply eq_refl
        | |destruct o;apply eq_refl|destruct o;apply eq_refl].
      cbn.
      rewrite H1,H;simpl.
      destruct o,o0;try apply eq_refl;simpl.
        simpl.
        unfold unbdnat.
        destruct Z_lt_dec.
        * rewrite <- Z.compare_gt_iff in l.
          rewrite l;apply eq_refl.
        * apply Z.compare_ngt_iff in n.
          destruct Z.compare;try apply eq_refl; (destruct (n eq_refl)).
    - (* >= *)
      cbn.
      unfold nraenv_eval in H,H0.      
      rewrite H0,H2.
      destruct  (value_of_X a1) eqn:D1;[apply eq_refl| |apply eq_refl|apply eq_refl].
      simpl.
      destruct  (value_of_Y a2) eqn:D2;
        [destruct o;apply eq_refl
        | |destruct o;apply eq_refl|destruct o;apply eq_refl].
      simpl.
      unfold olift.
      rewrite H1,H;simpl.
      destruct o,o0;try apply eq_refl;simpl.
        simpl.
        destruct Z_le_dec.
        -- apply Z.compare_ge_iff in l.
           destruct Z.compare;try apply eq_refl; (destruct (l eq_refl)).
        -- apply Z.compare_nge_iff in n.
           rewrite n;apply eq_refl.
    - (* >=. *)
      cbn.
      unfold nraenv_eval in H,H0.
      rewrite H0,H2.
      destruct  (value_of_X a1) eqn:D1;[apply eq_refl|apply eq_refl|apply eq_refl| ].
      simpl.
      destruct  (value_of_Y a2) eqn:D2;
        [destruct o;apply eq_refl
        |destruct o;apply eq_refl|destruct o;apply eq_refl| ].
      simpl.
      unfold olift.
      rewrite H1,H;simpl.
      destruct o,o0;try apply eq_refl;simpl.
      simpl.
      now destruct float_le.
    - (* >. *)
      cbn.
      unfold nraenv_eval in H,H0.
      rewrite H0,H2.
      destruct  (value_of_X a1) eqn:D1;[apply eq_refl|apply eq_refl|apply eq_refl| ].
      simpl.
      destruct  (value_of_Y a2) eqn:D2;
        [destruct o;apply eq_refl
        |destruct o;apply eq_refl|destruct o;apply eq_refl| ].
      simpl.
      unfold olift.
      rewrite H1,H;simpl.
      destruct o,o0;try apply eq_refl;simpl.
      simpl.
      now destruct float_lt.
    - (* < *)
      cbn.
      unfold nraenv_eval in H,H0.      
      rewrite H0,H2.
      destruct  (value_of_X a1) eqn:D1;[apply eq_refl| |apply eq_refl|apply eq_refl].
      simpl.
      destruct  (value_of_Y a2) eqn:D2;
        [destruct o;apply eq_refl
        | |destruct o;apply eq_refl|destruct o;apply eq_refl].
      simpl.
      unfold olift.
      rewrite H,H1;simpl.
      destruct o,o0;try apply eq_refl;simpl.
        destruct Z_lt_dec.
        * rewrite <- Z.compare_gt_iff in l.
          rewrite Z.compare_antisym.
          rewrite l;apply eq_refl.
        * apply Z.compare_nlt_iff in n.
          destruct Z.compare;try apply eq_refl; (destruct (n eq_refl)).
    - (* <= *)
      cbn.
      unfold nraenv_eval in H,H0.      
      rewrite H0,H2.
      destruct  (value_of_X a1) eqn:D1;[apply eq_refl| |apply eq_refl|apply eq_refl].
      simpl.
      destruct  (value_of_Y a2) eqn:D2;
        [destruct o;apply eq_refl
        | |destruct o;apply eq_refl|destruct o;apply eq_refl].
      simpl.
      unfold olift.
      rewrite H1,H;simpl.
      destruct o,o0;try apply eq_refl;simpl.
        destruct Z_le_dec.
        * apply Z.compare_le_iff in l.
          destruct Z.compare;try apply eq_refl; (destruct (l eq_refl)).
        * apply Z.compare_nle_iff in n.
          rewrite n;apply eq_refl.      
    - (* <=. *)
      cbn.
      unfold nraenv_eval in H,H0.
      rewrite H0,H2.
      destruct  (value_of_X a1) eqn:D1;[apply eq_refl|apply eq_refl|apply eq_refl| ].
      simpl.
      destruct  (value_of_Y a2) eqn:D2;
        [destruct o;apply eq_refl
        |destruct o;apply eq_refl|destruct o;apply eq_refl| ].
      simpl.
      unfold olift.
      rewrite H1,H;simpl.
      destruct o,o0;try apply eq_refl;simpl.
      simpl.
      now destruct float_le.
    - (* <> *)
      cbn.
      rewrite H0,H2.
      destruct  (value_of_X a1) eqn:D1;destruct o;try apply eq_refl.
      + cbn.
        destruct  (value_of_Y a2) eqn:D2;try apply eq_refl.
        simpl;unfold olift.
        unfold nraenv_eval in H,H0.
        rewrite H;simpl.
        rewrite H1;simpl.
        destruct o;[|apply eq_refl].
        simpl.
        { destruct string_dec.
          - subst.
            rewrite string_compare_refl.
            apply eq_refl.
          - destruct string_compare eqn:D3;try apply eq_refl.
            rewrite string_string_order_compare in D3.
            rewrite StringOrder.compare_eq_iff in D3.
            destruct (n D3).
        }
      + cbn.
        destruct  (value_of_Y a2) eqn:D2;try apply eq_refl.
        simpl;unfold olift.
        unfold nraenv_eval in H.
        rewrite H,H1;apply eq_refl.
      + cbn.
        destruct  (value_of_Y a2) eqn:D2;try apply eq_refl.
        simpl;unfold olift.
        unfold nraenv_eval in H,H0.
        rewrite H.
        simpl.
        rewrite H1;simpl.
        destruct o;[|apply eq_refl].
        simpl.
        { destruct Z.eq_dec.
          - subst.
            rewrite Z.compare_refl.
            apply eq_refl.
          - destruct Z.compare eqn:D3;try apply eq_refl.
            rewrite Z.compare_eq_iff in D3.
            destruct (n D3).
        }
      + cbn.
        destruct  (value_of_Y a2) eqn:D2;try apply eq_refl.
        simpl;unfold olift.
        unfold nraenv_eval in H.
        rewrite H,H1;apply eq_refl.        
      + cbn.
        destruct  (value_of_Y a2) eqn:D2;try apply eq_refl.
        simpl;unfold olift.
        unfold nraenv_eval in H,H0.
        rewrite H.
        simpl.
        rewrite H1;simpl.
        destruct o;[|apply eq_refl].
        simpl.
        now destruct float_eq_dec.
      + cbn.
        destruct  (value_of_Y a2) eqn:D2;try apply eq_refl.
        simpl;unfold olift.
        unfold nraenv_eval in H.
        rewrite H,H1;apply eq_refl.
    - (* <. *)
      cbn.
      unfold nraenv_eval in H,H0.
      rewrite H0,H2.
      destruct  (value_of_X a1) eqn:D1;[apply eq_refl|apply eq_refl|apply eq_refl| ].
      simpl.
      destruct  (value_of_Y a2) eqn:D2;
        [destruct o;apply eq_refl
        |destruct o;apply eq_refl|destruct o;apply eq_refl| ].
      simpl.
      unfold olift.
      rewrite H1,H;simpl.
      destruct o,o0;try apply eq_refl;simpl.
      simpl.
      now destruct float_lt.
  Qed.
  
  Definition predicate_to_nraenv (p:Tuple.predicate TRcd)
             (ltype : list (Tuple.type TRcd)) (lnra : list nraenv) :=
    match ltype,lnra with
    |  t1::t2::nil, n1::n2::nil =>
       predicate_to_nraenv_bin p t1 t2 n1 n2
    | _,_ =>
      NRAEnvConst (dright dunit)
    end.
  
  Lemma predicate_to_nraenv_ignores_did :
    forall X pred (l:list X),
    forall f1  f3 br i dreg did1 did2 ,
      (forall x,
          In x l -> 
          nraenv_eval br i (f1 x)  dreg did1 =
          nraenv_eval br i (f1 x)  dreg did2 ) ->
      (* (forall ft, In ft l -> f3 ft = t) -> *)
      nraenv_eval br i (predicate_to_nraenv pred (map f3 l) (map f1 l)) dreg did1 =
      nraenv_eval br i (predicate_to_nraenv pred (map f3 l) (map f1 l)) dreg did2.
  Proof.
    unfold nraenv_eval.
    do 10 intro.
    intros Hx.
    destruct l as [|a1 l];[apply eq_refl| ].
    destruct l as [|a2 l];[apply eq_refl| ].
    destruct l as [|a3 l];[|apply eq_refl].
    refine
      (predicate_to_nraenv_bin_ignores_did
         _ _ _ _ _ _ _ _ _ _ _ _ _).
    apply Hx;left;apply eq_refl.
    apply Hx;right;left;apply eq_refl.
  Qed.

  Lemma predicate_to_nraenv_eq :
    forall X pred (l:list X),
    forall f1 f2 f3 br i dreg,(forall x,
                             In x l -> forall did,
                               nraenv_eval br i (f1 x)  dreg did =
                               Some
                                 (value_to_data  (f2 x))) ->
                         (forall ft, In ft l -> f3 ft = Tuple.type_of_value TNull (f2 ft)) ->            
                         forall did,
                           nraenv_eval br i (predicate_to_nraenv pred (map f3 l) (map f1 l)) dreg did =
                           Some
                             (bool3_to_data   (interp_predicate TRcd pred (map f2 l))).
  Proof.
    unfold nraenv_eval.
    do 10 intro.
    intros Hx did.
    destruct pred as [s].
    destruct l as [|a1 l].
    - repeat (destruct s as [ |[[ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ]] s];try apply eq_refl).
    - destruct l as [|a2 l].
      + repeat (destruct s as [ |[[ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ]] s];try apply eq_refl);
          cbn;destruct (f2 a1);destruct o;apply eq_refl.          
      + destruct l as [|a3 l].
        * refine
            (predicate_to_nraenv_bin_eq
               _ _ _ _ _ _ _ _ _ _ _ _ _ _ _).
          apply H;left;apply eq_refl.
          apply H;right;left;apply eq_refl.
          apply Hx;left;apply eq_refl.
          apply Hx;right;left;apply eq_refl.
        * repeat (destruct s as [ |[[ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ]] s];try apply eq_refl);
            cbn;destruct (f2 a1),(f2 a2);destruct o,o0;apply eq_refl.
  Qed.
  
  Definition predicate_to_nraenv_quant (p:Tuple.predicate TRcd)
             (ltype : list (Tuple.type TRcd)) (lnra : list nraenv)
             (* (qtype : list (Tuple.type TRcd)) *)
             (sa : Fset.set (A TNull) ) (nq : nraenv) :=
    match ltype,lnra, ({{{sa}}}) with
    |  t1::t2::nil,n1::n2::nil,nil =>
       NRAEnvMap (predicate_to_nraenv_bin p t1 t2 n1 n2) nq
    | t1::nil,n1::nil,tq::nil =>      
      NRAEnvMap
        (predicate_to_nraenv_bin
           p t1 (type_of_attribute _ tq) n1
           (NRAEnvUnop (OpDot (attribute_to_string tq)) NRAEnvID))
        nq
    | nil,nil,t1::t2::nil =>
      NRAEnvMap
        (predicate_to_nraenv_bin
           p  (type_of_attribute _ t1)  (type_of_attribute _ t2)           
           (NRAEnvUnop (OpDot (attribute_to_string t1)) NRAEnvID)
           (NRAEnvUnop (OpDot (attribute_to_string t2)) NRAEnvID))
        nq
    | _,_, _ =>    
      NRAEnvMap  (NRAEnvConst (dright dunit)) nq
    end.

  Lemma predicate_to_nraenv_quant_ignores_did :
    forall X pred (lx:list X),
    forall f1  f3 br i dreg did1 did2 s n,
      (
        nraenv_eval br i n  dreg did1 =
        nraenv_eval br i n  dreg did2 ) ->
      (* (forall x, *)
      (*     In x lx ->  *)
      (*     nraenv_eval br i (f1 x)  dreg did1 = *)
      (*     nraenv_eval br i (f1 x)  dreg did2 ) -> *)
      (* (forall ft, In ft l -> f3 ft = t) -> *)
      nraenv_eval br i (predicate_to_nraenv_quant pred (map f3 lx) (map f1 lx) s n) dreg did1 =
      nraenv_eval br i (predicate_to_nraenv_quant pred (map f3 lx) (map f1 lx) s n) dreg did2.
  Proof.
    unfold nraenv_eval.
    do 12 intro.
    intros Hn.
    unfold predicate_to_nraenv_quant.
    destruct lx as [|a1 lx],
                   ({{{s}}}) as [|a2 ls].
    - cbn;rewrite Hn;apply eq_refl.
    - cbn.
      destruct ls as [|a3 ls].
      cbn;rewrite Hn;apply eq_refl.
      destruct ls as [|a4 ls].
      cbn;rewrite Hn;apply eq_refl.
      cbn;rewrite Hn;apply eq_refl.
    - simpl.
      destruct lx as [|a6 lx].
      cbn;rewrite Hn;apply eq_refl.
      destruct lx as [|a7 lx].
      cbn;rewrite Hn;apply eq_refl.
      cbn;rewrite Hn;apply eq_refl.
    - simpl.
      destruct lx as [|a6 lx];cbn.
      + destruct ls as [|a3 ls].
        cbn;rewrite Hn;apply eq_refl.
        cbn;rewrite Hn;apply eq_refl.
      + destruct lx as [|a8 lx];cbn;rewrite Hn;apply eq_refl.
  Qed.

  Lemma predicate_to_nraenv_quant_eq_aux :
    forall X  pred ,
    forall nraenv_of_X  type_of_X,
    forall br i dreg t a (a1: X),
    (br
       ⊢ predicate_to_nraenv_bin pred (type_of_X a1) (GenericInstance.type_of_attribute a)
       (nraenv_of_X a1)
       ((fun x : attribute TNull =>
           NRAEnvUnop (OpDot (TnullTD.attribute_to_string x)) (NRAEnvConst (drec (tuple_as_dpairs t)))) a) @ₓ
       drec (tuple_as_dpairs t) ⊣ i; dreg)%nraenv =
    (br
       ⊢ predicate_to_nraenv_bin pred (type_of_X a1) (GenericInstance.type_of_attribute a)
       (nraenv_of_X a1)
       ((fun x : attribute TNull =>
           NRAEnvUnop (OpDot (TnullTD.attribute_to_string x)) NRAEnvID) a) @ₓ
       drec (tuple_as_dpairs t) ⊣ i; dreg)%nraenv.
  Proof.
    unfold nraenv_eval.
    intros.
    unfold predicate_to_nraenv_bin,NRAEnvNullPred.    
    destruct pred.
    repeat (destruct s as [ |[[ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ]] s];try apply eq_refl).
    - (* = *)
      simpl.
      destruct type_of_X;try apply eq_refl.
      + destruct GenericInstance.type_of_attribute;try apply eq_refl.
        simpl.
        apply f_equal.
        unfold olift2,olift.
        destruct nraenv_core_eval eqn:D1;[|apply eq_refl].
        apply eq_sym.
        apply BasicFacts.match_option_eq.        
        apply BasicFacts.match_option_eq.
        assert (X1 := dot_OpDot_NRAEnvID_eq br dreg i t a (drec (tuple_as_dpairs t))).
        unfold nraenv_eval in X1.
        simpl in X1.
        apply eq_sym.
        apply X1.
      + destruct GenericInstance.type_of_attribute;try apply eq_refl.
        simpl.
        apply f_equal.
        unfold olift2,olift.
        destruct nraenv_core_eval eqn:D1;[|apply eq_refl].
        apply eq_sym.
        apply BasicFacts.match_option_eq.        
        apply BasicFacts.match_option_eq.
        assert (X1 := dot_OpDot_NRAEnvID_eq br dreg i t a (drec (tuple_as_dpairs t))).
        unfold nraenv_eval in X1.
        simpl in X1.
        apply eq_sym.
        apply X1.
      + destruct GenericInstance.type_of_attribute;try apply eq_refl.
        simpl.
        apply f_equal.
        unfold olift2,olift.
        destruct nraenv_core_eval eqn:D1;[|apply eq_refl].
        apply eq_sym.
        apply BasicFacts.match_option_eq.
        apply BasicFacts.match_option_eq.
        assert (X1 := dot_OpDot_NRAEnvID_eq br dreg i t a (drec (tuple_as_dpairs t))).
        unfold nraenv_eval in X1.
        simpl in X1.
        apply eq_sym.
        apply X1.
    - (* > *)
      simpl.
      destruct type_of_X;try apply eq_refl.
      + destruct GenericInstance.type_of_attribute;try apply eq_refl.
        simpl.
        apply f_equal.
        unfold olift2,olift.       
        apply BasicFacts.match_option_eq.        
        apply BasicFacts.match_option_eq.
        assert (X1 := dot_OpDot_NRAEnvID_eq br dreg i t a (drec (tuple_as_dpairs t))).
        unfold nraenv_eval in X1.
        simpl in X1.
        apply X1.
    - (* >= *)
      simpl.
      destruct type_of_X;try apply eq_refl.
      + destruct GenericInstance.type_of_attribute;try apply eq_refl.
        simpl.
        apply f_equal.
        unfold olift2,olift.       
        apply BasicFacts.match_option_eq.        
        apply BasicFacts.match_option_eq.
        assert (X1 := dot_OpDot_NRAEnvID_eq br dreg i t a (drec (tuple_as_dpairs t))).
        unfold nraenv_eval in X1.
        simpl in X1.
        apply X1.
    - (* >=. *)
      simpl.
      destruct type_of_X;try apply eq_refl.
      + destruct GenericInstance.type_of_attribute;try apply eq_refl.
        simpl.
        apply f_equal.
        unfold olift2,olift.
        apply BasicFacts.match_option_eq.
        apply BasicFacts.match_option_eq.
        assert (X1 := dot_OpDot_NRAEnvID_eq br dreg i t a (drec (tuple_as_dpairs t))).
        unfold nraenv_eval in X1.
        simpl in X1.
        apply X1.
    - (* >. *)
      simpl.
      destruct type_of_X;try apply eq_refl.
      + destruct GenericInstance.type_of_attribute;try apply eq_refl.
        simpl.
        apply f_equal.
        unfold olift2,olift.
        apply BasicFacts.match_option_eq.
        apply BasicFacts.match_option_eq.
        assert (X1 := dot_OpDot_NRAEnvID_eq br dreg i t a (drec (tuple_as_dpairs t))).
        unfold nraenv_eval in X1.
        simpl in X1.
        apply X1.
    - (* < *)
      simpl.
      destruct type_of_X;try apply eq_refl.
      + destruct GenericInstance.type_of_attribute;try apply eq_refl.
        simpl.
        apply f_equal.
        unfold olift2,olift.
        destruct nraenv_core_eval eqn:D1;[|apply eq_refl].
        apply eq_sym.
        apply BasicFacts.match_option_eq.        
        apply BasicFacts.match_option_eq.
        assert (X1 := dot_OpDot_NRAEnvID_eq br dreg i t a (drec (tuple_as_dpairs t))).
        unfold nraenv_eval in X1.
        simpl in X1.
        apply eq_sym.
        apply X1.
    - (* <= *)
      simpl.
      destruct type_of_X;try apply eq_refl.
      + destruct GenericInstance.type_of_attribute;try apply eq_refl.
        simpl.
        apply f_equal.
        unfold olift2,olift.
        destruct nraenv_core_eval eqn:D1;[|apply eq_refl].
        apply eq_sym.
        apply BasicFacts.match_option_eq.        
        apply BasicFacts.match_option_eq.
        assert (X1 := dot_OpDot_NRAEnvID_eq br dreg i t a (drec (tuple_as_dpairs t))).
        unfold nraenv_eval in X1.
        simpl in X1.
        apply eq_sym.
        apply X1.
    - (* <=. *)
      simpl.
      destruct type_of_X;try apply eq_refl.
      + destruct GenericInstance.type_of_attribute;try apply eq_refl.
        simpl.
        apply f_equal.
        unfold olift2,olift.
        destruct nraenv_core_eval eqn:D1;[|apply eq_refl].
        apply eq_sym.
        apply BasicFacts.match_option_eq.
        apply BasicFacts.match_option_eq.
        assert (X1 := dot_OpDot_NRAEnvID_eq br dreg i t a (drec (tuple_as_dpairs t))).
        unfold nraenv_eval in X1.
        simpl in X1.
        apply eq_sym.
        apply X1.
    - (* <> *)
      simpl.
      destruct type_of_X;try apply eq_refl.
      + destruct GenericInstance.type_of_attribute;try apply eq_refl.
        simpl.
        apply f_equal.
        unfold olift2,olift.
        destruct nraenv_core_eval eqn:D1;[|apply eq_refl].
        apply eq_sym.
        apply BasicFacts.match_option_eq.        
        apply BasicFacts.match_option_eq.
        assert (X1 := dot_OpDot_NRAEnvID_eq br dreg i t a (drec (tuple_as_dpairs t))).
        unfold nraenv_eval in X1.
        simpl in X1.
        apply eq_sym.
        apply BasicFacts.match_option_eq.
        apply X1.
      + destruct GenericInstance.type_of_attribute;try apply eq_refl.
        simpl.
        apply f_equal.
        unfold olift2,olift.
        destruct nraenv_core_eval eqn:D1;[|apply eq_refl].
        apply eq_sym.
        apply BasicFacts.match_option_eq.        
        apply BasicFacts.match_option_eq.
        assert (X1 := dot_OpDot_NRAEnvID_eq br dreg i t a (drec (tuple_as_dpairs t))).
        unfold nraenv_eval in X1.
        simpl in X1.
        apply eq_sym.
        apply BasicFacts.match_option_eq.
        apply X1.
      + destruct GenericInstance.type_of_attribute;try apply eq_refl.
        simpl.
        apply f_equal.
        unfold olift2,olift.
        destruct nraenv_core_eval eqn:D1;[|apply eq_refl].
        apply eq_sym.
        apply BasicFacts.match_option_eq.
        apply BasicFacts.match_option_eq.
        assert (X1 := dot_OpDot_NRAEnvID_eq br dreg i t a (drec (tuple_as_dpairs t))).
        unfold nraenv_eval in X1.
        simpl in X1.
        apply eq_sym.
        apply BasicFacts.match_option_eq.
        apply X1.
    - (* <=. *)
      simpl.
      destruct type_of_X;try apply eq_refl.
      + destruct GenericInstance.type_of_attribute;try apply eq_refl.
        simpl.
        apply f_equal.
        unfold olift2,olift.
        destruct nraenv_core_eval eqn:D1;[|apply eq_refl].
        apply eq_sym.
        apply BasicFacts.match_option_eq.
        apply BasicFacts.match_option_eq.
        assert (X1 := dot_OpDot_NRAEnvID_eq br dreg i t a (drec (tuple_as_dpairs t))).
        unfold nraenv_eval in X1.
        simpl in X1.
        apply eq_sym.
        apply X1.
  Qed.

  Lemma predicate_to_nraenv_quant_eq_aux_2 :
    forall   pred ,
    forall br i dreg a b1 b2 ,      
 (br
       ⊢
            (predicate_to_nraenv_bin pred (GenericInstance.type_of_attribute b1)
               (GenericInstance.type_of_attribute b2)
               (NRAEnvUnop (OpDot (TnullTD.attribute_to_string b1)) NRAEnvID)
               (NRAEnvUnop (OpDot (TnullTD.attribute_to_string b2)) NRAEnvID)) @ₓ drec (tuple_as_dpairs a) ⊣ i;
    dreg)%nraenv =
  (br
        ⊢ predicate_to_nraenv_bin pred (GenericInstance.type_of_attribute b1)
            (GenericInstance.type_of_attribute b2)
            ((fun x : attribute TNull =>
              NRAEnvUnop (OpDot (TnullTD.attribute_to_string x)) (NRAEnvConst (drec (tuple_as_dpairs a)))) b1)
            ((fun x : attribute TNull =>
              NRAEnvUnop (OpDot (TnullTD.attribute_to_string x)) (NRAEnvConst (drec (tuple_as_dpairs a)))) b2) @ₓ
            drec (tuple_as_dpairs a) ⊣ i; dreg)%nraenv.
  Proof.
    unfold nraenv_eval.
    intros.
    unfold predicate_to_nraenv_bin,NRAEnvNullPred.    
    destruct pred.
    repeat (destruct s as [ |[[ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ]] s];try apply eq_refl).
    - (* = *)
      simpl.
      destruct GenericInstance.type_of_attribute;try apply eq_refl.
      + destruct GenericInstance.type_of_attribute;try apply eq_refl.
        simpl.
        unfold olift2,olift.
        assert (X1 := fun b => dot_OpDot_NRAEnvID_eq br dreg i a b (drec (tuple_as_dpairs a))).
        unfold nraenv_eval in X1.
        simpl in X1.
        repeat rewrite X1.
        apply eq_refl.
      + destruct GenericInstance.type_of_attribute;try apply eq_refl.
        simpl.
        unfold olift2,olift.
        assert (X1 := fun b => dot_OpDot_NRAEnvID_eq br dreg i a b (drec (tuple_as_dpairs a))).
        unfold nraenv_eval in X1.
        simpl in X1.
        repeat rewrite X1.
        apply eq_refl.     
      + destruct GenericInstance.type_of_attribute;try apply eq_refl.
        simpl.
        unfold olift2,olift.
        assert (X1 := fun b => dot_OpDot_NRAEnvID_eq br dreg i a b (drec (tuple_as_dpairs a))).
        unfold nraenv_eval in X1.
        simpl in X1.
        repeat rewrite X1.
        apply eq_refl.
    - (* > *)
      simpl.
      destruct GenericInstance.type_of_attribute;try apply eq_refl.
      + destruct GenericInstance.type_of_attribute;try apply eq_refl.
        simpl.
        unfold olift2,olift.
        assert (X1 := fun b => dot_OpDot_NRAEnvID_eq br dreg i a b (drec (tuple_as_dpairs a))).
        unfold nraenv_eval in X1.
        simpl in X1.
        repeat rewrite X1.
        apply eq_refl.
    - (* >= *)
      simpl.
      destruct GenericInstance.type_of_attribute;try apply eq_refl.
      + destruct GenericInstance.type_of_attribute;try apply eq_refl.
        simpl.
        unfold olift2,olift.
        assert (X1 := fun b => dot_OpDot_NRAEnvID_eq br dreg i a b (drec (tuple_as_dpairs a))).
        unfold nraenv_eval in X1.
        simpl in X1.
        repeat rewrite X1.
        apply eq_refl.
    - (* >=. *)
      simpl.
      destruct GenericInstance.type_of_attribute;try apply eq_refl.
      + destruct GenericInstance.type_of_attribute;try apply eq_refl.
        simpl.
        unfold olift2,olift.
        assert (X1 := fun b => dot_OpDot_NRAEnvID_eq br dreg i a b (drec (tuple_as_dpairs a))).
        unfold nraenv_eval in X1.
        simpl in X1.
        repeat rewrite X1.
        apply eq_refl.
    - (* >. *)
      simpl.
      destruct GenericInstance.type_of_attribute;try apply eq_refl.
      + destruct GenericInstance.type_of_attribute;try apply eq_refl.
        simpl.
        unfold olift2,olift.
        assert (X1 := fun b => dot_OpDot_NRAEnvID_eq br dreg i a b (drec (tuple_as_dpairs a))).
        unfold nraenv_eval in X1.
        simpl in X1.
        repeat rewrite X1.
        apply eq_refl.
    - (* < *)
      simpl.
      destruct GenericInstance.type_of_attribute;try apply eq_refl.
      + destruct GenericInstance.type_of_attribute;try apply eq_refl.
        simpl.
        unfold olift2,olift.
        assert (X1 := fun b => dot_OpDot_NRAEnvID_eq br dreg i a b (drec (tuple_as_dpairs a))).
        unfold nraenv_eval in X1.
        simpl in X1.
        repeat rewrite X1.
        apply eq_refl.
    - (* <= *)
      simpl.
      destruct GenericInstance.type_of_attribute;try apply eq_refl.
      + destruct GenericInstance.type_of_attribute;try apply eq_refl.
        simpl.
        unfold olift2,olift.
        assert (X1 := fun b => dot_OpDot_NRAEnvID_eq br dreg i a b (drec (tuple_as_dpairs a))).
        unfold nraenv_eval in X1.
        simpl in X1.
        repeat rewrite X1.
        apply eq_refl.
    - (* <=. *)
      simpl.
      destruct GenericInstance.type_of_attribute;try apply eq_refl.
      + destruct GenericInstance.type_of_attribute;try apply eq_refl.
        simpl.
        unfold olift2,olift.
        assert (X1 := fun b => dot_OpDot_NRAEnvID_eq br dreg i a b (drec (tuple_as_dpairs a))).
        unfold nraenv_eval in X1.
        simpl in X1.
        repeat rewrite X1.
        apply eq_refl.
    - (* <> *)
      simpl.
      destruct GenericInstance.type_of_attribute;try apply eq_refl.
      + destruct GenericInstance.type_of_attribute;try apply eq_refl.
        simpl.
        unfold olift2,olift.
        assert (X1 := fun b => dot_OpDot_NRAEnvID_eq br dreg i a b (drec (tuple_as_dpairs a))).
        unfold nraenv_eval in X1.
        simpl in X1.
        repeat rewrite X1.
        apply eq_refl.
      + destruct GenericInstance.type_of_attribute;try apply eq_refl.
        simpl.
        unfold olift2,olift.
        assert (X1 := fun b => dot_OpDot_NRAEnvID_eq br dreg i a b (drec (tuple_as_dpairs a))).
        unfold nraenv_eval in X1.
        simpl in X1.
        repeat rewrite X1.
        apply eq_refl.
      + destruct GenericInstance.type_of_attribute;try apply eq_refl.
        simpl.
        unfold olift2,olift.
        assert (X1 := fun b => dot_OpDot_NRAEnvID_eq br dreg i a b (drec (tuple_as_dpairs a))).
        unfold nraenv_eval in X1.
        simpl in X1.
        repeat rewrite X1.
        apply eq_refl.
    - (* <. *)
      simpl.
      destruct GenericInstance.type_of_attribute;try apply eq_refl.
      + destruct GenericInstance.type_of_attribute;try apply eq_refl.
        simpl.
        unfold olift2,olift.
        assert (X1 := fun b => dot_OpDot_NRAEnvID_eq br dreg i a b (drec (tuple_as_dpairs a))).
        unfold nraenv_eval in X1.
        simpl in X1.
        repeat rewrite X1.
        apply eq_refl.
  Qed.
  
  (* Lemma predicate_to_nraenv_quant_eq : *)
  (*   forall X Y pred (lx:list X) (y: Y), *)
  (*   forall nraenv_of_X value_of_X type_of_X, *)
  (*   forall nraenv_of_Y  labels_of_Y (listT_of_Y:Y -> list (tuple TRcd )), *)
  (*   forall br i dreg, *)
  (*     ( exists ld,Permutation  (listT_to_listD (listT_of_Y y)) ld /\ *)
  (*            forall did, nraenv_eval br i ( nraenv_of_Y y) dreg did = Some (dcoll ld) ) -> *)
  (*     (forall x, In x lx -> *)
  (*           forall did, *)
  (*             nraenv_eval br i (nraenv_of_X x)  dreg did = *)
  (*             Some (value_to_data  (value_of_X x))) -> *)
  (*     (forall x, In x lx -> type_of_X x = Tuple.type_of_value TRcd (value_of_X x)) ->       *)
  (*     (forall t, In t (listT_of_Y y) -> *)
  (*           labels_of_Y y =S= labels _ t /\ (forallb  (fun a => well_typed_attribute a t) ({{{labels_of_Y y}}})) = true) -> *)
  (*     exists ld', *)
  (*       Permutation ld' *)
  (*                   (map bool3_to_data *)
  (*                        (map *)
  (*                           (fun y:tuple _ => *)
  (*                              interp_predicate TNull pred (map value_of_X lx ++  *)
  (*                                                               (map (fun a => dot _ y a) ({{{labels _ y}}})))) *)
  (*                           (listT_of_Y y))) /\     *)
  (*       forall did, *)
  (*         nraenv_eval *)
  (*           br i *)
  (*           (predicate_to_nraenv_quant *)
  (*              pred (map type_of_X lx) *)
  (*              (map nraenv_of_X lx) *)
  (*              (labels_of_Y y) *)
  (*              (nraenv_of_Y y)) dreg did =Some (dcoll ld'). *)
  (* Proof. *)
  (*   unfold nraenv_eval;simpl TRcd. *)
  (*   do 14 intro. *)
  (*   intros [ld [Pld Hld]] Hx1 Hx2 Hlen.     *)
  (*   unfold predicate_to_nraenv_quant. *)
  (*   destruct lx as [|a1 [|a2 lx']] eqn:D1, *)
  (*                                      ({{{labels_of_Y y}}}) as [|b1 [ |b2 ls]] eqn:D2. *)
  (*   - simpl (map _ nil);cbv iota. *)
  (*     unfold app. *)
  (*     exists (map (fun _ => dright dunit) ld). *)
  (*     split. *)
  (*     + refine (Permutation_trans *)
  (*                 (Permutation_sym (Permutation_map _ Pld)) _). *)
  (*       unfold listT_to_listD.       *)
  (*       rewrite 2 map_map. *)
  (*       apply Permutation_map_2_alt. *)
  (*       intros. *)
  (*       apply eq_sym. *)
  (*       rewrite <- bool3_to_data_unknown. *)
  (*       apply Hlen in H. *)
  (*       rewrite (Fset.elements_spec1 _ _ _ (proj1 H)) in D2. *)
  (*       rewrite D2. *)
  (*       destruct pred. *)
  (*       repeat (destruct s as [ |[[ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ]] s];try apply eq_refl). *)
  (*     + intros did. *)
  (*       cbn. *)
  (*       rewrite Hld. *)
  (*       unfold olift,lift_oncoll,lift. *)
  (*       rewrite lift_map_map. *)
  (*       apply eq_refl. *)
  (*   - simpl (map _ nil);cbv iota. *)
  (*     unfold app. *)
  (*     exists (map (fun _ => dright dunit) ld). *)
  (*     split. *)
  (*     +  refine (Permutation_trans *)
  (*                  (Permutation_sym (Permutation_map _ Pld)) _). *)
  (*        unfold listT_to_listD.       *)
  (*        rewrite 2 map_map. *)
  (*        apply Permutation_map_2_alt. *)
  (*        intros. *)
  (*        apply eq_sym. *)
  (*        rewrite <- bool3_to_data_unknown. *)
  (*        apply Hlen in H. *)
  (*        rewrite (Fset.elements_spec1 _ _ _ (proj1 H)) in D2. *)
  (*        rewrite D2. *)
  (*        destruct pred. *)
  (*        repeat (destruct s as [ |[[ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ]] s];try apply eq_refl); *)
  (*          simpl map;destruct TuplesImpl.Tuple2.dot;destruct o;apply eq_refl. *)
  (*     + intros did. *)
  (*       simpl. *)
  (*       rewrite Hld. *)
  (*       unfold olift,lift_oncoll,lift. *)
  (*       rewrite lift_map_map. *)
  (*       apply eq_refl.       *)
  (*   -  destruct ls as [|b3 ls]. *)
  (*      + simpl (map _ (_::nil)). *)
  (*     cbv iota beta. *)
  (*     assert (X1 := Pld). *)
  (*     apply Permutation_listT_exists_eq in Pld. *)
  (*     destruct Pld as [lt [Plt <-]]. *)
  (*     exists ( *)
  (*         map *)
  (*           (fun y' => bool3_to_data (interp_predicate _ pred ( map (fun a : attribute TNull => dot TNull y' a) ({{{labels TNull y'}}})))) *)
  (*           lt). *)
  (*     split. *)
  (*     * rewrite map_map. *)
  (*       refine (Permutation_map _ (Permutation_sym Plt)). *)
  (*     * intros. *)
  (*       simpl. *)
  (*       rewrite Hld. *)
  (*       simpl. *)
  (*       unfold lift,olift. *)
  (*       assert (Hlen := fun t H => Hlen t (Permutation_in _ (Permutation_sym Plt)  H)). *)
  (*       clear X1 Hld Plt. *)
  (*       induction lt;[apply eq_refl| ]. *)
  (*       simpl. *)
  (*       assert (A1 := predicate_to_nraenv_bin_eq_2  _ _ pred b1 b2). *)
  (*       assert (A1 := A1  *)
  (*                      (fun x => *)
  (*                         (NRAEnvUnop *)
  (*                            (OpDot (TnullTD.attribute_to_string x)) (NRAEnvConst (drec (tuple_as_dpairs a)) )))). *)
  (*       assert (A1 := A1 (fun x => dot _ a x) GenericInstance.type_of_attribute). *)
  (*          assert (A1 := A1  *)
  (*                      (fun x => *)
  (*                         (NRAEnvUnop *)
  (*                            (OpDot (TnullTD.attribute_to_string x)) (NRAEnvConst (drec (tuple_as_dpairs a)) )))). *)
  (*       assert (A1 := A1 (fun x => dot _ a x) GenericInstance.type_of_attribute). *)
  (*       assert (A1 := A1 br i dreg (drec (tuple_as_dpairs a))).  *)
  (*       (* (Hx2 _ (or_introl eq_refl) (drec (tuple_as_dpairs a) ))).  *)        *)
  (*       (* assert (A1 := A1 (Hx2 _ (or_introl eq_refl))). *) *)
  (*       assert (X1 := proj2 (Hlen _ (or_introl eq_refl))).         *)
  (*       rewrite forallb_forall in X1. *)
  (*       assert (X3 := X1 _ (or_introl eq_refl)). *)
  (*       assert (X2 := X1 _ (or_intror (or_introl eq_refl))). *)
  (*       unfold well_typed_attribute in X3,X2. *)
  (*       rewrite Oset.eq_bool_true_iff in X3,X2. *)
  (*       apply eq_sym in X3. *)
  (*       apply eq_sym in X2. *)
  (*       assert (H1 := fun did => dot_OpDot br dreg i a b1 did). *)
  (*       assert (H2 := fun did => dot_OpDot br dreg i a b2 did). *)
  (*       rewrite (Fset.elements_spec1 _ _ _ (proj1 (Hlen _ (or_introl eq_refl)))) in D2. *)
  (*       simpl TRcd in H1,H2. *)
  (*       rewrite Fset.mem_elements,D2 in H1,H2. *)
  (*       simpl in H1,H2. *)
  (*       rewrite Oset.eq_bool_refl in H1,H2. *)
  (*       assert (H1 := fun did => H1 did eq_refl). *)
  (*       assert (A1 := A1 (H1 (drec (tuple_as_dpairs a))) X3). *)
  (*       rewrite  orb_true_r in H2. *)
  (*       assert (H2 := fun did => H2 did eq_refl).  *)
  (*       assert (A1 := A1 (H2  (drec (tuple_as_dpairs a))) X2). *)
  (*       simpl in A1. *)
  (*       assert (X5 :=  (predicate_to_nraenv_quant_eq_aux_2 pred br i dreg a b1 b2)). *)
  (*       unfold nraenv_eval in X5,A1. *)
  (*       simpl in X5,A1. *)
  (*       rewrite X5. *)
  (*       rewrite  A1. *)
  (*       unfold lift. *)
  (*       assert (Hlen' := fun t H => Hlen t (or_intror H)). *)
  (*       assert (IHlt := IHlt Hlen'). *)
  (*       destruct lift_map;[|discriminate]. *)
  (*       inversion IHlt. *)
  (*       simpl in D2. *)
  (*       rewrite D2. *)
  (*       apply eq_refl. *)
  (*      +  simpl (map _ nil);cbv iota. *)
  (*         unfold app. *)
  (*         exists (map (fun _ => dright dunit) ld). *)
  (*         split. *)
  (*         *  refine (Permutation_trans *)
  (*                      (Permutation_sym (Permutation_map _ Pld)) _). *)
  (*            unfold listT_to_listD.       *)
  (*            rewrite 2 map_map. *)
  (*            apply Permutation_map_2_alt. *)
  (*            intros. *)
  (*            apply eq_sym. *)
  (*            rewrite <- bool3_to_data_unknown. *)
  (*            apply Hlen in H. *)
  (*            rewrite (Fset.elements_spec1 _ _ _ (proj1 H)) in D2. *)
  (*            rewrite D2. *)
  (*            destruct pred. *)
  (*            repeat (destruct s as [ |[[ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ]] s];try apply eq_refl); *)
  (*              simpl map;destruct TuplesImpl.Tuple2.dot,TuplesImpl.Tuple2.dot;destruct o,o0;apply eq_refl. *)
  (*         *  intros did. *)
  (*            simpl. *)
  (*            rewrite Hld. *)
  (*            unfold olift,lift_oncoll,lift. *)
  (*            rewrite lift_map_map. *)
  (*            apply eq_refl.               *)
  (*   - simpl (map _ nil);cbv iota. *)
  (*     unfold app. *)
  (*     exists (map (fun _ => dright dunit) ld). *)
  (*     split. *)
  (*     *  refine (Permutation_trans *)
  (*                  (Permutation_sym (Permutation_map _ Pld)) _). *)
  (*        unfold listT_to_listD.       *)
  (*        rewrite 2 map_map. *)
  (*        apply Permutation_map_2_alt. *)
  (*        intros. *)
  (*        apply eq_sym. *)
  (*        rewrite <- bool3_to_data_unknown. *)
  (*        apply Hlen in H. *)
  (*        rewrite (Fset.elements_spec1 _ _ _ (proj1 H)) in D2. *)
  (*        rewrite D2. *)
  (*        destruct pred. *)
  (*        repeat (destruct s as [ |[[ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ]] s];try apply eq_refl); *)
  (*          simpl map; destruct (value_of_X a1);destruct o;apply eq_refl. *)
  (*     *  intros did. *)
  (*        simpl. *)
  (*        rewrite Hld. *)
  (*        unfold olift,lift_oncoll,lift. *)
  (*        rewrite lift_map_map. *)
  (*        apply eq_refl.               *)
  (*   - simpl (map _ (_::nil)). *)
  (*     cbv iota beta. *)
  (*     assert (X1 := Pld). *)
  (*     apply Permutation_listT_exists_eq in Pld. *)
  (*     destruct Pld as [lt [Plt <-]]. *)
  (*     exists ( *)
  (*         map *)
  (*           (fun y' => bool3_to_data (interp_predicate _ pred (value_of_X a1:: map (fun a : attribute TNull => dot TNull y' a) ({{{labels TNull y'}}})))) *)
  (*           lt). *)
  (*     split. *)
  (*     + rewrite map_map. *)
  (*       refine (Permutation_map _ (Permutation_sym Plt)). *)
  (*     + intros. *)
  (*       simpl. *)
  (*       rewrite Hld. *)
  (*       simpl. *)
  (*       unfold lift,olift. *)
  (*       assert (Hlen := fun t H => Hlen t (Permutation_in _ (Permutation_sym Plt)  H)). *)
  (*       clear X1 Hld Plt. *)
  (*       induction lt;[apply eq_refl| ]. *)
  (*       simpl. *)
  (*       assert (A1 := predicate_to_nraenv_bin_eq_2  _ _ pred a1 b1). *)
  (*       assert (A1 := A1 nraenv_of_X value_of_X  type_of_X). *)
  (*       assert (A1 := A1  *)
  (*                      (fun x => *)
  (*                         (NRAEnvUnop *)
  (*                            (OpDot (TnullTD.attribute_to_string x)) (NRAEnvConst (drec (tuple_as_dpairs a)) )))). *)
  (*       assert (A1 := A1 (fun x => dot _ a x) GenericInstance.type_of_attribute). *)
  (*       assert (A1 := A1 br i dreg (drec (tuple_as_dpairs a))  (Hx1 _ (or_introl eq_refl) (drec (tuple_as_dpairs a) ))).         *)
  (*       assert (A1 := A1 (Hx2 _ (or_introl eq_refl))). *)
  (*       assert (X1 := proj2 (Hlen _ (or_introl eq_refl))).         *)
  (*       rewrite forallb_forall in X1. *)
  (*       assert (X1 := X1 _ (or_introl eq_refl)). *)
  (*       unfold well_typed_attribute in X1. *)
  (*       rewrite Oset.eq_bool_true_iff in X1. *)
  (*       apply eq_sym in X1. *)
  (*       assert (H := fun did => dot_OpDot br dreg i a b1 did). *)
  (*       rewrite (Fset.elements_spec1 _ _ _ (proj1 (Hlen _ (or_introl eq_refl)))) in D2. *)
  (*       simpl TRcd in H. *)
  (*       rewrite Fset.mem_elements,D2 in H. *)
  (*       simpl in H. *)
  (*       rewrite Oset.eq_bool_refl in H. *)
  (*       assert (H := fun did => H did eq_refl).         *)
  (*       assert (A1 := A1 (H (drec (tuple_as_dpairs a))) X1). *)
  (*       assert (X2 := eq_trans (eq_sym A1)  (predicate_to_nraenv_quant_eq_aux _ _ _ _ _ _ _ _ _ _ )). *)
  (*       unfold nraenv_eval in X2;simpl in X2. *)
  (*       rewrite <- X2. *)
  (*       unfold lift. *)
  (*       assert (Hlen' := fun t H => Hlen t (or_intror H)). *)
  (*       assert (IHlt := IHlt Hlen'). *)
  (*       destruct lift_map;[|discriminate]. *)
  (*       inversion IHlt. *)
  (*       simpl in D2. *)
  (*       rewrite D2. *)
  (*       apply eq_refl. *)
  (*   - simpl (map _ nil);cbv iota. *)
  (*     unfold app. *)
  (*     exists (map (fun _ => dright dunit) ld). *)
  (*     split. *)
  (*     + refine (Permutation_trans *)
  (*                 (Permutation_sym (Permutation_map _ Pld)) _). *)
  (*       unfold listT_to_listD.       *)
  (*       rewrite 2 map_map. *)
  (*       apply Permutation_map_2_alt. *)
  (*       intros. *)
  (*       apply eq_sym. *)
  (*       rewrite <- bool3_to_data_unknown. *)
  (*       apply Hlen in H. *)
  (*       rewrite (Fset.elements_spec1 _ _ _ (proj1 H)) in D2. *)
  (*       rewrite D2. *)
  (*       destruct pred. *)
  (*       repeat (destruct s as [ |[[ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ]] s];try apply eq_refl); *)
  (*         simpl map; destruct (value_of_X a1),TuplesImpl.Tuple2.dot;destruct o,o0;apply eq_refl. *)
  (*     +  intros did. *)
  (*        simpl. *)
  (*        rewrite Hld. *)
  (*        unfold olift,lift_oncoll,lift. *)
  (*        rewrite lift_map_map. *)
  (*        apply eq_refl. *)
  (*   - destruct lx'. *)
  (*     + simpl. *)
  (*       exists ( *)
  (*           map *)
  (*             (fun _ => bool3_to_data (interp_predicate _ pred (value_of_X a1 :: value_of_X a2 :: nil))) *)
  (*             ld). *)
  (*       split. *)
  (*       *  refine (Permutation_trans *)
  (*                    (Permutation_sym (Permutation_map _ Pld)) _). *)
  (*          unfold listT_to_listD.       *)
  (*          rewrite 2 map_map. *)
  (*          apply Permutation_map_2_alt. *)
  (*          intros. *)
  (*          apply eq_sym. *)
  (*          apply f_equal.        *)
  (*          apply Hlen in H. *)
  (*          rewrite (Fset.elements_spec1 _ _ _ (proj1 H)) in D2. *)
  (*          simpl in D2;rewrite D2. *)
  (*          apply eq_refl. *)
  (*       *  *)
  (*         unfold olift,lift_oncoll,lift. *)
  (*         assert (A1 := predicate_to_nraenv_bin_eq _ pred a1 a2 nraenv_of_X). *)
  (*         assert (A1 := A1 value_of_X  type_of_X br i dreg). *)
  (*         intro did.  *)
  (*         rewrite Hld. *)
  (*         clear Pld Hld. *)
  (*         induction ld;[apply eq_refl| ]. *)
  (*         simpl. *)
  (*                   assert (A1 := (A1  a) (Hx1  _ (or_introl eq_refl) a)).         *)
  (*         assert (A1 := A1 (Hx1 _ (or_intror (or_introl eq_refl)) a)). *)
  (*         assert (A1 := A1 (Hx2 _ (or_introl eq_refl))).         *)
  (*         assert (A1 := A1 (Hx2 _ (or_intror (or_introl eq_refl)))).      *)
  (*         unfold nraenv_eval in A1. *)
  (*         rewrite A1. *)
  (*         unfold lift. *)
  (*         destruct lift_map;[|discriminate]. *)
  (*         inversion IHld. *)
  (*         apply eq_refl. *)
  (*     + simpl (map _ nil);cbv iota. *)
  (*       unfold app. *)
  (*       exists (map (fun _ => dright dunit) ld). *)
  (*       split. *)
  (*       * refine (Permutation_trans *)
  (*                   (Permutation_sym (Permutation_map _ Pld)) _). *)
  (*         unfold listT_to_listD.       *)
  (*         rewrite 2 map_map. *)
  (*         apply Permutation_map_2_alt. *)
  (*         intros. *)
  (*         apply eq_sym. *)
  (*         rewrite <- bool3_to_data_unknown. *)
  (*         apply Hlen in H. *)
  (*         rewrite (Fset.elements_spec1 _ _ _ (proj1 H)) in D2. *)
  (*         rewrite D2. *)
  (*         destruct pred. *)
  (*         repeat (destruct s as [ |[[ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ]] s];try apply eq_refl); *)
  (*           simpl map; destruct (value_of_X a1),(value_of_X a2);destruct o,o0;apply eq_refl. *)
  (*       *  intros did. *)
  (*          simpl. *)
  (*          rewrite Hld. *)
  (*          unfold olift,lift_oncoll,lift. *)
  (*          rewrite lift_map_map. *)
  (*          apply eq_refl. *)
  (*   - *)
  (*     simpl (map _ nil);cbv iota. *)
  (*     unfold app. *)
  (*     exists (map (fun _ => dright dunit) ld). *)
  (*     split. *)
  (*     +  destruct lx'. *)
  (*        * refine (Permutation_trans *)
  (*                    (Permutation_sym (Permutation_map _ Pld)) _). *)
  (*          unfold listT_to_listD.       *)
  (*          rewrite 2 map_map. *)
  (*          apply Permutation_map_2_alt. *)
  (*          intros. *)
  (*          apply eq_sym. *)
  (*          rewrite <- bool3_to_data_unknown. *)
  (*          apply Hlen in H. *)
  (*          rewrite (Fset.elements_spec1 _ _ _ (proj1 H)) in D2. *)
  (*          rewrite D2. *)
  (*          destruct pred. *)
  (*          repeat (destruct s as [ |[[ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ]] s];try apply eq_refl); *)
  (*            simpl map; destruct (value_of_X a1),(value_of_X a2);destruct o,o0;apply eq_refl. *)
  (*        *  refine (Permutation_trans *)
  (*                     (Permutation_sym (Permutation_map _ Pld)) _). *)
  (*           unfold listT_to_listD.       *)
  (*           rewrite 2 map_map. *)
  (*           apply Permutation_map_2_alt. *)
  (*           intros. *)
  (*           apply eq_sym. *)
  (*           rewrite <- bool3_to_data_unknown. *)
  (*           apply Hlen in H. *)
  (*           rewrite (Fset.elements_spec1 _ _ _ (proj1 H)) in D2. *)
  (*           rewrite D2. *)
  (*           destruct pred. *)
  (*           repeat (destruct s as [ |[[ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ]] s];try apply eq_refl); *)
  (*             simpl map; destruct (value_of_X a1),(value_of_X a2);destruct o,o0;apply eq_refl.         *)
  (*     +  intros did. *)
  (*        destruct lx';simpl. *)
  (*        * rewrite Hld. *)
  (*          unfold olift,lift_oncoll,lift. *)
  (*          rewrite lift_map_map. *)
  (*          apply eq_refl.         *)
  (*        * rewrite Hld. *)
  (*          unfold olift,lift_oncoll,lift. *)
  (*          rewrite lift_map_map. *)
  (*          apply eq_refl. *)
  (*   -  simpl (map _ nil);cbv iota. *)
  (*      unfold app. *)
  (*      exists (map (fun _ => dright dunit) ld). *)
  (*      split. *)
  (*      +  destruct lx'. *)
  (*         * refine (Permutation_trans *)
  (*                     (Permutation_sym (Permutation_map _ Pld)) _). *)
  (*           unfold listT_to_listD.       *)
  (*           rewrite 2 map_map. *)
  (*           apply Permutation_map_2_alt. *)
  (*           intros. *)
  (*           apply eq_sym. *)
  (*           rewrite <- bool3_to_data_unknown. *)
  (*           apply Hlen in H. *)
  (*           rewrite (Fset.elements_spec1 _ _ _ (proj1 H)) in D2. *)
  (*           rewrite D2. *)
  (*           destruct pred. *)
  (*           repeat (destruct s as [ |[[ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ]] s];try apply eq_refl); *)
  (*             simpl map; destruct (value_of_X a1),(value_of_X a2);destruct o,o0;apply eq_refl. *)
  (*         *  refine (Permutation_trans *)
  (*                      (Permutation_sym (Permutation_map _ Pld)) _). *)
  (*            unfold listT_to_listD.       *)
  (*            rewrite 2 map_map. *)
  (*            apply Permutation_map_2_alt. *)
  (*            intros. *)
  (*            apply eq_sym. *)
  (*            rewrite <- bool3_to_data_unknown. *)
  (*            apply Hlen in H. *)
  (*            rewrite (Fset.elements_spec1 _ _ _ (proj1 H)) in D2. *)
  (*            rewrite D2. *)
  (*            destruct pred. *)
  (*            repeat (destruct s as [ |[[ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ]] s];try apply eq_refl); *)
  (*              simpl map; destruct (value_of_X a1),(value_of_X a2);destruct o,o0;apply eq_refl.         *)
  (*      +  intros did. *)
  (*         destruct lx';simpl. *)
  (*         * rewrite Hld. *)
  (*           unfold olift,lift_oncoll,lift. *)
  (*           rewrite lift_map_map. *)
  (*           apply eq_refl.         *)
  (*         * rewrite Hld. *)
  (*           unfold olift,lift_oncoll,lift. *)
  (*           rewrite lift_map_map. *)
  (*           apply eq_refl. *)
  (* Qed. *)

  Lemma predicate_to_nraenv_quant_eq :
    forall X Y pred (lx:list X) (y: Y),
    forall nraenv_of_X value_of_X type_of_X,
    forall nraenv_of_Y  labels_of_Y (listT_of_Y:Y -> list (tuple TRcd )),
    forall br i dreg ld,
      ( Permutation  (listT_to_listD (listT_of_Y y)) ld /\
             forall did, nraenv_eval br i ( nraenv_of_Y y) dreg did = Some (dcoll ld) ) ->
      (forall x, In x lx ->
            forall did,
              nraenv_eval br i (nraenv_of_X x)  dreg did =
              Some (value_to_data  (value_of_X x))) ->
      (forall x, In x lx -> type_of_X x = Tuple.type_of_value TRcd (value_of_X x)) ->      
      (forall t, In t (listT_of_Y y) ->
            labels_of_Y y =S= labels _ t /\ (forallb  (fun a => well_typed_attribute a t) ({{{labels_of_Y y}}})) = true) ->
      exists ld',
        Permutation ld'
                    (map bool3_to_data
                         (map
                            (fun y:tuple _ =>
                               interp_predicate TNull pred (map value_of_X lx ++ 
                                                                (map (fun a => dot _ y a) ({{{labels _ y}}}))))
                            (listT_of_Y y))) /\    
        forall did,
          nraenv_eval
            br i
            (predicate_to_nraenv_quant
               pred (map type_of_X lx)
               (map nraenv_of_X lx)
               (labels_of_Y y)
               (nraenv_of_Y y)) dreg did =Some (dcoll ld').
  Proof.
    unfold nraenv_eval;simpl TRcd.
    do 14 intro.
    intros ld [Pld Hld] Hx1 Hx2 Hlen.    
    unfold predicate_to_nraenv_quant.
    destruct lx as [|a1 [|a2 lx']] eqn:D1,
                                       ({{{labels_of_Y y}}}) as [|b1 [ |b2 ls]] eqn:D2.
    - simpl (map _ nil);cbv iota.
      unfold app.
      exists (map (fun _ => dright dunit) ld).
      split.
      + refine (Permutation_trans
                  (Permutation_sym (Permutation_map _ Pld)) _).
        unfold listT_to_listD.      
        rewrite 2 map_map.
        apply Permutation_map_2_alt.
        intros.
        apply eq_sym.
        rewrite <- bool3_to_data_unknown.
        apply Hlen in H.
        rewrite (Fset.elements_spec1 _ _ _ (proj1 H)) in D2.
        rewrite D2.
        destruct pred.
        repeat (destruct s as [ |[[ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ]] s];try apply eq_refl).
      + intros did.
        cbn.
        rewrite Hld.
        unfold olift,lift_oncoll,lift.
        rewrite lift_map_map.
        apply eq_refl.
    - simpl (map _ nil);cbv iota.
      unfold app.
      exists (map (fun _ => dright dunit) ld).
      split.
      +  refine (Permutation_trans
                   (Permutation_sym (Permutation_map _ Pld)) _).
         unfold listT_to_listD.      
         rewrite 2 map_map.
         apply Permutation_map_2_alt.
         intros.
         apply eq_sym.
         rewrite <- bool3_to_data_unknown.
         apply Hlen in H.
         rewrite (Fset.elements_spec1 _ _ _ (proj1 H)) in D2.
         rewrite D2.
         destruct pred.
         repeat (destruct s as [ |[[ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ]] s];try apply eq_refl);
           simpl map;destruct TuplesImpl.Tuple2.dot;destruct o;apply eq_refl.
      + intros did.
        simpl.
        rewrite Hld.
        unfold olift,lift_oncoll,lift.
        rewrite lift_map_map.
        apply eq_refl.      
    -  destruct ls as [|b3 ls].
       + simpl (map _ (_::nil)).
      cbv iota beta.
      assert (X1 := Pld).
      apply Permutation_listT_exists_eq in Pld.
      destruct Pld as [lt [Plt <-]].
      exists (
          map
            (fun y' => bool3_to_data (interp_predicate _ pred ( map (fun a : attribute TNull => dot TNull y' a) ({{{labels TNull y'}}}))))
            lt).
      split.
      * rewrite map_map.
        refine (Permutation_map _ (Permutation_sym Plt)).
      * intros.
        simpl.
        rewrite Hld.
        simpl.
        unfold lift,olift.
        assert (Hlen := fun t H => Hlen t (Permutation_in _ (Permutation_sym Plt)  H)).
        clear X1 Hld Plt.
        induction lt;[apply eq_refl| ].
        simpl.
        assert (A1 := predicate_to_nraenv_bin_eq_2  _ _ pred b1 b2).
        assert (A1 := A1 
                       (fun x =>
                          (NRAEnvUnop
                             (OpDot (TnullTD.attribute_to_string x)) (NRAEnvConst (drec (tuple_as_dpairs a)) )))).
        assert (A1 := A1 (fun x => dot _ a x) GenericInstance.type_of_attribute).
           assert (A1 := A1 
                       (fun x =>
                          (NRAEnvUnop
                             (OpDot (TnullTD.attribute_to_string x)) (NRAEnvConst (drec (tuple_as_dpairs a)) )))).
        assert (A1 := A1 (fun x => dot _ a x) GenericInstance.type_of_attribute).
        assert (A1 := A1 br i dreg (drec (tuple_as_dpairs a))). 
        (* (Hx2 _ (or_introl eq_refl) (drec (tuple_as_dpairs a) ))).  *)       
        (* assert (A1 := A1 (Hx2 _ (or_introl eq_refl))). *)
        assert (X1 := proj2 (Hlen _ (or_introl eq_refl))).        
        rewrite forallb_forall in X1.
        assert (X3 := X1 _ (or_introl eq_refl)).
        assert (X2 := X1 _ (or_intror (or_introl eq_refl))).
        unfold well_typed_attribute in X3,X2.
        rewrite Oset.eq_bool_true_iff in X3,X2.
        apply eq_sym in X3.
        apply eq_sym in X2.
        assert (H1 := fun did => dot_OpDot br dreg i a b1 did).
        assert (H2 := fun did => dot_OpDot br dreg i a b2 did).
        rewrite (Fset.elements_spec1 _ _ _ (proj1 (Hlen _ (or_introl eq_refl)))) in D2.
        simpl TRcd in H1,H2.
        rewrite Fset.mem_elements,D2 in H1,H2.
        simpl in H1,H2.
        rewrite Oset.eq_bool_refl in H1,H2.
        assert (H1 := fun did => H1 did eq_refl).
        assert (A1 := A1 (H1 (drec (tuple_as_dpairs a))) X3).
        rewrite  orb_true_r in H2.
        assert (H2 := fun did => H2 did eq_refl). 
        assert (A1 := A1 (H2  (drec (tuple_as_dpairs a))) X2).
        simpl in A1.
        assert (X5 :=  (predicate_to_nraenv_quant_eq_aux_2 pred br i dreg a b1 b2)).
        unfold nraenv_eval in X5,A1.
        simpl in X5,A1.
        rewrite X5.
        rewrite  A1.
        unfold lift.
        assert (Hlen' := fun t H => Hlen t (or_intror H)).
        assert (IHlt := IHlt Hlen').
        destruct lift_map;[|discriminate].
        inversion IHlt.
        simpl in D2.
        rewrite D2.
        apply eq_refl.
       +  simpl (map _ nil);cbv iota.
          unfold app.
          exists (map (fun _ => dright dunit) ld).
          split.
          *  refine (Permutation_trans
                       (Permutation_sym (Permutation_map _ Pld)) _).
             unfold listT_to_listD.      
             rewrite 2 map_map.
             apply Permutation_map_2_alt.
             intros.
             apply eq_sym.
             rewrite <- bool3_to_data_unknown.
             apply Hlen in H.
             rewrite (Fset.elements_spec1 _ _ _ (proj1 H)) in D2.
             rewrite D2.
             destruct pred.
             repeat (destruct s as [ |[[ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ]] s];try apply eq_refl);
               simpl map;destruct TuplesImpl.Tuple2.dot,TuplesImpl.Tuple2.dot;destruct o,o0;apply eq_refl.
          *  intros did.
             simpl.
             rewrite Hld.
             unfold olift,lift_oncoll,lift.
             rewrite lift_map_map.
             apply eq_refl.              
    - simpl (map _ nil);cbv iota.
      unfold app.
      exists (map (fun _ => dright dunit) ld).
      split.
      *  refine (Permutation_trans
                   (Permutation_sym (Permutation_map _ Pld)) _).
         unfold listT_to_listD.      
         rewrite 2 map_map.
         apply Permutation_map_2_alt.
         intros.
         apply eq_sym.
         rewrite <- bool3_to_data_unknown.
         apply Hlen in H.
         rewrite (Fset.elements_spec1 _ _ _ (proj1 H)) in D2.
         rewrite D2.
         destruct pred.
         repeat (destruct s as [ |[[ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ]] s];try apply eq_refl);
           simpl map; destruct (value_of_X a1);destruct o;apply eq_refl.
      *  intros did.
         simpl.
         rewrite Hld.
         unfold olift,lift_oncoll,lift.
         rewrite lift_map_map.
         apply eq_refl.              
    - simpl (map _ (_::nil)).
      cbv iota beta.
      assert (X1 := Pld).
      apply Permutation_listT_exists_eq in Pld.
      destruct Pld as [lt [Plt <-]].
      exists (
          map
            (fun y' => bool3_to_data (interp_predicate _ pred (value_of_X a1:: map (fun a : attribute TNull => dot TNull y' a) ({{{labels TNull y'}}}))))
            lt).
      split.
      + rewrite map_map.
        refine (Permutation_map _ (Permutation_sym Plt)).
      + intros.
        simpl.
        rewrite Hld.
        simpl.
        unfold lift,olift.
        assert (Hlen := fun t H => Hlen t (Permutation_in _ (Permutation_sym Plt)  H)).
        clear X1 Hld Plt.
        induction lt;[apply eq_refl| ].
        simpl.
        assert (A1 := predicate_to_nraenv_bin_eq_2  _ _ pred a1 b1).
        assert (A1 := A1 nraenv_of_X value_of_X  type_of_X).
        assert (A1 := A1 
                       (fun x =>
                          (NRAEnvUnop
                             (OpDot (TnullTD.attribute_to_string x)) (NRAEnvConst (drec (tuple_as_dpairs a)) )))).
        assert (A1 := A1 (fun x => dot _ a x) GenericInstance.type_of_attribute).
        assert (A1 := A1 br i dreg (drec (tuple_as_dpairs a))  (Hx1 _ (or_introl eq_refl) (drec (tuple_as_dpairs a) ))).        
        assert (A1 := A1 (Hx2 _ (or_introl eq_refl))).
        assert (X1 := proj2 (Hlen _ (or_introl eq_refl))).        
        rewrite forallb_forall in X1.
        assert (X1 := X1 _ (or_introl eq_refl)).
        unfold well_typed_attribute in X1.
        rewrite Oset.eq_bool_true_iff in X1.
        apply eq_sym in X1.
        assert (H := fun did => dot_OpDot br dreg i a b1 did).
        rewrite (Fset.elements_spec1 _ _ _ (proj1 (Hlen _ (or_introl eq_refl)))) in D2.
        simpl TRcd in H.
        rewrite Fset.mem_elements,D2 in H.
        simpl in H.
        rewrite Oset.eq_bool_refl in H.
        assert (H := fun did => H did eq_refl).        
        assert (A1 := A1 (H (drec (tuple_as_dpairs a))) X1).
        assert (X2 := eq_trans (eq_sym A1)  (predicate_to_nraenv_quant_eq_aux _ _ _ _ _ _ _ _ _ _ )).
        unfold nraenv_eval in X2;simpl in X2.
        rewrite <- X2.
        unfold lift.
        assert (Hlen' := fun t H => Hlen t (or_intror H)).
        assert (IHlt := IHlt Hlen').
        destruct lift_map;[|discriminate].
        inversion IHlt.
        simpl in D2.
        rewrite D2.
        apply eq_refl.
    - simpl (map _ nil);cbv iota.
      unfold app.
      exists (map (fun _ => dright dunit) ld).
      split.
      + refine (Permutation_trans
                  (Permutation_sym (Permutation_map _ Pld)) _).
        unfold listT_to_listD.      
        rewrite 2 map_map.
        apply Permutation_map_2_alt.
        intros.
        apply eq_sym.
        rewrite <- bool3_to_data_unknown.
        apply Hlen in H.
        rewrite (Fset.elements_spec1 _ _ _ (proj1 H)) in D2.
        rewrite D2.
        destruct pred.
        repeat (destruct s as [ |[[ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ]] s];try apply eq_refl);
          simpl map; destruct (value_of_X a1),TuplesImpl.Tuple2.dot;destruct o,o0;apply eq_refl.
      +  intros did.
         simpl.
         rewrite Hld.
         unfold olift,lift_oncoll,lift.
         rewrite lift_map_map.
         apply eq_refl.
    - destruct lx'.
      + simpl.
        exists (
            map
              (fun _ => bool3_to_data (interp_predicate _ pred (value_of_X a1 :: value_of_X a2 :: nil)))
              ld).
        split.
        *  refine (Permutation_trans
                     (Permutation_sym (Permutation_map _ Pld)) _).
           unfold listT_to_listD.      
           rewrite 2 map_map.
           apply Permutation_map_2_alt.
           intros.
           apply eq_sym.
           apply f_equal.       
           apply Hlen in H.
           rewrite (Fset.elements_spec1 _ _ _ (proj1 H)) in D2.
           simpl in D2;rewrite D2.
           apply eq_refl.
        * 
          unfold olift,lift_oncoll,lift.
          assert (A1 := predicate_to_nraenv_bin_eq _ pred a1 a2 nraenv_of_X).
          assert (A1 := A1 value_of_X  type_of_X br i dreg).
          intro did. 
          rewrite Hld.
          clear Pld Hld.
          induction ld;[apply eq_refl| ].
          simpl.
                    assert (A1 := (A1  a) (Hx1  _ (or_introl eq_refl) a)).        
          assert (A1 := A1 (Hx1 _ (or_intror (or_introl eq_refl)) a)).
          assert (A1 := A1 (Hx2 _ (or_introl eq_refl))).        
          assert (A1 := A1 (Hx2 _ (or_intror (or_introl eq_refl)))).     
          unfold nraenv_eval in A1.
          rewrite A1.
          unfold lift.
          destruct lift_map;[|discriminate].
          inversion IHld.
          apply eq_refl.
      + simpl (map _ nil);cbv iota.
        unfold app.
        exists (map (fun _ => dright dunit) ld).
        split.
        * refine (Permutation_trans
                    (Permutation_sym (Permutation_map _ Pld)) _).
          unfold listT_to_listD.      
          rewrite 2 map_map.
          apply Permutation_map_2_alt.
          intros.
          apply eq_sym.
          rewrite <- bool3_to_data_unknown.
          apply Hlen in H.
          rewrite (Fset.elements_spec1 _ _ _ (proj1 H)) in D2.
          rewrite D2.
          destruct pred.
          repeat (destruct s as [ |[[ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ]] s];try apply eq_refl);
            simpl map; destruct (value_of_X a1),(value_of_X a2);destruct o,o0;apply eq_refl.
        *  intros did.
           simpl.
           rewrite Hld.
           unfold olift,lift_oncoll,lift.
           rewrite lift_map_map.
           apply eq_refl.
    -
      simpl (map _ nil);cbv iota.
      unfold app.
      exists (map (fun _ => dright dunit) ld).
      split.
      +  destruct lx'.
         * refine (Permutation_trans
                     (Permutation_sym (Permutation_map _ Pld)) _).
           unfold listT_to_listD.      
           rewrite 2 map_map.
           apply Permutation_map_2_alt.
           intros.
           apply eq_sym.
           rewrite <- bool3_to_data_unknown.
           apply Hlen in H.
           rewrite (Fset.elements_spec1 _ _ _ (proj1 H)) in D2.
           rewrite D2.
           destruct pred.
           repeat (destruct s as [ |[[ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ]] s];try apply eq_refl);
             simpl map; destruct (value_of_X a1),(value_of_X a2);destruct o,o0;apply eq_refl.
         *  refine (Permutation_trans
                      (Permutation_sym (Permutation_map _ Pld)) _).
            unfold listT_to_listD.      
            rewrite 2 map_map.
            apply Permutation_map_2_alt.
            intros.
            apply eq_sym.
            rewrite <- bool3_to_data_unknown.
            apply Hlen in H.
            rewrite (Fset.elements_spec1 _ _ _ (proj1 H)) in D2.
            rewrite D2.
            destruct pred.
            repeat (destruct s as [ |[[ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ]] s];try apply eq_refl);
              simpl map; destruct (value_of_X a1),(value_of_X a2);destruct o,o0;apply eq_refl.        
      +  intros did.
         destruct lx';simpl.
         * rewrite Hld.
           unfold olift,lift_oncoll,lift.
           rewrite lift_map_map.
           apply eq_refl.        
         * rewrite Hld.
           unfold olift,lift_oncoll,lift.
           rewrite lift_map_map.
           apply eq_refl.
    -  simpl (map _ nil);cbv iota.
       unfold app.
       exists (map (fun _ => dright dunit) ld).
       split.
       +  destruct lx'.
          * refine (Permutation_trans
                      (Permutation_sym (Permutation_map _ Pld)) _).
            unfold listT_to_listD.      
            rewrite 2 map_map.
            apply Permutation_map_2_alt.
            intros.
            apply eq_sym.
            rewrite <- bool3_to_data_unknown.
            apply Hlen in H.
            rewrite (Fset.elements_spec1 _ _ _ (proj1 H)) in D2.
            rewrite D2.
            destruct pred.
            repeat (destruct s as [ |[[ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ]] s];try apply eq_refl);
              simpl map; destruct (value_of_X a1),(value_of_X a2);destruct o,o0;apply eq_refl.
          *  refine (Permutation_trans
                       (Permutation_sym (Permutation_map _ Pld)) _).
             unfold listT_to_listD.      
             rewrite 2 map_map.
             apply Permutation_map_2_alt.
             intros.
             apply eq_sym.
             rewrite <- bool3_to_data_unknown.
             apply Hlen in H.
             rewrite (Fset.elements_spec1 _ _ _ (proj1 H)) in D2.
             rewrite D2.
             destruct pred.
             repeat (destruct s as [ |[[ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ]] s];try apply eq_refl);
               simpl map; destruct (value_of_X a1),(value_of_X a2);destruct o,o0;apply eq_refl.        
       +  intros did.
          destruct lx';simpl.
          * rewrite Hld.
            unfold olift,lift_oncoll,lift.
            rewrite lift_map_map.
            apply eq_refl.        
          * rewrite Hld.
            unfold olift,lift_oncoll,lift.
            rewrite lift_map_map.
            apply eq_refl.
  Qed.

  Global Instance tnull_PN : PredicateToNRAEnv.PredicateToNRAEnv :=
    PredicateToNRAEnv.mk_C
      tnull_TD tnull_BD well_typed_predicate
      predicate_to_nraenv predicate_to_nraenv_ignores_did predicate_to_nraenv_eq.
    
  Definition and_or_to_nraenv a nf1 nf2  :=
    match a with
    | And_F  => NRAEnvAnd3 nf1 nf2
    | Or_F => NRAEnvOr3 nf1 nf2
    end.
  
  Lemma and_or_to_nraenv_ignores_did :
    forall a n1 n2,
    forall did1 did2 br i dreg,        
      nraenv_eval br i n1 dreg did1 = nraenv_eval br i n1 dreg did2 ->      
      nraenv_eval br i (and_or_to_nraenv a n1 n2) dreg did1 =
      nraenv_eval br i (and_or_to_nraenv a n1 n2) dreg did2.
  Proof.
    unfold nraenv_eval.
    intros [];do 7 intro;intros  H1.
    - simpl.
      unfold NRAEnvAnd3.
      cbn.
      unfold olift.     
      rewrite H1;simpl.
      destruct ( (br ⊢ₑ nraenv_to_nraenv_core n1 @ₑ did1 ⊣ i; dreg)%nraenv_core ) as [d| ];
        [|apply eq_refl].
      destruct d;apply eq_refl.
    - simpl.
      unfold NRAEnvOr3.
      cbn.
      unfold olift.
      rewrite H1;simpl.
      destruct ( (br ⊢ₑ nraenv_to_nraenv_core n1 @ₑ did2 ⊣ i; dreg)%nraenv_core ) as [d| ];[|apply eq_refl].
      destruct d;apply eq_refl.
  Qed.
  
  Lemma and_or_to_nraenv_eq :
    forall a n1 n2,
    forall b1 b2 br i dreg,        
      (forall did,nraenv_eval br i n1 dreg did = Some (bool3_to_data b1)) ->
      (forall did, nraenv_eval br i n2 dreg did = Some (bool3_to_data b2)) ->
      forall did,
        nraenv_eval br i (and_or_to_nraenv a n1 n2) dreg did =
        Some
          (bool3_to_data   (interp_conj Bool3.Bool3 a b1 b2)).
  Proof.      
    unfold nraenv_eval in *.
    intros [] n1 n2 b1 b2 br i dreg Hn1 Hn2 did.
    - refine (NRAEnvAnd3_eq _ _ _ _ _ _ _  Hn1 Hn2 _).
    - refine (NRAEnvOr3_eq _ _ _ _ _ _ _  Hn1 Hn2 _).
  Qed.
  
  Lemma NRAEnvCollNotEmpty_eq :
    forall  relname f1 f2 (q:query TRcd relname),
    forall   br i dreg,              
      ( exists ld,Permutation (bagT_to_listD (f2 q)) ld /\
             forall did, nraenv_eval br i (f1 q) dreg did = Some (dcoll ld) ) ->
      forall did,
        nraenv_eval br i (NRAEnvCollNotEmpty (f1 q)) dreg did =
        Some
          (dbool (negb (Febag.is_empty BTupleT (f2 q)))).
  Proof.
    unfold nraenv_eval in *.
    intros.
    destruct H as [ld Hld].
    rewrite (febag_is_empty_is_Z_le_bcount_0 _ ld).
    - cbn.
      unfold olift,olift2.
      rewrite (proj2 Hld).
      unfold lift,ondcoll.
      simpl.
      destruct ld;apply eq_refl.
    - intros.
      split;apply Permutation_in;[apply Hld| ].
      apply Permutation_sym;apply Hld.
  Qed.

  Definition NRAEnvSqlExists nq :=
    NRAEnvUnop OpLeft  (NRAEnvCollNotEmpty nq).    
  
  Lemma NRAEnvSqlExists_ignores_did :
    forall  nq,
    forall did1 did2 br i dreg,        
      nraenv_eval br i nq dreg did1 = nraenv_eval br i nq dreg did2 ->      
      nraenv_eval br i (NRAEnvSqlExists nq) dreg did1 =
      nraenv_eval br i (NRAEnvSqlExists nq) dreg did2.
  Proof.
    unfold nraenv_eval.
    do 6 intro;intros  H1.
    simpl.
    rewrite H1.
    apply eq_refl.
  Qed.

  Lemma NRAEnvSqlExists_eq :
    forall  relname f1 f2 (q:query TRcd relname),
    forall   br i dreg,              
      ( exists ld, Permutation (bagT_to_listD (f2 q)) ld /\ forall did, nraenv_eval br i (f1 q) dreg did = Some (dcoll ld) ) ->
      forall did,
        nraenv_eval br i (NRAEnvSqlExists (f1 q)) dreg did =
        Some
          (bool3_to_data (if Febag.is_empty BTupleT (f2 q) then false3 else true3)).
  Proof.
    unfold nraenv_eval in *.
    intros.
    destruct H as [ld Hld].
    rewrite (febag_is_empty_is_Z_le_bcount_0 _ ld).
    - cbn.
      unfold olift,olift2.
      rewrite (proj2 Hld did).
      unfold lift,ondcoll.
      simpl.
      destruct ld;apply eq_refl.
    - intros.
      split;apply Permutation_in;[apply Hld| ].
      apply Permutation_sym;apply Hld.
  Qed.

  Lemma NRAEnvSqlExists_eq_alt :
    forall  (f1 : query TRcd relname -> nraenv) (f2 : query TRcd relname -> bagT)
       (q : query TRcd relname) (br : list (string * string)) (i : list (string * data)) 
       (dreg : data),
      ( exists ld : list data, Permutation (bagT_to_listD (f2 q)) ld /\ forall did : data,
            nraenv_eval br i (f1 q) dreg did = Some (dcoll ld)) ->
      forall did : data,
        nraenv_eval br i (NRAEnvApp (NRAEnvEither NRAEnvID NRAEnvID) (NRAEnvSqlExists (f1 q))) dreg did =
        Some (dbool (if Febag.is_empty BTupleT (f2 q) then false else true)).
  Proof.
    unfold nraenv_eval.
    intros.
    assert (A1 := NRAEnvSqlExists_eq _ f1 f2 q br i dreg H did).
    unfold nraenv_eval in A1.
    simpl in A1.
    simpl;unfold olift,olift2 in *.
    rewrite A1.
    destruct Febag.is_empty_;apply eq_refl.
  Qed.  

  Definition NRAEnvExists3 nb3l :=
    let only_trues :=
        NRAEnvSelect
          (NRAEnvEither
             NRAEnvID
             (NRAEnvConst (dbool false)))
          nb3l in
    let test_true := NRAEnvHdDef
                     (dright dunit)
                     only_trues
    in
    let only_nulls :=
        NRAEnvSelect
          (NRAEnvEither
             (NRAEnvConst (dbool false))
             (NRAEnvConst (dbool true)))
          nb3l in
    let test_null := NRAEnvHdDef
                      (dleft (dbool false))
                      only_nulls
    in
    NRAEnvApp
      (NRAEnvEither
         (NRAEnvConst (dleft (dbool true)))
         (NRAEnvApp
            (NRAEnvEither (NRAEnvConst (dleft (dbool false)))
                          (NRAEnvConst (dright (dunit))) )
            test_null)
      )
      test_true.

  Lemma NRAEnvExists3_ignores_did :
    forall  ndl,
    forall did1 did2 br i dreg,        
      nraenv_eval br i ndl dreg did1 = nraenv_eval br i ndl dreg did2 ->      
      nraenv_eval br i (NRAEnvExists3 ndl) dreg did1 =
      nraenv_eval br i (NRAEnvExists3 ndl) dreg did2.
  Proof.
    unfold nraenv_eval.
    do 6 intro;intros  H1.
    simpl.
    rewrite H1.
    apply eq_refl.
  Qed.
  
  Lemma NRAEnvExists3_eq_aux1 :
    forall b3l,
      lift_filter
        (fun d1 : data =>
           match match d1 with
                 | dleft d0 => Some d0
                 | dright _ => Some (dbool false)
                 | _ => None
                 end with
           | Some (dbool b0) => Some b0
           | _ => None
           end) (map bool3_to_data b3l) = Some (filter  (fun d1 : data =>
                                                           match d1 with
                                                           | dleft (dbool b) => b
                                                           | dright _ =>  false
                                                           | _ => false
                                                           end ) (map bool3_to_data b3l)).
  Proof.
    induction b3l;[apply eq_refl| ].
    simpl in *.
    rewrite IHb3l.
    destruct a;apply eq_refl.
  Qed.

  Lemma NRAEnvExists3_eq_aux2 :
    forall b3l br i nd3l dreg ,
      (forall did,nraenv_eval br i nd3l dreg did = Some (dcoll (map bool3_to_data b3l))) ->      
      match
        match
          binary_op_eval br OpBagNth
                         (dcoll
                            (filter (fun d1 : data => match d1 with
                                                     | dleft (dbool b) => b
                                                     | _ => false
                                                     end) (map bool3_to_data b3l))) (dnat 0)
        with
        | Some x' => nraenv_core_eval br i (cNRAEnvEither cNRAEnvID (cNRAEnvConst (dright dunit))) dreg x'
        | None => None
        end
      with
      | Some x' =>
        nraenv_core_eval br i
                         (cNRAEnvEither (cNRAEnvConst (dleft (dbool true)))
                                        (cNRAEnvApp (cNRAEnvEither (cNRAEnvConst (dleft (dbool false))) (cNRAEnvConst (dright dunit)))
                                                    (cNRAEnvApp (cNRAEnvEither cNRAEnvID (cNRAEnvConst (dleft (dbool false))))
                                                                (cNRAEnvBinop OpBagNth
                                                                              (cNRAEnvSelect (cNRAEnvEither (cNRAEnvConst (dbool false)) (cNRAEnvConst (dbool true)))
                                                                                             (nraenv_to_nraenv_core nd3l)) (cNRAEnvConst (dnat 0)))))) dreg x'
      | None => None
      end =    
      match
        match
          binary_op_eval br OpBagNth
                         (dcoll
                            (filter (fun d1 : data => match d1 with
                                                     | dleft (dbool b) => b
                                                     | _ => false
                                                     end) (map bool3_to_data b3l))) (dnat 0)
        with
        | Some x' => nraenv_core_eval br i (cNRAEnvEither cNRAEnvID (cNRAEnvConst (dright dunit))) dreg x'
        | None => None
        end
      with
      | Some x' =>
        nraenv_core_eval br i
                         (cNRAEnvEither (cNRAEnvConst (dleft (dbool true)))
                                        (cNRAEnvApp (cNRAEnvEither (cNRAEnvConst (dleft (dbool false))) (cNRAEnvConst (dright dunit)))
                                                    (cNRAEnvApp (cNRAEnvEither cNRAEnvID (cNRAEnvConst (dleft (dbool false))))
                                                                (cNRAEnvBinop OpBagNth
                                                                              (cNRAEnvSelect (cNRAEnvEither (cNRAEnvConst (dbool false)) (cNRAEnvConst (dbool true)))
                                                                                             (nraenv_to_nraenv_core (NRAEnvConst (dcoll (map bool3_to_data b3l))))) (cNRAEnvConst (dnat 0)))))) dreg x'
      | None => None
      end.
  Proof.
    intros.
    destruct binary_op_eval;[|apply eq_refl].
    destruct nraenv_core_eval;[|apply eq_refl].
    destruct d0;try apply eq_refl.
    simpl.
    unfold nraenv_eval in H.
    rewrite H.
    simpl.
    do 2 apply f_equal.
    apply f_equal2;[|apply eq_refl].
    unfold lift.
    apply BasicFacts.match_option_eq.
    rewrite ListFacts.map_id;
      [apply eq_refl| ].
    intros d1.
    rewrite in_map_iff.
    intros [b [Hb Gb]].
    subst d1.
    destruct b;apply eq_refl.
  Qed.
  
  Lemma NRAEnvExists3_eq :
    forall  b3l br i nd3l dreg,
      (forall did,nraenv_eval br i nd3l dreg did = Some (dcoll (map bool3_to_data b3l))) ->
      forall did,
        nraenv_eval br i (NRAEnvExists3 nd3l) dreg did =
        Some (bool3_to_data ((Bool3.Bool.existsb Bool3.Bool3 (fun x => x)) b3l)).
  Proof.
    unfold nraenv_eval.
    intros.
    unfold NRAEnvExists3.
    simpl nraenv_to_nraenv_core.
    rewrite nraenv_core_eval_unfold.
    unfold olift.
    rewrite nraenv_core_eval_unfold;unfold olift.
    rewrite nraenv_core_eval_unfold;unfold olift2.
    rewrite nraenv_core_eval_unfold;unfold olift.
    rewrite H.    
    unfold lift_oncoll,lift.
    rewrite NRAEnvExists3_eq_aux1.
    simpl ( nraenv_core_eval br i (cNRAEnvConst (dnat 0)) dreg did ).
    cbv iota beta.
    rewrite NRAEnvExists3_eq_aux2;[|apply H].
    clear H.
    induction b3l.
    - apply eq_refl.
    - destruct a.
      + apply eq_refl.
      + simpl binary_op_eval in *.
        simpl Bool.existsb in *.
        rewrite <- IHb3l;clear IHb3l.
        destruct filter.
        * cbn.
          do 2 apply f_equal.
          apply f_equal2;[|apply eq_refl].
          unfold lift.
          apply BasicFacts.match_option_eq.
          destruct lift_filter;apply eq_refl.
        * destruct d;try apply eq_refl.
          cbn.
          do 2 apply f_equal.
          apply f_equal2;[|apply eq_refl].
          destruct lift_filter;apply eq_refl.
      + simpl binary_op_eval in *.
        simpl Bool.existsb in *.        
        destruct filter.
        * simpl in IHb3l.
          unfold olift,olift2,lift in IHb3l.
          simpl.
          unfold olift,olift2,lift.
          destruct lift_filter;[|discriminate].
          {
            simpl in *;destruct l;simpl in *.
            - inversion IHb3l.
              destruct (Bool.existsb Bool3 (fun x : bool3 => x) b3l);
                try discriminate;apply eq_refl.
            - destruct d;try discriminate;
                destruct (Bool.existsb Bool3 (fun x : bool3 => x) b3l);
                try discriminate;apply eq_refl.
          }          
        * {
            destruct d;try discriminate.
            - inversion IHb3l.
              destruct (Bool.existsb Bool3 (fun x : bool3 => x) b3l);
                try discriminate;apply eq_refl.
            - simpl in *.
              unfold olift,olift2,lift in *.
              destruct lift_filter;[|discriminate].              
              simpl in *;destruct l0;simpl in *.
              + inversion IHb3l;
                  destruct (Bool.existsb Bool3 (fun x : bool3 => x) b3l);
                  try discriminate;apply eq_refl.
              + destruct d0;try discriminate;
                  destruct (Bool.existsb Bool3 (fun x : bool3 => x) b3l);
                  try discriminate;apply eq_refl.
          }
  Qed.
  
  Lemma NRAEnvExists3_eq_permut :
    forall  b3l br i nd3l dreg,
      (forall did,nraenv_eval br i nd3l dreg did = Some (dcoll (map bool3_to_data b3l))) ->
      forall did b3l2,
        Permutation b3l b3l2 ->
        nraenv_eval br i (NRAEnvExists3 nd3l) dreg did =
        Some (bool3_to_data ((Bool3.Bool.existsb Bool3.Bool3 (fun x => x)) b3l2)).
  Proof.
    intros.
    refine (eq_trans
              (NRAEnvExists3_eq _ _ _ _ _ H _ )
              _).
    do 2 apply f_equal.
    rewrite eq_bool3_iff;repeat split;intro H1.
    - rewrite (Bool.existsb_exists_true Bool3) in *.
      destruct H1 as [x [H1 H2]].
      exists x;split;[|apply H2].
      apply (Permutation_in _ H0 H1).
    - rewrite (Bool.existsb_exists_false Bool3) in *.
      intros x Hx.
      apply H1.
      apply (Permutation_in _ (Permutation_sym H0) Hx).
    - rewrite Bool.existsb_not_forallb_not in *.
      replace unknown3 with (Bool.negb Bool3 unknown3);
        [|apply eq_refl].
      assert ((Bool.forallb Bool3 (fun x : Bool.b Bool3 => Bool.negb Bool3 x) b3l) = unknown3).
      {     replace unknown3 with (Bool.negb Bool3 unknown3);
              [|apply eq_refl].
            rewrite <- H1.
            rewrite Bool.negb_negb.
            apply eq_refl.
      }
      clear H1.              
      apply f_equal.
      rewrite forallb3_forall_unknown3 in *.
      destruct H2 as [[x [Hx Gx]] H2].
      split.
      + exists x;split;[|apply Gx].
        apply (Permutation_in _ H0 Hx).
      + intros.
        assert (A1 := Permutation_in _ (Permutation_sym H0) H1).
        apply (H2 _ A1).
  Qed.
  
  Definition NRAEnvForall3 nb3l :=
    let only_false :=
        NRAEnvSelect
          (NRAEnvEither
             (NRAEnvUnop OpNeg NRAEnvID)
             (NRAEnvConst (dbool false)))
          nb3l in
    let test_false := NRAEnvHdDef
                     (dright dunit)
                     only_false
    in
    let only_nulls :=
        NRAEnvSelect
          (NRAEnvEither
             (NRAEnvConst (dbool false))
             (NRAEnvConst (dbool true)))
          nb3l in
    let test_null := NRAEnvHdDef
                      (dleft (dbool false))
                      only_nulls
    in
    NRAEnvApp
      (NRAEnvEither
         (NRAEnvConst (dleft (dbool false)))
         (NRAEnvApp
            (NRAEnvEither (NRAEnvConst (dleft (dbool true)))
                          (NRAEnvConst (dright (dunit))) )
            test_null)
      )
      test_false.
  
  Lemma NRAEnvForall3_ignores_did :
    forall  ndl,
    forall did1 did2 br i dreg,        
      nraenv_eval br i ndl dreg did1 = nraenv_eval br i ndl dreg did2 ->      
      nraenv_eval br i (NRAEnvForall3 ndl) dreg did1 =
      nraenv_eval br i (NRAEnvForall3 ndl) dreg did2.
  Proof.
    unfold nraenv_eval.
    do 6 intro;intros  H1.
    simpl.
    rewrite H1.
    apply eq_refl.
  Qed.

  Lemma NRAEnvForall3_eq_aux1 :
    forall b3l,
      lift_filter
        (fun d1 : data =>
           match match d1 with
                 | dleft d0 =>  unudbool negb d0
                 | dright _ => Some (dbool false)
                 | _ => None
                 end with
           | Some (dbool b0) => Some b0
           | _ => None
           end) (map bool3_to_data b3l) = Some (filter  (fun d1 : data =>
                                                           match d1 with
                                                           | dleft (dbool b) => negb b
                                                           | dright _ =>  false
                                                           | _ => false
                                                           end ) (map bool3_to_data b3l)).
  Proof.
    induction b3l;[apply eq_refl| ].
    simpl in *.
    rewrite IHb3l.
    destruct a;apply eq_refl.
  Qed.

  Lemma NRAEnvForall3_eq_aux2 :
    forall b3l br i nd3l dreg ,
      (forall did,nraenv_eval br i nd3l dreg did = Some (dcoll (map bool3_to_data b3l))) ->
      match
        match
          binary_op_eval br OpBagNth
                         (dcoll
                            (filter (fun d1 : data => match d1 with
                                                     | dleft (dbool b) => negb b
                                                     | _ => false
                                                     end) (map bool3_to_data b3l))) (dnat 0)
        with
        | Some x' => nraenv_core_eval br i (cNRAEnvEither cNRAEnvID (cNRAEnvConst (dright dunit))) dreg x'
        | None => None
        end
      with
      | Some x' =>
        nraenv_core_eval br i
                         (cNRAEnvEither (cNRAEnvConst (dleft (dbool false)))
                                        (cNRAEnvApp (cNRAEnvEither (cNRAEnvConst (dleft (dbool true))) (cNRAEnvConst (dright dunit)))
                                                    (cNRAEnvApp (cNRAEnvEither cNRAEnvID (cNRAEnvConst (dleft (dbool false))))
                                                                (cNRAEnvBinop OpBagNth
                                                                              (cNRAEnvSelect (cNRAEnvEither (cNRAEnvConst (dbool false)) (cNRAEnvConst (dbool true)))
                                                                                             (nraenv_to_nraenv_core nd3l)) (cNRAEnvConst (dnat 0)))))) dreg x'
      | None => None
      end
      =
      match
        match
          binary_op_eval br OpBagNth
                         (dcoll
                            (filter (fun d1 : data => match d1 with
                                                     | dleft (dbool b) => negb b
                                                     | _ => false
                                                     end) (map bool3_to_data b3l))) (dnat 0)
        with
        | Some x' => nraenv_core_eval br i (cNRAEnvEither cNRAEnvID (cNRAEnvConst (dright dunit))) dreg x'
        | None => None
        end
      with
      | Some x' =>
        nraenv_core_eval br i
                         (cNRAEnvEither (cNRAEnvConst (dleft (dbool false)))
                                        (cNRAEnvApp (cNRAEnvEither (cNRAEnvConst (dleft (dbool true))) (cNRAEnvConst (dright dunit)))
                                                    (cNRAEnvApp (cNRAEnvEither cNRAEnvID (cNRAEnvConst (dleft (dbool false))))
                                                                (cNRAEnvBinop OpBagNth
                                                                              (cNRAEnvSelect (cNRAEnvEither (cNRAEnvConst (dbool false)) (cNRAEnvConst (dbool true)))
                                                                                             (nraenv_to_nraenv_core (NRAEnvConst (dcoll (map bool3_to_data b3l))))) (cNRAEnvConst (dnat 0)))))) dreg x'
      | None => None
      end.    
  Proof.
    intros.
    destruct binary_op_eval;[|apply eq_refl].
    destruct nraenv_core_eval;[|apply eq_refl].
    destruct d0;try apply eq_refl.
    simpl.
    unfold nraenv_eval in H.
    rewrite H.
    simpl.
    do 2 apply f_equal.
    apply f_equal2;[|apply eq_refl].
    unfold lift.
    apply BasicFacts.match_option_eq.
    rewrite ListFacts.map_id;
      [apply eq_refl| ].
    intros d1.
    rewrite in_map_iff.
    intros [b [Hb Gb]].
    subst d1.
    destruct b;apply eq_refl.
  Qed.

  Lemma NRAEnvForall3_eq :
    forall  b3l br i nd3l dreg,
      (forall did,nraenv_eval br i nd3l dreg did = Some (dcoll (map bool3_to_data b3l))) ->
      forall did,
        nraenv_eval br i (NRAEnvForall3 nd3l) dreg did =
        Some (bool3_to_data ((Bool3.Bool.forallb Bool3.Bool3 (fun x => x)) b3l)).
  Proof.
    unfold nraenv_eval.
    intros.
    unfold NRAEnvForall3.
    simpl nraenv_to_nraenv_core.
    rewrite nraenv_core_eval_unfold.
    unfold olift.
    rewrite nraenv_core_eval_unfold;unfold olift.
    rewrite nraenv_core_eval_unfold;unfold olift2.
    rewrite nraenv_core_eval_unfold;unfold olift.
    rewrite H.    
    unfold lift_oncoll,lift.
    rewrite NRAEnvForall3_eq_aux1.
    simpl ( nraenv_core_eval br i (cNRAEnvConst (dnat 0)) dreg did ).
    cbv iota beta.
    rewrite NRAEnvForall3_eq_aux2;[|apply H].
    clear H.
    induction b3l.
    - apply eq_refl.
    - destruct a;
        [|apply eq_refl| ].
      + simpl binary_op_eval in *.
        simpl Bool.forallb in *.        
        destruct filter.
        * simpl in IHb3l.
          unfold olift,olift2,lift in IHb3l.
          simpl.
          unfold olift,olift2,lift.
          destruct lift_filter;[|discriminate].
          {
            simpl in *;destruct l;simpl in *.
            - inversion IHb3l.
              destruct (Bool.forallb Bool3 (fun x : bool3 => x) b3l);
                try discriminate;apply eq_refl.
            - destruct d;try discriminate;
                destruct (Bool.forallb Bool3 (fun x : bool3 => x) b3l);
                try discriminate;apply eq_refl.
          }          
        * {
            destruct d;try discriminate.
            - inversion IHb3l.
              destruct (Bool.forallb Bool3 (fun x : bool3 => x) b3l);
                try discriminate;apply eq_refl.
            - simpl in *.
              unfold olift,olift2,lift in *.
              destruct lift_filter;[|discriminate].              
              simpl in *;destruct l0;simpl in *.
              + inversion IHb3l;
                  destruct (Bool.forallb Bool3 (fun x : bool3 => x) b3l);
                  try discriminate;apply eq_refl.
              + destruct d0;try discriminate;
                  destruct (Bool.forallb Bool3 (fun x : bool3 => x) b3l);
                  try discriminate;apply eq_refl.
          }          
      + simpl binary_op_eval in *.
        simpl Bool.forallb in *.        
        destruct filter.
        * simpl in IHb3l.
          unfold olift,olift2,lift in IHb3l.
          simpl.
          unfold olift,olift2,lift.
          destruct lift_filter;[|discriminate].
          {
            simpl in *;destruct l;simpl in *.
            - inversion IHb3l.
              destruct (Bool.forallb Bool3 (fun x : bool3 => x) b3l);
                try discriminate;apply eq_refl.
            - destruct d;try discriminate;
                destruct (Bool.forallb Bool3 (fun x : bool3 => x) b3l);
                try discriminate;apply eq_refl.
          }          
        * {
            destruct d;try discriminate.
            - inversion IHb3l.
              destruct (Bool.forallb Bool3 (fun x : bool3 => x) b3l);
                try discriminate;apply eq_refl.
            - simpl in *.
              unfold olift,olift2,lift in *.
              destruct lift_filter;[|discriminate].              
              simpl in *;destruct l0;simpl in *.
              + inversion IHb3l;
                  destruct (Bool.forallb Bool3 (fun x : bool3 => x) b3l);
                  try discriminate;apply eq_refl.
              + destruct d0;try discriminate;
                  destruct (Bool.forallb Bool3 (fun x : bool3 => x) b3l);
                  try discriminate;apply eq_refl.
          }
  Qed.

  Lemma NRAEnvForall3_eq_permut :
    forall  b3l br i nd3l dreg,
      (forall did,nraenv_eval br i nd3l dreg did = Some (dcoll (map bool3_to_data b3l))) ->
      forall did b3l2,
        Permutation b3l b3l2 ->
        nraenv_eval br i (NRAEnvForall3 nd3l) dreg did =
        Some (bool3_to_data ((Bool3.Bool.forallb Bool3.Bool3 (fun x => x)) b3l2)).
  Proof.
    intros.
    refine (eq_trans
              (NRAEnvForall3_eq _ _ _ _ _ H _ )
              _).
    do 2 apply f_equal.
    rewrite eq_bool3_iff;repeat split;intro H1.
    - rewrite (Bool.forallb_forall_true Bool3) in *.
      intros x Hx.
      apply H1.
      apply (Permutation_in _ (Permutation_sym H0) Hx).
    - rewrite (Bool.forallb_forall_false Bool3) in *.
      destruct H1 as [x [H1 H2]].
      exists x;split;[|apply H2].
      apply (Permutation_in _ H0 H1).
    -  rewrite forallb3_forall_unknown3 in *.
       destruct H1 as [[x [Hx Gx]] H2].
       split.
       + exists x;split;[|apply Gx].
         apply (Permutation_in _ H0 Hx).
       + intros.
         assert (A1 := Permutation_in _ (Permutation_sym H0) H1).
         apply (H2 _ A1).
  Qed.

  Definition quantifier_to_nraenv
             (quant:quantifier)  (nb3l : nraenv)   :=
    match quant with
    | Exists_F =>
      NRAEnvExists3 nb3l
    | Forall_F =>
      NRAEnvForall3 nb3l
    end.

  Lemma quantifier_to_nraenv_ignores_did :
    forall  (quant:quantifier)  ndl,
    forall did1 did2 br i dreg,        
      nraenv_eval br i ndl dreg did1 = nraenv_eval br i ndl dreg did2 ->      
      nraenv_eval br i (quantifier_to_nraenv quant ndl) dreg did1 =
      nraenv_eval br i (quantifier_to_nraenv quant ndl) dreg did2.
  Proof.
    intros [ ];
      [apply NRAEnvForall3_ignores_did| 
       apply NRAEnvExists3_ignores_did].
  Qed.
  
  Lemma quantifier_to_nraenv_eq :
    forall  quant b3l br i nd3l dreg,
      (forall did,nraenv_eval br i nd3l dreg did = Some (dcoll (map bool3_to_data b3l))) ->
      forall did,
        nraenv_eval br i (quantifier_to_nraenv quant nd3l) dreg did =
        Some (bool3_to_data (interp_quant Bool3.Bool3 quant (fun x => x) b3l)).
    intros [];
      [apply NRAEnvForall3_eq|apply NRAEnvExists3_eq].
  Qed.

  Lemma quantifier_to_nraenv_eq_permut :
    forall  quant b3l br i nd3l dreg,
      (forall did,nraenv_eval br i nd3l dreg did = Some (dcoll (map bool3_to_data b3l))) ->
      forall did b3l2,
        Permutation b3l b3l2 ->
        nraenv_eval br i (quantifier_to_nraenv quant nd3l) dreg did =
        Some (bool3_to_data (interp_quant Bool3.Bool3 quant (fun x => x) b3l2)).
    intros [];
      [apply NRAEnvForall3_eq_permut|apply NRAEnvExists3_eq_permut].
  Qed.

  Definition NRAEnvFilterNulls ndl :=
    NRAEnvSelect
      (NRAEnvEither
         (NRAEnvConst (dbool false))
         (NRAEnvConst (dbool true))) ndl.
  
  Lemma NRAEnvFilterNulls___eq :
    forall  vl br i ndl dreg,
      (forall did,nraenv_eval br i ndl dreg did = Some (dcoll (map value_to_data vl))) ->
      forall did,
        nraenv_eval br i (NRAEnvFilterNulls ndl) dreg did =
        Some (dcoll (filter is_dnull (map value_to_data vl))).
  Proof.
    unfold nraenv_eval.
    intros.
    simpl.
    rewrite H.
    unfold olift,lift;simpl.
    rewrite lift_filter_is_dnull_on_value_list.
    apply eq_refl.
  Qed.
  
  Definition NRAEnvRecContainsNulls la n :=
    let values := NRAEnvRecExtractValues la n in
    let only_nulls := NRAEnvFilterNulls values in
    let test := NRAEnvHdDef (dleft dunit) only_nulls in
    NRAEnvApp
      (NRAEnvEither
         (NRAEnvConst (dbool false))
         (NRAEnvConst (dbool true))) test.
  
  Lemma  NRAEnvRecContainsNulls_e_aux1 :
    forall t,
    exists l,
      lift_map
        (fun din : data =>
           match din with
           | dleft _ => Some (dbool false)
           | dright _ => Some (dbool true)
           | _ => None
           end) (map snd (tuple_as_dpairs t))
      = Some l.
  Proof.
    unfold tuple_as_dpairs.
    intro t.
    rewrite 2 lift_map_map_fusion.
    induction  (TupleToData.tuple_as_pairs t) as [|[a1 v1] lt];
      [exists nil;apply eq_refl| ].
    destruct IHlt.
    simpl.
    unfold lift.
    destruct v1;destruct o;simpl;simpl in H;rewrite H;
      try (exists (dbool false :: x);apply eq_refl);
      exists (dbool true :: x);apply eq_refl.
  Qed.
  
  Lemma  NRAEnvRecContainsNulls_e_aux2 :
    forall t,
    exists l,
      lift_filter
        (fun d1 : data =>
           match
             match d1 with
             | dleft _ => Some (dbool false)
             | dright _ => Some (dbool true)
             | _ => None
             end
           with
           | Some (dbool b) => Some b
           | _ => None
           end)(map snd (tuple_as_dpairs t))
      = Some l.
  Proof.
    unfold tuple_as_dpairs.
    intro t.
    rewrite map_map.
    unfold snd.
    induction  (TupleToData.tuple_as_pairs t) as [|[a1 v1] lt];
      [exists nil;apply eq_refl| ].
    destruct IHlt.
    simpl.
    unfold lift.
    destruct v1;destruct o;simpl;simpl in H;rewrite H;
      try (exists x;apply eq_refl).
    exists (dright (dstring ""):: x);apply eq_refl.
    exists (dright (dnat 0):: x);apply eq_refl.
    exists (dright (dbool true):: x);apply eq_refl. 
    exists (dright (dfloat float_zero) :: x);apply eq_refl.
  Qed.
  
  Lemma  NRAEnvRecContainsNulls_e_aux3 :
    forall t,
      lift_filter
        (fun d1 : data =>
           match
             match d1 with
             | dleft _ => Some (dbool false)
             | dright _ => Some (dbool true)
             | _ => None
             end
           with
           | Some (dbool b) => Some b
           | _ => None
           end)(map snd (tuple_as_dpairs t))
      = Some (filter is_dnull (map snd (tuple_as_dpairs t))).
  Proof.
    unfold tuple_as_dpairs.
    intro t.
    rewrite map_map.    
    simpl.
    rewrite <- map_map.
    apply lift_filter_is_dnull_on_value_list.
  Qed.    
  
  Lemma NRAEnvRecContainsNulls_ignores_did :
    forall  n1 ,
    forall   br i dreg la did1 did2,
      nraenv_eval br i n1 dreg did1 =
      nraenv_eval br i n1 dreg did2 ->
      nraenv_eval br i (NRAEnvRecContainsNulls la n1 ) dreg did1 =
      nraenv_eval br i (NRAEnvRecContainsNulls la n1 ) dreg did2.
  Proof.
    unfold nraenv_eval.
    do 7 intro.
    intros Hn1.
    unfold NRAEnvRecContainsNulls.
    simpl;unfold olift,olift2.
    do 4 apply BasicFacts.match_option_eq.
    refine (NRAEnvRecExtractValues_ignores_did _ _ _ _ _ _ _ _).
    apply Hn1.
  Qed.

  Lemma existsb_is_dnull_contains_nulls_eq :
    forall t,
      existsb is_dnull (map snd (tuple_as_dpairs t)) =
      contains_nulls t.
  Proof.
    intros.
    unfold contains_nulls.
    unfold tuple_as_dpairs.
    unfold TupleToData.tuple_as_pairs.
    rewrite 2 map_map.
    unfold snd.
    replace ({{{labels TNull t}}}) with ({{{TupleToData.labels t}}});[|auto].
    induction ({{{TupleToData.labels t}}});intros.
    - apply eq_refl.
    -  rewrite map_cons.
       rewrite 2 existsb_cons.
       replace (dot TNull t a) with ( (TupleToData.dot t a) ) in *;[|auto].
       apply f_equal2;[|apply IHl].       
       destruct (TupleToData.dot t a);destruct o;subst;
         apply eq_refl.
  Qed.
  
  Lemma NRAEnvRecContainsNulls_eq :
    forall  t n,
    forall br i dreg,
      (forall did,nraenv_eval br i n dreg did = Some (drec(tuple_as_dpairs t))) ->
      forall did,
        nraenv_eval
          br i
          (NRAEnvRecContainsNulls
             (domain (tuple_as_dpairs t))
             n) dreg did =
        Some
          (dbool (contains_nulls t)).
  Proof.
    unfold nraenv_eval.
    intro t;intros.
    simpl nraenv_core_eval.
    unfold olift2.
    assert (A1 := NRAEnvRecExtractValues___eq
                   _ _ _ _ _ H (NoDup_domain_tuple_as_dpairs _)).
    unfold nraenv_eval in A1.
    unfold olift.
    rewrite A1;clear A1.
    clear H.
    unfold lift.
    unfold ondcoll.
    rewrite (lift_map_edot_domain_snd t).
    simpl lift_oncoll.
    destruct (NRAEnvRecContainsNulls_e_aux2 t) as [bl Abl].
    simpl in Abl;rewrite Abl.
    unfold contains_nulls.
    repeat rewrite <- domain_labels_eq in *.
    unfold tuple_as_dpairs in Abl.
    unfold TupleToData.tuple_as_pairs in Abl.
    rewrite 2 map_map in Abl.
    unfold snd in Abl.
    replace ({{{labels TNull t}}}) with ({{{TupleToData.labels t}}});[|auto].
    revert bl Abl.
    induction ({{{TupleToData.labels t}}});intros.
    - inversion Abl;subst;apply eq_refl.
    -  rewrite map_cons in Abl.
       unfold lift_filter in Abl.
       unfold existsb.
       unfold lift in Abl.
       replace (dot TNull t a) with ( (TupleToData.dot t a) ) in *;[|auto].      
       destruct lift_filter eqn:D1 in Abl;
         [|destruct (TupleToData.dot t a);destruct o;discriminate].
       apply IHl in D1.
       destruct (TupleToData.dot t a);destruct o;inversion Abl;subst;
         try apply eq_refl;apply D1.
  Qed.

  Lemma NRAEnvRecContainsNulls_eq_depend :
    forall  t n,
    forall br i dreg did,
      (nraenv_eval br i n dreg did = Some (drec(tuple_as_dpairs t))) ->
      nraenv_eval
        br i
        (NRAEnvRecContainsNulls
           (domain (tuple_as_dpairs t))
           n) dreg did =
      Some
        (dbool (contains_nulls t)).
  Proof.
    unfold nraenv_eval.
    intro t;intros.
    simpl nraenv_core_eval.
    unfold olift2.
    assert (A1 := NRAEnvRecExtractValues___eq_depend
                   _ _ _ _ _ _ H (NoDup_domain_tuple_as_dpairs _)).
    unfold nraenv_eval in A1.
    unfold olift.
    simpl in A1;rewrite A1;clear A1.
    clear H.
    unfold lift.
    unfold ondcoll.
    assert (G1 := lift_map_edot_domain_snd t);simpl in G1.
    rewrite G1;clear G1.
    simpl lift_oncoll.
    destruct (NRAEnvRecContainsNulls_e_aux2 t) as [bl Abl].
    simpl in Abl;rewrite Abl.
    unfold contains_nulls.
    repeat rewrite <- domain_labels_eq in *.
    unfold tuple_as_dpairs in Abl.
    unfold TupleToData.tuple_as_pairs in Abl.
    rewrite 2 map_map in Abl.
    unfold snd in Abl.
    replace ({{{labels TNull t}}}) with ({{{TupleToData.labels t}}});[|auto].
    revert bl Abl.
    induction ({{{TupleToData.labels t}}});intros.
    - inversion Abl;subst;apply eq_refl.
    -  rewrite map_cons in Abl.
       unfold lift_filter in Abl.
       unfold existsb.
       unfold lift in Abl.
       replace (dot TNull t a) with ( (TupleToData.dot t a) ) in *;[|auto].      
       destruct lift_filter eqn:D1 in Abl;
         [|destruct (TupleToData.dot t a);destruct o;discriminate].
       apply IHl in D1.
       destruct (TupleToData.dot t a);destruct o;inversion Abl;subst;
         try apply eq_refl;apply D1.
  Qed.

  Definition NRAEnvMemberOf3 la nsl  lq nq :=
    NRAEnvIf
      (NRAEnvCollNotEmpty nq)
      (NRAEnvIf         
         (NRAEnvRecContainsNulls la nsl)         
         (NRAEnvConst (dright dunit))
         (NRAEnvIf
            (NRAEnvBinop OpContains nsl nq)
            (NRAEnvConst (dleft (dbool true)))
            (NRAEnvIf
               ((NRAEnvCollNotEmpty (NRAEnvSelect (NRAEnvRecContainsNulls lq NRAEnvID) nq)))
               (NRAEnvConst (dright dunit))
               (NRAEnvConst (dleft (dbool false))))
         )
      )
      (NRAEnvConst (dleft (dbool false))).

  Lemma NRAEnvMemberOf3_eq_aux :
    forall sl e (l : list TupleToData.tuple) (ld : list data),
      Permutation (listT_to_listD l) ld ->
      (if negb (if Z_le_dec (Z.of_nat (bcount (listT_to_listD l))) 0 then true else false)
       then
         if contains_nulls (projection TNull e (Select_List TNull (_Select_List TNull sl)))
         then dright dunit
         else
           if
             if
               in_dec data_eq_dec
                      (drec (tuple_as_dpairs (projection TNull e (Select_List TNull (_Select_List TNull sl))))) ld
             then true
             else false
           then dleft (dbool true)
           else
             if
               negb
                 (if
                     Z_le_dec
                       (Z.of_nat
                          (bcount
                             (filter
                                (fun b : data => match b with
                                                | drec r => existsb is_dnull (map snd r)
                                                | _ => false
                                                end) ld))) 0
                   then true
                   else false)
             then dright dunit
             else dleft (dbool false)
       else dleft (dbool false)) =
      bool3_to_data
        (Bool.existsb (B TRcd)
                      (fun x : tuple TNull =>
                         match Oeset.compare (OTuple TNull) (projection TNull e (Select_List TNull (_Select_List TNull sl))) x with
                         | Eq =>
                           if contains_nulls (projection TNull e (Select_List TNull (_Select_List TNull sl)))
                           then unknownB
                           else Bool.true (B TRcd)
                         | _ =>
                           if contains_nulls (projection TNull e (Select_List TNull (_Select_List TNull sl))) || contains_nulls x
                           then unknownB
                           else Bool.false (B TRcd)
                         end) l).
  Proof.
    intros sl e l ld Hperm.
    rewrite <- (Permutation_in_dec_eq  data_eq_dec Hperm).
    unfold bcount in *.
    rewrite <- (Permutation_length_filter_eq  _ Hperm).
    clear Hperm ld.
    rewrite <- existsb_is_dnull_contains_nulls_eq in *.
    apply eq_sym.
    destruct Z_le_dec as [D1|D1];simpl negb;cbv iota.
    - assert (Aux1 : l = nil).
      { destruct l;[apply eq_refl| ].
        simpl in  D1.
        rewrite Z.le_ngt in D1.
        destruct (D1 (Pos2Z.pos_is_pos _)).
      }
      subst;apply eq_refl.
    - destruct existsb eqn:D2.
      + simpl orb;cbv iota.
        rewrite <- bool3_to_data_unknown.
        apply f_equal.
        rewrite Bool.existsb_not_forallb_not.
        replace unknown3 with (Bool.negb _ unknownB) by auto.
        apply f_equal.       
        rewrite forallb3_forall_unknown3.
        split.
        * destruct l;
            [destruct (D1 (Z.le_refl _))| ].
          exists t;split;
            [left;apply eq_refl| ].
          simpl orb.
          destruct Oeset.compare;apply eq_refl.
        *  intros x Hx.
           simpl orb.
           intro X.
           destruct Oeset.compare;discriminate.        
      + simpl orb.
        destruct in_dec as [D3|D3].
        *  simpl orb;cbv iota.
           rewrite <- bool3_to_data_true.
           apply f_equal.
           rewrite (Bool.existsb_exists_true Bool3).
           rewrite <- mem_tuple_as_pairs_listT in D3.
           rewrite Oeset.mem_bool_true_iff in D3.
           destruct D3 as [t [D41 D42]].
           exists t;split;[apply D42| ].
           simpl TRcd in *;rewrite D41.
           apply eq_refl.
        * { destruct Z_le_dec as [D4|D4].
            - simpl negb;cbv iota.
              rewrite <- bool3_to_data_false.
              apply f_equal.
              refine (proj2 (Bool.existsb_exists_false (B TNull) _ _) _).
              intros x Hx.
              simpl orb.
              destruct Oeset.compare eqn:D5.
              +  assert (X1 := tuple_as_dpairs_eq).
                 simpl TRcd in X1.
                 rewrite X1 in D5.
                 rewrite D5 in D3.
                 destruct (D3 (in_map _ _ _ Hx)).
              + assert (D6: contains_nulls x = false).
                {
                  assert (Aux1:  (filter (fun b : data => match b with
                                                         | drec r => existsb is_dnull (map snd r)
                                                         | _ => false
                                                         end) (listT_to_listD l)) = nil).
                  { revert D4;clear.
                    destruct filter;intros;[apply eq_refl| ].
                    rewrite Z.le_ngt in D4.
                    destruct (D4 (Pos2Z.pos_is_pos _)).
                  }                 
                  assert (Aux2 := filter_nil_implies_not_pred _ _ Aux1).
                  rewrite <- existsb_is_dnull_contains_nulls_eq.
                  apply (Aux2 _ (in_map _ _ _ Hx)).                 
                }
                rewrite D6;apply eq_refl.
              + assert (D6: contains_nulls x = false).
                {
                  assert (Aux1:  (filter (fun b : data => match b with
                                                         | drec r => existsb is_dnull (map snd r)
                                                         | _ => false
                                                         end) (listT_to_listD l)) = nil).
                  { revert D4;clear.
                    destruct filter;intros;[apply eq_refl| ].
                    rewrite Z.le_ngt in D4.
                    destruct (D4 (Pos2Z.pos_is_pos _)).
                  }                 
                  assert (Aux2 := filter_nil_implies_not_pred _ _ Aux1).
                  rewrite <- existsb_is_dnull_contains_nulls_eq.
                  apply (Aux2 _ (in_map _ _ _ Hx)).                 
                }
                rewrite D6;apply eq_refl.
            - simpl negb;cbv iota.
              rewrite <- bool3_to_data_unknown.
              apply f_equal.
              rewrite Bool.existsb_not_forallb_not.
              replace unknown3 with (Bool.negb _ unknownB) by auto.
              apply f_equal.       
              rewrite forallb3_forall_unknown3.
              split.
              + 
                assert (exists t, In t (filter (fun b : data => match b with
                                                          | drec r => existsb is_dnull (map snd r)
                                                          | _ => false
                                                          end) (listT_to_listD l))).
                {
                  clear D1 D2.
                  induction l;
                    [destruct (D4 (Z.le_refl _))| ].
                  simpl in D4.         
                  destruct existsb eqn:D1.
                  - exists (drec  (tuple_as_dpairs a));simpl;rewrite D1;
                      left;apply eq_refl.
                  - simpl.
                    rewrite D1.
                    apply IHl in D4.
                    apply D4.
                    intro;apply D3;right;apply H.
                }
                destruct H.
                rewrite filter_In in H.
                destruct H.
                destruct x;try discriminate.
                assert (G1 := in_listT_is_drec _ _ H).
                destruct G1;inversion H1;subst.
                assert (G2 := mem_tuple_as_pairs_listT).
                simpl TRcd in G2.
                rewrite <- G2 in H.
                rewrite Oeset.mem_bool_true_iff in H.
                destruct H as [t1 [H H2]].
                exists t1;split;[apply H2| ].
                rewrite <- existsb_is_dnull_contains_nulls_eq.
                assert (X1 := tuple_as_dpairs_eq).
                simpl TRcd in X1.
                rewrite X1 in H.
                rewrite H in *.
                clear x H H1.
                destruct Oeset.compare eqn:D5.
                * rewrite X1 in D5.
                  rewrite D5 in *.
                  simpl in H0,D2;rewrite D2 in H0;discriminate.
                * simpl;rewrite H0;apply eq_refl.
                * simpl;rewrite H0;apply eq_refl.
              +  intros.
                 destruct Oeset.compare eqn:D5.
                 * intro;destruct existsb eqn:G3;try discriminate.
                   apply (in_map (fun x => drec (tuple_as_dpairs x)) l x) in H.
                   assert (X1 := tuple_as_dpairs_eq).
                   simpl TRcd in X1.
                   rewrite X1 in D5.
                   rewrite D5 in *.
                   destruct (D3 H).
                 * intro;destruct contains_nulls;discriminate.
                 * intro;destruct contains_nulls;discriminate.
          }
  Qed.
  
  Lemma NRAEnvMemberOf3_ignores_did :
    forall  n1 n2,
    forall   br i dreg la lq did1 did2,
      nraenv_eval br i n2 dreg did1 =
      nraenv_eval br i n2 dreg did2 ->
      nraenv_eval br i (NRAEnvMemberOf3 la n1 lq n2) dreg did1 =
      nraenv_eval br i (NRAEnvMemberOf3 la n1 lq n2) dreg did2.
  Proof.
    unfold nraenv_eval.
    do 9 intro.
    intros Hn2.
    unfold NRAEnvMemberOf3.
    simpl.
    unfold olift,olift2.
    do 7 apply BasicFacts.match_option_eq.
    apply Hn2.
  Qed.
  
  Lemma NRAEnvMemberOf3_eq:
    forall basesort sl_to_nraenv   q_to_nraenv q_to_bag,
    forall (sl:list (select TNull)) (q:query TNull relname) (e:env _),
    forall   br i dreg,
      (* forall t, *)
      (forall did, nraenv_eval
                br i
                (sl_to_nraenv sl) dreg did =
              Some (drec (tuple_as_dpairs (projection _ e (Select_List _ (_Select_List _ sl)))))) ->
      ( exists ld, Permutation (bagT_to_listD (q_to_bag q)) ld /\ forall did,
            nraenv_eval
              br i
              (q_to_nraenv q) dreg did = Some (dcoll ld)
      ) ->
      (forall t,
          t inBE (q_to_bag q) -> (labels _ t) =S= (SqlAlgebra.sort basesort q)(*  /\ *)
      (* forall a, *)
      (*   a inS (labels _ t) -> EnvToNRAEnv.well_typed_attribute a t = true *)) ->
      forall did,
        nraenv_eval
          br i
          (NRAEnvMemberOf3
             (let s' := map (fun x : select TRcd =>
                              match x with
                              | Select_As _ e a => (a, e)
                              end) sl in
              map attribute_to_string ({{{Fset.mk_set (A TNull) (map fst s')}}}))
             (sl_to_nraenv sl)
             (map attribute_to_string ({{{(SqlAlgebra.sort basesort q)}}}))
             (q_to_nraenv q)) dreg did =
        Some (bool3_to_data
                (let p :=  projection _ e (Select_List _ (_Select_List _ sl)) in
                 interp_quant
                   (Tuple.B _) Exists_F
                   (fun x : tuple _  =>
                      match Oeset.compare (OTuple _) p x with
                      | Eq => if contains_nulls p then unknownB else Bool.true (B _)
                      | _ => if contains_nulls p || contains_nulls x then unknownB else Bool.false (B _)
                      end)
                   ((Febag.elements BTupleT
                                    (( (q_to_bag q))))))).
  Proof.
    do 10 intro.
    intros H1 H2 Typ did.
    assert (X0 := H2).
    destruct H2 as [ld [H2 H3]] .
    unfold NRAEnvMemberOf3.
    simple refine
           (eq_trans
              (NRAEnvIf_spec_strong_depend_all
                 (NRAEnvCollNotEmpty (q_to_nraenv q))
                 (negb (Febag.is_empty BTupleT (q_to_bag q)))
                 (NRAEnvConst (dleft (dbool false)))
                 (dleft (dbool false))
                 _
                 (if contains_nulls (projection TNull e (Select_List TNull (_Select_List TNull sl)))
                  then dright dunit
                  else
                    if
                      if
                        in_dec data_eq_dec
                               (drec (tuple_as_dpairs (projection TNull e (Select_List TNull (_Select_List TNull sl))))) ld
                      then true
                      else false
                    then dleft (dbool true)
                    else
                      if
                        negb
                          (if
                              Z_le_dec
                                (Z.of_nat
                                   (bcount
                                      (filter
                                         (fun b : data => match b with
                                                         | drec r => existsb is_dnull (map snd r)
                                                         | _ => false
                                                         end) ld))) 0
                            then true
                            else false)
                      then dright dunit
                      else dleft (dbool false))
                 _ _ _ _
                 (NRAEnvCollNotEmpty_eq _ _ _ _ _ _ _ X0 _ )
                 _
                 (fun _ => eq_refl)
              )
              _).
    - intro Hyes.
      simple refine
             (eq_trans
                (NRAEnvIf_spec_strong_depend_all
                   _
                   (contains_nulls (projection TNull e (Select_List TNull (_Select_List TNull sl))))
                   _
                   (if
                       if
                         in_dec data_eq_dec
                                (drec (tuple_as_dpairs (projection TNull e (Select_List TNull (_Select_List TNull sl))))) ld
                       then true
                       else false
                     then dleft (dbool true)
                     else
                       if
                         negb
                           (if
                               Z_le_dec
                                 (Z.of_nat
                                    (bcount
                                       (filter
                                          (fun b : data => match b with
                                                          | drec r => existsb is_dnull (map snd r)
                                                          | _ => false
                                                          end) ld))) 0
                             then true
                             else false)
                       then dright dunit
                       else dleft (dbool false))
                   (NRAEnvConst (dright dunit))
                   (dright dunit)
                   _ _ _ _
                   _
                   (fun _ => eq_refl)
                   _
                )
                _).
      + assert (X1 := ATermToNRAEnv.ATermToNRAEnv.domain_projection_select_list_eq).
        cbv zeta in X1.
        simpl TRcd in *.
        rewrite (X1 _ e).
        simple refine
               (eq_trans
                  (NRAEnvRecContainsNulls_eq_depend _ _ _ _ _ _ (H1 _) )
                  eq_refl).
      + intros Hno2.
        simple refine
               (eq_trans
                  (NRAEnvIf_spec_strong_depend_all
                     _
                     (if
                         in_dec
                           data_eq_dec
                           (drec
                              (tuple_as_dpairs
                                 (projection
                                    TNull e (Select_List
                                               TNull (_Select_List
                                                        TNull sl)))))
                           ld
                       then true
                       else  false)
                     _
                     (if
                         negb
                           (if
                               Z_le_dec
                                 (Z.of_nat
                                    (bcount
                                       (filter (fun b : data => match b with
                                                               | drec r => existsb is_dnull (map snd r)
                                                               | _ => false
                                                               end) ld))) 0
                             then true
                             else false)
                       then dright dunit
                       else dleft (dbool false))
                     (NRAEnvConst (dleft (dbool true)))
                     (dleft (dbool true))
                     _ _ _ _
                     _
                     (fun _ => eq_refl)
                     _
                  )
                  _).
        *  unfold nraenv_eval in *.
           simpl nraenv_core_eval.
           rewrite H1,H3.
           unfold olift,olift2,ondcoll.
           destruct in_dec;apply eq_refl.
        * intro Hno.
          { simple refine
                   (eq_trans
                      (NRAEnvIf_spec_strong_depend_all
                         _
                         (negb
                            (if
                                Z_le_dec
                                  (Z.of_nat
                                     (bcount
                                        (filter
                                           (fun b : data => match b with
                                                           | drec r => existsb is_dnull (map snd r)
                                                           | _ => false
                                                           end) ld))) 0
                              then true
                              else false))
                         (NRAEnvConst (dleft (dbool false)))
                         (dleft (dbool false))
                         (NRAEnvConst (dright dunit))
                         (dright dunit)
                         _ _ _ _
                         _
                         (fun _ => eq_refl)
                         (fun _ => eq_refl)
                      )
                      _).
            -
              unfold NRAEnvCollNotEmpty.
              { simple refine
                       (eq_trans
                          (NRAEnvUnOpNeg_refine_depend _ _ _ _ _     (if
                                                                         Z_le_dec
                                                                           (Z.of_nat
                                                                              (bcount
                                                                                 (filter
                                                                                    (fun b : data => match b with
                                                                                                    | drec r => existsb is_dnull (map snd r)
                                                                                                    | _ => false
                                                                                                    end) ld))) 0
                                                                       then true
                                                                       else false) _ _)
                          _).
                - simple refine
                         (eq_trans
                            (NRAEnvBinOpLe_refine_depend _ _ _ _ _ (Z.of_nat (bcount (filter (fun b => 
                                                                                              match b with
                                                                                              | drec r =>
                                                                                                (existsb is_dnull (map snd r))
                                                                                              | _ => false
                                                                                              end) ld))) 0 _ _ _ _)
                            _).
                  + simple refine
                           (eq_trans
                              (NRAEnvUnOpCount_refine_depend _ _ _ _ _ (filter (fun b => 
                                                                                  match b with
                                                                                  | drec r =>
                                                                                    (existsb is_dnull (map snd r))
                                                                                  | _ => false
                                                                                  end) ld) _ _)
                              eq_refl).
                    simple refine
                           (eq_trans
                              (NRAEnvSelect_eval_filter_alt_depend _ _ _ _ (fun b : data => match b with
                                                                                           | drec r => existsb is_dnull (map snd r)
                                                                                           | _ => false
                                                                                           end) (fun x => x) _ _ _ ld _ _ _ )
                              _).
                    * rewrite map_id;apply H3.
                    * intros.                   
                      destruct (Permutation_listT_is_tuple _ H2 _ H) as [t Ht].
                      subst d.
                      apply (Permutation_in _ (Permutation_sym H2)) in H.
                      apply inBE_tuple_as_pairs_bagT in H.
                      assert (A2 := Typ _ H).
                      simpl TRcd in *.
                      rewrite <- (Fset.elements_spec1 _ _ _ A2).
                      assert (G1 := domain_labels_eq).
                      simpl TRcd in *.
                      rewrite <- G1.
                      { simple refine
                               (eq_trans
                                  (NRAEnvRecContainsNulls_eq_depend _ _ _ _ _ _ _)
                                  _).
                        - apply eq_refl.
                        - rewrite existsb_is_dnull_contains_nulls_eq.
                          apply eq_refl.
                      }
                    * rewrite map_id.
                      apply eq_refl.
                  + apply eq_refl.
                  + apply eq_refl.
                - apply eq_refl.
              }
            - apply eq_refl.
          }
        * apply eq_refl.
      + apply eq_refl.
    -  apply f_equal.
       rewrite (febag_is_empty_is_Z_le_bcount_0 _ (bagT_to_listD (q_to_bag q)));swap 1 2.
       + intro d;split;intro H;apply H.
       (* refine (Permutation_in _ H2 H ).         *)
       (* refine (Permutation_in _ (Permutation_sym H2) H ). *)
       + (* apply Permutation_sym in H2. *)
         revert  H2;clear.
         intro H3;revert ld H3.
         unfold bagT_to_listD.
         set ( l := Febag.elements BTupleT (q_to_bag q)).
         generalize l.
         unfold interp_quant.
         clear l.
         apply NRAEnvMemberOf3_eq_aux.
  Qed.

  Variable faac : float_add_assoc_comm.
  
  Global Instance tnull_FN basesort :
    @FormulaToNRAEnv.FormulaToNRAEnv 
      tnull_frt tnull_TD tnull_SN  tnull_EN tnull_FTN
      (tnull_AN faac) (tnull_ATN faac) relname ORN  (tnull_IN basesort)
      tnull_BD tnull_PN :=
    FormulaToNRAEnv.mk_C
      (tnull_ATN faac) (tnull_IN basesort) _
      and_or_to_nraenv and_or_to_nraenv_ignores_did
      and_or_to_nraenv_eq _ NRAEnvNot3_ignores_did NRAEnvNot3_eq
      _ NRAEnvIsTrue3_eq _ NRAEnvSqlExists_ignores_did NRAEnvSqlExists_eq
      _ quantifier_to_nraenv_ignores_did quantifier_to_nraenv_eq_permut
      NRAEnvMemberOf3
      NRAEnvMemberOf3_ignores_did
      (NRAEnvMemberOf3_eq _  )
      predicate_to_nraenv_quant predicate_to_nraenv_quant_ignores_did
      predicate_to_nraenv_quant_eq.      

End Sec.
