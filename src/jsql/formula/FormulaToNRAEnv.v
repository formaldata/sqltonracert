(************************************************************************************)
(**                                                                                 *)
(**                          The SQLToNRACert Library                               *)
(**                                                                                 *)
(**                       LMF, CNRS & Université Paris-Saclay                       *)
(**                                                                                 *)
(**                        Copyright 2016-2022 : FormalData                         *)
(**                                                                                 *)
(**         Authors: Véronique Benzaken                                             *)
(**                  Évelyne Contejean                                              *)
(**                  Mohammed Hachmaoui                                             *)
(**                  Chantal Keller                                                 *)
(**                                                                                 *)
(************************************************************************************)

Set Implicit Arguments.

Require Import Permutation Bool ZArith.

From SQLFS Require Import Formula Bool3 FiniteBag.

From SQLFS Require Import SqlAlgebra.

Require Import Qcert.NRAEnv.NRAEnvRuntime Qcert.Utils.Utils.
From SQLFS Require Import FiniteSet OrderedSet FiniteCollection FlatData.

Require Import ATermToNRAEnv InstanceToNRAEnv NRAEnvRefine.

Module BoolBToData.
  
  Export TupleToData.
  
  Class BoolBToData {frt:foreign_runtime} (B : Bool.Rcd) :=
    mk_C
      { dtrue : data;
        dfalse : data;
        dunkown : data;
        unknownB : Bool.b B;
        boolB_to_data :
          Bool.b B -> data;
        boolB_to_data_true :
          boolB_to_data (Bool.true B) = dtrue;
        boolB_to_data_false :
          boolB_to_data (Bool.false B) = dfalse;
        boolB_to_data_unkown :
          boolB_to_data unknownB = dunkown;
        normalize_data_dtrue :
          forall br, normalize_data br dtrue = dtrue;
      }.
  
End BoolBToData.

Module PredicateToNRAEnv.
  
  Export BoolBToData.
  Export  ATermToNRAEnv InstanceToNRAEnv.
  
  Class PredicateToNRAEnv
        `{frt:foreign_runtime} 
        `{TD : @TupleToData frt}
        `{BD : @BoolBToData  frt (B TRcd)}
    : Type :=
    mk_C
      {
        well_typed_predicate :
          predicate TRcd -> list (type TRcd) -> bool;
        predicate_to_nraenv :
          predicate TRcd -> list (type TRcd) -> list nraenv -> nraenv;
        predicate_to_nraenv_ignores_did :
          forall X pred (l:list X),
          forall f1  f3 br i dreg did1 did2 ,
            (forall x,
                In x l -> 
                nraenv_eval br i (f1 x)  dreg did1 =
                nraenv_eval br i (f1 x)  dreg did2 ) ->
            (* (forall ft, In ft l -> f3 ft = t) -> *)
            nraenv_eval
              br i
              (predicate_to_nraenv pred (map f3 l) (map f1 l))
              dreg did1 =
            nraenv_eval
              br i
              (predicate_to_nraenv pred (map f3 l) (map f1 l))
              dreg did2;
        predicate_to_nraenv_eq :
          forall X pred (lx:list X),
          forall nraenv_of_X value_of_X type_of_X,
          forall br i dreg,
            (forall x, In x lx ->
                  forall did,
                    nraenv_eval br i (nraenv_of_X x)  dreg did =
                    Some (value_to_data  (value_of_X x))) ->
            (forall ft,
                In ft lx ->
                type_of_X ft =
                type_of_value TRcd (value_of_X ft)) ->            
            forall did,
              nraenv_eval
                br i
                (predicate_to_nraenv
                   pred (map type_of_X lx) (map nraenv_of_X lx))
                dreg did =
              Some(boolB_to_data
                     (interp_predicate
                        TRcd pred (map value_of_X lx)));
      }. 

End PredicateToNRAEnv.

Module FormulaToNRAEnv.
  
  Export ATermToNRAEnv InstanceToNRAEnv PredicateToNRAEnv.

  Notation sort := (SqlAlgebra.sort).

  Class FormulaToNRAEnv
        `{frt:foreign_runtime}         
        `{TD : @TupleToData frt}        
        `{SN : @SymbolToNRAEnv frt TD}       
        {EN : @EnvToNRAEnv frt TD}
        `{FTN : @FTermToNRAEnv frt TD SN EN}        
        `{AN : @AggregateToNRAEnv frt TD }
        `{ATN : @ATermToNRAEnv frt TD SN AN EN  FTN}
        {relname : Type}
        {ORN : Oset.Rcd relname}
        `{IN : @InstanceToNRAEnv frt TD _ ORN}
        `{BD : @BoolBToData  frt (B TRcd)}
        `{PN :  @PredicateToNRAEnv frt TD BD} : Type :=
    mk_C
      {
        and_or_to_nraenv :
          and_or -> nraenv -> nraenv -> nraenv;
        and_or_to_nraenv_ignores_did :
          forall a n1 n2,
          forall did1 did2 br i dreg,        
            nraenv_eval br i n1 dreg did1 = nraenv_eval br i n1 dreg did2 ->      
            nraenv_eval br i (and_or_to_nraenv a n1 n2) dreg did1 =
            nraenv_eval br i (and_or_to_nraenv a n1 n2) dreg did2;        
        and_or_to_nraenv_eq :
          forall a n1 n2,
          forall b1 b2 br i dreg,        
            (forall did,nraenv_eval br i n1 dreg did = Some (boolB_to_data b1)) ->
            (forall did, nraenv_eval br i n2 dreg did = Some (boolB_to_data b2)) ->
            forall did,
              nraenv_eval br i (and_or_to_nraenv a n1 n2) dreg did =
              Some
                (boolB_to_data   (interp_conj (B TRcd) a b1 b2));
        NRAEnvNotB : nraenv -> nraenv;
        NRAEnvNotB_ignores_did :
          forall  nf,
          forall did1 did2 br i dreg,        
            nraenv_eval br i nf dreg did1 = nraenv_eval br i nf dreg did2 ->      
            nraenv_eval br i (NRAEnvNotB nf) dreg did1 =
            nraenv_eval br i (NRAEnvNotB nf) dreg did2;
        NRAEnvNotB_eq :
          forall n1,
          forall b1 br i dreg,        
            (forall did,nraenv_eval br i n1 dreg did = Some (boolB_to_data b1)) ->
            forall did,
              nraenv_eval br i (NRAEnvNotB n1) dreg did =
              Some
                (boolB_to_data   (Bool.negb (B TRcd) b1));        
        NRAEnvIsTrueB : nraenv -> nraenv;
        NRAEnvIsTrueB_eq :
          forall n1,
          forall b1 br i dreg,        
            (forall did,nraenv_eval br i n1 dreg did = Some (boolB_to_data b1)) ->
            forall did,
              nraenv_eval br i (NRAEnvIsTrueB n1) dreg did =
              Some
                (dbool  (Bool.is_true (B TRcd)  b1));
        NRAEnvSqlExists (nq: nraenv) : nraenv;
        NRAEnvSqlExists_ignores_did :
          forall  nq,
          forall did1 did2 br i dreg,        
            nraenv_eval br i nq dreg did1 = nraenv_eval br i nq dreg did2 ->      
            nraenv_eval br i (NRAEnvSqlExists nq) dreg did1 =
            nraenv_eval br i (NRAEnvSqlExists nq) dreg did2;
        NRAEnvSqlExists_eq :
          forall  relname f1  f2 (q: SqlAlgebra.query TRcd relname),
          forall   br i dreg,              
            (exists ld,
                Permutation (bagT_to_listD (f2 q)) ld /\
                forall did,
                  let tuple_as_dpairs t :=
                      (map
                         (fun x =>
                            (attribute_to_string (fst x),
                             value_to_data (snd x)))
                         (tuple_as_pairs t)) in
                  let listT_to_listD lt :=
                      map (fun x =>  (drec (tuple_as_dpairs x))) lt
                  in
                  let bagT_to_listD b :=
                      listT_to_listD (Febag.elements (Fecol.CBag (CTuple TRcd)) b) in
                  nraenv_eval br i (f1 q) dreg did = Some (dcoll ld)) ->
            forall did,
              nraenv_eval br i (NRAEnvSqlExists (f1 q)) dreg did =
              Some
                (boolB_to_data
                   (if Febag.is_empty (Fecol.CBag (CTuple TRcd)) (f2 q)
                    then Bool.false (B TRcd) else Bool.true (B TRcd)));
        quantifier_to_nraenv :
          quantifier ->  nraenv -> nraenv;
        quantifier_to_nraenv_ignores_did :
          forall  (quant:quantifier)  ndl,
          forall did1 did2 br i dreg,        
            nraenv_eval br i ndl dreg did1 = nraenv_eval br i ndl dreg did2 ->      
            nraenv_eval br i (quantifier_to_nraenv quant ndl) dreg did1 =
            nraenv_eval br i (quantifier_to_nraenv quant ndl) dreg did2;
        quantifier_to_nraenv_eq_permut :
          forall  quant b3l br i nd3l dreg,
            (forall did,nraenv_eval br i nd3l dreg did = Some (dcoll (map boolB_to_data b3l))) ->
            forall did b3l2,
              Permutation b3l b3l2 ->
              nraenv_eval br i (quantifier_to_nraenv quant nd3l) dreg did =
              Some (boolB_to_data (interp_quant (B TRcd) quant (fun x => x) b3l2));
        NRAEnvMemberOfB (ls: list String.string) (nsl : nraenv)
                        (lq: list String.string): nraenv -> nraenv;
        NRAEnvMemberOfB_ignores_did :
          forall  n1 n2,
          forall   br i dreg la lq did1 did2,
            nraenv_eval br i n2 dreg did1 =
            nraenv_eval br i n2 dreg did2 ->
            nraenv_eval br i (NRAEnvMemberOfB la n1 lq n2) dreg did1 =
            nraenv_eval br i (NRAEnvMemberOfB la n1 lq n2) dreg did2;
        NRAEnvMemberOfB_eq:
          forall  sl_to_nraenv   q_to_nraenv q_to_bag,
          forall (sl:list (select TRcd)) (q:query TRcd relname) (e:env ),
          forall   br i dreg,
            (* forall t, *)
            (forall did, nraenv_eval
                      br i
                      (sl_to_nraenv sl) dreg did =
                    Some (drec
                            (tuple_as_dpairs
                               (projection
                                  TRcd e
                                  (Select_List
                                     TRcd (_Select_List TRcd sl)))))) ->
            ( exists ld, Permutation (bagT_to_listD (q_to_bag q)) ld /\
                    forall did,  nraenv_eval
                              br i
                              (q_to_nraenv q) dreg did = Some (dcoll ld)
            ) ->
            (forall t,
                      t inBE (q_to_bag q) ->
                      (labels t) =S= SqlAlgebra.sort basesort q) ->
            forall did,
              nraenv_eval
                br i
                (NRAEnvMemberOfB
                   (let s' := map (fun x : select TRcd =>
                                    match x with
                                    | Select_As _ e a => (a, e)
                                    end) sl in
                    map attribute_to_string ({{{Fset.mk_set A (map fst s')}}}))
                   (sl_to_nraenv sl)
                   (map attribute_to_string ({{{(sort basesort q)}}}))
                   (q_to_nraenv q)) dreg did =
              Some (boolB_to_data
                      (let p :=  projection TRcd e (Select_List TRcd (_Select_List TRcd sl)) in
                       interp_quant
                         (Tuple.B TRcd) Exists_F
                         (fun x : tuple  =>
                            match Oeset.compare OTuple p x with
                            | Eq => if contains_nulls p then unknownB else Bool.true (B _)
                            | _ => if contains_nulls p || contains_nulls x then unknownB else Bool.false (B _)
                            end)
                         ((Febag.elements BTupleT
                                          (( (q_to_bag q)))))));
        predicate_to_nraenv_quant (p:Tuple.predicate TRcd)
             (ltype : list (Tuple.type TRcd)) (lnra : list nraenv)             
             (sa : Fset.set A) (nq : nraenv) : nraenv;
        predicate_to_nraenv_quant_ignores_did :
    forall X pred (lx:list X),
    forall f1  f3 br i dreg did1 did2 s n,
      (
        nraenv_eval br i n  dreg did1 =
        nraenv_eval br i n  dreg did2 ) ->
      (* (forall x, *)
      (*     In x lx ->  *)
      (*     nraenv_eval br i (f1 x)  dreg did1 = *)
      (*     nraenv_eval br i (f1 x)  dreg did2 ) -> *)
      (* (forall ft, In ft l -> f3 ft = t) -> *)
      nraenv_eval br i (predicate_to_nraenv_quant pred (map f3 lx) (map f1 lx) s n) dreg did1 =
      nraenv_eval br i (predicate_to_nraenv_quant pred (map f3 lx) (map f1 lx) s n) dreg did2;
        predicate_to_nraenv_quant_eq :
    forall X Y pred (lx:list X) (y: Y),
    forall nraenv_of_X value_of_X type_of_X,
    forall nraenv_of_Y  labels_of_Y (listT_of_Y:Y -> list (tuple )),
    forall br i dreg ld,
      (Permutation  (listT_to_listD (listT_of_Y y)) ld /\
             forall did, nraenv_eval br i ( nraenv_of_Y y) dreg did = Some (dcoll ld) ) ->
      (forall x, In x lx ->
            forall did,
              nraenv_eval br i (nraenv_of_X x)  dreg did =
              Some (value_to_data  (value_of_X x))) ->
      (forall x, In x lx -> type_of_X x = Tuple.type_of_value TRcd (value_of_X x)) ->      
      (forall t, In t (listT_of_Y y) ->
            labels_of_Y y =S= labels  t /\ (forallb  (fun a => well_typed_attribute a t) ({{{labels_of_Y y}}})) = true) ->
      exists ld',
        Permutation ld'
                    (map boolB_to_data
                         (map
                            (fun y:tuple =>
                               interp_predicate TRcd pred (map value_of_X lx ++ 
                                                                (map (fun a => dot  y a) ({{{labels  y}}}))))
                            (listT_of_Y y))) /\    
        forall did,
          nraenv_eval
            br i
            (predicate_to_nraenv_quant
               pred (map type_of_X lx)
               (map nraenv_of_X lx)
               (labels_of_Y y)
               (nraenv_of_Y y)) dreg did =Some (dcoll ld');
      }.

  Section Sec.

    Hypothesis relname : Type.
    Hypothesis ORN : Oset.Rcd relname.
    Context {frt : foreign_runtime}.
    Context {TD:TupleToData}.
    Context {SN:SymbolToNRAEnv}.
    Context {EN : EnvToNRAEnv}.
    Context {AN:AggregateToNRAEnv}.
    Context {FTN:FTermToNRAEnv}.    
    Context {ATN:ATermToNRAEnv}.
    Context {IN : InstanceToNRAEnv ORN}.
    Context {BD : BoolBToData (B TRcd)}.
    Context {PN : PredicateToNRAEnv}.
    Context {FN : FormulaToNRAEnv }.

    Notation query := (query TRcd relname).    
    Notation q_formula :=
      (sql_formula TRcd query ).
    
    Arguments Q_Empty_Tuple {T} {relname} .
    Arguments Q_Table {T} {relname} .
    Arguments Sql_True {T} {dom} .

    Section Translation.

      Section Formulas.
        
        Variable query_to_nraenv :   list ((Fset.set A) * group_by)   -> query ->  (@nraenv frt).
          
        Fixpoint formula_to_nraenv_gen nenv (f : q_formula) : nraenv :=
          match f with
          | Sql_Conj c f1 f2 =>
            and_or_to_nraenv
              c
              (formula_to_nraenv_gen nenv f1)
              (formula_to_nraenv_gen nenv f2)
          | Sql_Not f =>
            NRAEnvNotB (formula_to_nraenv_gen nenv f)
          | Sql_True => NRAEnvConst dtrue
          | Sql_Pred _ p lag =>
            predicate_to_nraenv
              p
              (map type_of_aggterm lag)
              (map (aggterm_to_nraenv nenv) lag)
          | Sql_Exists  _  q =>
            NRAEnvSqlExists (query_to_nraenv nenv q)
          | Sql_In s q =>  
            NRAEnvMemberOfB
              (let s' := map
                          (fun x : select TRcd =>
                             match x with
                             | Select_As _ e a => (a, e)
                             end) s in
               map
                 attribute_to_string
                 ({{{Fset.mk_set A (map fst s')}}}))
              (select_list_to_nraenv nenv (_Select_List _ s))
              (map attribute_to_string ({{{(sort basesort q)}}}))
              (query_to_nraenv nenv q)
          | Sql_Quant quant  p l q =>
            (* Q_Sigma (a op aq) q <> empty if {aq} = sort(q) and l = [a] *)
            let lq := Fset.elements _ (SqlAlgebra.sort  (basesort ) q) in
            (* let newreg := *)
            (*     fun z n => *)
            (*       NRAEnvAppEnv z *)
            (*                    (NRAEnvBinop  *)
            (*                       OpRecConcat *)
            (*                       (NRAEnvUnop  (OpRec "tl") NRAEnvEnv) *)
            (*                       (NRAEnvUnop  (OpRec "slc") (NRAEnvUnop OpBag  n))) *)
            (* in *)
            (* let l := *)
            (*     NRAEnvMap *)
            (*       (predicate_to_nraenv *)
            (*          p *)
            (*          (map type_of_aggterm l ++ map (fun a => type_of_attribute _ a) lq) *)
            (*          ((map (aggterm_to_nraenv   nenv)   l) ++ *)
            (*                                                map (fun aq => (newreg (env_dot_to_nraenv ((sort (basesort ) q,Group_Fine  _)::nenv) aq) NRAEnvID)) lq *)
            (*       )) (query_to_nraenv nenv q) *)
            (* in *)
            let newreg :=
                fun z =>
                  NRAEnvAppEnv z
                                   (NRAEnvBinop 
                                      OpRecConcat
                                      (NRAEnvUnop  (OpRec "tl") NRAEnvEnv)
                                      (NRAEnvUnop  (OpRec "slc") (NRAEnvUnop OpBag  NRAEnvID)))
            in
                let nq :=
                    (* NRAEnvMap  ( (newreg (select_list_to_nraenv     ((sort (basesort ) q,Group_Fine  _)::nenv) (_Select_List _ (map (fun a => Select_As _ (A_Expr  (F_Dot  a)) a) lq))))) *) (query_to_nraenv nenv q) in 
                let l :=                    
                    predicate_to_nraenv_quant
              p
              (map type_of_aggterm l (* ++ map (fun a => type_of_attribute _ a) lq *))
              (map (aggterm_to_nraenv   nenv)   l)
              ((sort basesort q))
              nq
                in
            quantifier_to_nraenv quant l                                 
          end.
        
      End Formulas.
      
    End Translation.

    Section Lemmas.

      Notation q_formula :=
        (sql_formula TRcd query).

      Notation query_size q :=
        (Tree.tree_size
           ((@tree_of_query TRcd  relname) q)).
      Notation sql_formula_size f :=
        (Tree.tree_size
           (tree_of_sql_formula
              (@tree_of_query TRcd  relname )f)).
      Notation size_funterm ft :=
        (@size_funterm value attribute  ft).   
      
      Notation eval_query i :=
        (eval_query  basesort
                     (fun r =>
                        match Oset.find ORN r i with
                        | Some b => b
                        |None => Febag.empty _
                        end) unknownB
                     contains_nulls).
      
      Notation eval_sql_formula :=
        (Formula.eval_sql_formula 
           unknownB
           contains_nulls).
      
      Arguments Q_Empty_Tuple {T} {relname}.
      Arguments Q_Table {T} {relname}.
      Arguments Sql_True {T} {dom}.  

      Notation sort := (SqlAlgebra.sort).

      Variable query_to_nraenv : nenv -> query -> nraenv.
      
      Hypothesis nraenv_eval_query_id_eq :
        forall  br i f q,
          query_size q <= sql_formula_size f ->
          forall  aenv,
          forall (reg id1 id2:data),
            nraenv_eval
              br (rec_sort (instance_to_bindings i))
              (query_to_nraenv  aenv  q) reg id1 =
            nraenv_eval
              br (rec_sort (instance_to_bindings i))
              (query_to_nraenv  aenv  q) reg id2.
      
      Notation formula_to_nraenv :=
        (formula_to_nraenv_gen query_to_nraenv).

      Lemma nraenv_eval_formula_id_eq :
        forall br i f (aenv:nenv),
        forall reg id1 id2,
          nraenv_eval
            br (rec_sort (instance_to_bindings i))
            (formula_to_nraenv   aenv f) reg id1 =
          nraenv_eval
            br (rec_sort (instance_to_bindings i))
            (formula_to_nraenv  aenv f) reg id2.
      Proof.
        intros br i f.
        simpl foreign_runtime_data in *.
        unfold  nraenv_eval  in *.
        set
          (n :=
             (Tree.tree_size
                (tree_of_sql_formula
                   (@tree_of_query TRcd _ )  f))).
        assert (Hn := le_n n);unfold n at 1 in Hn.
        clearbody n; revert f Hn.
        induction n as [ | n];intros f Hn env ;
          [destruct f;inversion Hn| intros reg id1 id2].
        destruct f as [ | | | | | |  ].
        - simpl.
          apply sql_formula_size_Conj in Hn.
          assert (IHf1 := IHn _ (proj1 Hn) env   reg id1 id2).
          assert (IHf2 := IHn _ (proj2 Hn) env  reg id1 id2).
          unfold nraenv_eval in IHf1,IHf2.
          refine (and_or_to_nraenv_ignores_did _ _ _ _ _ _ _ _ IHf1).
        - simpl .
          apply sql_formula_size_Not in Hn.
          assert (IHf1 := IHn _ Hn env  reg id1 id2).
          refine (NRAEnvNotB_ignores_did _ _ _ _ _ _ IHf1).
        - apply eq_refl.
        - simpl.
          refine (predicate_to_nraenv_ignores_did _ _ _ _ _ _ _ _ _ _ ).
          intros;apply aggterm_to_nraenv_ignores_did.
        -  assert (A1 : query_size q0 <= sql_formula_size (Sql_Quant  q p l q0) ).
           {
             do 3 apply le_S.
             apply Plus.le_plus_trans.
             apply Plus.le_plus_trans.
             apply le_n.
           }
           assert (A3 := nraenv_eval_query_id_eq br i (Sql_Quant  q p l q0)).
           simpl formula_to_nraenv.
           (* destruct l;[apply eq_refl| ].        *)
           (* destruct l;[|apply eq_refl]. *)
           (* destruct ({{{sort basesort q0}}}) eqn:D1; *)
           (*   [apply eq_refl| ]. *)
           (* destruct l;[|apply eq_refl].        *)
           refine (quantifier_to_nraenv_ignores_did _ _ _ _ _ _ _ _ ).
           refine (predicate_to_nraenv_quant_ignores_did p l (aggterm_to_nraenv env) _ _ _ _ _ _ _ _ _ ).
           apply (A3 _ A1).
        - simpl.      
          refine (NRAEnvMemberOfB_ignores_did  _ _ _ _ _ _ _ _ _ _ ).
          + assert (Aux1: query_size q <= sql_formula_size (Sql_In l  q)) .
            {
              do 2 apply le_S.
              apply Plus.le_plus_trans.
              simpl.
              rewrite Plus.plus_comm,Plus.plus_0_r.
              apply le_S.
              apply Plus.le_plus_trans.
              apply le_n.
            }
            apply (nraenv_eval_query_id_eq _ _ _ _ Aux1).
        - cbn.
          assert (Aux1: query_size q <= sql_formula_size (Sql_Exists _  q)) .
          {
            do 2 apply le_S.
            apply Plus.le_plus_trans.
            apply Plus.le_plus_trans.
            apply le_n.
          }     
          refine (NRAEnvSqlExists_ignores_did _ _ _ _ _ _ _).
          apply (nraenv_eval_query_id_eq _ _ _ _ Aux1).
      Qed.

      Notation BTupleT := (Fecol.CBag (CTuple TRcd)).
      Notation bagT := (Febag.bag BTupleT).    
      
      Lemma query_to_nraenv_is_empty :
        forall br i,
          well_sorted_instance i = true ->
          forall f q,
            query_size q <= sql_formula_size f ->
            forall env did,
              well_formed_e TRcd env = true ->
              forall dq,
                nraenv_eval
                  br
                  (rec_sort (instance_to_bindings i))
                  (query_to_nraenv  (map fst env)  q)
                  (env_to_data  env) did =
                Some (dcoll dq) ->
                Permutation  (bagT_to_listD ((eval_query i) env q)) dq ->
                Febag.is_empty BTupleT ((eval_query i) env q) =
                if ZArith_dec.Z_le_dec (BinInt.Z.of_nat (bcount dq)) 0
                then true else false.
      Proof.
        intros.
        apply febag_is_empty_is_Z_le_bcount_0.
        intro d;split.
        - apply Permutation_in.
          apply H3.
        -  apply Permutation_in.
           apply Permutation_sym.
           apply H3.
      Qed.

      Hypothesis ws_instance_tuples :
        forall i env (q:query),
          well_sorted_instance i = true ->
          forall t, t inBE (eval_query  i env   q) ->
               labels  t =S= sort basesort q.

      Variable is_complete_instance_q :
        (list (relname * (Febag.bag (Fecol.CBag (CTuple TRcd))))) -> query -> bool.

      Variable is_translatable_q :
        query -> bool.

      Variable more_well_formed_q :
              list (Fset.set (Tuple.A TRcd) * Env.group_by TRcd * list (Tuple.tuple TRcd)) ->
              SqlAlgebra.query TRcd relname -> bool.
      
        Hypothesis well_typed_instance_queries :
          forall i e q,
            well_typed_e e = true ->
            well_typed_instance i = true ->
            is_complete_instance_q i q = true ->
            is_translatable_q q = true ->
            well_typed_list_tuple (Febag.elements _ ((eval_query i) e q)) = true.
          

      Fixpoint is_complete_instance_f
               (i:(list (relname * (Febag.bag (Fecol.CBag (CTuple TRcd))))))
               (f:q_formula) :=
        match f with
        | Sql_Conj TRcd f1 f2 =>
          is_complete_instance_f  i f1 && is_complete_instance_f  i f2
        | Sql_Not f0 => is_complete_instance_f i f0
        | Sql_True => true
        | Sql_Pred _ _ l => true
        | Sql_Quant _ _ l q =>
          is_complete_instance_q i q
        | Sql_In s q =>          
          is_complete_instance_q i q
        | Sql_Exists _ q =>               
          is_complete_instance_q i q
        end.

      Fixpoint is_translatable_f
               (f:q_formula) :=
        match f with
        | Sql_Conj TRcd f1 f2 =>
          is_translatable_f f1 && is_translatable_f f2
        | Sql_Not f0 => is_translatable_f f0
        | Sql_True => true
        | Sql_Pred _ _ l => true
        | Sql_Quant _ _ l q =>
          is_translatable_q q
        | Sql_In s q =>          
          is_translatable_q q
        | Sql_Exists _ q =>               
          is_translatable_q q
        end.

      
      Fixpoint more_well_formed_f e
               (f:q_formula) :=
        match f with
        | Sql_Conj TRcd f1 f2 =>
          more_well_formed_f  e f1 && more_well_formed_f  e f2
        | Sql_Not f0 => more_well_formed_f e f0
        | Sql_True => true
        | Sql_Pred _ _ l => true
        | Sql_Quant _ _ l q =>
          more_well_formed_q e q
        | Sql_In s q =>          
          more_well_formed_q e q
        | Sql_Exists _ q =>               
          more_well_formed_q e q
        end.

      Lemma hyp_query_ind : 
        forall n br i,
          (forall (q : query),
              query_size q <= S n ->
              is_complete_instance_q i q = true ->
              is_translatable_q q = true ->
              (* well_typed_q q = true -> *)
              forall (env : list (nslice * list tuple)) ,
                well_typed_e env =  true ->
                well_formed_e _  env = true ->
                is_a_translatable_e env = true ->
                well_formed_q basesort env q =  true ->
                more_well_formed_q  env q =  true ->             
                exists ld : list data,
                  Permutation (bagT_to_listD ((eval_query i) env q)) ld /\
                  forall (did : data),
                    nraenv_core_eval
                      br (rec_sort (instance_to_bindings i))
                      (nraenv_to_nraenv_core (query_to_nraenv (map fst env) q))
                      (env_to_data env) did =
                    Some (dcoll ld))  ->
          (forall q : query,
              query_size q <=  n ->
              is_complete_instance_q i q = true ->
              is_translatable_q q = true ->
              (* well_typed_q q = true -> *)
              forall (env : list (nslice * list tuple )) ,
                well_typed_e env =  true ->
                well_formed_e _  env = true ->
                is_a_translatable_e env = true ->
                well_formed_q basesort env q =  true ->                
                more_well_formed_q  env q =  true ->
                exists ld,
                  Permutation
                    (bagT_to_listD ((eval_query i) env q))
                    ld /\
                  forall (did : data),
                    nraenv_core_eval
                      br (rec_sort (instance_to_bindings i))
                      (nraenv_to_nraenv_core
                         (query_to_nraenv (map fst env) q))
                      (env_to_data  env) did =
                    Some (dcoll ld)).
      Proof.
        do 3 intro;intros H q Hn.
        apply H.
        apply le_S.
        apply Hn.
      Qed.        

      Lemma predicate_to_nraenv_eq_2 :
          forall X Y pred (lx:list X) (ly: list Y),
          forall nraenv_of_X value_of_X type_of_X,
          forall nraenv_of_Y value_of_Y type_of_Y,
          forall br i dreg,
            (forall x, In x lx ->
                  forall did,
                    nraenv_eval br i (nraenv_of_X x)  dreg did =
                    Some (value_to_data  (value_of_X x))) ->
            (forall x, In x lx -> type_of_X x = Tuple.type_of_value TRcd (value_of_X x)) ->
            (forall y, In y ly ->
                  forall did,
                    nraenv_eval br i (nraenv_of_Y y)  dreg did =
                    Some (value_to_data  (value_of_Y y))) ->
            (forall y, In y ly -> type_of_Y y = Tuple.type_of_value TRcd (value_of_Y y)) ->
            forall did,
              nraenv_eval
                br i
                (predicate_to_nraenv
                   pred (map type_of_X lx ++ map type_of_Y ly) (map nraenv_of_X lx ++ map nraenv_of_Y ly)) dreg did =
              Some(boolB_to_data   (interp_predicate TRcd pred (map value_of_X lx ++ map value_of_Y ly))).
      Proof.
        intros.
        rewrite  map_app_sum.
        rewrite  (map_app_sum nraenv_of_X).
        rewrite  (map_app_sum value_of_X).
        refine (predicate_to_nraenv_eq _ _ _ _ _ _ _ _ _ _ _).
        - intros.
          destruct  (In_app _ _ _ H3).
          + rewrite in_map_iff in H4.
            destruct H4 as [x1 [H4 H5]].
            destruct x;[|discriminate].
            apply H.
            inversion H4;subst;apply H5.
           + rewrite in_map_iff in H4.
            destruct H4 as [x1 [H4 H5]].
            destruct x;[discriminate| ].
            apply H1.
            inversion H4;subst;apply H5.
        - intros ft H3.
          apply In_app in H3.
          destruct H3.
          +  rewrite in_map_iff in H3.
            destruct H3 as [x3 [H3 H4]].
            destruct ft;[|discriminate].
            apply H0;inversion H3;subst;apply H4.
          +   rewrite in_map_iff in H3.
            destruct H3 as [x3 [H3 H4]].
            destruct ft;[discriminate| ].
            apply H2;inversion H3;subst;apply H4.
      Qed.

      Lemma Permutation_map_one_side :
        forall X Y (f:X -> Y) l1 l2,
          Permutation l1 (map f l2) ->
          exists l3, l1 = map f l3 /\ Permutation l3 l2.
      Proof.
        do 5 intro.
        rewrite <- _permut_eq_Permutation.
        revert l2.
        induction l1 as [|a1 l1].
        - intros.
          inversion H as [H1 H2| ].
          apply eq_sym in H2.
          apply map_eq_nil in H2.
          subst.
          exists nil;auto.
        - intros.
          inversion H;subst.
          apply eq_sym in H2.
          apply ListFacts.map_app_alt in H2.
          destruct H2 as [m1 [m2 [-> [<- M1]]]].
          destruct (map_eq_cons _ _ _ _ M1) as [x4 [m4 [-> [ <- <-]]]].
          rewrite <-  map_app in H4.
          apply IHl1 in H4.
          destruct H4 as [k1 [-> H5]].
          exists (x4::k1);split.
          apply eq_refl.
          apply Permutation_cons_app.
          apply H5.
      Qed.
      
      Lemma formula_to_nraenv_is_sound :
        forall br i,  well_sorted_instance i = true ->
                 well_typed_instance i = true ->
                 forall f,
                   is_complete_instance_f i f = true ->
                   is_translatable_f f = true ->
                   (* well_typed_f f = true -> *)
                   (forall q,
                       query_size q <= sql_formula_size f ->
                       is_complete_instance_q i q  = true ->
                       is_translatable_q q  = true ->
                       forall env,
                         well_typed_e env =  true ->
                         well_formed_e _ env = true ->
                         is_a_translatable_e env = true ->
                         well_formed_q basesort env q =  true ->
                         more_well_formed_q env q =  true ->                      
                         exists ld, Permutation
                                 (bagT_to_listD ((eval_query i) env q))
                                 ld
                               /\
                               forall did,
                                 nraenv_eval
                                   br (rec_sort (instance_to_bindings i))
                                   (query_to_nraenv  (map fst env)  q)
                                   (env_to_data  env) did = Some (dcoll ld))
                   ->
                   forall env,                     
                     well_typed_e env =  true ->
                     well_formed_e _ env = true ->
                     is_a_translatable_e env = true ->
                     well_formed_f  basesort  env f = true ->
                     more_well_formed_f    env f = true ->
                     forall  did,
                       nraenv_eval
                         br (rec_sort (instance_to_bindings i))
                         (formula_to_nraenv  (map fst env)  f)
                         (env_to_data env) did =
                       Some (boolB_to_data
                               (eval_sql_formula (eval_query i) env f)).
      Proof.
        intros br i WI Ti f.
        unfold nraenv_eval.
        set (n := sql_formula_size  f).
        assert (Hn := le_n n);unfold n at 1 in Hn.
        clearbody n; revert f Hn .
        induction n as [ | n];[intros f Hn;destruct f;inversion Hn| ].
        intros [o f1 f2| f| | p l|quant p l sq | l sq| sq]
               Hn Tc Tf (* Typ *) Hq env Typ WS Te Wt Wf did.
        -                           (* Sql_Conj - Qed *)
          simpl.
          simpl in Wt,Tc,Tf,Wf.
          rewrite andb_true_iff in Wt,Tc,Tf,Wf.
          refine (and_or_to_nraenv_eq _ _ _ _ _ _ _ _ _ _ _).
          refine (IHn _
                      (proj1 (sql_formula_size_Conj Hn))
                      (proj1 Tc) (proj1 Tf) (hyp_query_ind _ Hq)
                      env Typ WS Te (proj1 Wt) (proj1 Wf)).      
          refine (IHn _
                      (proj2 (sql_formula_size_Conj Hn))
                      (proj2 Tc) (proj2 Tf) (hyp_query_ind _   Hq)
                      env Typ WS   Te (proj2 Wt) (proj2 Wf)).
        -                           (* Sql_Not - Qed *)
          simpl.
          refine (NRAEnvNotB_eq _ _ _ _ _ _ _ ).
          refine (IHn _
                      (sql_formula_size_Not Hn)
                      Tc Tf (hyp_query_ind _ Hq)
                      env Typ WS Te Wt Wf).      
        -                           (* Sql_True - Qed *)
          simpl;rewrite boolB_to_data_true.
          rewrite normalize_data_dtrue.
          apply eq_refl.
        -                           (* Sql_Pred - Qed *)
          cbn.
          refine (predicate_to_nraenv_eq
                    _ _ _ _ _ _ _ _ _ _ _ );swap 1 2.
          + intros ft H;
              rewrite (type_of_aggterm_eq _ _ Typ);
              apply eq_refl.
          + intros.
            apply aggterm_to_nraenv_is_sound;auto.
            simpl in Wt.
            rewrite forallb_forall in Wt.
            apply (Wt _ H).
        -                           (* Sql_Quant *)
          apply andb_true_iff in Wt.
          simpl in Tf.
          
          destruct (Hq sq (le_S _ _ (sql_formula_size_Quant   Hn)) Tc Tf env Typ WS Te (proj1 Wt) Wf) as
              [ ld [Pld Aq]].          
          assert (A1 := predicate_to_nraenv_quant_eq p l sq (aggterm_to_nraenv (map fst env))).
          assert (A1 := A1 (interp_aggterm TRcd env) type_of_aggterm ).
          assert (A1 := A1 (query_to_nraenv (map fst env)) (sort basesort)).
          assert (A1 := A1 (fun q => Febag.elements BTupleT ((eval_query i) env q))).
          assert (A1 := A1 br  (rec_sort (instance_to_bindings i))).
          assert (A1 := A1 (env_to_data env) ld (conj Pld Aq)).
          assert (A2 := aggterm_to_nraenv_is_sound br (rec_sort (instance_to_bindings i))).
          assert (A2 := fun ag did => A2 ag env did Typ Te WS).
          rewrite forallb_forall in Wt.
          assert (A2 := fun ag H did => A2 ag did  ((proj2 Wt) ag H)).          
          assert (A1 := A1 A2).
          clear A2.
          assert (A3 := fun ag  => eq_sym (type_of_aggterm_eq ag env Typ)).
          assert (A1 := A1 (fun x H => A3 x)).
          assert (A4 := ws_instance_tuples  _ env sq  WI).
          assert (A4 := fun t H => (A4 t (Febag.in_elements_mem _ _ _ H))).
          assert (Aux1 :
                    (forall t : tuple,
                        In t ((fun q : query => Febag.elements BTupleT ((eval_query i) env q)) sq) ->
                        sort basesort sq =S= labels t /\
                        forallb (fun a : attribute => well_typed_attribute a t) ({{{sort basesort sq}}}) = true)).
          {
            intros;split.
            - apply A4 in H.
              rewrite (Fset.equal_eq_2 _ _ _ _ H).
              apply Fset.equal_refl.
            -  assert (X1 := well_typed_instance_queries  env  Typ Ti Tc Tf).
               unfold well_typed_list_tuple in X1.
               rewrite forallb_forall in X1.
               rewrite <- (Fset.elements_spec1 _ _ _ (A4 _ H)).
               apply (X1 _ H).
          }          
          assert (A1 := A1 Aux1).
          destruct A1 as [ld' [Pld' Aq']].
          destruct (Permutation_map_one_side _ _ Pld') as [lt' [-> X1]].          
          simple refine (eq_trans
                           (@quantifier_to_nraenv_eq_permut
                              _  _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
                              _ _ _ _  _
                              (map
                                 (fun x : tuple => interp_predicate TRcd p (map (interp_aggterm TRcd env) l ++ map (dot x) ({{{labels x}}})))
              (Febag.elements BTupleT ((eval_query i) env sq)))  X1)
                           _);swap 1 2.
          + do 2 apply f_equal.
            rewrite interp_quant_id.
            apply eq_refl.
          + intros did1.
            apply Aq'.
        -                           (* Sql_In  *)
          rewrite eval_sql_formula_unfold.
          simpl nraenv_core_eval.
          simpl in Wt.
          rewrite andb_true_iff in Wt.
          set (sl_to_nraenv :=
                 fun l =>
                   (select_list_to_nraenv_
                      (map fst env)
                      (map
                         (fun x : attribute =>
                            (x,
                             match
                               Oset.find OAtt x
                                         (map
                                            (fun x0 : select TRcd =>
                                               match x0 with
                                               | Select_As _ e a => (a, e)
                                               end) l)
                             with
                             | Some e => e
                             | None =>
                               A_Expr
                                 (F_Constant
                                    TRcd
                                    (default_value
                                       TRcd (type_of_attribute TRcd x)))
                             end))
                         ({{{Fset.mk_set
                              A
                              (map fst
                                   (map (fun x : select TRcd =>
                                           match x with
                                           | Select_As _ e a => (a, e)
                                           end) l))}}})))).
          set (q_to_nraenv :=
                 fun q =>
                   (query_to_nraenv (map fst env) q)).
          set (q_to_bag :=
                 fun q =>
                   ((eval_query i) env q)).
          refine (NRAEnvMemberOfB_eq sl_to_nraenv q_to_nraenv q_to_bag
                                     _ _ _ _ _ _ _ _ _ _).
          + intro did1.
            refine (eq_trans                  
                      (select_list_to_nraenv_is_sound _ _ _ _ Typ WS Te _ (proj2 Wt))
                      _).
            do 2 apply f_equal.
            apply tuple_as_dpairs_eq.
            unfold projection,sort_of_select_list.
            apply mk_tuple_eq;[|intros;apply eq_refl].
            rewrite map_map.
            rewrite (Fset.equal_eq_1 _ _ _ _  (Fset.mk_set_idem _ _  )).
            rewrite Fset.equal_spec.
            intros.
            do 2 apply f_equal.
            apply map_ext;intros;destruct a;apply eq_refl.
          + refine (Hq _ (le_S _ _ (sql_formula_size_In Hn)) Tc Tf _ Typ WS Te  (proj1 Wt) Wf).
          + subst q_to_bag.
            intros.
            apply (ws_instance_tuples  _ _ _ WI _ H).
        -                           (* Sql_Exists - Qed *)
          cbn.
          refine (NRAEnvSqlExists_eq _ _ _ _ _ _ _ _).
          simpl.
          assert (Aux1: query_size sq <= sql_formula_size (Sql_Exists _  sq)) .
          {
            do 2 apply le_S.
            apply Plus.le_plus_trans.
            apply Plus.le_plus_trans.
            apply le_n.
          }
          assert (Aux2: query_size sq <= S n) .
          {
            refine(Le.le_trans _ _ _ _ Hn).
            apply Aux1.
          }
          apply (Hq _  Aux2 Tc Tf   _  Typ WS Te  Wt Wf).
      Qed.  

    End Lemmas.
    
  End Sec.

End FormulaToNRAEnv.
