(************************************************************************************)
(**                                                                                 *)
(**                          The SQLToNRACert Library                               *)
(**                                                                                 *)
(**                       LMF, CNRS & Université Paris-Saclay                       *)
(**                                                                                 *)
(**                        Copyright 2016-2022 : FormalData                         *)
(**                                                                                 *)
(**         Authors: Véronique Benzaken                                             *)
(**                  Évelyne Contejean                                              *)
(**                  Mohammed Hachmaoui                                             *)
(**                  Chantal Keller                                                 *)
(**                                                                                 *)
(************************************************************************************)

Require Export TnullTD.

Require Import FormulaToNRAEnv Qcert.NRAEnv.NRAEnvRuntime.
From SQLFS Require Import Bool3.

Section Sec.

  
  Definition bool3_to_data b : data :=
    match b with
    | Bool3.true3 => dleft (dbool true)
    | Bool3.false3 => dleft (dbool false)
    | Bool3.unknown3 => dright dunit
    end.

  Lemma bool3_to_data_true :
    bool3_to_data (Bool.true Bool3.Bool3) =
    dleft (dbool true).
  Proof.
    apply eq_refl.
  Qed.
  
  Lemma bool3_to_data_false :
    bool3_to_data (Bool.false Bool3.Bool3) =
    dleft (dbool false).
  Proof.
    auto.
  Qed.   
  
  Lemma bool3_to_data_unknown :
    bool3_to_data Bool3.unknown3 =
    dright dunit.
  Proof.
    auto.
  Qed.
  
  Lemma normalize_data_dtrue :
    forall br : brand_relation_t,
      normalize_data br (dleft (dbool true))  = dleft (dbool true).
  Proof.
    auto.
  Qed.    
  
  Global Instance tnull_BD : BoolBToData.BoolBToData (FTuples.Tuple.B TupleToData.TupleToData.TRcd) :=
    BoolBToData.mk_C
      tnull_frt Bool3.Bool3 _ _ bool3_to_data_true
      bool3_to_data_false bool3_to_data_unknown
      normalize_data_dtrue.


End Sec.
