(************************************************************************************)
(**                                                                                 *)
(**                          The SQLToNRACert Library                               *)
(**                                                                                 *)
(**                       LMF, CNRS & Université Paris-Saclay                       *)
(**                                                                                 *)
(**                        Copyright 2016-2022 : FormalData                         *)
(**                                                                                 *)
(**         Authors: Véronique Benzaken                                             *)
(**                  Évelyne Contejean                                              *)
(**                  Mohammed Hachmaoui                                             *)
(**                  Chantal Keller                                                 *)
(**                                                                                 *)
(************************************************************************************)

Require Import List String ZArith.

Require Import Qcert.NRAEnv.NRAEnvRuntime Qcert.Utils.Utils.

Section Sec.

  Lemma NRAEnvUnOpLeft_refine :
    forall (fruntime : foreign_runtime) (br : list (string * string)) (i : list (string * data)) dreg
      (n : nraenv) (d : data) ,
      (forall  did : data, (br ⊢ n @ₓ did ⊣ i; dreg)%nraenv = Some d) ->
      forall  did : data,
        (br ⊢ NRAEnvUnop OpLeft n @ₓ did ⊣ i; dreg)%nraenv =
        Some (dleft d).
  Proof.
    unfold nraenv_eval in *.
    intros.
    cbn;rewrite H.
    apply eq_refl.
  Qed.
  
  Lemma NRAEnvUnOpLeft_refine_depend :
    forall (fruntime : foreign_runtime) (br : list (string * string)) (i : list (string * data)) dreg
      (n : nraenv) (d : data) ,
    forall  did : data, (br ⊢ n @ₓ did ⊣ i; dreg)%nraenv = Some d ->
                   (br ⊢ NRAEnvUnop OpLeft n @ₓ did ⊣ i; dreg)%nraenv =
                   Some (dleft d).
  Proof.
    unfold nraenv_eval in *.
    intros.
    cbn;rewrite H.
    apply eq_refl.
  Qed.

  Lemma NRAEnvBinOpNatDivt_refine :
    forall (fruntime : foreign_runtime) (br : list (string * string)) (i : list (string * data)) 
      (n1 n2 : nraenv) z1 z2 dreg,
      (forall did : data, (br ⊢ n1 @ₓ did ⊣ i; dreg)%nraenv = Some (dnat z1)) ->
      (forall  did : data, (br ⊢ n2 @ₓ did ⊣ i; dreg)%nraenv = Some (dnat z2)) ->    
      forall  did : data,
        (br ⊢ NRAEnvBinop (OpNatBinary NatDiv) n1 n2 @ₓ did ⊣ i; dreg)%nraenv =
        Some (dnat (Z.quot z1 z2)).
  Proof.
    unfold nraenv_eval in *.
    intros.
    cbn;rewrite H,H0.    
    apply eq_refl.
  Qed.

  Lemma NRAEnvBinOpNatDivt_refine_depend :
    forall (fruntime : foreign_runtime) (br : list (string * string)) (i : list (string * data)) 
      (n1 n2 : nraenv) z1 z2 dreg did,
      ( (br ⊢ n1 @ₓ did ⊣ i; dreg)%nraenv = Some (dnat z1)) ->
      ( (br ⊢ n2 @ₓ did ⊣ i; dreg)%nraenv = Some (dnat z2)) ->    
      (br ⊢ NRAEnvBinop (OpNatBinary NatDiv) n1 n2 @ₓ did ⊣ i; dreg)%nraenv =
      Some (dnat (Z.quot z1 z2)).
  Proof.
    unfold nraenv_eval in *.
    intros.
    cbn;rewrite H,H0.    
    apply eq_refl.
  Qed.

  Lemma NRAEnvBinOpFloatDivt_refine :
    forall (fruntime : foreign_runtime) (br : list (string * string)) (i : list (string * data))
      (n1 n2 : nraenv) f1 f2 dreg,
      (forall did : data, (br ⊢ n1 @ₓ did ⊣ i; dreg)%nraenv = Some (dfloat f1)) ->
      (forall  did : data, (br ⊢ n2 @ₓ did ⊣ i; dreg)%nraenv = Some (dfloat f2)) ->
      forall  did : data,
        (br ⊢ NRAEnvBinop (OpFloatBinary FloatDiv) n1 n2 @ₓ did ⊣ i; dreg)%nraenv =
        Some (dfloat (float_div f1 f2)).
  Proof.
    unfold nraenv_eval in *.
    intros.
    cbn;rewrite H,H0.
    apply eq_refl.
  Qed.

  Lemma NRAEnvUnOpFloatMean_refine :
    forall (fruntime : foreign_runtime) (br : list (string * string)) (i : list (string * data))
      (m : nraenv) f z dreg,
      (forall did : data, (br ⊢ NRAEnvUnop OpFloatSum m @ₓ did ⊣ i; dreg)%nraenv = Some (dfloat f)) ->
      (forall  did : data, (br ⊢ NRAEnvUnop OpCount m @ₓ did ⊣ i; dreg)%nraenv = Some (dnat z)) ->
      forall  did : data,
        (br ⊢ NRAEnvUnop OpFloatMean m @ₓ did ⊣ i; dreg)%nraenv =
        Some (dfloat match z with
       | Z0 => float_zero
       | _ =>
           float_div f
             (float_of_int z)
       end).
  Proof.
    unfold nraenv_eval in *.
    intros.
    cbn.
    unfold lifted_farithmean, olift.
    simpl in *.
    case_eq (br ⊢ₑ nraenv_to_nraenv_core m @ₑ did ⊣ i; dreg)%nraenv_core.
    - intros d Hd.
      assert (H1 := H did). rewrite Hd in H1.
      assert (H2 := H0 did). rewrite Hd in H2.
      simpl in *.
      unfold lifted_fsum in H1.
      unfold float_list_arithmean.
      unfold lift_oncoll.
      destruct d; try discriminate.
      simpl in *. inversion H2. subst z. clear H2. unfold bcount.
      unfold lift.
      case_eq (lifted_fbag l); [intros l'| ]; intro Heq; rewrite Heq in H1; try discriminate.
      simpl in H1. inversion H1. subst f. clear H1.
      assert (H2:Datatypes.length l' = Datatypes.length l).
      { clear H H0 Hd. revert l l' Heq. clear.
        induction l as [ |lx lxs IHlxs]; intros [ |l'x l'xs]; unfold lifted_fbag; auto; simpl.
        - discriminate.
        - case (ondfloat (fun x : float => x) lx); try discriminate.
          intros f. unfold lift. case (lift_map (ondfloat (fun x : float => x)) lxs); discriminate.
        - case_eq (ondfloat (fun x : float => x) lx); try discriminate.
          intros f Heq. unfold lift.
          case_eq (lift_map (ondfloat (fun x : float => x)) lxs); try discriminate.
          intros l Hl H. inversion H. subst f l.
          rewrite IHlxs; auto.
      }
      rewrite H2. case (Datatypes.length l); reflexivity.
    - intro Hd. assert (H1 := H did). rewrite Hd in H1. discriminate.
  Qed.
  
  Lemma NRAEnvUnOpNatMax_refine :
    forall (fruntime : foreign_runtime) (br : list (string * string)) (i : list (string * data)) dreg
      (n : nraenv) l ,
      (forall  did : data, (br ⊢ n @ₓ did ⊣ i; dreg)%nraenv = Some (dcoll (map dnat l))) ->
      forall  did : data,
        (br ⊢ NRAEnvUnop OpNatMax n @ₓ did ⊣ i; dreg)%nraenv =
        Some (dnat (bnummax l)).
  Proof.
    unfold nraenv_eval in *.
    intros.
    cbn;rewrite H.
    unfold olift.
    unfold lifted_max.
    unfold lift.
    unfold lifted_zbag.
    rewrite lift_map_map_fusion.
    simpl.
    rewrite lift_map_map.
    rewrite map_id.
    apply eq_refl.
  Qed.

  Lemma NRAEnvUnOpNatMax_refine_depend :
    forall (fruntime : foreign_runtime) (br : list (string * string)) (i : list (string * data)) dreg
      (n : nraenv) l did ,(br ⊢ n @ₓ did ⊣ i; dreg)%nraenv = Some (dcoll (map dnat l)) ->
                          (br ⊢ NRAEnvUnop OpNatMax n @ₓ did ⊣ i; dreg)%nraenv =
                          Some (dnat (bnummax l)).
  Proof.
    unfold nraenv_eval in *.
    intros.
    cbn;rewrite H.
    unfold olift.
    unfold lifted_max.
    unfold lift.
    unfold lifted_zbag.
    rewrite lift_map_map_fusion.
    simpl.
    rewrite lift_map_map.
    rewrite map_id.
    apply eq_refl.
  Qed.

  Lemma NRAEnvUnOpNatMax_refine_alt :
    forall (fruntime : foreign_runtime) X (br : list (string * string)) (i : list (string * data)) dreg
      (n : nraenv) l  (f:X -> _),
      (forall  did : data, (br ⊢ n @ₓ did ⊣ i; dreg)%nraenv = Some (dcoll (map f l))) ->
      forall  did : data,
        (br ⊢ NRAEnvUnop OpNatMax n @ₓ did ⊣ i; dreg)%nraenv =
        lifted_max (map f l).
  Proof.
    unfold nraenv_eval in *.
    intros.
    cbn;rewrite H.
    unfold olift.
    apply eq_refl.
  Qed.

  Lemma NRAEnvUnOpNatMax_refine_alt_depend :
    forall (fruntime : foreign_runtime) X (br : list (string * string)) (i : list (string * data)) dreg
      (n : nraenv) l  (f:X -> _), forall  did : data,
        
        (br ⊢ n @ₓ did ⊣ i; dreg)%nraenv = Some (dcoll (map f l)) ->
        (br ⊢ NRAEnvUnop OpNatMax n @ₓ did ⊣ i; dreg)%nraenv =
        lifted_max (map f l).
  Proof.
    unfold nraenv_eval in *.
    intros.
    cbn;rewrite H.
    unfold olift.
    apply eq_refl.
  Qed.

  Lemma NRAEnvUnOpFloatBagMax_refine_alt :
    forall (fruntime : foreign_runtime) X (br : list (string * string)) (i : list (string * data)) dreg
      (n : nraenv) l  (f:X -> _),
      (forall  did : data, (br ⊢ n @ₓ did ⊣ i; dreg)%nraenv = Some (dcoll (map f l))) ->
      forall  did : data,
        (br ⊢ NRAEnvUnop OpFloatBagMax n @ₓ did ⊣ i; dreg)%nraenv =
        lift_oncoll lifted_fmax (dcoll (map f l)).
  Proof.
    unfold nraenv_eval in *.
    intros.
    cbn;rewrite H.
    unfold olift.
    apply eq_refl.
  Qed.

  Lemma NRAEnvBinOpOr_refine :
    forall (fruntime : foreign_runtime) (br : list (string * string)) (i : list (string * data)) 
      (n1 n2 : nraenv) z1 z2 dreg,
      (forall did : data, (br ⊢ n1 @ₓ did ⊣ i; dreg)%nraenv = Some (dbool z1)) ->
      (forall  did : data, (br ⊢ n2 @ₓ did ⊣ i; dreg)%nraenv = Some (dbool z2)) ->    
      forall  did : data,
        (br ⊢ NRAEnvBinop OpOr n1 n2 @ₓ did ⊣ i; dreg)%nraenv =
        Some (dbool (orb z1 z2)).
  Proof.
    unfold nraenv_eval in *.
    intros.
    cbn;rewrite H,H0.    
    apply eq_refl.
  Qed.
  
  Lemma NRAEnvBinOpOr_refine_depend :
    forall (fruntime : foreign_runtime) (br : list (string * string)) (i : list (string * data)) 
      (n1 n2 : nraenv) z1 z2 dreg did,
      (br ⊢ n1 @ₓ did ⊣ i; dreg)%nraenv = Some (dbool z1) ->
      ((br ⊢ n2 @ₓ did ⊣ i; dreg)%nraenv = Some (dbool z2)) ->    
      (br ⊢ NRAEnvBinop OpOr n1 n2 @ₓ did ⊣ i; dreg)%nraenv =
      Some (dbool (orb z1 z2)).
  Proof.
    unfold nraenv_eval in *.
    intros.
    cbn.
    unfold olift2.
    rewrite H,H0.    
    apply eq_refl.
  Qed.
  
  Lemma NRAEnvMap_eval_map :
    forall  (fruntime : foreign_runtime)br i f dreg,
    forall n1 n2 l,
      (forall did, nraenv_eval br i n2 dreg did = Some (dcoll l)) ->
      (forall d, In d l -> nraenv_eval br i n1 dreg d =
                     Some (f d)) ->
      forall did,
        nraenv_eval br i (NRAEnvMap n1 n2) dreg did =
        Some
          (dcoll (map f l)).
  Proof.
    unfold nraenv_eval.
    intros frt br i f dreg n1 n2 l H1 H2 did.
    simpl.
    unfold olift.
    rewrite H1.
    unfold lift_oncoll,lift.
    clear H1.
    induction l;[apply eq_refl| ].
    assert (IH := IHl (fun x H => H2 x (or_intror H))).
    simpl.
    unfold lift.
    destruct lift_map;[|discriminate].
    rewrite (H2 _ (or_introl eq_refl)).
    inversion IH.
    apply eq_refl.
  Qed.

  Lemma NRAEnvMap_eval_map_depend :
    forall  (fruntime : foreign_runtime)br i f dreg,
    forall n1 n2 l,
    forall did, nraenv_eval br i n2 dreg did = Some (dcoll l) ->
           (forall d, In d l -> nraenv_eval br i n1 dreg d =
                          Some (f d)) ->
           nraenv_eval br i (NRAEnvMap n1 n2) dreg did =
           Some
             (dcoll (map f l)).
  Proof.
    unfold nraenv_eval.
    intros frt br i f dreg n1 n2 l did H1 H2 .
    simpl.
    unfold olift.
    rewrite H1.
    unfold lift_oncoll,lift.
    clear H1.
    induction l;[apply eq_refl| ].
    assert (IH := IHl (fun x H => H2 x (or_intror H))).
    simpl.
    unfold lift.
    destruct lift_map;[|discriminate].
    rewrite (H2 _ (or_introl eq_refl)).
    inversion IH.
    apply eq_refl.
  Qed.

  Lemma NRAEnvMap_eval_map_alt :
    forall  (fruntime : foreign_runtime) X br i f f1 dreg,
    forall n1 n2 (l:list X),
      (forall did, nraenv_eval br i n2 dreg did = Some (dcoll (map f1 l))) ->
      (forall d, In d l -> nraenv_eval br i n1 dreg (f1 d) =
                     Some (f d)) ->
      forall did,
        nraenv_eval br i (NRAEnvMap n1 n2) dreg did =
        Some
          (dcoll (map f l)).
  Proof.
    unfold nraenv_eval.
    intros frt X br i f f1 dreg n1 n2 l H1 H2 did.
    simpl.
    unfold olift.
    rewrite H1.
    unfold lift_oncoll,lift.
    clear H1.
    induction l;[apply eq_refl| ].
    assert (IH := IHl (fun x H => H2 x (or_intror H))).
    simpl.
    unfold lift.
    destruct lift_map;[|discriminate].
    rewrite (H2 _ (or_introl eq_refl)).
    inversion IH.
    apply eq_refl.
  Qed.

  Lemma NRAEnvMap_eval_map_alt_depend :
    forall  (fruntime : foreign_runtime) X br i f f1 dreg,
    forall n1 n2 (l:list X),
    forall did, nraenv_eval br i n2 dreg did = Some (dcoll (map f1 l)) ->
           (forall d, In d l -> nraenv_eval br i n1 dreg (f1 d) =
                          Some (f d)) ->
           nraenv_eval br i (NRAEnvMap n1 n2) dreg did =
           Some
             (dcoll (map f l)).
  Proof.
    unfold nraenv_eval.
    intros frt X br i f f1 dreg n1 n2 l did H1 H2 .
    simpl.
    unfold olift.
    rewrite H1.
    unfold lift_oncoll,lift.
    clear H1.
    induction l;[apply eq_refl| ].
    assert (IH := IHl (fun x H => H2 x (or_intror H))).
    simpl.
    unfold lift.
    destruct lift_map;[|discriminate].
    rewrite (H2 _ (or_introl eq_refl)).
    inversion IH.
    apply eq_refl.
  Qed.

  Lemma NRAEnvMap_eval_map_alt_2 :
    forall  (fruntime : foreign_runtime) X br i f f1 dreg,
    forall n1 n2 (l:list X),
      (forall  d, In d l -> normalize_data br (f1 d) = f1 d) ->
      (forall did, nraenv_eval br i n2 dreg did = Some (dcoll (map f1 l))) ->
      (forall d, In d l -> forall did, nraenv_eval br i (NRAEnvApp n1 (NRAEnvConst (f1 d))) dreg did =
                            Some (f d)) ->
      forall did,
        nraenv_eval br i (NRAEnvMap n1 n2) dreg did =
        Some
          (dcoll (map f l)).
  Proof.
    unfold nraenv_eval.
    intros frt X br i f f1 dreg n1 n2 l H0 H1 H2 did.
    simpl.
    unfold olift.
    rewrite H1.
    unfold lift_oncoll,lift.
    clear H1.
    induction l;[apply eq_refl| ].
    assert (IH := IHl (fun x H => H0 x (or_intror H)) (fun x H => H2 x (or_intror H))).
    simpl.
    unfold lift.
    destruct lift_map;[|discriminate].
    simpl in H2.
    assert (H3 := H2 _ (or_introl eq_refl) did).
    rewrite H0 in H3.
    rewrite H3.
    inversion IH.
    apply eq_refl.
    left;apply eq_refl.
  Qed.
  
  Lemma NRAEnvMap_eval_map_alt_2_depend :
    forall  (fruntime : foreign_runtime) X br i f f1 dreg,
    forall n1 n2 (l:list X),
      (forall  d, In d l -> normalize_data br (f1 d) = f1 d) ->
      forall did, nraenv_eval br i n2 dreg did = Some (dcoll (map f1 l)) ->
             (forall d, In d l ->  nraenv_eval br i (NRAEnvApp n1 (NRAEnvConst (f1 d))) dreg did =
                             Some (f d)) ->             
             nraenv_eval br i (NRAEnvMap n1 n2) dreg did =
             Some
               (dcoll (map f l)).
  Proof.
    unfold nraenv_eval.
    intros frt X br i f f1 dreg n1 n2 l H0 did H1 H2 .
    simpl.
    unfold olift.
    rewrite H1.
    unfold lift_oncoll,lift.
    clear H1.
    induction l;[apply eq_refl| ].
    assert (IH := IHl (fun x H => H0 x (or_intror H)) (fun x H => H2 x (or_intror H))).
    simpl.
    unfold lift.
    destruct lift_map;[|discriminate].
    simpl in H2.
    assert (H3 := H2 _ (or_introl eq_refl) ).
    rewrite H0 in H3.
    rewrite H3.
    inversion IH.
    apply eq_refl.
    left;apply eq_refl.
  Qed.

  Lemma NRAEnvSelect_eval_filter_alt :
    forall  (fruntime : foreign_runtime) X br i f f1 dreg,
    forall n1 n2 (l:list X),
      (forall did, nraenv_eval br i n2 dreg did = Some (dcoll (map f1 l))) ->
      (forall d, In d l -> nraenv_eval br i n1 dreg (f1 d) =
                     Some (dbool (f d))) ->
      forall did,
        nraenv_eval br i (NRAEnvSelect n1 n2) dreg did =
        Some
          (dcoll (map f1 (filter f l))).
  Proof.
    unfold nraenv_eval.
    intros frt X br i f f1 dreg n1 n2 l H1 H2 did.
    simpl.
    unfold olift.
    rewrite H1.
    unfold lift_oncoll,lift.
    clear H1.
    induction l;[apply eq_refl| ].
    assert (IH := IHl (fun x H => H2 x (or_intror H))).
    simpl.
    unfold lift.
    destruct lift_filter;[|discriminate].
    rewrite (H2 _ (or_introl eq_refl)).
    inversion IH.
    destruct (f a);
      apply eq_refl.
  Qed.
  
  Lemma NRAEnvSelect_eval_filter_alt_depend :
    forall  (fruntime : foreign_runtime) X br i f f1 dreg,
    forall n1 n2 (l:list X),
    forall did, nraenv_eval br i n2 dreg did = Some (dcoll (map f1 l)) ->
           (forall d, In d l -> nraenv_eval br i n1 dreg (f1 d) =
                          Some (dbool (f d))) ->
           nraenv_eval br i (NRAEnvSelect n1 n2) dreg did =
           Some
             (dcoll (map f1 (filter f l))).
  Proof.
    unfold nraenv_eval.
    intros frt X br i f f1 dreg n1 n2 l did H1 H2.
    simpl.
    unfold olift.
    rewrite H1.
    unfold lift_oncoll,lift.
    clear H1.
    induction l;[apply eq_refl| ].
    assert (IH := IHl (fun x H => H2 x (or_intror H))).
    simpl.
    unfold lift.
    destruct lift_filter;[|discriminate].
    rewrite (H2 _ (or_introl eq_refl)).
    inversion IH.
    destruct (f a);
      apply eq_refl.
  Qed.
  
  Lemma NRAEnvApp_refine_depend :
    forall (fruntime : foreign_runtime) (br : list (string * string))
      (i : list (string * data)) dreg
      (n2 n1 : nraenv) (d d2: data) ,
    forall  did : data, (br ⊢ n2 @ₓ did ⊣ i; dreg)%nraenv = Some d ->
                   (br ⊢ n1 @ₓ d ⊣ i; dreg)%nraenv = Some d2 ->                   
                   (br ⊢ NRAEnvApp n1 n2 @ₓ did ⊣ i; dreg)%nraenv =
                   Some d2.
  Proof.
    unfold nraenv_eval in *.
    intros.
    cbn;rewrite H.
    simpl.
    apply H0.
  Qed.

  
  Lemma NRAEnvUnOpNeg_refine_depend :
  forall (fruntime : foreign_runtime)  (br : list (string * string)) (i : list (string * data))
    (dreg : data) (n : nraenv)  b (did : data),
  (br ⊢ n @ₓ did ⊣ i; dreg)%nraenv = Some (dbool b) ->
  (br ⊢ NRAEnvUnop OpNeg n @ₓ did ⊣ i; dreg)%nraenv = Some (dbool (negb  b)).
  Proof.
    unfold nraenv_eval.
    intros;cbn.
    rewrite H;apply eq_refl.
  Qed.
  
  Lemma NRAEnvUnOpCount_refine_depend :
  forall (fruntime : foreign_runtime)  (br : list (string * string)) (i : list (string * data))
    (dreg : data) (n : nraenv)  l (did : data),
  (br ⊢ n @ₓ did ⊣ i; dreg)%nraenv = Some (dcoll l) ->
  (br ⊢ NRAEnvUnop OpCount n @ₓ did ⊣ i; dreg)%nraenv = Some (dnat (Z.of_nat (bcount l))).
  Proof.
    unfold nraenv_eval.
    intros;cbn.
    rewrite H;apply eq_refl.
  Qed.
  
  Lemma NRAEnvBinOpLe_refine_depend:
  forall (fruntime : foreign_runtime) (br : list (string * string)) (i : list (string * data)) 
    (n1 n2 : nraenv) z1 z2 (dreg did : data),
  (br ⊢ n1 @ₓ did ⊣ i; dreg)%nraenv = Some (dnat z1) ->
  (br ⊢ n2 @ₓ did ⊣ i; dreg)%nraenv = Some (dnat z2) ->
  (br ⊢ NRAEnvBinop OpLe n1 n2 @ₓ did ⊣ i; dreg)%nraenv = Some (dbool  (if Z_le_dec z1 z2 then true else false)).
  Proof.
    unfold nraenv_eval in *.
    intros;cbn.
    rewrite H,H0.
    cbn.
    apply eq_refl.
  Qed.

  Definition ITE {fruntime : foreign_runtime} (p q l r:nraenv) :=
    NRAEnvApp
      (NRAEnvEither l r)
      (NRAEnvUnop OpSingleton (NRAEnvSelect p (NRAEnvUnop OpBag q))).

  Lemma NRAEnvITE_refine :
    forall (fruntime : foreign_runtime) (br : list (string * string)) (i : list (string * data))
      (p q l r : nraenv) resl resr dreg b,
      (forall did : data, (br ⊢ NRAEnvApp p q @ₓ did ⊣ i; dreg)%nraenv = Some (dbool b)) ->
      (b = true -> forall  did : data, (br ⊢ l @ₓ did ⊣ i; dreg)%nraenv = resl) ->
      (b = false -> forall  did : data, (br ⊢ r @ₓ did ⊣ i; dreg)%nraenv = resr) ->
      forall  did : data,
        (br ⊢ ITE p q l r @ₓ did ⊣ i; dreg)%nraenv =
        if b then resl else resr.
  Proof.
    unfold nraenv_eval in *.
    intros fruntime br i p q l r resl resr dreg b H Hl Hr did.
    assert (H1:=H did); clear H.
    revert H1. cbn.
    unfold olift.
    case_eq (br ⊢ₑ nraenv_to_nraenv_core q @ₑ did ⊣ i; dreg)%nraenv_core; try discriminate.
    intros d Hd Hp.
    unfold lift_oncoll, lift. simpl. rewrite Hp.
    destruct b; simpl; auto.
  Qed.

End Sec.
