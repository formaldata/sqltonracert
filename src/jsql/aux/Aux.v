(************************************************************************************)
(**                                                                                 *)
(**                          The SQLToNRACert Library                               *)
(**                                                                                 *)
(**                       LMF, CNRS & Université Paris-Saclay                       *)
(**                                                                                 *)
(**                        Copyright 2016-2022 : FormalData                         *)
(**                                                                                 *)
(**         Authors: Véronique Benzaken                                             *)
(**                  Évelyne Contejean                                              *)
(**                  Mohammed Hachmaoui                                             *)
(**                  Chantal Keller                                                 *)
(**                                                                                 *)
(************************************************************************************)

Set Implicit Arguments.

Require Import NArith Permutation List.

From SQLFS Require Import OrderedSet.

Require Import Qcert.Utils.Utils.

Require Export AuxStandard.
Require Export AuxQCert.
Require Export AuxDatacert.

Section Strings.

  Lemma string_string_order_compare :
    forall a1 a2, string_compare a1 a2 = StringOrder.compare a1 a2.
  Proof.
    induction a1;[intros;apply eq_refl|intros [ ];[apply eq_refl| ] ].
    simpl.
    rewrite ascii_order_N_compare.
    apply match_compare_eq;try apply eq_refl.
    apply IHa1.
  Qed.

  Lemma ostring_string_order_compare :
    forall a1 a2, Oset.compare Ostring a1 a2 = StringOrder.compare a1 a2.
  Proof.
    apply string_string_order_compare.
  Qed.
  
  Lemma string_compare_refl :
    forall s, string_compare s s = Eq.
  Proof.
    induction s;[apply eq_refl| ].
    simpl.
    rewrite N.compare_refl.
    apply IHs.
  Qed.
  
End Strings.

Section Lift_Map.
  
  Lemma Permutation_lift_map_Permutation :
    forall A X (eqdec:EquivDec.EqDec X eq ) (f:A -> option X) l1 l2, Permutation l1 l2 ->
                                                               forall l3, lift_map f l1 = Some l3 ->
                                                                     exists l4, Permutation l3 l4 /\ lift_map f l2 = Some l4.
  Proof.
    intros A X eqdec f.
    induction l1 as [|x1 l1];intros l2 H1 l3 H2.
    - inversion H2;subst.
      apply Permutation_nil in H1;subst.
      exists nil;split;[constructor|apply eq_refl].
    - rewrite <-  _permut_eq_Permutation in H1.
      inversion H1;subst.      
      rewrite _permut_eq_Permutation in H5.
      destruct l3 as [|x3 l3];[| ].
      + simpl in H2.
        destruct (f b);try discriminate.
        unfold lift in H2.
        destruct (lift_map f l1);discriminate.
      + simpl in H2.
        destruct (f b) eqn:Db;[|discriminate].
        unfold lift in H2.
        destruct (lift_map f l1) eqn:Df;[|discriminate].
        inversion H2;subst;clear H2.
        destruct (IHl1 _ H5 l3 eq_refl) as [x [Hx1 Hx2]].
        destruct (lift_map_app_eq _ _ _ Hx2) as [lx1 [lx2 [-> Hx3]]].
        exists (lx1 ++ x3::lx2);split.
        * refine (Permutation_trans _ (Permutation_middle _ _ _)).
          apply (Permutation_cons eq_refl  Hx1).
        * apply (lift_map_app _ _ _ (proj1 Hx3)).
          simpl.
          rewrite Db;unfold lift.
          rewrite (proj2 Hx3).
          apply eq_refl.
  Qed.

  Lemma Permutation_lift_map_Permutation_None :
    forall A X (eqdec:EquivDec.EqDec X eq ) (f:A -> option X) l1 l2, Permutation l1 l2 ->
                                                               lift_map f l1 = None ->  lift_map f l2 = None.
  Proof.
    intros A X eqdec f l1 l2.
    rewrite <- _permut_eq_Permutation.
    revert l2.    
    induction l1;intros l2 Heq Hl1.
    - inversion Heq;apply Hl1.
    -inversion Heq;subst.
     simpl in Hl1.
     rewrite lift_map_over_app.
     unfold olift2.
     simpl.
     assert (IH := IHl1 _ H3).
     unfold lift in *.
     destruct (f b);[| destruct ( lift_map f l0);apply eq_refl].      
     destruct ( lift_map f l1);[discriminate| ].
     assert (IH := IH eq_refl).
     rewrite lift_map_over_app in IH.
     unfold olift2 in IH.
     destruct ( lift_map f l0);[|apply eq_refl].
     destruct ( lift_map f l3);[discriminate|apply eq_refl].
  Qed.

End Lift_Map.

From SQLFS Require Import SqlAlgebra flat.Env FiniteSet FlatData.
Require Import Bool.

Section WF.
  
  Import Tuple.

  Hypothesis T :Rcd.
  Hypothesis relname : Type.
  Hypothesis basesort : relname -> Fset.set (A T).
  
  Notation query_size  q := (Tree.tree_size ((@tree_of_query T relname ) q)).
  Notation sql_formula_size   f := (Tree.tree_size (Formula.tree_of_sql_formula (@tree_of_query T relname) f)).

  
  Lemma fresh_att_in_env_eq :
    forall sa e1 e2,
      weak_equiv_env T e1 e2 ->      
      fresh_att_in_env sa e1 = fresh_att_in_env sa e2.
  Proof.
    intros  sa e1 e2 Heq.
    unfold fresh_att_in_env.
    rewrite 2 Fset.is_empty_spec.
    rewrite BasicFacts.eq_bool_iff.
    repeat rewrite Fset.equal_spec;split;intro H.
    - intros a.
      rewrite <- H.
      rewrite 2 Fset.mem_inter.
      apply f_equal.
      rewrite BasicFacts.eq_bool_iff;split;intro G1.
      + rewrite  Fset.mem_Union.
        rewrite  Fset.mem_Union in G1.
        destruct G1 as [s [G1 G2]].
        rewrite in_map_iff in G1.
        destruct G1 as [ [ [] ] [G1 G3]].
        subst.
        unfold weak_equiv_env in Heq.        
        assert (A4 := Forall2_In_r Heq G3).
        destruct A4 as [[[]] [ A4 A5]].
        exists s0;split.
        * rewrite in_map_iff.
          exists (s0, g0, l0);split;[apply eq_refl| apply A4].
        * simpl in A5.
          rewrite (Fset.mem_eq_2 _ _ _ (proj1 A5)).
          apply G2.
      + rewrite  Fset.mem_Union.
        rewrite  Fset.mem_Union in G1.
        destruct G1 as [s [G1 G2]].
        rewrite in_map_iff in G1.
        destruct G1 as [ [ [] ] [G1 G3]].
        subst.
        unfold weak_equiv_env in Heq.
        assert (A4 := Forall2_In_l Heq G3).
        destruct A4 as [[[]] [ A4 A5]].
        exists s0;split.
        * rewrite in_map_iff.
          exists (s0, g0, l0);split;[apply eq_refl| apply A4].
        * simpl in A5.
          rewrite <- (Fset.mem_eq_2 _ _ _ (proj1 A5)).
          apply G2.
     - intros a.
      rewrite <- H.
      rewrite 2 Fset.mem_inter.
      apply f_equal.
      rewrite BasicFacts.eq_bool_iff;split;intro G1.
      + rewrite  Fset.mem_Union.
        rewrite  Fset.mem_Union in G1.
        destruct G1 as [s [G1 G2]].
        rewrite in_map_iff in G1.
        destruct G1 as [ [ [] ] [G1 G3]].
        subst.
        unfold weak_equiv_env in Heq.
        assert (A4 := Forall2_In_l Heq G3).
        destruct A4 as [[[]] [ A4 A5]].
        exists s0;split.
        * rewrite in_map_iff.
          exists (s0, g0, l0);split;[apply eq_refl| apply A4].
        * simpl in A5.
          rewrite <- (Fset.mem_eq_2 _ _ _ (proj1 A5)).
          apply G2.
      + rewrite  Fset.mem_Union.
        rewrite  Fset.mem_Union in G1.
        destruct G1 as [s [G1 G2]].
        rewrite in_map_iff in G1.
        destruct G1 as [ [ [] ] [G1 G3]].
        subst.
        unfold weak_equiv_env in Heq.
        assert (A4 := Forall2_In_r Heq G3).
        destruct A4 as [[[]] [ A4 A5]].
        exists s0;split.
        * rewrite in_map_iff.
          exists (s0, g0, l0);split;[apply eq_refl| apply A4].
        * simpl in A5.
          rewrite  (Fset.mem_eq_2 _ _ _ (proj1 A5)).
          apply G2.
  Qed.             
  
  Lemma well_formed_q_well_formed_f_eq_conj :
    forall  n e1 e2,
      weak_equiv_env T e1 e2 ->
      (forall q, query_size   q <= n -> well_formed_q basesort e1 q = well_formed_q basesort e2 q) /\
      (forall f, sql_formula_size   f <= n -> well_formed_f basesort e1 f = well_formed_f basesort e2 f).
  Proof.
    induction n;intros e1 e2 Heq;[split;intros [ ] H;inversion H|split].
    - intros [ ] Hn.
      + apply eq_refl.
      + apply (fresh_att_in_env_eq _ Heq).
      + simpl.
        rewrite (proj1 (IHn _ _ Heq) _ (proj1 (query_size_Set  Hn))).
        rewrite (proj1 (IHn _ _ Heq) _ (proj2 (query_size_Set  Hn))).
        apply eq_refl.
      + simpl.
        rewrite (proj1 (IHn _ _ Heq) _ (proj1 (query_size_Join   Hn))).
        rewrite (proj1 (IHn _ _  Heq) _ (proj2 (query_size_Join Hn))).
        apply eq_refl.
      + simpl.
        rewrite (proj1 (IHn _ _ Heq) _ ((query_size_Pi   Hn))).
        destruct _s.
         repeat rewrite <-  Bool.andb_assoc.
         apply f_equal.
         rewrite  (fresh_att_in_env_eq _ Heq).
         rewrite 2 (Bool.andb_comm (well_formed_s _ _  _) _).
         apply f_equal.
         refine (well_formed_s_eq _ _ ).
         constructor;[|apply Heq].
         simpl.
        unfold Tuple.default_tuple;repeat split.
        rewrite  (Fset.equal_eq_1 _ _ _ _ (Tuple.labels_mk_tuple _ _ _)).
        rewrite  (Fset.equal_eq_2 _ _ _ _ (Tuple.labels_mk_tuple _ _ _)).
        apply Fset.equal_refl.
      + rewrite 2 (well_formed_q_unfold _ _ (Q_Sigma  _ _)).
        rewrite (proj1 (IHn _ _ Heq) _ (proj1 (query_size_Sigma Hn))).
        apply f_equal.
        refine (proj2 (IHn _ _ _) _ (proj2 (query_size_Sigma Hn))).
        constructor;[|apply Heq].
        simpl.
        unfold default_tuple;repeat split.
        rewrite  (Fset.equal_eq_1 _ _ _ _ (labels_mk_tuple _ _ _)).
        rewrite  (Fset.equal_eq_2 _ _ _ _ (labels_mk_tuple _ _ _)).
        apply Fset.equal_refl.
      +  rewrite 2 (well_formed_q_unfold _ _ (Q_Gamma  _ _ _ _)).
         rewrite (proj1 (IHn _ _ Heq) _ (proj1 (query_size_Gamma Hn))).
         do 4 rewrite <-  Bool.andb_assoc.
         apply f_equal.
          rewrite  (fresh_att_in_env_eq _ Heq).
          rewrite 2 (Bool.andb_comm (forallb _ _  ) _).
          do 2 rewrite <-  Bool.andb_assoc.         
          apply f_equal.
          assert (Auxx :
                    forall a b c d,   a = c /\ b = d -> a && b = c  && d) by (intros a b c d  [ ];subst;apply eq_refl).
          apply Auxx;clear Auxx;split.          
          * refine (proj2 (IHn _ _ _) _ (proj2 (query_size_Gamma Hn))).
            constructor;[|apply Heq].
            simpl;unfold default_tuple;repeat split.      
            rewrite  (Fset.equal_eq_1 _ _ _ _ (labels_mk_tuple _ _ _)).
            rewrite  (Fset.equal_eq_2 _ _ _ _ (labels_mk_tuple _ _ _)).
            apply Fset.equal_refl.
          * apply ListFacts.forallb_eq.
            intros x Hx.
            refine (well_formed_ag_eq _ _ _ _ _).
              constructor;[|apply Heq].
            simpl;unfold default_tuple;repeat split.      
            rewrite  (Fset.equal_eq_1 _ _ _ _ (labels_mk_tuple _ _ _)).
            rewrite  (Fset.equal_eq_2 _ _ _ _ (labels_mk_tuple _ _ _)).
            apply Fset.equal_refl.
    - intros [ ] Hn.
      + simpl.
        rewrite (proj2 (IHn _ _ Heq) _ (proj1 (sql_formula_size_Conj  Hn))).
        rewrite (proj2 (IHn _ _ Heq) _ (proj2 (sql_formula_size_Conj  Hn))).
        apply eq_refl.
      + simpl.
        rewrite (proj2 (IHn _ _ Heq) _ ( (sql_formula_size_Not Hn))).
        apply eq_refl.
      + apply eq_refl.
      + simpl .
        apply ListFacts.forallb_eq.
        intros x Hx.
        apply (well_formed_ag_eq _ _ _ _ Heq).
      + simpl.
        rewrite (proj1 (IHn _ _ Heq) _ ( (sql_formula_size_Quant  Hn))).
        apply f_equal.
        apply ListFacts.forallb_eq.
        intros x Hx.
        apply (well_formed_ag_eq _ _ _ _ Heq).
      + simpl.
        rewrite (proj1 (IHn _ _ Heq) _ ( (sql_formula_size_In Hn))).
        apply f_equal.
        apply (well_formed_s_eq _ Heq).
      + simpl.
        apply (proj1 (IHn _ _ Heq) _ ( (sql_formula_size_Exists Hn))).
  Qed.
  
  Lemma well_formed_q_eq :
    forall  q e1 e2,
      weak_equiv_env T e1 e2 -> well_formed_q basesort e1 q = well_formed_q basesort e2 q.
  Proof.
    intros q e1 e2 Heq.
    refine (proj1 (well_formed_q_well_formed_f_eq_conj  (query_size   q) Heq ) _ (le_n _)).
  Qed.

  Lemma well_formed_f_eq :
    forall  f e1 e2,
      weak_equiv_env T e1 e2 ->  well_formed_f basesort e1 f = well_formed_f basesort e2 f.
  Proof.
    intros f e1 e2 Heq.
    refine (proj2 (well_formed_q_well_formed_f_eq_conj  (sql_formula_size   f) Heq ) _ (le_n _)).
  Qed.


  Lemma well_formed_e_eq_true :
    forall e1 e2,
      equiv_env T e1 e2 ->
      well_formed_e T e1 = true -> well_formed_e T e2 = true.
  Proof.
    unfold equiv_env.
    induction e1 as [|[[s1 g1] lt1]];intros e2 Heq He1;
      [inversion Heq;apply eq_refl| ].
    inversion Heq as  [ | X1 [[s2 g2] lt2] X2 e3 Hs Heq2].
    subst;clear Heq.
    rewrite well_formed_e_unfold in He1.
    rewrite andb_true_iff in He1.    
    assert (IHe3  := IHe1 _ Heq2 (proj2 He1)).
    rewrite well_formed_e_unfold.
    rewrite andb_true_iff.
    split;[|apply IHe3].
    rewrite 2 andb_true_iff in He1.    
    destruct Hs as [H1 [H2 H3]].
    rewrite 2 andb_true_iff;repeat split;swap 1 3.
    -  rewrite Fset.is_empty_spec,Fset.equal_spec.
       rewrite Fset.is_empty_spec,Fset.equal_spec in He1.
       intros a.
       rewrite Fset.empty_spec,Fset.mem_inter.
       assert (Ha := proj2 (proj1 He1) a).
       rewrite Fset.empty_spec,Fset.mem_inter in Ha.
       rewrite andb_false_iff in Ha.
       rewrite andb_false_iff.
       rewrite (Fset.mem_eq_2 _ _ _ H1) in Ha.
       destruct Ha as [Ha|Ha];[left;apply Ha|right].
       rewrite mem_Union_false.
       rewrite  mem_Union_false in Ha.
       intros s Hs.
       rewrite in_map_iff in Hs.
       destruct Hs as [ [[]] [-> Hs]].
       assert (A4 := Forall2_In_r Heq2 Hs).
       destruct A4 as [[[]] [ A4 A5]].
       assert (Hs0 := Ha s0).
       rewrite in_map_iff in Hs0.
       destruct A5 as [A5 [A6 A7]].
       rewrite <- (Fset.mem_eq_2 _ _ _ A5).
       apply Hs0.
       exists (s0,g0,l0);split;[apply eq_refl|apply A4].
    - rewrite <- (proj2 (proj1 (proj1 He1)));apply eq_sym.
      apply (Oeset.forallb_eq (OTuple T));
        [intros x;apply (Oeset.permut_mem_bool_eq _ H3)| ].
      intros t1 t2 Ht1 Heqt.
      rewrite (Fset.equal_eq_2 _ _ _ _ H1).
      rewrite (Fset.equal_eq_1 _ _ _ _ (tuple_eq_labels _ _ _ Heqt)).
      apply eq_refl.
    - subst.
      destruct He1 as [[[G1 G4] G2] G3].
      destruct g2;swap 1 2.
      + destruct lt1;[discriminate|destruct lt1;[|discriminate]].
        inversion H3;subst.
        inversion H5.
        apply eq_sym in H.
        apply app_eq_nil in H.
        destruct H;subst.
        apply eq_refl.
      + rewrite andb_true_iff in G1.
        rewrite andb_true_iff.
        destruct G1 as [G1 G5].
        { split.
          - rewrite <- G1;apply eq_sym.
            apply forallb_ext.
            intros.
            apply well_formed_ag_eq.
            apply equiv_env_weak_equiv_env.
            apply (env_t_eq T _ _ _ _ Heq2).
            apply (mk_tuple_eq_1 _ _ _ _ H1).
          - destruct lt1;[discriminate| ].
            inversion H3;subst.
            destruct l1.
            + simpl.
              simpl in G5.
              rewrite andb_true_iff in G5.
              rewrite andb_true_iff.
              split.
              * rewrite <- (proj1 G5).
                apply forallb_ext.
                intros.
                apply f_equal2.
                apply eq_sym;apply interp_aggterm_eq.
                apply env_t_eq;[apply Heq2|apply H2].
                apply eq_sym;apply interp_aggterm_eq.
                apply env_t_eq;[apply Heq2|apply H2].
              * rewrite <- (proj2 G5).
                apply eq_sym.
                apply (Oeset.forallb_eq (OTuple T));
                  [intros x;apply (Oeset.permut_mem_bool_eq _ H5)| ].
                intros t1 t2 Ht1 Heqt.
                apply forallb_ext.
                intros.
                { apply f_equal2.
                  - apply interp_aggterm_eq.
                    apply env_t_eq;[apply Heq2|apply Heqt].
                  - apply interp_aggterm_eq.
                    apply env_t_eq;[apply Heq2|apply H2]. }
            + simpl;simpl in G5.
               rewrite andb_true_iff in G5.
              rewrite andb_true_iff.
              split.
              * rewrite <- (proj1 G5).
                apply forallb_ext.
                intros.
                rewrite 2 Oset.eq_bool_refl.
                apply eq_refl.
              * destruct G5 as [G5 G6].
                rewrite
                  (Oeset.forallb_eq (OTuple T)  _                                    
                                    (fun t0  =>
                                       forallb
                                         (fun x  => Oset.eq_bool (OVal T) (interp_aggterm T (env_t T e1 t0) x) (interp_aggterm T (env_t T e1 t) x)) l)
                                    _        ((t0 :: l1) ++ l2) (fun x => Oeset.permut_mem_bool_eq x H5)  ) in G6;swap 1 2.
                 intros t1 t2 Ht1 Heqt.
                apply forallb_ext.
                intros.
                { apply f_equal2.
                  - apply interp_aggterm_eq.
                    apply env_t_eq_2;apply Heqt.
                  - apply interp_aggterm_eq.
                    apply equiv_env_refl. }
                rewrite forallb_app in G6;simpl in G6.
                rewrite forallb_app;simpl.
                rewrite andb_true_iff.
                rewrite andb_true_iff in G6.
                destruct G6 as [[G6 G8] G7].
                { repeat split.
                  - rewrite <- G8.
                    apply (Oeset.forallb_eq (OTuple T) _ _ _ _ (fun x => eq_refl)).
                    intros x1 x2 Hx1l1 Hx1x2.
                    apply forallb_ext.
                    intros;apply eq_sym.
                    apply f_equal2.
                    apply interp_aggterm_eq.
                    apply env_t_eq;[apply Heq2|apply Oeset.compare_eq_sym;apply Hx1x2].
                    apply eq_sym.
                    rewrite forallb_forall in G6.
                    apply G6 in H.
                    rewrite Oset.eq_bool_true_iff in H.
                    rewrite <- H.
                    apply eq_sym.
                    apply interp_aggterm_eq.
                    apply env_t_eq_1;apply Heq2.
                  - rewrite forallb_forall.
                    rewrite forallb_forall in G6 .
                    intros.
                    apply G6 in H.
                    rewrite Oset.eq_bool_true_iff.                    
                    rewrite Oset.eq_bool_true_iff in H.
                    refine (eq_trans _ (eq_trans H _)).
                    + rewrite H.
                      apply eq_sym.
                      apply interp_aggterm_eq.
                      apply env_t_eq;[apply Heq2|apply H2].
                    + rewrite <- H.
                      apply interp_aggterm_eq.
                      apply env_t_eq_1;apply Heq2.
                  - rewrite forallb_forall in G7.
                    rewrite forallb_forall.
                    intros x Hx.
                    apply G7 in Hx.
                    rewrite forallb_forall in Hx.
                    rewrite forallb_forall.
                    intros x1 Hx1.
                    assert (Hx2 := Hx1).
                    apply Hx in Hx1.
                    rewrite Oset.eq_bool_true_iff.
                    rewrite Oset.eq_bool_true_iff in Hx1.
                    refine (eq_trans _ (eq_trans Hx1 _)).
                    + 
                      apply eq_sym.
                      apply interp_aggterm_eq.
                      apply env_t_eq_1;apply Heq2.
                    + rewrite forallb_forall in G6.
                      apply G6 in Hx2.
                      rewrite Oset.eq_bool_true_iff in Hx2.
                      rewrite <- Hx2.
                       apply interp_aggterm_eq.
                       apply env_t_eq_1;apply Heq2.
                }
        }
  Qed.
  
  Lemma well_formed_e_eq :
    forall e1 e2,
      equiv_env T e1 e2 ->
      well_formed_e T e1 = well_formed_e T e2.
  Proof.
    intros e1 e2 Heq.
    rewrite BasicFacts.eq_bool_iff.
    assert (Heq' := proj1 (equiv_env_sym T _ _) Heq) .
    refine (conj (well_formed_e_eq_true Heq) (well_formed_e_eq_true Heq')).
  Qed.

  
End WF.

Section Permutation.

      Lemma Permutation_map_inj_Permutation :
    forall X Y (f:X -> Y),
      (forall x1 x2, f x1 = f x2 -> x1 = x2) ->
    forall l1 l2,
     Permutation (map f l1)
                 (map f l2) <->
     Permutation l1 l2.
  Proof.
    do 3 intro.
    intro Hinj.
    do 2 intro;split;
      [|apply Permutation_map].
    rewrite  <- 2 _permut_eq_Permutation.
    revert l2.
    induction l1.
    - intros.
      inversion H.
      destruct l2;[constructor|discriminate].
    - intros l2 Hl2.
      inversion Hl2;subst.
      destruct ( map_app_break  _   (eq_sym H1)) as [lx1 [lx2 [ -> [-> Hx]]]].
      destruct (map_eq_cons _ _ _ _ (eq_sym Hx)) as [z [lz [-> [Hz <-]]]].
      inversion Hz;subst.
      clear Hx Hz H1.
      rewrite <- map_app in H3.
      assert (IH := IHl1 _ H3).
      constructor;[apply Hinj;rewrite H0;apply eq_refl| apply IH].
  Qed.

End Permutation.


Lemma fold_right_map {A B C} (f:C -> A -> A) (g:B -> C) (l:list B) (a:A) :
  fold_right f a (map g l) = fold_right (fun x y => f (g x) y) a l.
Proof.
  revert l a. induction l as [ |b l IHl]; auto.
  intro a; simpl. now rewrite IHl.
Qed.
