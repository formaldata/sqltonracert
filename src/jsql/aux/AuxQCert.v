(************************************************************************************)
(**                                                                                 *)
(**                          The SQLToNRACert Library                               *)
(**                                                                                 *)
(**                       LMF, CNRS & Université Paris-Saclay                       *)
(**                                                                                 *)
(**                        Copyright 2016-2022 : FormalData                         *)
(**                                                                                 *)
(**         Authors: Véronique Benzaken                                             *)
(**                  Évelyne Contejean                                              *)
(**                  Mohammed Hachmaoui                                             *)
(**                  Chantal Keller                                                 *)
(**                                                                                 *)
(************************************************************************************)

Set Implicit Arguments.

Require Import List EquivDec NArith Arith Permutation String Psatz ZArith.

Require Import Qcert.Utils.Utils.
Require Import Qcert.Data.DataRuntime.

Require Import AuxStandard.

Section Mult.

  Lemma mult_app :
    forall (A : Type) (eqdec : EqDec A eq)  (s t : list A) (x : A),
      mult (s ++ t) x = mult s x + mult t x.
  Proof.
    intros;apply bunion_mult.
  Qed.

  Lemma mult_filter :
    forall A (eqdec : EqDec A eq) (l:list A) f x,
      mult (List.filter f l) x = mult l x * (if f x then 1 else 0).
  Proof.
    induction l as [|a1 l];intros f x;[apply eq_refl| ].
    simpl.
    destruct (equiv_dec x a1) eqn:D1.
    - unfold equiv in e;subst.
      destruct (f a1) eqn:D2.
      + simpl.
        rewrite IHl,D2.
        rewrite Nat.mul_1_r.
        rewrite D1;apply eq_refl.
      + rewrite IHl,D2;do 2 rewrite Nat.mul_0_r.
        apply eq_refl.
    - destruct (f a1) eqn:D2.
      + cbn;rewrite D1;apply IHl.
      + apply IHl.
  Qed.

  Lemma mult_map_Some_eq :
    forall A (eqdec : EqDec A eq) (l:list A) (x:A),
      mult l x = mult (map Some l) (Some x).
  Proof.
    intros A eqdec.
    induction l as [|a1 l];intros x;[apply eq_refl| ].
    simpl.
    destruct (equiv_dec x _) as [Y1|N1], (equiv_dec (Some x) _) as [Y2|N2].
    - apply f_equal;apply IHl.
    - inversion Y1;subst.
      destruct (N2 eq_refl).
    - inversion Y2;subst.
      destruct (N1 eq_refl).
    - apply IHl.
  Qed.

  Lemma mult_0_equiv_nin:
    forall (A : Type) (eqdec : EqDec A eq) (l : list A) (x : A),
      mult l x = 0 <-> ~ In x l.
  Proof.
    intros A eqdec.
    induction l as [|dl l];intros d.
    - apply (conj (fun _ H2 => H2) (fun _ => eq_refl)).
    - refine (conj (fun H1 H2 => _) (fun H => _)).
      + destruct H2 as [H2|H2].
        * subst;simpl in H1;destruct equiv_dec as [H2|H2].
          discriminate.
          apply H2;apply eq_refl.
        * simpl in H1;destruct equiv_dec as [H3|H3].
          discriminate.
          rewrite IHl in H1.
          apply (H1 H2).
      + simpl in H;apply Classical_Prop.not_or_and in H.
        destruct H as [H1 H2].
        rewrite <- IHl in H2.
        simpl;rewrite H2;clear H2.
        destruct equiv_dec as [H2|H2];[|apply eq_refl].
        apply eq_sym in H2.
        destruct (H1 H2).
  Qed.
  
  Lemma Permutation_Forall_mult_1 :
    forall A {eqdec : EqDec A eq} (l1:list A) l2,
      Permutation l1 l2 ->
      Forall (fun x => mult  l1 x = mult l2 x) l1.
  Proof.
    intros A eqdec l1 l2 H.
    rewrite Forall_forall.
    intros x Hx.
    apply bags_equal_same_mult1 in H.
    apply H.
  Qed.
  
  Lemma Permutation_Forall_mult_2 :
    forall A {eqdec : EqDec A eq} (l1:list A) l2,
      Permutation l1 l2 ->
      Forall (fun x => mult  l1 x = mult l2 x) l2.
  Proof.
    intros A eqdec l1 l2 H.
    rewrite Forall_forall.
    intros x Hx.
    apply bags_equal_same_mult1 in H.
    apply H.
  Qed.

  Lemma Permutation_Forall_mult :
    forall A {eqdec : EqDec A eq} (l1:list A) l2,
      Permutation l1 l2 <->
      Forall (fun x => mult  l1 x = mult l2 x) l1 /\ Forall (fun x => mult  l1 x = mult l2 x) l2.
  Proof.
    intros A eqdec l1 l2.
    refine
      (conj (fun H => conj (Permutation_Forall_mult_1 H)  (Permutation_Forall_mult_2 H)) _).
    intros [H1 H2].
    apply bags_same_mult_has_equal;intro d.
    destruct (in_dec eqdec d l1) as [Hin1|Hnin1].
    - rewrite Forall_forall in H1;refine (H1 _ Hin1).
    - destruct (in_dec eqdec d l2) as [Hin2|Hnin2].
      + rewrite Forall_forall in H2;refine (H2 _ Hin2).
      + rewrite (proj2 (mult_0_equiv_nin _ _ _) Hnin1).
        rewrite (proj2 (mult_0_equiv_nin _ _ _) Hnin2).
        apply eq_refl.
  Qed.
  
End Mult.

Section LiftFilter.
  
  Lemma lift_filter_Some_filter :
    forall A (l:list A) f l',
      lift_filter f l = Some l' -> l' = filter (fun x => match f x with | Some b => b | None => false end) l.
  Proof.
    intros A.
    induction l;intros f l' H.
    - destruct l';[apply eq_refl|discriminate].
    - simpl in *.
      destruct (f a);[|discriminate].
      destruct ( lift_filter f l ) eqn:D1;[|discriminate].
      apply IHl in D1.
      rewrite <- D1.
      destruct b;inversion H;apply eq_refl.
  Qed.

  Lemma lift_filter_some_filter_alt :
    forall A (l:list A) f,
      (forall x, In x l -> exists z, f x = Some z) ->
      lift_filter f l = Some  (filter (fun x => match f x with | Some b => b | None => false end) l).
  Proof.
    intros A l f Aux.
    induction l;[apply eq_refl| ].
    simpl.
    destruct (Aux _ (or_introl eq_refl) ) as [b ->].
    rewrite IHl;[destruct b;apply eq_refl| ].
    intros x Hx.
    apply Aux;right;apply Hx.
  Qed.
  
  Lemma lift_filter_bcount_le_some :
    forall A f (l1:list A) l2 ,
      lift_filter f l1 = Some l2 ->
      bcount l2 <= bcount l1.
  Proof.
    intros A f.
    fix thm 1.
    intros [|a1 l1] l2 H.
    - inversion H;subst.
      apply le_n.
    - simpl in H.
      destruct (f a1) eqn:D1;[|discriminate].
      destruct (lift_filter f l1) eqn:D2;[|discriminate].
      destruct b.
      + inversion H.
        apply (le_n_S _ _ (thm _ _ D2)).
      + inversion H;subst.
        apply (le_S _ _ ( thm _ _ D2)).
  Qed.

  Lemma lift_filter_some_bcount_eq_all_in_true :
    forall A f (l1:list A) l2 ,
      lift_filter f l1 = Some l2 ->
      bcount l1 = bcount l2 <-> forall x, In x l1 -> f x = Some true.
  Proof.
    intros A f.
    fix thm 1.
    intros [|d1 l1] [|d2 l2] Hl;split.
    - intros Hc x Hx;inversion Hx.
    - intros Hx;apply eq_refl.
    - intros Hc x Hx;inversion Hx.
    - inversion Hl.
    - intros Hc x Hx;inversion Hc.
    - intros Hx.
      assert (A1 := Hx d1 (or_introl eq_refl)).
      simpl in Hl;rewrite A1 in Hl.
      destruct (lift_filter f l1) eqn:D1;simpl in D1;
        discriminate.
    - intros Hc x Hx.
      destruct Hx as [Hx|Hx];[subst| ].
      + inversion Hl as [I1];clear Hl.
        destruct (f x) eqn:D1;[destruct b;[apply eq_refl| ]|discriminate].
        destruct (lift_filter f l1) eqn:D2;simpl in D2;
          [|discriminate ].
        inversion Hc as [I2];clear Hc.
        rewrite <- D1.
        inversion I1;subst;clear I1.
        assert (A1 := lift_filter_bcount_le_some  f _ D2).
        simpl in A1;rewrite I2 in A1.
        destruct (PeanoNat.Nat.nle_succ_diag_l _ A1).
      + simpl lift_filter in Hl.
        destruct (f d1) eqn:D1;[destruct b;[ | ]|discriminate].
        * destruct (lift_filter f l1) eqn:D2;simpl in D2;
            [|discriminate ].
          inversion Hc as [I2];clear Hc.
          inversion Hl;subst.
          refine (proj1 (thm _ _ D2) I2 _ Hx).
        * destruct (lift_filter f l1) eqn:D2;simpl in D2;
            [|discriminate ].
          inversion Hc as [I2];clear Hc.
          inversion Hl;subst.
          assert (A1 := lift_filter_bcount_le_some  f  _ D2).
          simpl in A1;rewrite I2 in A1.
          destruct (PeanoNat.Nat.nle_succ_diag_l _ A1).
    - intros Hx.
      assert (A1 := Hx d1 (or_introl eq_refl)).
      simpl in Hl;rewrite A1 in Hl.
      destruct (lift_filter f l1) eqn:D1;simpl in D1;
        [|discriminate].
      simpl.
      apply f_equal.
      inversion Hl;subst;clear Hl.
      refine (proj2 (thm _ _ D1) _).
      intros x2 Hx2.
      apply Hx;right;apply Hx2.
  Qed.

  Lemma lift_filter_some_in :
    forall A f l l',
      lift_filter f l = Some l' ->
      forall (a:A), In a l' -> In a l /\ f a =  Some  true.
  Proof.
    intros A f.
    fix thm 1.
    intros  [|b l] l' Hl a.
    - inversion Hl;contradiction.
    - intro Ha;simpl in Hl.
      destruct (f b) eqn:D1;[|discriminate].
      unfold lift in  Hl.
      destruct (lift_filter f l) eqn:D2;simpl in D2;
        [|discriminate].
      destruct b0;inversion Hl;subst;clear Hl.
      + destruct Ha as [Ha| Ha].
        * subst;split;[left;apply eq_refl|apply D1 ].
        * assert (A1 := thm _ _ D2 _ Ha);split;[right| ];apply A1.
      + assert (A1 := thm _ _ D2 _ Ha);split;[right| ];apply A1.
  Qed.

  
    
  Lemma lift_filter_some_in_bool :
    forall A f l l',
      lift_filter f l = Some l' ->
      forall (a:A), In a l -> exists b, f a =  Some  b.
  Proof.
    intros A f.
    fix thm 1.
    intros  [|b l] l' Hl a.
    - inversion Hl;contradiction.
    - intro Ha;simpl in Hl.
      destruct (f b) eqn:D1;[|discriminate].
      unfold lift in  Hl.
      destruct (lift_filter f l) eqn:D2;simpl in D2;
        [|discriminate].
      destruct b0;inversion Hl;subst;clear Hl.
      + destruct Ha as [Ha| Ha].
        * subst;exists true;apply D1.
        * assert (A1 := thm _ _ D2 _ Ha);apply A1.
      + destruct Ha as [Ha| Ha].
        * subst;exists false;apply D1.
        * assert (A1 := thm _ _ D2 _ Ha);apply A1.
  Qed.

  
  Lemma Permutation_filter_alt :
    forall (A : Type) (f1 f2 : A -> bool) (l l' : list A),
      (forall x : A, In x l -> f1 x = f2 x)  ->
      Permutation l l' -> Permutation (filter f1 l) (filter f2 l').
  Proof.
    intros.    
    rewrite (filter_ext_in _ f2 _ H).
    apply (Permutation_filter _ _ _ H0).
  Qed.
  
  Lemma  permutation_lift_filter_weak :
    forall A f1 f2 (l1:list A),
      (forall x y, In x l1 -> f2 x = Some y -> f1 x  = y) ->
      forall l2 ,
        Permutation l1 l2 ->
        forall l',
          lift_filter f2 l2 = Some l' ->
          Permutation
            (filter f1 l1) l'.
  Proof.
    intros. 
    assert (H2 := H1);apply lift_filter_Some_filter in H1.
    rewrite H1.
    refine (Permutation_filter_alt _ _ _ H0).
    intros x Hx.        
    destruct (lift_filter_some_in_bool  _ _  H2 _ (Permutation_in _ H0 Hx)) as [b H3].
    rewrite (H _ _ Hx H3),H3.
    apply eq_refl.
  Qed.
  
End LiftFilter.

Section LiftMap.
  
  Lemma lift_map_some_in_2 :
    forall A B f l l',
      lift_map f l = Some l' ->
      forall (a:A), In a l' ->
               exists x:B, In x l /\ f x = Some a.
  Proof.
    intros A B f.
    fix thm 1.
    intros  [|b l] l' Hl a.
    - inversion Hl;contradiction.
    - intro Ha;simpl in Hl.
      destruct (f b) eqn:D1;[|discriminate].
      unfold lift in  Hl.
      destruct (lift_map f l) eqn:D2;simpl in D2;
        [|discriminate].
      inversion Hl;subst;clear Hl.
      destruct Ha as [Ha| Ha].
      +  exists b;split;[left;apply eq_refl|subst;apply D1 ].
      + assert (IH := thm l _ D2 _ Ha).
        destruct IH as [x' IH1].
        exists x';split;[right| ];apply IH1.
  Qed.

  Lemma lift_map_some_in_1 :
    forall A B f l l',
      lift_map f l = Some l' ->
      forall (a:A), In a l ->
                    exists x:B, In x l' /\ f a = Some x.
  Proof.
    intros A B f.
    fix thm 1.
    intros  [|b l] l' Hl a.
    - inversion Hl;contradiction.
    - intro Ha;simpl in Hl.
      destruct (f b) eqn:D1;[|discriminate].
      unfold lift in  Hl.
      destruct (lift_map f l) eqn:D2;simpl in D2;
        [|discriminate].
      inversion Hl;subst;clear Hl.
      destruct Ha as [Ha| Ha].
      +  exists b0;split;[left;apply eq_refl|subst;apply D1 ].
      + assert (IH := thm l _ D2 _ Ha).
        destruct IH as [x' IH1].
        exists x';split;[right| ];apply IH1.
  Qed.

  Lemma lift_map_orecconcat_some_in :
    forall  (fdata : ForeignData.foreign_data ) l l',
    forall b, lift_map (fun x : data => orecconcat b x) l =Some l' ->
              forall (a:data), List.In a l' ->
                               exists r, List.In (drec r) l /\  orecconcat b (drec r)  =  Some a.
  Proof.
    intros fdata.
    fix thm 1.
    intros  [|a1 l] l' b Hl a.
    - inversion Hl;contradiction.
    - intro Ha;unfold lift_map at 1 in Hl.
      destruct (orecconcat b a1) eqn:D1;[|discriminate].
      unfold lift in Hl.
      destruct (lift_map (fun x : data => orecconcat b x) l ) eqn:D2.
      + assert (D3 := D2);unfold lift_map,lift in D2;rewrite D2 in Hl;clear D2.
        inversion Hl;subst.
        destruct Ha as [Ha| Ha].
        * subst. destruct a1,b;try discriminate.
          exists l1;split;[left;apply eq_refl| apply D1].
        * assert (IH := thm l l0 _ D3 a Ha).
          destruct IH as [x' IH1].
          exists x';split;[right| ];apply IH1.
      + unfold lift_map,lift in D2;rewrite D2 in Hl;discriminate.
  Qed.

  Lemma lift_map_app_eq :
    forall A B (f:A -> option B) l1 l2 m,
      lift_map f (l1 ++ l2) = Some m ->
      exists m1 m2,
        m = m1 ++ m2 /\
        lift_map f l1 = Some m1 /\
        lift_map f l2 = Some m2.
  Proof.
    intros A B f.
    induction l1 as [|a1 l1];intros l2 m H1.
    - exists nil,m;intuition.
    - simpl in H1.
      destruct (f a1) eqn:D1;[|discriminate].
      unfold lift in H1.
      destruct (lift_map f (l1 ++ l2)) eqn:D2;[|discriminate].
      inversion H1;subst;clear H1.
      destruct (IHl1 _ _ D2) as [m1 [m2 [-> [H2 H3]]]].
      exists (b :: m1), m2;intuition.
      simpl.
      rewrite D1.
      unfold lift.
      rewrite H2.
      apply eq_refl.
  Qed.

  Lemma lift_map_app :
    forall A B (f:A -> option B) l1 l2 m1 m2,
      lift_map f l1 = Some m1 ->
      lift_map f l2 = Some m2 ->
      lift_map f (l1 ++ l2) = Some (m1 ++ m2).
  Proof.
    intros A B f.
    induction l1 as [|a1 l1];intros l2 m1 m2 H1 H2.
    - destruct m1;[apply H2|discriminate].
    - simpl in H1;simpl.
      destruct (f a1) eqn:D1;[|discriminate].
      unfold lift in H1.
      destruct (lift_map f l1) eqn:D2;[|discriminate].
      inversion H1;subst;clear H1.
      apply (IHl1 _ _ _ eq_refl) in H2.
      unfold lift.
      rewrite H2;apply eq_refl.
  Qed.

  Lemma lift_flat_map_app_eq :
    forall A B (f:A -> option (list B)) l1 l2 m,
      lift_flat_map f (l1 ++ l2) = Some m ->
      exists m1 m2,
        m = m1 ++ m2 /\
        lift_flat_map f l1 = Some m1 /\
        lift_flat_map f l2 = Some m2.
  Proof.
    intros A B f.
    induction l1 as [|a1 l1];intros l2 m H1.
    - exists nil,m;intuition.
    - simpl in H1.
      destruct (f a1) eqn:D1;[|discriminate].
      unfold lift in H1.
      destruct (lift_flat_map f (l1 ++ l2)) eqn:D2;[|discriminate].
      inversion H1;subst;clear H1.
      destruct (IHl1 _ _ D2) as [m1 [m2 [-> [H2 H3]]]].
      exists (l ++ m1), m2;intuition.
      simpl.
      rewrite D1.
      unfold lift.
      rewrite H2.
      apply eq_refl.
  Qed.

  Lemma lift_flat_map_app :
    forall A B (f:A -> option (list B)) l1 l2 m1 m2,
      lift_flat_map f l1 = Some m1 ->
      lift_flat_map f l2 = Some m2 ->
      lift_flat_map f (l1 ++ l2) = Some (m1 ++ m2).
  Proof.
    intros A B f.
    induction l1 as [|a1 l1];intros l2 m1 m2 H1 H2.
    - destruct m1;[apply H2|discriminate].
    - simpl in H1;simpl.
      destruct (f a1) eqn:D1;[|discriminate].
      unfold lift in H1.
      destruct (lift_flat_map f l1) eqn:D2;[|discriminate].
      inversion H1;subst;clear H1.
      apply (IHl1 _ l0 _ eq_refl) in H2.
      unfold lift.
      rewrite H2.
      apply f_equal.
      apply app_assoc.
  Qed.

  Lemma Permutation_map_Some_eq :
    forall A (eqdec : EquivDec.EqDec A eq) (l1 l2 : list A),
      Permutation (map Some l1) (map Some l2) ->
      Permutation l1 l2.
  Proof.
    intros A eqdec l1 l2 Hp.
    apply bags_equal_same_mult1 in Hp.
    apply bags_same_mult_has_equal.
    intro a.
    rewrite mult_map_Some_eq.
    rewrite (mult_map_Some_eq _  l2).
    apply Hp.
  Qed.

  Lemma  Permutation_lift_map :
    forall A B (eqB_dec : EquivDec.EqDec B eq) f1 (l:list A),
    forall f2 (l1:list A) (l2:list B),
      (forall a, In a l -> Some  (f1 a) = f2 a ) ->
      lift_map f2 l1 = Some l2 ->
      Permutation l l1 ->
      Permutation  (map f1 l) l2.
  Proof.
    intros A B eqB_dec f1 l f2 l1 l2  Hf Hl Hp.
    apply Permutation_map with (f := f2) in Hp.
    rewrite listo_to_olist_simpl_lift_map in Hl.
    apply listo_to_olist_some in Hl.
    rewrite Hl in Hp.
    apply (Permutation_map_Some_eq eqB_dec).
    refine (Permutation_trans  _ Hp ).
    rewrite map_map.
    apply bags_same_mult_has_equal.
    intros d;apply f_equal3;[apply eq_refl| |apply eq_refl].
    apply map_ext_in;apply Hf.
  Qed.

  Lemma  Permutation_lift_map_weak :
    forall A B (eqB_dec : EquivDec.EqDec B eq) f1 (l:list A),
    forall f2 (l1:list A) (l2:list B),
      (forall a b, In a l -> f2 a = Some b ->   b = f1 a  ) ->
      lift_map f2 l1 = Some l2 ->
      Permutation l l1 ->
      Permutation  (map f1 l) l2.
  Proof.
    intros A B eqB_dec f1 l f2 l1 l2  Hf Hl Hp.
    refine (Permutation_lift_map _ _ _ _ Hl Hp).
    intros a Ha.
    assert (A1 := Permutation_in _ Hp Ha).
    rewrite listo_to_olist_simpl_lift_map in Hl.
    apply listo_to_olist_some in Hl.
    assert (A2 := map_Some_in_1 _ _ _ Hl _ A1).
    destruct A2 as [b [A2 A3]].
    assert (A4 := Hf _ _ Ha A3).
    rewrite <- A4,A3.
    apply eq_refl.
  Qed.


  Lemma lift_map_Some_spec :
    forall A B (f:A -> option B) l1, (forall a, In a l1 -> exists b, f a = Some b) <-> exists l2, lift_map f l1 = Some l2.
  Proof.
    clear.
    intros A B f.
    induction l1 as [|a1 l1].
    - split;intros H;[exists nil;apply eq_refl| intros a []].
    -  split;intros Hl1.
     + destruct (proj1 IHl1 (fun x Hx => Hl1 x (or_intror Hx)))
         as [l2 Hl2].
       simpl;rewrite Hl2;simpl.
       destruct (Hl1 _ (or_introl eq_refl)) as [b1 Hb1].
       rewrite Hb1.
       exists (b1::l2);apply eq_refl.
     + destruct Hl1 as [l2 Hl2].
       simpl in Hl2.
       destruct (f a1) eqn:D1;[|discriminate].
       unfold lift in Hl2;simpl in Hl2.
       destruct lift_map eqn:D2;[|discriminate].
       inversion Hl2;subst;clear Hl2.
       intros a [H|H].
       * subst;exists b;apply D1.
       * revert a H.
         rewrite IHl1.
         exists l;apply eq_refl.
  Qed.

  
  Lemma NoDup_lift_map :
    forall A B f (l : list A) (g : list B),      
      (forall a b, In a g -> In b g -> f a = f b -> a = b) ->
      NoDup g -> lift_map f g = Some l -> NoDup l.
  Proof.
    intros A B f.
    induction l.
    + intros lg Hinj H1 H2;constructor.
    +  intros lg Hinj H1 H2.
       destruct lg;[discriminate| ].
       simpl in H2.
       destruct (f b) eqn:D0;[|discriminate].
       simpl in H2;unfold lift in H2.
       destruct lift_map eqn:D1;[|discriminate].
       inversion H2;subst;clear H2.
       inversion H1;subst;clear H1.
       constructor;[|refine (IHl _ _ H3 D1);do 4 intro;apply Hinj;right;assumption].
       intro N1.
       apply H2.
       assert (A1 := lift_map_some_in_2 _ _ D1 _ N1).         
       destruct A1 as [x [A1 A2]].
       rewrite <- A2 in D0.
       rewrite (Hinj _ _ (or_introl eq_refl) (or_intror A1) D0).
       apply A1.
  Qed.

End LiftMap.

Section BDistinct.
  
  Lemma In_bdistinct :
    forall A (eqA_dec: EquivDec.EqDec A eq) (l:list A) x, In x (bdistinct l) -> In x l.
  Proof.
    intros A eqA_dec.
    induction l;intros x Hx;[inversion Hx| ].
    simpl in Hx.
    destruct (mult (bdistinct l) a ) eqn:D1;
      [simpl in Hx|right;apply IHl;apply Hx].
    destruct Hx;
      [left;subst;apply eq_refl|right;apply IHl;assumption].
  Qed.


  Lemma ldeqA_bdistinct :
    forall A (eqdec : EquivDec.EqDec A eq) (l1 l2:list A),
      ldeqA l1 l2 -> ldeqA (bdistinct l1) (bdistinct l2).
  Proof.
    intros A eqdec.
    intros l1 l2 H.
    apply bags_equal_same_mult1 in H.
    apply bags_same_mult_has_equal.
    unfold mult_equiv in H.
    intros a.
    assert (H := H a).
    do 2 rewrite bdistinct_mult.
    rewrite H.
    apply eq_refl.
  Qed.
  
  Lemma Permutation_bdistinct :
    forall A (eqdec : EquivDec.EqDec A eq) (l1 l2:list A),
      Permutation l1 l2 -> Permutation (bdistinct l1) (bdistinct l2).
  Proof.
    exact ldeqA_bdistinct.
  Qed.

End BDistinct.

Section BMinus.
  
  Lemma bminus_In_alt :
    forall (A : Type) (eqdec : EquivDec.EqDec A eq) x (l1 l2 : list A),
      List.In x (bminus l1 l2) -> List.In x l2.
  Proof.
    intros A eqdec x.
    induction l1 as [|a1 l1];intros l.
    - exact (fun x => x) .
    - intros H1;assert (H2 := H1).
      simpl in H1;apply remove_one_In in H1.
      apply (IHl1 _ H1).
  Qed.
  
End BMinus.

Require Import Qcert.Utils.Utils.

Section RecSort.
  
  Lemma rec_sort_in :
    forall frt_data (r : list (String.string * @data frt_data)) (x : String.string * @data frt_data),
      List.In  x (rec_sort r) -> List.In x r.
  Proof.
    induction r;intros x Hr;
      [inversion Hr| ].
    simpl in Hr.
    apply in_insertion_sort_insert in Hr.
    destruct Hr as [Hr| Hr].
    rewrite Hr;left;apply eq_refl.
    right;apply IHr;apply Hr.
  Qed.

  Lemma rec_sort_in_nodup :
    forall frt_data  (r : list (String.string * @data frt_data)) (x : String.string * data),
      List.NoDup (domain r) ->
      List.In x (rec_sort r) <-> List.In x r.
  Proof.
    do 4 intro.
    apply rec_sort_perm in H.
    split;apply Permutation.Permutation_in;
      [|apply H].
    apply Permutation.Permutation_sym;apply H.
  Qed.

  Lemma rec_sort_perm_eq_iff :
    forall (K : Type) (odt : ODT) (A : Type) (l l' : list (K * A)),
      List.NoDup (domain l) -> List.NoDup (domain l') ->
      (Permutation.Permutation l l' <-> rec_sort l = rec_sort l').
  Proof.
    intros K odt A l l' H1 H2;split.
    - apply (drec_sort_perm_eq _ _ H1).
    - intros H3.
      rewrite (rec_sort_perm _ H1).
      apply Permutation.Permutation_sym.
      rewrite (rec_sort_perm _ H2).
      rewrite H3.
      apply Permutation.Permutation_refl.
  Qed.

  
  Lemma omap_product_element_is_drec :
    forall (fdata : ForeignData.foreign_data), forall l1 l2 l3,
      omap_product (fun _ : data => Some (dcoll l2)) l1 = Some l3 ->
      forall d,
        In d l3 ->   exists r : list (string * data), d = drec r.
  Proof.
    unfold omap_product.
    induction l1;intros l2 l3 H1 d Hd.
    - inversion H1;subst;destruct Hd.
    -  simpl in H1.
       unfold oncoll_map_concat in H1.
       destruct omap_concat eqn:D1;[|discriminate].
       unfold lift in H1.
       destruct lift_flat_map eqn:D2;[|discriminate].
       inversion H1;subst;clear H1.
       apply In_app in Hd.
       destruct Hd as [Hd|Hd].
       + unfold omap_concat in D1.
         revert l d Hd D1.
         clear.
         induction l2;intros.
         * inversion D1;subst;destruct Hd.
         * simpl in D1.
           destruct a,a0;try discriminate.
           unfold lift in D1;simpl in D1.
           destruct lift_map eqn:D2;[|discriminate].
           inversion D1;subst;clear D1.
           destruct Hd as [Hd|Hd].
           -- exists (rec_concat_sort l0 l1);rewrite Hd;apply eq_refl.
           -- apply (IHl2 _ _ Hd eq_refl).
       + apply (IHl1 _ _ D2 _ Hd).
  Qed.
  
End RecSort.

Require Import Qcert.NRAEnv.NRAEnvRuntime.

Section StringOrder.
  
  Lemma string_order_compare_append_2 :
    forall s s1 s2,
      StringOrder.compare (s ++ s1) (s ++ s2) =
      StringOrder.compare s1 s2.
  Proof.
    induction s;
      [split;intros;apply eq_refl| ].
    intros s1 s2;simpl.
    rewrite AsciiOrder.compare_refl_eq.
    apply IHs.
  Qed.

  Lemma string_order_append_2_alt :
    forall a1 a2,
      a1 = a2 -> forall a3 a4, StringOrder.compare (a1 ++ a3) (a2 ++ a4) = StringOrder.compare a3 a4.
  Proof.
    intros a1 a2 H;rewrite H;clear H.
    induction a2;intros a3 a4;[apply eq_refl| ].
    simpl.
    rewrite AsciiOrder.compare_refl_eq.
    apply IHa2.
  Qed.    

  Lemma string_order_compare_lt_gt :
    forall s1 s2,
      StringOrder.compare s1 s2 = Lt <-> StringOrder.compare s2 s1 = Gt.
  Proof.
    intros s1 s2;split.
    { revert s1 s2.
      induction s1;intros [ ] H.
      - discriminate.
      - apply eq_refl.
      - discriminate.
      - simpl in H;simpl.
        unfold AsciiOrder.compare in *.
        rewrite PeanoNat.Nat.compare_antisym.
        destruct PeanoNat.Nat.compare ;try discriminate.
        + apply (IHs1 _ H).
        + apply eq_refl. }
    { revert s1 s2.
      induction s1;intros [ ] H.
      - discriminate.
      - apply eq_refl.
      - discriminate.
      - simpl in H;simpl.
        unfold AsciiOrder.compare in *.
        rewrite PeanoNat.Nat.compare_antisym.
        destruct PeanoNat.Nat.compare ;try discriminate.
        + apply (IHs1 _ H).
        + apply eq_refl. }
  Qed.

  Lemma string_order_compare_antisym :
    forall s1 s2,
      StringOrder.compare s2 s1 = CompOpp (StringOrder.compare s1 s2).
  Proof.
    intros s1 s2.
    destruct (StringOrder.compare s1 s2) eqn:D1.
    - rewrite StringOrder.compare_eq_iff in D1;subst.
      apply StringOrder.compare_refl_eq.
    - apply (proj1 (string_order_compare_lt_gt  _ _) D1).
    - apply (proj2 (string_order_compare_lt_gt  _ _) D1).
  Qed.
  
  Lemma ascii_order_N_compare :
    forall a1 a2,
      N.compare (Ascii.N_of_ascii a1) (Ascii.N_of_ascii a2) = AsciiOrder.compare a1 a2.
  Proof.
    intros a1 a2.
    unfold AsciiOrder.compare.
    unfold Ascii.nat_of_ascii.
    apply N2Nat.inj_compare.
  Qed.

  
    (* Lemma dsum_permut l1 l2 : Permutation.Permutation l1 l2 -> dsum l1 = dsum l2. *)
    (* Proof. *)
    (*   induction 1 *)
    (*     as [ | *)
    (*          [ |z| | | | | | | | | ] l3 l4 H IH| *)
    (*          [ |z1| | | | | | | | | ] *)
    (*            [ |z2| | | | | | | | | ] ds| *)
    (*          l1 l2 l3 H1 H2 IH1 IH2]; simpl; auto. *)
    (*   - now rewrite IH. *)
    (*   - case (dsum ds); auto. simpl. *)
    (*     intro z. now replace (z2 + (z1 + z))%Z *)
    (*                with (z1 + (z2 + z))%Z by lia. *)
    (*   - transitivity (dsum l2); auto. *)
    (* Qed. *)
  Context {fdata : ForeignData.foreign_data}.
  
    Lemma bcount_permut l1 l2 : Permutation.Permutation l1 l2 -> @Bag.bcount (@data fdata) l1 = Bag.bcount l2.
    Proof.
      induction 1
        as [ |
             [ |z| | | | | | | | | ] l3 l4 H IH|
             [ |z1| | | | | | | | | ]
               [ |z2| | | | | | | | | ] ds|
             l1 l2 l3 H1 H2 IH1 IH2]; simpl; auto.
      rewrite H2;apply IH2.
    Qed.

    
    Lemma dsum_permut l1 l2 : Permutation.Permutation l1 l2 ->   dsum l1 = dsum l2.
    Proof.
      induction 1
        as [ |
             [ |z| | | | | | | | | ] l3 l4 H IH|
             [ |z1| | | | | | | | | ]
               [ |z2| | | | | | | | | ] ds|
             l1 l2 l3 H1 H2 IH1 IH2]; simpl; auto.
      - now rewrite IH.
      - case (dsum ds); auto. simpl.
        intro z. now replace (z2 + (z1 + z))%Z
                   with (z1 + (z2 + z))%Z by lia.
      - transitivity (dsum l2); auto.
    Qed.
    

    

  Lemma nraenv_core_eval_unfold :
    forall (fruntime : foreign_runtime) (h : list (string * string)) (constant_env : list (string * data)) ,
    forall  (e : nraenv_core) (envin din : data),
      
    nraenv_core_eval h constant_env e envin din = 
    match e with
  | cNRAEnvGetConstant s => edot constant_env s
  | cNRAEnvID => Some din
  | cNRAEnvConst rd => Some (normalize_data h rd)
  | cNRAEnvBinop bop e1 e2 =>
      olift2 (fun d1 d2 : data => binary_op_eval h bop d1 d2) (nraenv_core_eval  h constant_env e1 envin din)
        (nraenv_core_eval  h constant_env e2 envin din)
  | cNRAEnvUnop uop e0 => olift (fun d1 : data => unary_op_eval h uop d1) (nraenv_core_eval  h constant_env e0 envin din)
  | cNRAEnvMap e2 e1 =>
      let aux_map :=
        fun d : data => lift_oncoll (fun c1 : list data => lift dcoll (lift_map (nraenv_core_eval  h constant_env e2 envin) c1)) d
        in
      olift aux_map (nraenv_core_eval  h constant_env e1 envin din)
  | cNRAEnvMapProduct e2 e1 =>
      let aux_mapconcat :=
        fun d : data =>
        lift_oncoll (fun c1 : list data => lift dcoll (omap_product (nraenv_core_eval  h constant_env e2 envin) c1)) d in
      olift aux_mapconcat (nraenv_core_eval  h constant_env e1 envin din)
  | cNRAEnvProduct e1 e2 =>
      let aux_product :=
        fun d : data =>
        lift_oncoll
          (fun c1 : list data => lift dcoll (omap_product (fun _ : data => nraenv_core_eval  h constant_env e2 envin din) c1)) d
        in
      olift aux_product (nraenv_core_eval  h constant_env e1 envin din)
  | cNRAEnvSelect e2 e1 =>
      let pred :=
        fun d1 : data => match nraenv_core_eval  h constant_env e2 envin d1 with
                         | Some (dbool b) => Some b
                         | _ => None
                         end in
      let aux_select := fun d : data => lift_oncoll (fun d1 : list data => lift dcoll (lift_filter pred d1)) d in
      olift aux_select (nraenv_core_eval  h constant_env e1 envin din)
  | cNRAEnvDefault e1 e2 =>
      olift (fun d1 : data => match d1 with
                              | dcoll nil => nraenv_core_eval  h constant_env e2 envin din
                              | _ => Some d1
                              end) (nraenv_core_eval  h constant_env e1 envin din)
  | cNRAEnvEither e1 e2 =>
      match din with
      | dleft d1 => nraenv_core_eval  h constant_env e1 envin d1
      | dright d2 => nraenv_core_eval  h constant_env e2 envin d2
      | _ => None
      end
  | cNRAEnvEitherConcat e1 e2 =>
      match nraenv_core_eval  h constant_env e1 envin din with
      | Some (dleft (drec l)) =>
          match nraenv_core_eval  h constant_env e2 envin din with
          | Some (drec t) => Some (dleft (drec (rec_concat_sort l t)))
          | _ => None
          end
      | Some (dright (drec r)) =>
          match nraenv_core_eval  h constant_env e2 envin din with
          | Some (drec t) => Some (dright (drec (rec_concat_sort r t)))
          | _ => None
          end
      | _ => None
      end
  | cNRAEnvApp e2 e1 => olift (fun d1 : data => nraenv_core_eval  h constant_env e2 envin d1) (nraenv_core_eval  h constant_env e1 envin din)
  | cNRAEnvEnv => Some envin
  | cNRAEnvAppEnv e2 e1 => olift (fun env1 : data => nraenv_core_eval  h constant_env e2 env1 din) (nraenv_core_eval  h constant_env e1 envin din)
  | cNRAEnvMapEnv e0 =>
      lift_oncoll
        (fun c1 : list data => lift dcoll (lift_map (fun env1 : data => nraenv_core_eval  h constant_env e0 env1 din) c1)) envin
    end.
  Proof.
    destruct e;intros;apply eq_refl.
  Qed.

  
End StringOrder.
