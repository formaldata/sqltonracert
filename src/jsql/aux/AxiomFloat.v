(************************************************************************************)
(**                                                                                 *)
(**                          The SQLToNRACert Library                               *)
(**                                                                                 *)
(**                       LMF, CNRS & Université Paris-Saclay                       *)
(**                                                                                 *)
(**                        Copyright 2016-2022 : FormalData                         *)
(**                                                                                 *)
(**         Authors: Véronique Benzaken                                             *)
(**                  Évelyne Contejean                                              *)
(**                  Mohammed Hachmaoui                                             *)
(**                  Chantal Keller                                                 *)
(**                                                                                 *)
(************************************************************************************)

Require Import Floats Qcert.Utils.Float.
Require Import ZArith.


(** These three axioms are valid, we have to assume them since
    [float_max] and [float_of_int] are only realized at extraction. **)

(* Associativity and commutativity of [float_max] *)
Axiom float_max_assoc : forall n m p : float,
    float_max n (float_max m p) = float_max (float_max n m) p.
Axiom float_max_comm : forall n m : float, float_max n m = float_max m n.

(* Injection of positive integers into floats returns either a positive
   finite float or plus infinity *)
Axiom float_of_int_pos :
  forall p,
    (float_of_int (Z.pos p) = float_infinity) \/
    (exists m e, float_of_int (Z.pos p) = SF2Prim (S754_finite false m e) /\
                 bounded 53 1024 m e = true).


(** These two axioms are invalid but:
    - We isolate their application to the proof of correctness of the
      compilation of the aggregates [sum] and [avg] on floats. In
      particular, the proof of correctness of the compiler itself
      (independently of supported values) does not rely on them at all,
      nor the proof of correctness of the compilation of the other
      aggregate, function and predicate symbols.
    - We use them only in a "fair" way, not using the possible
      derivation of `False` from them. **)

(* Associativity and commutativity of float_add *)

Record float_add_assoc_comm :=
  { float_add_assoc : forall n m p : float,
      float_add n (float_add m p) = float_add (float_add n m) p;
    float_add_comm : forall n m : float, float_add n m = float_add m n
  }.
