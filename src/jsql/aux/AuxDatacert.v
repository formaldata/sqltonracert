(************************************************************************************)
(**                                                                                 *)
(**                          The SQLToNRACert Library                               *)
(**                                                                                 *)
(**                       LMF, CNRS & Université Paris-Saclay                       *)
(**                                                                                 *)
(**                        Copyright 2016-2022 : FormalData                         *)
(**                                                                                 *)
(**         Authors: Véronique Benzaken                                             *)
(**                  Évelyne Contejean                                              *)
(**                  Mohammed Hachmaoui                                             *)
(**                  Chantal Keller                                                 *)
(**                                                                                 *)
(************************************************************************************)

Set Implicit Arguments.

Require Import List Permutation SetoidList Bool.

From SQLFS Require Import OrderedSet.
From SQLFS Require Import ListPermut.
From SQLFS Require Import FiniteSet.
From SQLFS Require Import FiniteBag ListSort.

Require  Import AuxStandard.

Section Osets.

  
      Lemma Oset_mem_bool_false_iff :
        forall (A : Type) (OA : Oset.Rcd A) (a : A) (l : list A),
          Oset.mem_bool OA a l = false <-> ~ In a l.
      Proof.
        intros A OA a l.
        rewrite <- (Oset.mem_bool_true_iff OA).
        rewrite (not_true_iff_false ( Oset.mem_bool OA a l)).
        refine (conj (fun x => x) (fun x => x)).
      Qed.

      
 
    Lemma quicksort_nil :
      forall X (OX: Oeset.Rcd X) l,
        quicksort OX l = nil <-> l = nil.
    Proof.
      intros X OX l.
      rewrite quicksort_equation.
      destruct l;[apply (conj (fun x => x) (fun x => x))| ].
      destruct ListSort.partition as [l1 l2].
      destruct (quicksort _ l1);split;discriminate.
    Qed.
    
  Lemma oset_find_2_map :
    forall (A B : Type) (OA : Oset.Rcd A)
      (C : Type) (f : C -> A) (g : C -> B) l a,
      (forall a1 a2, In a1 l -> In a2 l -> f a1 = f a2 -> a1 = a2) ->
      In a l -> Oset.find OA (f a) (map (fun x => (f x, g x)) l) = Some (g a).
  Proof.
    intros A B OA C f g l a;
      induction l as [ | a1 l];intros H Ha;[inversion Ha| ].
    simpl;destruct (Oset.eq_bool _ (f a) (f a1)) eqn:D1.
    - rewrite Oset.eq_bool_true_iff in D1.
      do 2 apply f_equal.
      apply eq_sym.
      apply (H _ _ Ha );[left;apply eq_refl|apply D1]. 
    - simpl in Ha; destruct Ha as [Ha | Ha].
      + rewrite Ha in D1;unfold Oset.eq_bool in D1;rewrite Oset.compare_eq_refl in D1; discriminate.
      + apply IHl; [ do 4 intro; apply H; right | ];assumption.
  Qed.  

  Lemma Permutation_map_2 :
    forall (A X : Type)  (OA: Oeset.Rcd A)
      (f1 f2 : list A -> list X) (l : list (list A)),
      (forall x , In x l -> ListSort.quicksort OA x = x /\ Permutation (f1 x)  (f2 x) /\ f1 (ListSort.quicksort OA x) = f2 (ListSort.quicksort OA x) ) ->
      Permutation (map f1 l) (map f2 l).
  Proof.
    intros A X OA f1 f2.
    induction l as [|x l];intros Hx;
      [constructor|idtac].
    simpl;apply Permutation_cons.
    - assert (A1 := Hx x (or_introl eq_refl) ).      
      rewrite <- (proj1 A1).
      apply A1.
    - refine (IHl (fun x H => Hx x (or_intror H))).
  Qed.


    Lemma Oset_compare_lt_wd :
        forall A (OA : Oset.Rcd A),
          Morphisms.Proper (Morphisms.respectful eq (Morphisms.respectful eq iff))  (fun x y => Oset.compare OA x y = Lt).
      Proof.
        intros A OA.
        constructor;subst;refine (fun x => x).
      Qed.
      
End Osets.

Section ListPermut.

  Lemma _permut_swap :
    forall A (x:A) y l,
      _permut eq (y :: x :: l) (x :: y :: l).
  Proof.
    intros A x y l.
    replace (x::y::l) with ((x::nil) ++ (y::l));[|apply eq_refl].
    apply Pcons;[apply eq_refl| ].
    apply _permut_refl.
    intros;apply eq_refl.
  Qed.

  Lemma _permut_cons :
    forall A (x:A) l1 l2,
      _permut eq l1 l2 ->
      _permut eq (x :: l1) (x :: l2).
  Proof.
    intros A x l1 l2.
    replace (x::l1) with ((x::nil) ++ l1);[|apply eq_refl].
    replace (x::l2) with ((x::nil) ++ l2);[|apply eq_refl].
    rewrite <- _permut_app1;[apply (fun x => x)|apply BasicFacts.equivalence_eq].
  Qed.
  
  Lemma _permut_in :
    forall A R (l1 l2:list (list A)),
      _permut R l1 l2 -> forall la, In la l1 -> exists lb, In lb l2 /\ R la lb.
  Proof.
    intros A R.
    induction l1 as [|la1 l1];intros l2 Hl la Hla.
    - inversion Hla.
    - inversion Hl as [|a b l3 l4 l5 H1 H2 [H3 H5] H4 ].
      subst a l3 l2.
      destruct Hla as [D1|D1];subst.
      + exists b;split;[|apply H1].
        apply in_app_iff;right;left;apply eq_refl.
      + destruct (IHl1 _ H2 _ D1) as [la2 [D2 G2]].
        exists la2;split;[|apply G2].
        apply (ListFacts.in_app _ _ _ _ D2).            
  Qed.
  
  Lemma _permut_eq_Permutation :
    forall A (l1 l2 :list A),
      _permut eq l1 l2 <-> Permutation l1 l2.
  Proof.
    intros A l1 l2;split;intro Heq.
    - induction Heq;[constructor| ].
      rewrite H.
      refine (Permutation_trans _ (Permutation_middle _ _ _) ).
      apply Permutation_cons;[apply eq_refl| apply IHHeq].
    - induction Heq.
      + apply Pnil.
      + apply (_permut_cons _ IHHeq).
      + apply _permut_swap.
      + refine (_permut_trans _ IHHeq1 IHHeq2).
        do 6 intro;apply eq_trans.
  Qed.
  
  Lemma Permutation_map_alt_3 :
    forall A B C (f1:A -> list B) f2 (f3:A -> C) f4 (l1 l2:list A),
      (forall b, In b l1 -> Permutation (f1 b) (f2 b) -> f3 b = f4 b) ->
      Permutation l1 l2 -> eqlistA (@Permutation _) (map f1 l1) (map f2 l1) ->
      Permutation (map f3 l1) (map f4 l2).
  Proof.
    intros A D C f1 f2 f3 f4.
    induction l1;intros l2 Aux H1 H2.
    - apply Permutation_nil in H1;subst.
      constructor.
    - rewrite eqlistA_map_iff in H2.
      assert (Ha := H2 a (or_introl eq_refl)).
      simpl in H2.
      assert (H3 := fun x Hx => H2 x  (or_intror Hx)).      
      clear H2.
      rewrite <- eqlistA_map_iff in H3.
      destruct l2;[destruct (Permutation_nil_cons (Permutation_sym H1))| ].
      rewrite <- _permut_eq_Permutation in H1.
      inversion H1;subst.
      rewrite map_app.
      refine (Permutation_trans _ (Permutation_app_comm _ _)).
      rewrite <- map_app.
      simpl.
      apply Permutation_cons;[apply (Aux _ (or_introl eq_refl) Ha)| ].
      refine (IHl1 _ (fun x H1 => Aux x (or_intror H1)) _ H3).
      refine (Permutation_trans _ (Permutation_app_comm _ _)).
      rewrite <- _permut_eq_Permutation.
      apply H5.
  Qed.

  Lemma _permut_eq_permut_Permutation :
    forall A l1 l2,
      ListPermut._permut eq l1 l2 ->  ListPermut._permut (@Permutation A) l1 l2.
  Proof.
    intros A l1 l2 H.    
    refine (@ListPermut._permut_incl _ _  eq (@Permutation _) _ _ _ H).
    intros;subst;apply Permutation_refl.
  Qed.  

End ListPermut.

Section AllDiff.
  
  Lemma all_diff_NoDup_same :
    forall A (lx:list A),
      ListFacts.all_diff lx <-> NoDup lx.
  Proof.
    intros A lx;split;revert lx;induction lx as [|x lx];
      try constructor.
    - intro N1.
      destruct lx;[inversion N1|destruct N1 as [N1|N1] ].
      + destruct ((proj1 H) _ (or_introl eq_refl) (eq_sym N1)).
      + destruct ((proj1 H) _ (or_intror N1) eq_refl).
    - apply IHlx.
      destruct lx;[constructor|apply H].
    - intro H.
      inversion H;subst;clear H;
        destruct lx;[constructor| ].
      rewrite ListFacts.all_diff_unfold;split;[|apply IHlx;apply H3].
      intros a2 Ha2 N1;subst.
      destruct (H2 Ha2).
  Qed.

End AllDiff.

Section Fsets.
  
  Lemma fset_sorted_eq :
    forall X (OX:Oset.Rcd X) (FX:Fset.Rcd OX) l,
      Sorted (fun x y : X => Oset.compare OX x y = Lt)  l ->
      Fset.elements FX (Fset.mk_set _ l) = l.
  Proof.
    clear.
    intros X OX FX.
    fix thm 1.
    intros l  Hl.
    induction l;[apply Fset.elements_empty| ].
    inversion Hl;subst.
    apply IHl in H1.
    assert (A1 : a inS? (Fset.mk_set FX l) = false).
    {
      destruct l;[apply Fset.empty_spec| ].
      simpl;rewrite Fset.add_spec.
      inversion H2;subst.
      assert (A2 := Oset.eq_bool_ok OX a x ).
      rewrite H0 in A2.
      destruct Oset.eq_bool eqn:D1.
      - rewrite Oset.eq_bool_true_iff in D1;destruct (A2 D1).
      - rewrite Bool.orb_false_l.
        apply (@SetoidList.SortA_NoDupA  _ _ _ (fun x y : X => Oset.compare OX x y = Lt) (Oset.StrictOrder OX) ) in Hl.
        + inversion Hl;subst.
          rewrite Fset.mem_mk_set.
          rewrite <- Bool.not_true_iff_false.
          intro N1;apply H4.
          apply SetoidList.In_InA;[apply eq_equivalence| ].
          rewrite Oset.mem_bool_true_iff in N1.
          right;apply N1.
        + constructor;subst;apply (fun x => x ). }
    assert (A2 := Fset.elements_add_2 FX _ _ A1).
    destruct A2 as [l1 [l2 [A21 A22]]].
    assert (A2 := Fset.elements_spec3 FX (a addS Fset.mk_set FX l)).
    assert (A3 := Fset.elements_spec3 FX (Fset.mk_set FX l)).
    simpl.
    rewrite A21 in *;clear A21.
    rewrite A22 in *;clear A22.
    rewrite <- H1 in *;clear H1.
    clear thm IHl A3 A1 H2 l.
    destruct l1;[apply eq_refl| exfalso].
    inversion Hl;subst;inversion A2;subst.
    inversion H2;subst;clear Hl A2 H2 H1.
    assert ( A1 : Oset.compare OX x a = Lt -> False).
    { destruct (Oset.compare OX x a) eqn:D3;try discriminate.
      rewrite Oset.compare_lt_gt,D3 in H0;discriminate.
    }
    apply A1;clear H0 A1.
    eapply (SetoidList.SortA_InfA_InA eq_equivalence (Oset.StrictOrder OX) _ H3 H4).
    apply (SetoidList.In_InA eq_equivalence).
    apply in_or_app.
    right;left;apply eq_refl.
  Qed.

  Lemma mem_Union_false :
    forall (A : Type) (Elt : Oset.Rcd A) (FA : Fset.Rcd Elt) (a : A) (l : list (Fset.set FA)),
      a inS? Fset.Union FA l = false <-> (forall s : Fset.set FA, In s l -> a inS? s = false).
  Proof.
    intros A Elt FA a.
    induction l as [|s1 l].
    - split;intro H;[contradiction| apply Fset.empty_spec].
    - split;intro H.
      + intros s Hs.
        simpl in H.
        rewrite Fset.mem_union in H.
        apply Bool.orb_false_iff in H.
        destruct H as [H1 H2].
        rewrite IHl in H2.
        destruct Hs as [Hs|Hs];
          [subst;apply H1|apply (H2 _ Hs)].
      + simpl.
        rewrite Fset.mem_union.
        apply Bool.orb_false_iff.
        split;[apply H;left;apply eq_refl| ].
        rewrite IHl.
        intros s Hs.
        apply H;right;apply Hs.
  Qed.

  Lemma fset_elements_add_empty :
    forall X (OX:Oset.Rcd X) (FX:Fset.Rcd OX) (a:X), {{{a addS (Fset.empty FX)}}} = a::nil.
  Proof.
    intros x OX FX a.
    assert (A1 := Fset.elements_add_2 FX (emptysetS) a (Fset.empty_spec _ _)).
    destruct A1 as [l1 [l2 [A1 A2]]].
    rewrite Fset.elements_empty in A1.
    destruct l1;[|discriminate].
    destruct l2;[|discriminate].
    rewrite A2;apply eq_refl.
  Qed.

  Lemma no_dup_permutation_fset_mk_set :
    forall E (OE: Oset.Rcd E) (FE: Fset.Rcd OE) l,
      NoDup l -> Permutation ({{{Fset.mk_set FE l}}}) l.
  Proof.
    intros E OE FE.
    induction l.
    - intros _;simpl.
      rewrite Fset.elements_empty.
      constructor.
    - inversion 1;subst.
      simpl.
      assert (H1 : (a inS? Fset.mk_set FE l) = false).
      { rewrite Fset.mem_mk_set.
        rewrite <- (Oset.mem_bool_true_iff OE) in H2.
        apply (Bool.not_true_is_false _ H2). }
      destruct (Fset.elements_add_2  FE (Fset.mk_set FE l) a H1)
        as [l1 [l2 [H4 ->]]].
      rewrite H4 in IHl;clear H4;clear H1.
      apply Permutation_sym.
      apply (Permutation_cons_app _ _ _ (Permutation_sym  (IHl H3))).
  Qed.  

  Lemma sorted_eq_fset_mk_set :
    forall E (OE: Oset.Rcd E) (FE: Fset.Rcd OE) l,
      Sorted (fun x y => Oset.compare OE x y = Lt) l -> ({{{Fset.mk_set FE l}}}) = l.
  Proof.
    intros E OE FE l Hl.
    apply (ListSort.sorted_list_eq OE);
      [|apply Fset.elements_spec3|apply Hl ].
    intros e.
    rewrite <- Fset.mem_elements.
    rewrite Fset.mem_mk_set.
    apply eq_refl.
  Qed.

  Lemma no_dup_sorted_permutation_eq :
    forall E (OE: Oset.Rcd E) (FE: Fset.Rcd OE) l1 l2, 
      NoDup l1 -> 
      Sorted (fun x y => Oset.compare OE x y = Lt) l2 -> 
      Permutation l1 l2 -> l2 = ({{{ Fset.mk_set FE l1}}}).
  Proof.
    intros E OE FE l1 l2 H1 H2 H3.
    apply (ListSort.sorted_list_eq OE);
      [|apply H2|apply Fset.elements_spec3].
    rewrite <- (sorted_eq_fset_mk_set  FE H2).
    intro e.
    rewrite <- 2 Fset.mem_elements.
    rewrite 2  Fset.mem_mk_set.
    rewrite BasicFacts.eq_bool_iff.
    rewrite 2 Oset.mem_bool_true_iff.
    split;apply Permutation_in;
      [apply Permutation_sym|idtac];
      apply H3.
  Qed.

End Fsets.

Section Febags.
  
  Lemma febag_mem_eq_map:
    forall A (OA:Oeset.Rcd A)  (BA:Febag.Rcd OA),
    forall f, (forall x x',Oeset.compare OA x x' = Eq -> Oeset.compare OA (f x) (f x') = Eq) ->
         forall b a, a inBE b -> (f a) inBE  Febag.map BA BA f b.
  Proof.
    intros A OA BA f Aux b a Ha.
    rewrite Febag.mem_map.
    rewrite Febag.mem_unfold in Ha.
    rewrite Oeset.mem_bool_true_iff in Ha.
    destruct Ha as [a' [Ha1 Ha2]].
    exists a'.
    split;[|apply Ha2].
    apply Aux.
    apply Ha1.
  Qed.
  
End Febags.



From SQLFS Require Import Tree FlatData SqlAlgebra Formula (* DExpressions *).

Section Sizes.
  
  Hypothesis T : Tuple.Rcd.
  Hypothesis symb : Type.
  Hypothesis aggregate : Type.
  Hypothesis predicate : Type.
  Hypothesis relname : Type.
  
  Notation query_size q := (tree_size ((@tree_of_query T relname ) q)).
  Notation sql_formula_size f := (tree_size (tree_of_sql_formula (@tree_of_query T relname) f)).
  Notation size_funterm ft := (@size_funterm T ft).


  Lemma query_size_Set :
    forall n op q1 q2,
      query_size (Q_Set op q1 q2) <= S n ->
      (query_size q1 <= n) /\ (query_size q2 <= n).
  Proof.
    intros n op q1 q2 Hn.
    apply le_S_n in Hn.
    split;refine (Le.le_trans _ _ _ _ Hn).
    - apply Plus.le_plus_trans;
        apply Peano.le_n.
    - simpl;rewrite Plus.plus_comm.
      apply Plus.le_plus_trans.
      rewrite Plus.plus_0_r.
      apply Peano.le_n.
  Qed.

  Lemma query_size_Join :
    forall n  q1 q2,
      query_size (Q_NaturalJoin q1 q2) <= S n ->
      (query_size q1 <= n) /\ (query_size q2 <= n).
  Proof.
    intros n  q1 q2 Hn.
    apply le_S_n in Hn.
    split;refine (Le.le_trans _ _ _ _ Hn).
    - apply Plus.le_plus_trans;
        apply Peano.le_n.
    - simpl;rewrite Plus.plus_comm.
      apply Plus.le_plus_trans.
      rewrite Plus.plus_0_r.
      apply Peano.le_n.
  Qed.

  Lemma query_size_Pi :
    forall n l q,
      query_size (Q_Pi l q) <= S n  -> query_size q <= n.
  Proof.
    intros n l q Hn.
    simpl in Hn.
    apply le_S_n in Hn.
    refine (Le.le_trans _ _ _ _ Hn).
    simpl;rewrite Plus.plus_comm.
    destruct l.
    apply Plus.le_plus_trans.
    rewrite Plus.plus_0_r.
    apply Peano.le_n.
  Qed.

  Lemma query_size_Sigma :
    forall n f q,
      query_size (Q_Sigma f q) <= S n ->
      (query_size q <= n) /\
      (sql_formula_size  f <= n).
  Proof.
    intros n f q Hn.
    apply le_S_n in Hn.
    split.
    - refine (Le.le_trans _ _ _ _ Hn).
      simpl;rewrite Plus.plus_comm.
      apply Plus.le_plus_trans.
      rewrite Plus.plus_0_r.
      apply Peano.le_n.
    -  refine (Le.le_trans _ _ _ _ Hn).
       apply Plus.le_plus_trans.
       apply le_n.
  Qed.

  Lemma query_size_Gamma :
    forall n l f l' q,
      query_size (Q_Gamma l l' f q) <= S n ->
      (query_size q <= n) /\
      (sql_formula_size  f <= n).
  Proof.
    intros n l f l' q Hn.
    simpl in Hn.
    apply le_S_n in Hn.
    split.
    - refine (Le.le_trans _ _ _ _ Hn).
      destruct l.
      simpl;rewrite Plus.plus_comm.
      apply Plus.le_plus_trans.
      apply le_S.
      rewrite Plus.plus_comm.
      do 2 apply Plus.le_plus_trans.
      apply le_n.
    -  refine (Le.le_trans _ _ _ _ Hn).
       apply Plus.le_plus_trans.
       apply le_n.
  Qed.
  

  Lemma sql_formula_size_Conj :
    forall n a f1 f2,
      sql_formula_size (Sql_Conj a f1 f2) <= S n ->
      sql_formula_size  f1 <= n /\ sql_formula_size  f2 <= n.
  Proof.
    intros n a s1 s2 Hn.
    split.
    - apply le_S_n in Hn.
      refine (Le.le_trans _ _ _ _ Hn).
      apply Plus.le_plus_trans.
      apply le_n.
    - apply le_S_n in Hn.
      refine (Le.le_trans _ _ _ _ Hn).
      simpl.
      rewrite Plus.plus_comm.
      apply Plus.le_plus_trans.
      apply Plus.le_plus_trans.
      apply le_n.
  Qed.
  
  Lemma sql_formula_size_Not :
    forall n f,
      sql_formula_size (Sql_Not f) <= S n ->
      sql_formula_size  f <= n.
  Proof.
    intros n f Hn.
    apply le_S_n in Hn.
    refine (Le.le_trans _ _ _ _ Hn).
    apply Plus.le_plus_trans.
    apply le_n.
  Qed.

  Lemma sql_formula_size_In :
    forall n l q,
      sql_formula_size (Sql_In  l q) <= S n ->
      query_size q <= n.
  Proof.
    intros n l q Hn.
    apply le_S_n in Hn.
    refine (Le.le_trans _ _ _ _ Hn).
    simpl;rewrite Plus.plus_comm.
    rewrite Plus.plus_0_r,Plus.plus_0_l.
    do 2 apply Peano.le_S.
    rewrite Plus.plus_comm.
    apply Plus.le_plus_trans.
    apply le_n.
  Qed.

  Lemma sql_formula_size_Quant :
    forall n e p l q,
      sql_formula_size (Sql_Quant  e p  l q) <= S n ->
      query_size q <= n.
  Proof.
    intros n  e p l q Hn.
    apply le_S_n in Hn.
    refine (Le.le_trans _ _ _ _ Hn).
    simpl;rewrite Plus.plus_comm.
    do 2 apply le_S.
    rewrite Plus.plus_0_l.
    apply Plus.le_plus_trans.
    apply le_n.
  Qed.
  
  Lemma sql_formula_size_Exists :
    forall n q,
      sql_formula_size (Sql_Exists  _ q) <= S n ->
      query_size q <= n.
  Proof.
    intros n  q Hn.
    apply le_S_n in Hn.
    refine (Le.le_trans _ _ _ _ Hn).
    simpl;rewrite Plus.plus_comm.
    rewrite Plus.plus_0_r,Plus.plus_0_l.
    apply Peano.le_S.
    apply le_n.
  Qed.
  
  Lemma size_funterm_F_Expr :
    forall n s l,
      size_funterm    (F_Expr _ s l) <= S n ->
      forall ft, In ft l -> size_funterm ft <= n.
  Proof.
    intros n s l Hn ft Hft.
    apply le_S_n in Hn.
    refine (Le.le_trans _ _ _ _ Hn).
    apply (ListFacts.in_list_size _ _ ft Hft).
  Qed.

  Lemma size_aggterm_F_Expr :
    forall n s l,
      size_aggterm    T (A_fun T s l) <= S n ->
      forall ft, In ft l -> size_aggterm T ft <= n.
  Proof.
    intros n s l Hn ft Hft.
    apply le_S_n in Hn.
    refine (Le.le_trans _ _ _ _ Hn).
    apply (ListFacts.in_list_size _ _ ft Hft).
  Qed.
  
End Sizes.

(* From Datacert *) 
From SQLFS Require Import FiniteBag FiniteCollection.
(* ListSort. *)

Require Import NArith.
From SQLFS Require Import Bool3.

Section Envs.

  (* Q*cert *)
  
  (* Coq_sql *)
  Hypothesis T : Tuple.Rcd.
  
  Import Tuple.
  
  Notation value := (value T).
  Notation attribute := (attribute T).
  Notation A := (A T).
  Notation dot := (dot T).
  Notation labels := (labels T).
  Notation mk_tuple := (mk_tuple T).
  Notation BTupleT := (Fecol.CBag (CTuple T)).
  Notation bagT := (Febag.bag BTupleT).
  
  Hypothesis relname : Type.
  (* Hypothesis predicate : Type. *)
  (* Hypothesis symb : Type. *)
  (* Hypothesis aggregate : Type. *)
  Hypothesis ORN : Oset.Rcd relname.
  (* Hypothesis OPredicate : Oset.Rcd predicate. *)
  (* Hypothesis OSymb : Oset.Rcd symb. *)
  (* Hypothesis OAggregate : Oset.Rcd aggregate. *)
  Hypothesis basesort : relname -> Fset.set A.

  (* Interpretation *)
  Notation B := (Tuple.B T).
  Hypothesis interp_predicate : predicate T -> list (Tuple.value T) -> Bool.b B.
  Hypothesis interp_symb : symbol T -> list (Tuple.value T) -> Tuple.value T.
  Hypothesis interp_aggregate : aggregate T -> list (Tuple.value T) -> Tuple.value T.
  Hypothesis unknown : (Bool.b B).
  Hypothesis contains_nulls : Tuple.tuple T -> bool.
  Hypothesis contains_nulls_eq : forall t1 t2, t1 =t= t2 -> contains_nulls t1 = contains_nulls t2.
  
  Notation eval_sql_formula :=
    (Formula.eval_sql_formula (* B OSymb interp_predicate interp_symb interp_aggregate *) unknown
                              contains_nulls).
  
  Notation eval_query i :=
    (eval_query (* OSymb B interp_predicate interp_symb interp_aggregate *) basesort 
                (fun r => match Oset.find ORN r i with | Some b => b |None => Febag.empty _ end)
                unknown contains_nulls).

  Notation group_by  := (group_by T ).
  Notation query := (query T relname ).
  Notation q_formula := (sql_formula T query ).
  Notation funterm := (@funterm T).
  Notation aggterm := (@aggterm T).
  
  Arguments Q_Empty_Tuple {T} {relname}.
  Arguments Q_Table {T} {relname}.
  Arguments Sql_True {T} {dom} .

  
  Lemma t_env_equiv :
    forall i env q f, forall e e' : tuple T,
        (Febag.nb_occ (Fecol.CBag (CTuple T)) e ((eval_query i) env q) >= 1)%N ->
        e =t= e' ->
        Bool.is_true B (eval_sql_formula (eval_query i) (env_t _ env e) f) =
        Bool.is_true  B (eval_sql_formula (eval_query i) (env_t _ env e') f).
  Proof.
    intros i env q f u1 u2 _ Hu.
    replace (eval_sql_formula (eval_query i) (env_t _ env u1) f) with
        (eval_sql_formula (eval_query i) (env_t _ env u2) f);[apply eq_refl| ].
    apply eval_f_eq;[apply contains_nulls_eq | ].
    apply Oeset.compare_eq_sym;apply Hu.
  Qed.

  Lemma equiv_env_slice_refl :
    forall slc : Fset.set A * group_by * list (tuple T), equiv_env_slice T slc slc.
  Proof.
    intros [[ ]] .
    unfold equiv_env_slice.
    repeat split.
    apply Fset.equal_refl.
    intros;apply Oeset.permut_refl.
  Qed.
    
  
  Lemma equiv_env_cons_eq_1 :
    forall (s1 s2 :Fset.set A),
      s1 =S= s2 -> forall env (g:group_by) (l:list (tuple T)),
        Env.equiv_env  T ((s1, g, l) :: env) ((s2, g, l) :: env).
  Proof.
    intros s1 s2 Hs env g l.
    unfold env_t; constructor 2; [ | apply equiv_env_refl].
    simpl; repeat split.
    apply Hs.
    apply Oeset.permut_refl.
  Qed.

  Lemma equiv_env_cons_eq_2 :
    forall (g1 g2:group_by),
      g1 = g2 ->
      forall env s  (l:list (tuple T)) ,
        equiv_env T ( (s, g1, l) :: env) ((s, g2, l) :: env).
  Proof.
    intros g1 g2 Aux env s l.
    unfold env_t; constructor 2; [ | apply equiv_env_refl].
    simpl; repeat split;
      [apply Fset.equal_refl|
       apply Aux |
       apply _permut_refl;intros;apply Oeset.compare_eq_refl].
  Qed.
  
    Lemma equiv_env_cons_eq_3 :
    forall (l1 l2:list (tuple T)),
      l1 =PE= l2 -> forall env s (g:group_by),
        equiv_env T((s, g, l1) :: env) ((s, g, l2) :: env).
  Proof.
    intros l1 l2 Hs env s g.
    unfold env_t; constructor 2; [ | apply equiv_env_refl].
    simpl; repeat split;[apply Fset.equal_refl|apply Hs].
  Qed.

  Lemma equiv_env_cons_eq :
    forall s1 s2 (g1 g2:group_by) (l1 l2:list (tuple T)),
      s1 =S= s2 ->
      g1 = g2 ->
      l1 =PE= l2 -> forall env,
          equiv_env T ((s1,g1, l1) :: env) ((s2, g2, l2) :: env).
  Proof.
    intros s1 s2 g1 g2 l1 l2 Hs Hg Hl env .
    unfold env_t; constructor 2; [ | apply equiv_env_refl].
    simpl;repeat split;assumption.
  Qed.
  
  (* Lemma partition_list_expr_nil : *)
  (*   forall X V (OV:Oset.Rcd V)  (FV:Fset.Rcd OV) la, *)
  (*     (Partition.partition_list_expr FV nil (map (fun (a:X) f =>  f a) la)) = nil. *)
  (* Proof. *)
  (*   induction la as [|a la]; *)
  (*     [apply eq_refl| ]. *)
  (*   simpl. *)
  (*   rewrite IHla. *)
  (*   apply eq_refl. *)
  (* Qed. *)
  
  Lemma all_distinct_in_one :
    forall slc (env: env T ) (a:attribute),
      all_distinct T (map (fun x : _ * list (tuple T) => fst (fst x)) (slc::env)) = true /\ a inS (fst (fst slc)) ->
      forall s2 , In s2 ( map (fun x : _ * list (tuple T) => fst (fst x)) env) -> a inS? s2 = false.
  Proof.
    intros slc env a Hd s2 Hs2.
    rewrite map_cons in Hd.
    destruct Hd as  [H1 H2].
    simpl all_distinct in H1.
    rewrite Bool.Bool.andb_true_iff in H1.
    destruct H1 as [H1 H3].
    rewrite Fset.is_empty_spec in H1.
    rewrite Fset.equal_spec in H1.
    assert (Ha := H1 a).
    rewrite Fset.mem_inter,H2 in Ha.
    rewrite Bool.Bool.andb_true_l in Ha.
    rewrite Fset.empty_spec in Ha.
    rewrite mem_Union_false in Ha.
    apply (Ha _ Hs2).
  Qed.
  
  Lemma tuple_as_pairs_labels :
    forall  (t : Tuple.tuple T),
      {{{ labels  t }}} = map fst (Tuple.tuple_as_pairs T t).
  Proof.
    unfold Tuple.tuple_as_pairs.
    intro t.
    rewrite map_map;simpl fst.
    rewrite map_id;apply eq_refl.
  Qed.

  Lemma tuple_tuple_as_pairs_dot_in :
    forall (t : Tuple.tuple T) a,
      a inS labels  t ->
      Oset.find (Tuple.OAtt T) a (Tuple.tuple_as_pairs T t) = Some (dot t a).
  Proof.
    intros t a Ha.
    unfold Tuple.tuple_as_pairs.
    refine (Oset.find_map _ _ _ _ (Fset.mem_in_elements _ _ _ Ha)).
  Qed.

  (* Lemma tuple_tuple_as_pairs_dot_nin : *)
  (*   forall (t : Tuple.tuple T) a, *)
  (*     Fset.mem _ a (labels t) = false -> *)
  (*     Oset.find (Tuple.OAtt T) a (Tuple.tuple_as_pairs T t) = None *)
  (*     /\ dot t a = Tuple.default_value T (Tuple.type_of_attribute T a). *)
  (* Proof. *)
  (*   unfold Tuple.tuple_as_pairs. *)
  (*   intros t a Ha;split. *)
  (*   apply Oset.not_mem_find_none. *)
  (*   rewrite map_map,map_id. *)
  (*   rewrite <- Fset.mem_elements. *)
  (*   apply Ha. *)
  (* Qed. *)

  (* Lemma tuple_tuple_as_pairs_dot : *)
  (*   forall t a v, *)
  (*     dot t a = v <->  *)
  (*     ((Oset.find (Tuple.OAtt T) a (Tuple.tuple_as_pairs T t) = Some v) *)
  (*      \/ *)
  (*      ((Oset.find (Tuple.OAtt T) a (Tuple.tuple_as_pairs T t) = None) *)
  (*       /\ *)
  (*       (v = Tuple.default_value T (Tuple.type_of_attribute T a)))). *)
  (* Proof. *)
  (*   intros t a v;split;intro Hv. *)
  (*   - destruct (a inS? labels  t) eqn:D1. *)
  (*     + rewrite (tuple_tuple_as_pairs_dot_in  _ _ D1),Hv. *)
  (*       left;apply eq_refl. *)
  (*     + rewrite <- Hv. *)
  (*       right. *)
  (*       refine (tuple_tuple_as_pairs_dot_nin  _ _ D1). *)
  (*   - destruct Hv as [Hv|Hv]. *)
  (*     + assert (A1 := Oset.find_some _ _ _ Hv). *)
  (*       unfold Tuple.tuple_as_pairs in A1. *)
  (*       rewrite in_map_iff in A1. *)
  (*       destruct A1 as [x [A1 A2]]. *)
  (*       apply Fset.in_elements_mem in A2. *)
  (*       assert (A3 := tuple_tuple_as_pairs_dot_in  _ _ A2). *)
  (*       inversion A1;subst. *)
  (*       apply eq_refl. *)
  (*     + destruct Hv as [H1 H2]. *)
  (*       rewrite H2. *)
  (*       clear H2. *)
  (*       assert (A4 := Oset.find_none_alt _ _ _ H1). *)
  (*       unfold Tuple.tuple_as_pairs in A4. *)
  (*       rewrite map_map,map_id in A4. *)
  (*       unfold dot. *)
  (*       destruct (a inS? labels  t) eqn:D1;[|apply eq_refl]. *)
  (*       apply Fset.mem_in_elements in D1. *)
  (*       destruct (A4 D1). *)
  (* Qed. *)
  
  Lemma  tuple_tuple_as_pairs_in_labels :
    forall t a, (a inS labels t ) <-> In a (map fst (Tuple.tuple_as_pairs T t)).
  Proof.
    intros t a.
    rewrite  <- tuple_as_pairs_labels.
    rewrite Fset.mem_elements.
    apply Oset.mem_bool_true_iff.
  Qed.

  Lemma mk_tuple_in_add :
    forall s f a,
      a inS s ->
      mk_tuple s f =t=
      (mk_tuple (a addS s) (fun a' : attribute => if Oset.eq_bool (OAtt T) a a' then f a else f a')).
  Proof.
    intros s f a Hat.
    apply Tuple.tuple_eq;split.
    - rewrite (Fset.equal_eq_2 _ _ _ _ (Tuple.labels_mk_tuple _ _ _)).
      rewrite Fset.equal_spec.
      intro e.
      rewrite Fset.add_spec.
      rewrite (Fset.mem_eq_2 _ _ _  (Tuple.labels_mk_tuple _ _ _)).
      destruct (Oset.eq_bool (OAtt T) e a) eqn:D1;[|apply eq_refl].
      rewrite Oset.eq_bool_true_iff in D1.
      rewrite D1,Hat.
      apply eq_refl.
    - intros e He.
      rewrite 2 dot_mk_tuple.      
      rewrite Fset.add_spec.
      rewrite (Fset.mem_eq_2 _ _ _  (Tuple.labels_mk_tuple _ _ _)) in He.
      rewrite He,Bool.orb_true_r.
      destruct (Oset.eq_bool (OAtt T) a e ) eqn:D1;[|apply eq_refl].
      rewrite Oset.eq_bool_true_iff in D1;subst.
      apply eq_refl.
  Qed.
  
  Lemma mk_tuple_in_labels_add :
    forall t a ,
      a inS labels t ->
      t =t=
      (mk_tuple (a addS labels t) (fun a' : attribute => if Oset.eq_bool (OAtt T) a a' then dot t a else dot t a')).
  Proof.
    intros t a.
    rewrite <- (Oeset.compare_eq_1 _ _ _ _  (Tuple.mk_tuple_idem _ _ _ (Fset.equal_refl _ _))).
    apply mk_tuple_in_add.
  Qed.

  Lemma comparelA_labels_equal :
    forall l3 l4 : list (tuple T),
      comparelA (Oeset.compare (OTuple T)) l3 l4 = Eq ->
      match l3 with
      | nil => emptysetS
      | t :: _ => labels t
      end =S= match l4 with
              | nil => emptysetS
              | t :: _ => labels t
              end.
  Proof.
    induction l3;intros l4 Hl.
    - destruct l4;[|discriminate].
      apply Fset.equal_refl.
    - destruct l4;[discriminate| ].
      simpl in Hl.
      destruct Oeset.compare eqn:D1;try discriminate.
      apply (tuple_eq_labels _ _ _ D1).
  Qed.

  Lemma permut_quick_comparelA :
  forall (x1 x2 : list (tuple T)),
    x1 =PE= x2 ->
    comparelA (Oeset.compare (OTuple T)) (ListSort.quicksort (OTuple T) x1) (ListSort.quicksort (OTuple T) x2) = Eq.
Proof.
intros x1 x2 Hx.
apply (ListSort.sort_is_unique (OA := OTuple T)).
- apply ListSort.quick_sorted.
- apply ListSort.quick_sorted.
- apply Oeset.permut_trans with x1.
  + apply Oeset.permut_sym; apply (ListSort.quick_permut (OTuple T)).
  + apply Oeset.permut_trans with x2; [assumption | ].
    apply (ListSort.quick_permut (OTuple T)).
Qed.
    
  Lemma dot_aggterm_env_t :
    forall env t a,
      a inS labels  t ->
      dot t a = 
      interp_aggterm T (env_t T env t) (A_Expr _ (F_Dot _  a)).
  Proof.
    intros env t a Ha.
    unfold interp_funterm,
    interp_dot.
    simpl.
    rewrite Ha.
    apply eq_refl.
  Qed.
  
  Lemma dot_is_aggterm_subset :
    forall l env t, Fset.mk_set A l subS labels t ->
               mk_tuple (Fset.mk_set A l) (fun a => dot t a) =t=
               mk_tuple (Fset.mk_set A l) (fun a => interp_aggterm T (env_t T env t) (A_Expr _ (F_Dot _ a))).
  Proof.
    intros l env t Ht.
    apply mk_tuple_eq_2.
    intros.
    apply dot_aggterm_env_t .
    rewrite Fset.subset_spec in Ht.
    apply (Ht _ H).
  Qed.

  
    Definition type_of_funterm type_of_symbol f :=
        match f with
        | F_Constant _ c => type_of_value T c
        | F_Dot _ a =>   type_of_attribute T a
        | F_Expr _ f l => type_of_symbol f
        end.

    
  Lemma equiv_env_t_eq :
    forall (env:env T ) (t1 t2:tuple T) , t1 =t= t2 -> equiv_env T (env_t T env t1) (env_t T env t2).
  Proof.
    intros.        
    constructor 2; [ | apply equiv_env_refl].    
    simpl; repeat split.
    - refine( tuple_eq_labels _ _ _ H).
    - apply Oeset.permut_refl_alt.
      simpl;rewrite H;apply eq_refl.
  Qed.

  
  Lemma _permut_flat_map_strong :
    forall B,forall  llt llt3 : list (list B), _permut eq llt llt3 -> _permut eq (flat_map (fun x : _ => x) llt3)  (flat_map (fun x : list B => x) llt).
  Proof.
    induction llt.
    - intros llt3 H1;inversion H1;subst;constructor.
    - intros llt3 H1.
      inversion H1;subst.
      assert (A1 := IHllt _ H4).
      rewrite ListFacts.flat_map_app.             
      simpl.
      rewrite app_assoc.
      refine (_permut_trans _ (_permut_swapp (_permut_refl _ _ _)  (_permut_refl _ _ _)) _);try congruence.
      refine (_permut_trans _ _ (_permut_swapp (_permut_refl _ _ _)  (_permut_refl _ _ _)));try congruence.      
      rewrite app_assoc.
      rewrite ListFacts.flat_map_app in A1.
      refine (_permut_app  _ (_permut_refl _ _ _));try congruence.      
      refine (_permut_trans _ (_permut_swapp (_permut_refl _ _ _)  (_permut_refl _ _ _)) _);try congruence.
  Qed. 
  

  Lemma inter_is_empty_join_compatible_tuple_true :
    forall t1 t2,
      Fset.is_empty A (labels t1 interS labels t2) = true ->
      join_compatible_tuple T t1 t2 = true.
  Proof.
    intros t1 t2 H.
    unfold join_compatible_tuple.
    rewrite Fset.for_all_spec_alt.
    intros a Ha.
    rewrite Fset.is_empty_spec in H.
    rewrite Fset.equal_spec in H.
    rewrite H,Fset.empty_spec in Ha.
    discriminate.
  Qed.

  
    Lemma well_formed_e_cons :
      forall e slc, well_formed_e T (slc::e) = true -> well_formed_e T e = true.
    Proof.
      intros e slc H.
      rewrite cons_nil_app,app_comm_cons in H.
      apply well_formed_e_tail in H.
      apply H.
    Qed.


    Lemma well_formed_e_cons_spec :
      forall e s g lt,
        well_formed_e T ((s,g,lt)::e) = true <->
        match g with
        | Group_By _ g1 =>
          match lt with
          | nil => False
          | t1 :: _ =>
            forall ag : aggterm ,
              In ag g1 ->
              well_formed_ag T (env_t T e (default_tuple T s)) ag = true /\
              (forall t : tuple T,
                  In t lt ->
                  forall ag0 : aggterm , In ag0 g1 -> interp_aggterm T (env_t T e t) ag0 = interp_aggterm T (env_t T e t1) ag0)
          end
        | Group_Fine _ => match lt with
                         | _ :: nil => True
                         | _ => False
                         end
        end /\
        (forall a : attribute,
            (a inS? s) = false \/ (forall slc : Fset.set A * group_by * list (tuple T), In slc e -> (a inS? fst (fst slc)) = false)) /\
        (forall x : tuple T, In x lt -> labels x =S= s) /\ well_formed_e T e = true.
    Proof.
      intros e s g lt;split;intro H.
      - simpl in H.
        repeat rewrite Bool.Bool.andb_true_iff in H.
      destruct H as [[[H1 H3] H2] H5].
      rewrite forallb_forall in H3.
      rewrite Fset.is_empty_spec in H2.
      rewrite Fset.equal_spec in H2.
      assert (H4 := fun a => eq_trans (H2 a) (Fset.empty_spec _ _)).
      clear H2.
      assert (H2 := fun a => eq_trans (eq_sym (Fset.mem_inter _ _ _ _)) (H4 a)).
      clear H4.
      assert (H4 := fun a =>  (proj1 (andb_false_iff _ _) (H2 a))).
      clear H2.
      assert (forall a : attribute,
       (a inS? s) = false \/ (forall slc, In slc e -> a inS? fst (fst slc) = false)).
      { intro a.
        destruct (H4 a);[left;apply H| ].        
        rewrite mem_Union_false in H.
        right;intros.
        assert (Hs1 := H (fst (fst slc))).
        rewrite in_map_iff in Hs1.
        apply Hs1;exists slc;split;[|apply H0].
        destruct slc as [ [ ]];apply eq_refl. }
      assert (H6 : 
                match g with
                | Group_By _ g1 =>
                  match lt with
                  | nil => False
                  | t1 :: _ =>
                    forall ag, In ag g1 -> well_formed_ag T (env_t T e (default_tuple T s)) ag = true /\
             forall t, In t lt ->
                  forall ag, In ag g1 ->
                        interp_aggterm T (env_t T e t) ag = interp_aggterm T (env_t T e t1) ag
                  end
                | Group_Fine _ => match lt with
                                 | _ :: nil => True
                                 | _ => False
                         end
                end).
      {  destruct g;swap 1 2.
         - destruct lt;[discriminate|destruct lt;[constructor|discriminate]].
         - rewrite andb_true_iff,forallb_forall in H1.
           destruct H1.
           destruct lt;[discriminate| ].
           rewrite forallb_forall in H1.
           intros;split;[apply H0;assumption| ].
           intros.
           apply H1 in H6.
           rewrite forallb_forall in H6.
           rewrite <- (Oset.eq_bool_true_iff (OVal T)).
           apply H6;apply H7. }
      repeat split;assumption.
      - simpl in H.
        simpl.
        repeat rewrite andb_true_iff.
      destruct H as [H1 [H2 [H3 H4]]].
      rewrite forallb_forall.
      rewrite Fset.is_empty_spec.
      rewrite Fset.equal_spec.
      repeat split;try assumption.
        + destruct g.
          * destruct lt;[destruct H1| ].
            rewrite andb_true_iff,2 forallb_forall.
            split;[apply H1| ].
            intros.
            rewrite forallb_forall.
            intros.
            rewrite Oset.eq_bool_true_iff.
            destruct (H1 x0) as [G1 G2];[apply H0| ].
            apply G2;assumption.
          * destruct lt;[destruct H1|destruct lt;[apply eq_refl|destruct H1]].
        + intros a.
          rewrite Fset.empty_spec,Fset.mem_inter,andb_false_iff.
          destruct (H2 a);[left;apply H |right].
          rewrite mem_Union_false.
          intros s1 Hs1.
          rewrite in_map_iff in Hs1.
          destruct Hs1 as [[[]] [ -> Hs1]].
          apply (H (s1,g0,l) Hs1).
    Qed.          

    Lemma well_formed_e_cons_lt_neq_nil :
      forall e s g lt,
        well_formed_e T ((s, g, lt) :: e) = true -> lt <> nil.
    Proof.
      intros e s g lt We ->.
      apply well_formed_e_cons_spec in We.
      destruct g;destruct We as [[]].
    Qed.

    Lemma well_formed_e_cons_tuple :
      forall env s1 g1 lt1,
        well_formed_e T ((s1, g1, lt1) :: env) = true -> forall t, In t lt1 -> well_formed_e T ((s1, Group_Fine _, t :: nil) :: env) = true.
    Proof.
      intros env s1 g1 lt1.
      rewrite well_formed_e_cons_spec.
      intros [H1 [H2 [H3  H4]]] t Ht.
      rewrite well_formed_e_cons_spec.
      repeat split;try assumption.
      intros.
      destruct H;[subst|inversion H].
      apply (H3 _ Ht).
    Qed.

    
    Lemma well_formed_e_cons_tuple_alt :
      forall env s1 g1 lt1,
        well_formed_e T ((s1, g1, lt1) :: env) = true -> forall t, In t lt1 -> well_formed_e T ((s1, g1, t :: nil) :: env) = true.
    Proof.
      intros env s1 g1 lt1.
      rewrite well_formed_e_cons_spec.
      intros [H1 [H2 [H3  H4]]] t Ht.
      rewrite well_formed_e_cons_spec.
      repeat split;try assumption.
      -  destruct g1;[|constructor].
         destruct lt1;[destruct H1| ].
         intros ag Hag.
         assert (H5 := H1 ag Hag).
         split;[apply H5| ].
         intros.
         destruct H;[|destruct H].
         subst.
         apply eq_refl.
      - intros;apply H3.
        destruct H;[subst;apply Ht|destruct H].
    Qed.
      
    Lemma  well_formed_e_interp_dot : forall e s g lt,
        well_formed_e T ((s,g,lt)::e) = true ->
        forall a,
          a inS s ->
          interp_dot T e a =
          (default_value
             T (type_of_attribute T a)).
    Proof.
      intros e s g lt We a Ha.
      assert (Ha' : a inS? (Fset.Union A (map
                             (fun x =>
                                match x with
                                | (s1,_,_) => s1
                                end) e)) = false).
      {
        simpl in We.
        repeat rewrite andb_true_iff in We.       
        destruct We as [[[H1 H2] H3] H4].              
        rewrite Fset.is_empty_spec in H3.
        rewrite Fset.equal_spec in H3.
        assert (H3 := H3 a).
        rewrite Fset.empty_spec,Fset.mem_inter,Ha in H3.
        apply H3.
      }
      apply well_formed_e_cons in We.      
      revert We Ha';clear.
      induction e as [|[[s1 g1] lt1] e];intros;[apply eq_refl| ].
      simpl.
      simpl in Ha'.
      rewrite Fset.mem_union in Ha'.
      rewrite orb_false_iff in Ha'.
      assert (IHe := IHe (well_formed_e_cons _ _ We) (proj2 Ha')).
      destruct quicksort eqn:D1;[apply IHe| ].
      rewrite well_formed_e_cons_spec in We.
      destruct We as [H1 [H2 [H3 H4]]].
      assert (G1 : In t lt1).
      {
        rewrite (In_quicksort (OTuple T)),D1.
        left;apply eq_refl. }
      rewrite  (Fset.mem_eq_2 _ _ _ (H3 _ G1)),(proj1 Ha').
      apply IHe.
    Qed.
      
    Lemma interp_dot_mem_cons_nil :
      forall e s g lt,
        well_formed_e T ((s,g,lt)::e) = true ->
        forall a, a inS s  ->  interp_dot T ((s,g,lt)::e) a = interp_dot T ((s,g,lt)::nil) a.
    Proof.
      intros.
      simpl.
      assert (A1 := well_formed_e_cons_lt_neq_nil e s g H).
      destruct (quicksort (OTuple T) lt) eqn:D1.
      - apply quicksort_nil in D1.
        destruct (A1 D1).
      - destruct (Fset.mem _ _ (labels t));[apply eq_refl| ].
        refine (well_formed_e_interp_dot _ _ _ _  H _ H0).
    Qed.

            
        Lemma  well_formed_s_cons :
          forall e s sa,
            well_formed_s T e (sa::s) = true ->            
            well_formed_s T e s = true.
        Proof.
          unfold well_formed_s.          
          intros e s [ ].
          simpl.
          repeat rewrite andb_true_iff.
          rewrite forallb_forall.
          intros [[H3 H1] H2];repeat split.
          - intros [ ] Hx;apply H3 in Hx;apply Hx.
          - destruct  (map (fun x : select T => fst match x with
                                                        | Select_As _ e0 a1 => (a1, e0)
                                                   end) s);[apply eq_refl| ].
            rewrite andb_true_iff in H1.
            destruct H1 as [_ H1].
            apply H1.
          - rewrite Fset.is_empty_spec,Fset.equal_spec.
            rewrite Fset.is_empty_spec,Fset.equal_spec in H2.
            intros x.
            rewrite Fset.empty_spec.
            assert (Hx := H2 x).
            rewrite Fset.empty_spec in Hx.
            rewrite Fset.mem_inter in Hx.
            rewrite andb_false_iff in Hx.
            rewrite Fset.mem_inter.
            rewrite andb_false_iff.
            destruct Hx;[left|right;apply H].
            rewrite Fset.add_spec in H.
            rewrite orb_false_iff in H.
            apply H.
        Qed.

  Lemma well_formed_e_env_t :
    forall e s t,
      Fset.is_empty A (s interS Fset.Union A (map fst (map fst e))) = true ->
      labels t =S= s ->
      well_formed_e T e = true -> well_formed_e T  ((s, Group_Fine T , t :: nil) :: e)  = true.
  Proof.
     intros e s t Aux Ht We.
      rewrite  well_formed_e_cons_spec.
      repeat split;[|intros t2 [|[]];subst;apply Ht|apply We].
      intros a.
      rewrite Fset.is_empty_spec in Aux.
      rewrite Fset.equal_spec in Aux.
      assert (Aux := Aux a).
      rewrite Fset.empty_spec,Fset.mem_inter in Aux.
      rewrite andb_false_iff in Aux.
      destruct Aux;[left;apply H|right].
      intros slc Hs.
      rewrite mem_Union_false in H.
      apply H.
      rewrite in_map_iff;exists (fst slc);repeat split.
      rewrite in_map_iff;exists slc;repeat split.
      apply Hs.
  Qed.

  
  Lemma well_formed_s_Group_Fine :
    forall e sl s lt,
      well_formed_s T (env_t T e (default_tuple T s)) sl = true ->
      well_formed_s T ((s, Group_Fine T, lt) :: e) sl = true.
  Proof.
    intros e sl s lt.
    unfold well_formed_s.
    rewrite 4 andb_true_iff.
    rewrite 2 forallb_forall.
      intros [[H1 H2] H3];repeat split.
      - intros x Hx.
        apply H1 in Hx.
        destruct x.
        rewrite <- Hx.
        apply well_formed_ag_eq.
        constructor.
        +            split;[|apply eq_refl].
          unfold default_tuple.
          rewrite (Fset.equal_eq_2 _ _ _ _ (labels_mk_tuple _ _ _)).
          apply Fset.equal_refl.
        + apply equiv_env_weak_equiv_env.
          apply equiv_env_refl.
      - apply H2.
      - rewrite <- H3.
        apply Fset.is_empty_eq.
        apply Fset.inter_eq_2.
        rewrite Fset.equal_spec.
        intros a.
        unfold env_t;simpl.
        rewrite 2 Fset.mem_union.
        apply f_equal2;[|apply eq_refl].
        unfold default_tuple.
        apply eq_sym.
        apply (Fset.mem_eq_2 _ _ _ (labels_mk_tuple _ _ _)).
  Qed.
  
      Lemma well_formed_ft_variables_ft_empty :
        forall ft e,
          Fset.is_empty A (variables_ft T ft) = true ->
          well_formed_ft T e ft = true.
      Proof.
        unfold well_formed_ft.
        intros ft e.
        rewrite Fset.is_empty_spec,Fset.equal_spec.
        set (n := size_funterm T ft).
        assert (H : size_funterm T ft <= n) by apply le_n.
        generalize dependent n.
        intros n Hn.
        revert ft Hn e.
        induction n.
        - intros;destruct ft;inversion Hn.
        - destruct  ft as [v |a| ];intros Hn e Hft.
          + apply eq_refl.
          + assert (N1 := Hft a);clear Hft.
            rewrite Fset.empty_spec in N1.
            simpl in N1.
            rewrite Fset.singleton_spec in N1.
            rewrite Oset.eq_bool_refl in N1.
            discriminate.
          + simpl.
            rewrite orb_true_iff;right.
            rewrite forallb_forall.
            intros x Hx.
            apply (IHn _ (size_funterm_F_Expr Hn _ Hx)).
            intros a.
            assert (IH := Hft a);clear Hft.
            rewrite Fset.empty_spec in *.
            simpl in IH.
            rewrite mem_Union_false in IH.
            refine (IH (variables_ft T x) _).
            rewrite in_map_iff.
            exists x;split;[apply eq_refl|apply Hx].
      Qed.


      Lemma Fset_Union_map_eq_env :
        forall e1 e2 : list (Fset.set A * group_by * list (tuple T)),
          weak_equiv_env T e1 e2 ->
          Fset.Union A (map (fun x : Fset.set A * group_by * list (tuple T) => let (y, _) := x in let (s0, _) := y in s0) e1) =S=
          Fset.Union A (map (fun x : Fset.set A * group_by * list (tuple T) => let (y, _) := x in let (s0, _) := y in s0) e2).
      Proof.
        intros e1 e2.
        induction 1.
        apply Fset.equal_refl.
        destruct x as [[]],y as [[]].
        simpl.
        rewrite Fset.equal_spec.
        intros a.
        rewrite 2 Fset.mem_union.
        apply f_equal2.
        apply Fset.mem_eq_2;apply H.
        apply Fset.mem_eq_2;assumption.
      Qed.
        
  Lemma well_formed_s_eq :
    forall s e1 e2,
      weak_equiv_env T e1 e2 ->
       well_formed_s T e1 s = well_formed_s T e2 s.
  Proof.
    unfold well_formed_s.
    intros s e1 e2 Heq.
    rewrite  2 (andb_comm (forallb _ _) _).
    apply f_equal2.
    -  apply f_equal.   
      apply ListFacts.forallb_eq.
      intros [ ] Hx.
      apply (well_formed_ag_eq _ _ _ _ Heq).
    - apply Fset.is_empty_eq.
      apply Fset.inter_eq_2.
      revert e1 e2 Heq.
      apply Fset_Union_map_eq_env.
  Qed.

  Lemma weak_equiv_env_refl :
    forall Tu e , weak_equiv_env Tu e e.
  Proof.
    induction e;[constructor| ].
    constructor;[|apply IHe].
    unfold  weak_equiv_env_slice.
    destruct a as [[]];repeat split.
    apply Fset.equal_refl.
  Qed.
      
End Envs.

Section Partition.


Lemma NoDup_partition :
  forall A value : Type, forall (OV: Oset.Rcd value) (f:A -> _) l,
        NoDup (Partition.partition OV f l).
Proof.
  clear.
  intros.
  assert (A1 := Partition.partition_all_diff_values OV f l ).
  rewrite all_diff_NoDup_same in A1.
  apply (NoDup_map_inv _ _ A1).
Qed.

Lemma NoDup_map_snd_partition :
  forall A value : Type, forall (OV: Oset.Rcd value) (f:A -> _) l,
        NoDup (map snd (Partition.partition OV f l)).
Proof.
  clear.
  intros.
  assert (A1 := Partition.partition_all_diff_values OV f l).
  assert (A2 := Partition.in_partition_diff_nil OV f l).
  assert (A3 := Partition.partition_homogeneous_values OV f l).
  rewrite all_diff_NoDup_same in A1.
  set (lt := Partition.partition  OV f l) in *.
  revert dependent lt.
  intros lt A1 A2 A3.
  induction lt;[constructor| ].
  - rewrite map_cons.
    inversion A1;subst.
      simpl in A3,A2.
      assert (A5 := fun v k H => A3 v k (or_intror H)).      
      assert (A4 := fun v k H => A2 v k (or_intror H)).
      assert (IH := IHlt H2 A4 A5).      
      constructor;[|apply IH].
      intros N1.
      apply H1.
      destruct a.
      simpl in N1.
      destruct l0;[destruct (A2 _ _ (or_introl eq_refl) eq_refl)| ].
      assert (N3 := N1).
      rewrite in_map_iff in N1.
      destruct N1 as [[l2 l3] [N1 N2]].
      rewrite in_map_iff.
      destruct l3;[destruct (A2 _ _ (or_intror N2) eq_refl)| ].
      exists  (l2, a0 :: l3);split;[|apply N2].
      simpl in N1.
      rewrite <- (A5  _ _ N2 a0 (or_introl eq_refl)).      
      rewrite <- (A3  _ _ (or_introl eq_refl) a  (or_introl eq_refl)).
      inversion N1.
      apply eq_refl.
Qed.

      Lemma interp_quant_id :
        forall X B (f : X -> Bool.b B) quant l,
        interp_quant B quant (fun x : Bool.b _ => x) (map f l) =
        interp_quant B quant f l.
      Proof.
        intros;destruct quant;
          (induction l as [|a l IH];
           [|simpl in *;rewrite IH];apply eq_refl).
      Qed.
      
End Partition.
