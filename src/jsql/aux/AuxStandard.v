(************************************************************************************)
(**                                                                                 *)
(**                          The SQLToNRACert Library                               *)
(**                                                                                 *)
(**                       LMF, CNRS & Université Paris-Saclay                       *)
(**                                                                                 *)
(**                        Copyright 2016-2022 : FormalData                         *)
(**                                                                                 *)
(**         Authors: Véronique Benzaken                                             *)
(**                  Évelyne Contejean                                              *)
(**                  Mohammed Hachmaoui                                             *)
(**                  Chantal Keller                                                 *)
(**                                                                                 *)
(************************************************************************************)

Set Implicit Arguments.

Require Import Arith List Permutation SetoidList Bool  Classical_Prop Classical_Pred_Type.

Section Options.

  Lemma Some_inj :
    forall A (a1 a2 : A), Some a1 = Some a2 <-> a1 = a2.
  Proof.
    intros A a1 a2;split;
      intros Ha;inversion Ha;
        apply eq_refl.
  Qed.

End Options.

Section Lists.

  Section App.
    
    Lemma cons_nil_app :
      forall X lx (x:X), x::lx = x::nil ++ lx.
    Proof.
      intro X;induction lx;intro x;
        [apply eq_refl|idtac].
      rewrite  (IHlx a) at 2.
      apply eq_refl.
    Qed.

    Lemma app_nil_1_2 :
      forall {A} (l1 l2:list A),
        nil = l1 ++ l2 -> l1= nil /\ l2 = nil.
    Proof.
      intros  A l1 l2 H.
      destruct l2;destruct l1;
        try discriminate.
      split;apply eq_refl.
    Qed.

  End App.
  
  Lemma filter_false :
    forall A (l:list A ), filter (fun _ : A => false)  l = nil.
  Proof.
    intros;induction l;intuition.
  Qed.

  
  Lemma filter_cons :
    forall X f l (a : X), filter f (a::l) = if f a then a::(filter f l) else filter f l.
  Proof.
    intros;apply eq_refl.
  Qed.

  Lemma filter_ext_in :
    forall A (f g : A -> bool) (l : list A),
      (forall x, In x l -> f x = g x) ->
      filter f l = filter g l.
  Proof.
    induction l; simpl; trivial; intros.
    rewrite IHl by intuition.
    rewrite (H a) by intuition.
    trivial.
  Qed.

  
  Lemma map_Some_in_1 :
    forall A B f (l:list A) (l':list B),
      map f l = map Some l' ->
      (forall a, In a l -> exists b, In b l' /\ f a = Some b).
  Proof.
    intros A B f.
    induction l as [|a1 l].
    - intros [|b2 l']  Hl a Ha;
        [inversion Ha|discriminate].
    - intros [|b2 l']  Hl a Ha;[inversion Hl| ].
      inversion Hl as [[H1 H2]].
      assert (IH := IHl l' H2).
      destruct Ha as [Ha|Ha].
      + exists b2;split;[left;apply eq_refl| rewrite <- Ha;apply H1].
      + assert (Aux := IH a Ha).
        destruct Aux as [b' [I1 I2]].
        exists b';split;[right;apply I1|apply I2].
  Qed.

  Lemma map_Some_in_2 :
    forall A B f (l:list A) (l':list B),
      map f l = map Some l' -> (forall a, In a l' -> exists b, In b l /\ f b = Some a).
  Proof.
    intros A B f.
    induction l as [|a1 l].
    - intros [|b2 l']  Hl a Ha;[inversion Ha|discriminate].
    - intros [|b2 l']  Hl a Ha;[inversion Hl| ].
      inversion Hl as [[H1 H2]].
      assert (IH := IHl l' H2).
      destruct Ha as [Ha|Ha].
      + exists a1;split;[left;apply eq_refl| rewrite <- Ha;apply H1].
      + assert (Aux := IH a Ha).
        destruct Aux as [b' [I1 I2]].
        exists b';split;[right;apply I1|apply I2].
  Qed.

  Lemma pair_in_map_fst_snd:
    forall A B (l:list (A*B))  x (y:B), In (x, y) l -> In x (map fst l) /\  In y (map snd l).
  Proof.
    induction l as [|[x1 y1] l];intros x y Hin;[inversion Hin| ].
    simpl.
    simpl in Hin.
    destruct Hin as [Hin|Hin].
    - split;left;inversion Hin;apply eq_refl.
    - split;right;[apply (proj1 (IHl _ _ Hin))|apply (proj2 (IHl _ _ Hin))].
  Qed.

  Lemma NoDup_map_in_inj_ :
    forall X Y (f:X -> Y) l,
      NoDup (map f l) ->
      forall x1 x2,
        In x1 l -> In x2 l ->
        f x1 = f x2 -> x1 = x2.
  Proof.
    intros X Y f.
    fix thm 1.
    intros [|x l]  Hmap x1 x2 H1;
      [inversion H1|intros H2 Hf].
    inversion Hmap;subst;clear Hmap.
    assert (IH := thm _ H4 ).
    destruct H1 as [H1|H1],H2 as [H2|H2];subst.
    - apply eq_refl.
    - assert (A1 : In (f x1) (map f l)).
      { rewrite Hf,in_map_iff.
        exists x2;split;[apply eq_refl|apply H2]. }
      destruct (H3 A1).
    - assert (A1 : In (f x2) (map f l)).
      { rewrite <- Hf,in_map_iff.
        exists x1;split;[apply eq_refl|apply H1]. }
      destruct (H3 A1).
    - apply (IH _ _ H1 H2 Hf).
  Qed.

  Lemma NoDup_map_in_inj :
    forall X Y (f:X -> Y) l,
      NoDup l ->
      (NoDup (map f l) <->
       forall x1 x2,
         In x1 l -> In x2 l ->
         f x1 = f x2 -> x1 = x2).
  Proof.
    intros X Y f l H;split.
    - apply NoDup_map_in_inj_;apply H.
    - induction l;intro H1;[constructor| ].
      simpl.
      inversion H;subst.
      assert (IHl := IHl H4).
      constructor.
      + intro H5;apply H3.
        rewrite in_map_iff in H5.
        destruct H5 as [x [H5 H6]].
        assert (H1 := H1 a x (or_introl eq_refl) (or_intror H6)).
        apply eq_sym in H5.
        apply H1 in H5.
        rewrite H5.
        apply H6.
      + apply IHl.
        intros x1 x2 Hx1 Hx2 Heq.
        apply H1;try right;assumption.
  Qed.

        Lemma not_in_map_iff :
        forall (A B : Type) (f : A -> B)
          (l : list A) (y : B),
          ~ In y (map f l)
          <-> (forall x : A, f x <> y \/ ~ In x l).
      Proof.
        intros.
        rewrite  in_map_iff.
        split.
        - intros H x.
          apply Classical_Pred_Type.not_ex_all_not with (n := x) in H.
          apply (Classical_Prop.not_and_or _ _ H ).
        - intros H.
          apply Classical_Pred_Type.all_not_not_ex.
          intros n [H1 H2].
          destruct (H n).
          destruct (H0 H1).
          destruct (H0 H2).
      Qed.

      Lemma not_in_flat_map :
        forall (A B : Type) (f : A -> list B) (l : list A)
          (y : B),
          ~ In y (flat_map f l) <-> (forall x : A, ~ In x l \/ ~ In y (f x)).
      Proof.
        intros.
        rewrite in_flat_map.
        split.
        - intros H x.
          apply Classical_Pred_Type.not_ex_all_not with (n := x) in H.
          apply (Classical_Prop.not_and_or _ _ H ).
        - intros H.
          apply Classical_Pred_Type.all_not_not_ex.
          intros n [H1 H2].
          destruct (H n).
          destruct (H0 H1).
          destruct (H0 H2).
      Qed.


End Lists.

Section Permutation.

  Lemma Permutation_swap :
    forall (A : Type) (x y : A) (l1 l2 : list A),
      Permutation (y :: x :: l1) (x :: y :: l2) -> Permutation l1 l2.
  Proof.
    intros A x y l1 l2 H1.
    apply (@Permutation.Permutation_cons_inv  _ _ _ x).
    apply (@Permutation.Permutation_cons_inv  _ _ _ y).
    refine (Permutation.Permutation_trans  H1 _).
    apply Permutation.perm_swap.
  Qed.

  Lemma Permutation_map_2_alt :
    forall A B (f1:A -> B) f2 (l:list A),
      (forall x, In x l -> f1 x = f2 x) -> Permutation (map f1 l) (map f2 l).
  Proof.
    intros A C f1 f2.
    induction l;[constructor|intros Aux].
    simpl;apply Permutation_cons;
      [apply Aux;left;apply eq_refl| ].
    apply IHl.
    intros x Hx;apply Aux;right;apply Hx.
  Qed.

  Lemma Permutation_cons_inv :
    forall A (l1:list A) l2 a,
      Permutation (a::l1) l2 -> exists b l3 l4, a = b /\ Permutation l2  (l3 ++ b :: l4) /\ Permutation l1 (l3 ++ l4) /\ Permutation (a :: l1) (l3 ++ b :: l4).
  Proof.
    intros A.
    induction l1 as [|a1 l1];intros l2 a H.
    - apply Permutation_length_1_inv in H;subst l2.
      exists a,nil,nil;repeat split;apply Permutation_refl.
    - exists a,(a1::nil),l1;repeat split.
      + simpl.
        refine (Permutation_trans (Permutation_sym H) _).
        apply perm_swap.
      + apply Permutation_refl.
      + apply perm_swap.
  Qed.

  Lemma eqlistA_map_iff :
    forall A B R (f1:A -> list B) f2 (l:list A),
      eqlistA R (map f1 l) (map f2 l) <-> (forall x, In x l -> R (f1 x)  (f2 x)).
  Proof.
    intros A C R f1 f2.
    split.
    - induction l;intros H1 x Hx;[inversion Hx| ].
      inversion H1;subst.
      destruct Hx;[subst;apply H3| ].
      apply (IHl H5 _ H).
    - induction l;[constructor|intros Aux].
      simpl;apply eqlistA_cons;
        [apply Aux;left;apply eq_refl| ].
      apply IHl.
      intros x Hx;apply Aux;right;apply Hx.
  Qed.

  
    Lemma existsb_cons :
    forall A f (a:A) l,
      existsb f (a::l) = f a || existsb f l.
  Proof.
    simpl;intros;apply eq_refl.
  Qed.

  Lemma Permutation_in_dec_eq  :
    forall X eq_dec (l1 l2 : list X),
      Permutation l1 l2 ->
      forall x Y (i1 i2:Y),
        (if  in_dec eq_dec x l1
        then i1 else i2) =
        (if in_dec eq_dec x l2
        then i1 else i2).
  Proof.
    intros X eq_dec l1 l2 Hperm x Y i1 i2.
    destruct
      (in_dec _ _ l1)
      as [D1|D1],
         (in_dec _ _  l2)
        as [D2|D2];try apply eq_refl.
    - destruct (D2 (Permutation_in _ Hperm D1)).
    - destruct (D1 (Permutation_in _ (Permutation_sym Hperm) D2)).
  Qed.

  Lemma Permutation_length_filter_eq :
    forall X f (l1 l2 :  list X),
      Permutation l1 l2 ->
      Datatypes.length (filter f l1) =
      Datatypes.length (filter f l2).
  Proof.
    intros X f.
    induction 1 as [ |x l1 l2 Hp IH| |l1 l2 l3 Hp1 IH1 Hp2 IH2].
    - apply eq_refl.
    - simpl;destruct (f x);simpl;
        rewrite IH;apply eq_refl.
    - simpl;destruct (f x),(f y);
        apply eq_refl.
    - rewrite IH1;apply IH2.
  Qed.

  

      Lemma map_app_sum :
        forall A B X (A_to_X: A -> X) (B_to_X : B -> X),
          forall lx ly,
        map A_to_X lx ++ map B_to_X ly =
        map (fun x =>
               match x with
               | inl a => A_to_X a
               | inr b => B_to_X b
               end) (map (fun x => inl x) lx ++
                         map (fun y => inr y) ly).
      Proof.
        do 7 intro.
        rewrite map_app.
        rewrite 2 map_map.
        apply eq_refl.
      Qed.
  
End Permutation.

