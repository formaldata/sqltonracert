(************************************************************************************)
(**                                                                                 *)
(**                          The SQLToNRACert Library                               *)
(**                                                                                 *)
(**                       LMF, CNRS & Université Paris-Saclay                       *)
(**                                                                                 *)
(**                        Copyright 2016-2022 : FormalData                         *)
(**                                                                                 *)
(**         Authors: Véronique Benzaken                                             *)
(**                  Évelyne Contejean                                              *)
(**                  Mohammed Hachmaoui                                             *)
(**                  Chantal Keller                                                 *)
(**                                                                                 *)
(************************************************************************************)

Require Import List String ZArith.

Require Import Qcert.NRAEnv.NRAEnvRuntime Qcert.Utils.Utils.

Section NRAEnvHdDef.

  Context {fruntime : foreign_runtime}.
  
    Definition NRAEnvHdDef d n :=
        NRAEnvApp
        (NRAEnvEither  (NRAEnvID)     ((NRAEnvConst d)))
        (NRAEnvBinop OpBagNth n (NRAEnvConst (dnat 0%Z))) .

    Lemma NRAEnvHdDef_ignores_did :
      forall br i n,
        (forall reg id1 id2,
            nraenv_eval  br i n  reg id1  =
            nraenv_eval  br i  n reg id2) ->
        forall d reg (id1 id2: data),
          nraenv_eval  br i (NRAEnvHdDef d n )  reg id1  =
          nraenv_eval  br i  (NRAEnvHdDef d n ) reg id2.
    Proof.
      intros.
      cbn.
      apply f_equal.
      apply f_equal2;
        [apply H|apply eq_refl].
    Qed.
    
    Lemma NRAEnvHdDef_spec : forall br i n d,
        (forall reg did, nraenv_eval br i n reg did = Some d) ->
        forall d' dreg did,
          nraenv_eval br i (NRAEnvHdDef d' n ) dreg did =
          match d with
          | dcoll (a::_) => Some a
          | dcoll nil => Some (normalize_data br d')
          | _ =>  None
          end.
    Proof.
      unfold nraenv_eval.
      intros.
      cbn.
      unfold Lift.olift,Lift.olift2.
      rewrite H.
      destruct d;try apply eq_refl.
      simpl.
      destruct l;apply eq_refl.
    Qed.
    
        Lemma NRAEnvHdDef_spec_depend : forall br i n d did dreg,
        nraenv_eval br i n dreg did = Some d ->
        forall d' ,
          nraenv_eval br i (NRAEnvHdDef d' n ) dreg did =
          match d with
          | dcoll (a::_) => Some a
          | dcoll nil => Some (normalize_data br d')
          | _ =>  None
          end.
    Proof.
      unfold nraenv_eval.
      intros.
      cbn.
      unfold Lift.olift,Lift.olift2.
      rewrite H.
      destruct d;try apply eq_refl.
      simpl.
      destruct l;apply eq_refl.
    Qed.      

End NRAEnvHdDef.

Section NRAEnvIf.

  Context {fruntime : foreign_runtime}.
  
  Definition NRAEnvIf ncond nyes nno :=    
    NRAEnvApp
      (NRAEnvEither nyes nno) 
      (NRAEnvBinop
         OpBagNth  
         (NRAEnvSelect NRAEnvID (NRAEnvUnop OpBag ncond))
         (NRAEnvConst (dnat 0))).

  Lemma NRAEnvIf_spec :
    forall  ncond bcond nno dno nyes dyes br i  dreg,
      (forall did,nraenv_eval br i ncond dreg did = Some (dbool bcond)) ->            
      (forall did,nraenv_eval br i nyes dreg did = Some dyes) ->      
      (forall did,nraenv_eval br i nno dreg did = Some dno) ->
      forall did,
        nraenv_eval br i (NRAEnvIf ncond nyes nno) dreg did =
        Some (if bcond then dyes else dno).
  Proof.
    unfold nraenv_eval.
    do 9 intro.
    intros H1 H2 H3.
    intro did.
    simpl.
    unfold olift,olift2.
    rewrite H1;simpl.
    unfold lift.
    destruct bcond;
      [apply H2|apply H3].
  Qed.
  
  Lemma NRAEnvIf_spec_strong :
    forall  ncond bcond nno dno nyes dyes br i  dreg,
      (forall did,nraenv_eval br i ncond dreg did = Some (dbool bcond)) ->            
      (forall did, bcond = true -> nraenv_eval br i nyes dreg did = Some dyes) ->      
      (forall did,bcond = false -> nraenv_eval br i nno dreg did = Some dno ) ->
      forall did,
        nraenv_eval br i (NRAEnvIf ncond nyes nno) dreg did =
        Some (if bcond then dyes else dno).
  Proof.
    unfold nraenv_eval.
    do 9 intro.
    intros H1 H2 H3.
    intro did.
    simpl.
    unfold olift,olift2.
    rewrite H1;simpl.
    unfold lift.
    destruct bcond;
      [apply H2|apply H3];
      apply eq_refl.
  Qed.

  
  Lemma NRAEnvIf_spec_strong_depend :
    forall  ncond bcond nno dno nyes dyes br i  dreg did,
      (nraenv_eval br i ncond dreg did = Some (dbool bcond)) ->            
      (forall did, bcond = true -> nraenv_eval br i nyes dreg did = Some dyes) ->      
      (forall did,bcond = false -> nraenv_eval br i nno dreg did = Some dno ) ->      
        nraenv_eval br i (NRAEnvIf ncond nyes nno) dreg did =
        Some (if bcond then dyes else dno).
  Proof.
    unfold nraenv_eval.
    do 9 intro;intro did.
    intros H1 H2 H3.
    simpl.
    unfold olift,olift2.
    rewrite H1;simpl.
    unfold lift.
    destruct bcond;
      [apply H2|apply H3];
      apply eq_refl.
  Qed.


  Lemma NRAEnvIf_spec_depend_all :
    forall  ncond bcond nno dno nyes dyes br i  dreg did,
      (nraenv_eval br i ncond dreg did = Some (dbool bcond)) ->            
      (nraenv_eval br i nyes dreg (dbool true) = Some dyes) ->      
      (nraenv_eval br i nno dreg dunit = Some dno ) ->      
        nraenv_eval br i (NRAEnvIf ncond nyes nno) dreg did =
        Some (if bcond then dyes else dno).
  Proof.
    unfold nraenv_eval.
    do 9 intro;intro did.
    intros H1 H2 H3.
    simpl.
    unfold olift,olift2.
    rewrite H1;simpl.
    unfold lift.
    destruct bcond; [apply H2| apply H3];
      try apply eq_refl.
  Qed.

  Lemma NRAEnvIf_spec_strong_depend_all :
    forall  ncond bcond nno dno nyes dyes br i  dreg did,
      (nraenv_eval br i ncond dreg did = Some (dbool bcond)) ->            
      (bcond = true ->  nraenv_eval br i nyes dreg (dbool true) = Some dyes) ->      
      (bcond = false -> nraenv_eval br i nno dreg dunit = Some dno ) ->      
        nraenv_eval br i (NRAEnvIf ncond nyes nno) dreg did =
        Some (if bcond then dyes else dno).
  Proof.
    unfold nraenv_eval.
    do 9 intro;intro did.
    intros H1 H2 H3.
    simpl.
    unfold olift,olift2.
    rewrite H1;simpl.
    unfold lift.
    destruct bcond; [apply H2| apply H3];
      try apply eq_refl.
  Qed.

End NRAEnvIf.



Section NRAEnvRecExtractValues.

  Context {fruntime : foreign_runtime}.

    
  Fixpoint NRAEnvRecExtractValues la n :=
    match la with
    | nil => (NRAEnvConst (dcoll nil))
    | a::la =>      
      let val_of_a := (NRAEnvUnop OpBag (NRAEnvUnop (OpDot a) n)) in
      NRAEnvBinop
        OpBagUnion val_of_a
        (NRAEnvRecExtractValues la (NRAEnvUnop (OpRecRemove a) n))
    end.
  
  Lemma NRAEnvRecExtractValues_ignores_did :
    forall  br i la,
    forall    dreg n1 did1 did2,
      nraenv_eval br i n1 dreg did1 =
      nraenv_eval br i n1 dreg did2 ->
      nraenv_eval br i (NRAEnvRecExtractValues la n1 ) dreg did1 =
      nraenv_eval br i (NRAEnvRecExtractValues la n1 ) dreg did2.
  Proof.
    unfold nraenv_eval.
    do 2 intro.
    induction la;do 4 intro;intros Hn1;
      [apply eq_refl| ].
    simpl.
    rewrite Hn1.
    apply f_equal.
    apply IHla.
    simpl.
    rewrite Hn1.
    apply eq_refl.
  Qed.
  
  Lemma NRAEnvRecExtractValues___eq :
    forall  r br i n dreg,
      (forall did,nraenv_eval br i n dreg did = Some (drec r)) ->
      NoDup (domain r) ->
      forall did,
        nraenv_eval br i (NRAEnvRecExtractValues (domain r) n) dreg did =
        lift dcoll (lift_map (edot r) (domain r)).
  Proof.
    unfold nraenv_eval.
    induction r as [|[a v] r];do 4 intro;intros H1 H2 did.
    - apply eq_refl.
    - simpl.
      unfold olift2,olift.
      rewrite H1.
      inversion H2;subst;clear H2.
      rewrite IHr;[| |apply H4];clear IHr;swap 1 2.      
      + cbn.
        unfold olift.
        intros;rewrite H1.        
        simpl.
        destruct string_dec;
          [|destruct (n0 eq_refl)].
        rewrite nin_rremove;[apply eq_refl| apply H3].
      + unfold lift.        
        assert(A3 : (lift_map (edot (rremove r a)) (domain r)) =
                    (lift_map (edot  r) (domain r))).
        {
          apply lift_map_ext.
          intros.
          unfold edot.
          cbn.
          rewrite nin_rremove;[apply eq_refl| apply H3].
        }
        assert(A4 : (lift_map (edot ((a, v) :: r)) (domain r)) =
                    (lift_map (edot  r) (domain r))).
        {
          apply lift_map_ext.
          intros.
          unfold edot.
          cbn.
          destruct (in_dom_lookupr _ _ string_eqdec H).
          destruct assoc_lookupr;[apply eq_refl|discriminate].
        }
        simpl in A4.
        rewrite A4.    
        assert (Aux1 : (edot ((a, v) :: r) a = Some v)).
        {
          cbn.
          assert (X1 := @assoc_lookupr_nin_none _ _ _ _ string_eqdec H3).
          cbn in X1.
          destruct assoc_lookupr;
            [discriminate| ].
          destruct string_eqdec as [yes|no];
            [apply eq_refl| destruct (no eq_refl)].
        }
        simpl in Aux1;rewrite Aux1.
        clear A3 A4.
        destruct lift_map;apply eq_refl.
  Qed.
    
  Lemma NRAEnvRecExtractValues___eq_depend :
    forall  r br i n dreg did,
      (nraenv_eval br i n dreg did = Some (drec r)) ->
      NoDup (domain r) ->
        nraenv_eval br i (NRAEnvRecExtractValues (domain r) n) dreg did =
        lift dcoll (lift_map (edot r) (domain r)).
  Proof.
    unfold nraenv_eval.
    induction r as [|[a v] r];do 5 intro;intros H1 H2.
    - apply eq_refl.
    - simpl.
      unfold olift2,olift.
      rewrite H1.
      inversion H2;subst;clear H2.
      rewrite IHr;[| |apply H4];clear IHr;swap 1 2.      
      + cbn.
        unfold olift.
        intros;rewrite H1.        
        simpl.
        destruct string_dec;
          [|destruct (n0 eq_refl)].
        rewrite nin_rremove;[apply eq_refl| apply H3].
      + unfold lift.        
        assert(A3 : (lift_map (edot (rremove r a)) (domain r)) =
                    (lift_map (edot  r) (domain r))).
        {
          apply lift_map_ext.
          intros.
          unfold edot.
          cbn.
          rewrite nin_rremove;[apply eq_refl| apply H3].
        }
        assert(A4 : (lift_map (edot ((a, v) :: r)) (domain r)) =
                    (lift_map (edot  r) (domain r))).
        {
          apply lift_map_ext.
          intros.
          unfold edot.
          cbn.
          destruct (in_dom_lookupr _ _ string_eqdec H).
          destruct assoc_lookupr;[apply eq_refl|discriminate].
        }
        simpl in A4.
        rewrite A4.    
        assert (Aux1 : (edot ((a, v) :: r) a = Some v)).
        {
          cbn.
          assert (X1 := @assoc_lookupr_nin_none _ _ _ _ string_eqdec H3).
          cbn in X1.
          destruct assoc_lookupr;
            [discriminate| ].
          destruct string_eqdec as [yes|no];
            [apply eq_refl| destruct (no eq_refl)].
        }
        simpl in Aux1;rewrite Aux1.
        clear A3 A4.
        destruct lift_map;apply eq_refl.
  Qed.

End NRAEnvRecExtractValues.

Section NRAEnvCollNotEmpty.

      Context {fruntime : foreign_runtime}.

    Definition NRAEnvCollNotEmpty ndl :=
    (NRAEnvUnop OpNeg
                (NRAEnvBinop OpLe (NRAEnvUnop OpCount ndl)
                             (NRAEnvConst (@dnat _ 0)))).

    Lemma NRAEnvCollNotEmpty_ignores_did :
    forall  nq,
    forall did1 did2 br i dreg,        
      nraenv_eval br i nq dreg did1 = nraenv_eval br i nq dreg did2 ->      
        nraenv_eval br i (NRAEnvCollNotEmpty nq) dreg did1 =
        nraenv_eval br i (NRAEnvCollNotEmpty nq) dreg did2.
  Proof.
    unfold nraenv_eval.
    do 6 intro;intros  H1.
    simpl.
    rewrite H1.
    apply eq_refl.
  Qed.

End NRAEnvCollNotEmpty.
